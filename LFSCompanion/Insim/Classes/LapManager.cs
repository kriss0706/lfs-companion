﻿using LFSCompanion.Insim.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using LFSCompanion.Insim.Classes;

namespace LFSCompanion.Application.Classes
{
    public class LapManager
    {
        #region Fields
        private string LapDifference;
        private TimeSpan Split1;
        private TimeSpan Split2;
        private TimeSpan Split3;
        private TimeSpan Laptime;
        #endregion
        
        public LapGui Gui { get; private set; }

        #region Classes
        private Variables _vars;
        private StringFormatter _formatter;
        private ExceptionProcessor _exc;
        #endregion

        public LapManager(Variables variables, StringFormatter stringFormatter, ExceptionProcessor exception)
        {
            _vars = variables;
            _formatter = stringFormatter;
            _exc = exception;
            Gui = new LapGui();
        }

        #region Methods for fields

        #region Getters
        
        #endregion

        #region Setters

        #endregion

        #endregion

        /// <summary>
        /// Fetch fastest laptime (PB) with optional ordering
        /// </summary>
        /// <param name="Track"></param>
        /// <param name="Vehicle"></param>
        /// <param name="orderBy">Optional parameter to specify the field to order by</param>
        /// <returns></returns>
        public async Task<LaptimeModel?> getPb(string Track, string Vehicle, Func<LaptimeModel, object>? orderBy = null)
        {
            try
            {
                var query = _vars.LaptimeMessages
                    .Where(x => x.Track == Track && x.Vehicle == Vehicle);

                // Apply ordering if specified, otherwise order by Laptime by default
                query = query.OrderBy(orderBy ?? (x => x.Laptime));

                return query.Take(1).FirstOrDefault();
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    "getPB() in LapManager.cs", ex);
                return null;
            }
        }
        
        public async Task<double> getPbBySplit(string Track, string Vehicle, byte splitType)
        {
            try
            {
                var lap = _vars.LaptimeMessages
                    .Where(x => x.Track == Track && x.Vehicle == Vehicle)
                    .OrderBy(x => x.Laptime)
                    .Take(1)
                    .FirstOrDefault();

                if (lap == null)
                    return 0;

                return splitType switch
                {
                    1 => lap.Split1,
                    2 => lap.Split2,
                    3 => lap.Split3,
                    _ => 0
                };
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    "getPB() in LapManager.cs", ex);
                return 0;
            }
        }

        public async Task<double> getPredictedPb(string Track, Player conn)
        {
            try
            {
                var allLaps = _vars.LaptimeMessages
                    .Where(x => x.Track == _vars.TrackName && x.Vehicle == conn.CarId)
                    .ToList();

                var bestSplit1 = allLaps.Count != 0 ? allLaps.Min(x => x.Split1) : 0;
                var bestSplit2 = allLaps.Count != 0 && allLaps.Any(x => x.Split2 > 0) ? allLaps.Min(x => x.Split2) : 0;
                var bestSplit3 = allLaps.Count != 0 && allLaps.Any(x => x.Split3 > 0) ? allLaps.Min(x => x.Split3) : 0;

                if (conn.CarId == null)
                    return 0;

                var lapPb = getPb(_vars.TrackName, conn.CarId, x => x.Laptime).Result;

                if (lapPb == null || lapPb.Split1 == 0 || lapPb.Laptime == 0)
                    return 0;

                // Calculate split difference
                var split1Difference = lapPb.Split1 - bestSplit1;
                var split2Difference = lapPb.Split2 > 0 && bestSplit2 > 0 ? lapPb.Split2 - bestSplit2 : 0;
                var split3Difference = lapPb.Split3 > 0 && bestSplit3 > 0 ? lapPb.Split3 - bestSplit3 : 0;

                // Adjust the predicted PB by removing the differences from the actual PB
                var predictedPb = lapPb.Laptime - (split1Difference + split2Difference + split3Difference);

                // Ensure predicted PB does not exceed the actual PB and is valid
                if (predictedPb > lapPb.Laptime || predictedPb < 0)
                    predictedPb = lapPb.Laptime;

                return predictedPb;
            }
            catch (Exception e)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "getPredictedPb() in LapManager class", e);
                return 0;
            }
        }

        #region Getters
        
        /// <summary>
        /// Fetch the split by type
        /// </summary>
        /// <param name="splitType"></param>
        /// <returns></returns>
        public TimeSpan getSplit(byte splitType)
        {
            var value = TimeSpan.Zero;
            
            switch (splitType)
            {
                case 1:
                    value = Split1;
                    break;
                case 2:
                    value = Split2;
                    break;
                
                case 3:
                    value = Split3;
                    break;
            }

            return value;
        }

        public TimeSpan getLaptime()
        {
            return Laptime;
        }

        #endregion
        
        #region Setters
        
        /// <summary>
        /// Set the split by type
        /// </summary>
        /// <param name="splitType"></param>
        /// <param name="value"></param>
        public void setSplit(byte splitType, TimeSpan value)
        {
            switch (splitType)
            {
                case 1:
                    Split1 = value;
                    break;
                
                case 2:
                    Split2 = value;
                    break;
                
                case 3:
                    Split3 = value;
                    break;
            }
        }

        public void setLaptime(TimeSpan value)
        {
            Laptime = value;
        }
        
        #endregion
    }
}
