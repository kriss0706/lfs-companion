﻿using LFSCompanion.Insim.Models;
using InSimDotNet.Helpers;
using LFSCompanion.Application.Classes;
using LFSCompanion.Application.Extensions;
using LFSCompanion.Application.SQL.Seeders;
using LFSCompanion.Insim.Models.Enum;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InSimDotNet.Packets;
using static LFSCompanion.Insim.Models.Enum.ButtonEnum;
using static LFSCompanion.Insim.Models.Enum.MessageTypeEnum;
using BS = InSimDotNet.Packets.ButtonStyles;
using CarContact = LFSCompanion.Application.SQL.Models.CarContact;
using WindowState = LFSCompanion.Insim.Models.Enum.WindowStateEnum;

namespace LFSCompanion.Insim.Classes
{
    public class WindowFactory
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _im;
        private ButtonManager _clickIdManager;
        private StringFormatter _stringFormatter;
        private CarInfo _carInfo;
        private DateFormatter _dateFormatter;
        private ButtonManager _btn;
        private LapManager _lapManager;

        public WindowFactory(Variables variables, ExceptionProcessor exceptionProcessor, InSimMethods InSimMethods,
            ButtonManager clickIdManager, StringFormatter stringFormatter, CarInfo carInfo, DateFormatter dateFormatter, 
            ButtonManager btn, LapManager lapManager)
        {
            _var = variables;
            _exc = exceptionProcessor;
            _im = InSimMethods;
            _clickIdManager = clickIdManager;
            _stringFormatter = stringFormatter;
            _carInfo = carInfo;
            _dateFormatter = dateFormatter;
            _btn = btn;
            _lapManager = lapManager;
        }
        
        #region Functions
        
        private byte toByte(byte num)
        {
            byte a = Convert.ToByte(num);
            return a;
        }
        
        #endregion

        #region ' Templates '
        /// <summary>
        /// List the templates
        /// </summary>
        /// <param name="State"></param>
        /// <returns></returns>
        public async Task Templates(WindowState State)
        {
            if (_var.insim == null)
                return;

            switch (State)
            {
                case WindowState.OPEN:
                {
                    #region Variables/Buttons/Logic

                    #region Position Vars

                    // Background
                    byte backg_t = _btn.Location(CUSTOMIZER_WINDOW).Top;
                    byte background_h = 41;

                    // Customize User Interface (title)
                    byte title_t = Convert.ToByte(backg_t + 1);

                    byte Templates_t = Convert.ToByte(title_t + 15);

                    // Column 1 (Template Name)
                    byte Template_col1_clickId = 59;
                    byte col1_width = 20;
                    byte col1_left = 63;

                    // Column 2 (Date Created)
                    byte Template_col2_clickId = 64;
                    byte col2_width = 25;
                    byte col2_left = Convert.ToByte(col1_left + col1_width);

                    // Column 3 (Manage)
                    byte Template_col3_clickId = 69;
                    byte action_width = 15;
                    byte col3_left = Convert.ToByte(col2_left + col2_width);

                    // Column 4 (Use)
                    byte Template_col4_clickId = 74;
                    byte col4_left = Convert.ToByte(col3_left + action_width);

                    byte AddTemplate_T = Convert.ToByte(backg_t + 30);

                    #endregion

                    #region ' Recent Template / Slots '

                    #region Color Logic
                    if (_var.TemplateSlots >= 5)
                        _var.ColorTemplateSlots = "^1";
                    else
                        _var.ColorTemplateSlots = "^3";
                    #endregion

                    await _im.CreateBTN(BS.ISB_C2, 4, 75, 63, Convert.ToByte(title_t + 9), 57,
                        $"^7Recently applied template: ^3{_var.LastUsedTemplate} ^7- " +
                        $"Template slots: {_var.ColorTemplateSlots}{_var.TemplateSlots}/5");
                    #endregion

                    #region List the templates

                    #region Columns
                    await _im.CreateBTN(BS.ISB_DARK | BS.ISB_LEFT, 4, col1_width, col1_left, Templates_t, 54,
                        "^7" + "Name");

                    await _im.CreateBTN(BS.ISB_DARK | BS.ISB_LEFT, 4, col2_width, col2_left, Templates_t, 55,
                        "^7" + "Date Created");

                    await _im.CreateBTN(BS.ISB_DARK | BS.ISB_LEFT, 4, 30, col3_left, Templates_t, 56,
                        "^7" + "Action");
                    #endregion

                    Templates_t += 4;

                    #region Foreach Loop
                    foreach (var item in _var.Templates.Where(x => x.IsDeleted == 0)
                                 .OrderByDescending(x => x.Created)
                            )
                    {
                        await _im.CreateBTN(BS.ISB_LEFT, 5, col1_width, col1_left, Templates_t, Template_col1_clickId,
                            "^7" + item.Name);

                        await _im.CreateBTN(BS.ISB_LEFT, 5, col2_width, col2_left, Templates_t, Template_col2_clickId,
                            "^7" + $"{item.Created.Value.ToString("g")}");

                        await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_CLICK, 5, action_width, col3_left, Templates_t, Template_col3_clickId,
                            "^7" + $"Manage");

                        await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_CLICK, 5, action_width, col4_left, Templates_t, Template_col4_clickId,
                            "^2" + $"Use");

                        Templates_t += 5;
                        Template_col1_clickId++;
                        Template_col2_clickId++;
                        Template_col3_clickId++;
                        Template_col4_clickId++;

                        AddTemplate_T += 4;
                        background_h += 4;
                    }
                    #endregion

                    #endregion

                    #region ' Background & Title '
                    // Background
                    await _im.CreateBTN(BS.ISB_DARK, background_h, 77, 62, backg_t, 51, "");

                    // Customize User Interface (title)
                    await _im.CreateBTN(BS.ISB_LIGHT, 8, 75, 63, title_t, 52,
                        "^7" + "UI Templates");
                    #endregion

                    #region Import Template

                    #region Import Logic
                    if (_var.TemplateSlots >= 5)
                    {
                        _var.BSImportButton = BS.ISB_DARK;
                        _var.ColorImportButton = "^8";
                    }
                    else
                    {
                        _var.BSImportButton = BS.ISB_LIGHT | BS.ISB_CLICK;
                        _var.ColorImportButton = "^7";
                    }
                    #endregion

                    await _im.CreateBTN(_var.BSImportButton, 5, 11, Convert.ToByte(col4_left - 16), Convert.ToByte(AddTemplate_T + 1), 90,
                        $"{_var.ColorImportButton}Import");
                    #endregion

                    #region ' Add Template '

                    #region Logic
                    BS AddTemplateStyle = BS.ISB_LIGHT | BS.ISB_CLICK;
                    string AddTemplateColor = "^7";

                    if (_var.TemplateSlots == 5)
                    {
                        AddTemplateStyle = BS.ISB_DARK;
                        AddTemplateColor = "^8";
                    }
                    #endregion

                    await _im.CreateBTN(AddTemplateStyle, 7, Convert.ToByte(action_width + 4), Convert.ToByte(col4_left - 4), AddTemplate_T, 58,
                        $"{AddTemplateColor}Add");

                    #endregion

                    #region ' Save '
                    await _im.CreateBTN(BS.ISB_CLICK, 8, 8, 63, title_t, 200,
                        $"{_var.CloseSymbolColor}«");
                    #endregion

                    #endregion

                    break;
                }

                case WindowState.CLOSE:

                    #region Remove Buttons
                    await _im.ClearBtnRange(51, 90);
                    await _im.ClearBtn(200);
                    #endregion

                    break;
            }
        }

        /// <summary>
        /// Manage a specific template
        /// </summary>
        /// <param name="State"></param>
        /// <returns></returns>
        public async Task ManageTemplate(WindowState State)
        {
            if (_var.insim == null)
                return;

            switch (State)
            {
                case WindowState.OPEN:

                    #region Template Validation
                    if (_var.SelectedTemplate == null || _var.SelectedTemplate.Created == null)
                        return;
                    #endregion

                    #region ' Position Variables '

                    // Background
                    byte backg_t = _btn.Location(CUSTOMIZER_WINDOW).Top;
                    byte title_t = Convert.ToByte(backg_t + 1);

                    byte prop1 = Convert.ToByte(title_t + 10);
                    byte prop2 = Convert.ToByte(prop1 + 6);

                    byte action_t = Convert.ToByte(prop2 + 15);
                    byte export_t = Convert.ToByte(action_t + 14);

                    #endregion

                    #region ' Background & Title '
                    // Background
                    await _im.CreateBTN(BS.ISB_DARK, 59, 77, 62, backg_t, 51, "");

                    // Customize User Interface (title)
                    await _im.CreateBTN(BS.ISB_LIGHT, 8, 75, 63, title_t, 52,
                        "^7" + "Manage Template");
                    #endregion

                    #region ' Template Properties '

                    #region Name Property
                    await _im.CreateBTN(BS.ISB_RIGHT, 6, 38, 63, prop1, 53,
                        "^7Template Name: ");

                    await _im.CreateBTT(BS.ISB_LEFT | BS.ISB_LIGHT | BS.ISB_CLICK, 6, 36, 101, prop1, 54,
                        16, $"^7{_var.SelectedTemplate.Name}", "^0Please enter a template name");
                    #endregion

                    #region Date Property
                    await _im.CreateBTN(BS.ISB_RIGHT, 6, 38, 63, prop2, 55,
                        "^7Date Created: ");

                    await _im.CreateBTN(BS.ISB_LEFT, 6, 36, 101, prop2, 56,
                        $"^7{_var.SelectedTemplate.Created.Value.ToString("g")}");
                    #endregion

                    #endregion

                    #region ' Actions '

                    #region Delete Template
                    await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_CLICK, 12, 27, 73, action_t, 57,
                        "^1" + "Delete");
                    #endregion

                    #region Overwrite Template
                    await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_CLICK, 12, 27, 101, action_t, 58,
                        "^2" + "Overwrite");
                    #endregion

                    #region Export
                    await _im.CreateBTN(_var.BSExportButton, 5, 15, 113, export_t, 59,
                        "^7" + "Export");
                    #endregion

                    #endregion

                    #region ' Save '
                    await _im.CreateBTN(BS.ISB_CLICK, 8, 8, 63, title_t, 200,
                        $"{_var.CloseSymbolColor}«");
                    #endregion

                    break;

                case WindowState.CLOSE:
                    #region Remove Buttons
                    await _im.ClearBtnRange(51, 90);
                    await _im.ClearBtn(200);
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// Add a new template
        /// </summary>
        /// <param name="State"></param>
        /// <returns></returns>
        public async Task AddTemplate(WindowState State)
        {
            switch (State)
            {
                case WindowState.OPEN:
                    #region Add Template
                    #region ' Position Variables '

                    // Background
                    byte backg_t = _btn.Location(CUSTOMIZER_WINDOW).Top;
                    byte background_h = 59;
                    byte title_t = Convert.ToByte(backg_t + 1);

                    // Body
                    byte Notice_t = Convert.ToByte(title_t + 10);
                    byte Notice_sub_t = Convert.ToByte(Notice_t + 6);

                    byte Templateform_t = Convert.ToByte(Notice_sub_t + 10);

                    // Template Form Base
                    byte Templateform_base_L = 78;
                    byte Templateform_w = 23;

                    // Template Form Sub
                    byte Templateform_sub_L = Convert.ToByte(Templateform_base_L + Templateform_w);
                    byte Templateform_sub_w = 30;
                    #endregion

                    #region ' Background & Title '
                    // Background
                    await _im.CreateBTN(BS.ISB_DARK, background_h, 77, 62, backg_t, 51, "");

                    // Customize User Interface (title)
                    await _im.CreateBTN(BS.ISB_LIGHT, 8, 75, 63, title_t, 52,
                        "^7" + "Add Template");
                    #endregion

                    #region ' Notice '
                    await _im.CreateBTN(BS.ISB_C2, 6, 75, 63, Notice_t, 53,
                        $"^6Important Notice");

                    await _im.CreateBTN(BS.ISB_C2, 5, 75, 63, Notice_sub_t, 54,
                        $"^7New templates are based on your current button positions.");
                    #endregion

                    #region Body
                    await _im.CreateBTN(BS.ISB_LEFT, 6, Templateform_w, Templateform_base_L, Templateform_t, 55,
                        "^7Template Name");

                    #region Input Logic
                    string TemplateName = _var.TemplateName;
                    if (TemplateName.Length == 0)
                        TemplateName = "Template Name";
                    #endregion

                    await _im.CreateBTT(BS.ISB_LIGHT | BS.ISB_LEFT | BS.ISB_CLICK, 6, Templateform_sub_w,
                        Templateform_sub_L, Templateform_t, 56,
                        30, $"{TemplateName}", "^0Enter a name for the template");

                    // Proceed button
                    await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_CLICK, 8, Templateform_w,
                        Convert.ToByte(Templateform_sub_L + (Templateform_sub_w - Templateform_w)), Convert.ToByte(Templateform_t + 10),
                        57, "^2Proceed");

                    #endregion

                    #region ' Save '
                    await _im.CreateBTN(BS.ISB_CLICK, 8, 8, 63, title_t, 200,
                        $"{_var.CloseSymbolColor}«");
                    #endregion
                    #endregion
                    break;

                case WindowState.CLOSE:
                    #region Remove Buttons
                    await _im.ClearBtnRange(51, 79);
                    await _im.ClearBtn(200);
                    #endregion
                    break;
            }
        }

        public async Task ImportTemplate(WindowState State)
        {
            if (_var.insim == null)
                return;

            switch (State)
            {
                case WindowState.OPEN:

                    #region ' Position Variables '
                    // Background
                    byte backg_t = _btn.Location(CUSTOMIZER_WINDOW).Top;

                    // Customize User Interface (title)
                    byte title_t = Convert.ToByte(backg_t + 1);

                    byte instruction_t = Convert.ToByte(title_t + 10);
                    byte instruction2_t = Convert.ToByte(instruction_t + 5);

                    // Body
                    byte Notice_t = Convert.ToByte(title_t + 10);
                    byte Notice_sub_t = Convert.ToByte(Notice_t + 6);

                    byte Templateform_t = Convert.ToByte(Notice_sub_t + 10);

                    // Template Form Base
                    byte Templateform_base_L = 73;
                    byte Templateform_w = 23;

                    // Template Form Sub
                    byte Templateform_sub_L = Convert.ToByte(Templateform_base_L + Templateform_w);
                    byte Templateform_sub_w = 30;
                    #endregion

                    #region ' Buttons '

                    #region ' Background & Title '
                    // Background
                    await _im.CreateBTN(BS.ISB_DARK, 47, 57, 72, backg_t, 51, "");

                    // Customize User Interface (title)
                    await _im.CreateBTN(BS.ISB_LIGHT, 8, 55, 73, title_t, 52, "^7" + "Import Template");
                    #endregion

                    #region Instructions
                    await _im.CreateBTN(BS.ISB_C2, 5, 55, 73, instruction_t, 53, "^7" +
                            "Specify the file you've put in");

                    await _im.CreateBTN(BS.ISB_C2, 5, 55, 73, instruction2_t, 54, "^7" +
                            "^3Template Imports ^7folder in the insim directory");
                    #endregion

                    #region Proceed

                    await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_CLICK, 8, 35,
                        83, Convert.ToByte(Templateform_t),
                        55, "^2Proceed");

                    #endregion

                    #region Return to last window
                    await _im.CreateBTN(BS.ISB_CLICK, 8, 8, 73, title_t, 200,
                        $"{_var.CloseSymbolColor}«");
                    #endregion

                    #endregion

                    break;

                case WindowState.CLOSE:
                    #region Remove Buttons
                    await _im.ClearBtnRange(51, 56);
                    await _im.ClearBtn(200);
                    #endregion
                    break;
            }
        }

        #endregion

        #region ' Laptimes Panel '
        public async Task laptimePanel(WindowState state)
        {
            if (_var.insim == null)
                return;

            switch (state)
            {
                case WindowState.OPEN:

                    var conn = _im.getByUcid(_var.UCID);

                    foreach (var g in _var.Players)
                        if (g.Value.IsLocal)
                            conn = g.Value;

                    if (conn == null)
                        return;
                    
                    #region ' Horizontal Actions/Sorting '

                    #region CURRENT_VEHICLE
                    if (_var.LaptimeSortVehicle)
                    {
                        _var.BSLaptimeSortVehicle = BS.ISB_CLICK | BS.ISB_LIGHT;
                        _var.ColorLaptimeSortVehicle = "^3";
                    }
                    else
                    {
                        _var.BSLaptimeSortVehicle = BS.ISB_CLICK | BS.ISB_DARK;
                        _var.ColorLaptimeSortVehicle = "^8";
                    }
                    #endregion

                    #region CURRENT_TRACK
                    if (_var.LaptimeSortTrack)
                    {
                        _var.BSLaptimeSortTrack = BS.ISB_CLICK | BS.ISB_LIGHT;
                        _var.ColorLaptimeSortTrack = "^3";
                    }
                    else
                    {
                        _var.BSLaptimeSortTrack = BS.ISB_CLICK | BS.ISB_DARK;
                        _var.ColorLaptimeSortTrack = "^8";
                    }
                    #endregion

                    #region BEST_PB
                    if (_var.LaptimeSortBestPB)
                    {
                        _var.BSLaptimeSortBestPB = BS.ISB_CLICK | BS.ISB_LIGHT;
                        _var.ColorLaptimeSortBestPB = "^3";
                    }
                    else
                    {
                        _var.BSLaptimeSortBestPB = BS.ISB_CLICK | BS.ISB_DARK;
                        _var.ColorLaptimeSortBestPB = "^8";
                    }
                    #endregion

                    #endregion

                    #region Cache
                    var baseQuery = _var.LaptimeMessages
                        .OrderByDescending(x => x.Date)
                        .ToList();

                    if (_var.LaptimeSortVehicle)
                    {
                        baseQuery = baseQuery
                            .OrderBy(x => x.Laptime)
                            .Where(x => x.Vehicle == conn.CarId)
                            .ToList();
                    }

                    if (_var.LaptimeSortTrack)
                    {
                        baseQuery = baseQuery
                            .OrderBy(x => x.Laptime)
                            .Where(x => x.Track.Contains(_var.TrackName))
                            .ToList();
                    }

                    if (_var.LaptimeSortBestPB)
                    {
                        baseQuery = _var.LaptimeMessages
                            .Where(x => (!_var.LaptimeSortTrack || x.Track == _var.TrackName) &&  // Filter by track if selected
                                        (!_var.LaptimeSortVehicle || x.Vehicle == conn.CarId))    // Filter by car if selected
                            .GroupBy(x => new { x.Track, x.Vehicle })   // Group by track & vehicle
                            .Select(g => g.OrderBy(x => x.Laptime).First()) // Select the best PB for each car on that track
                            .ToList();
                    }



                    _var.LaptimeMessagesCache = baseQuery.ToList();

                    var limitedList = baseQuery
                        .Skip(_var.MessagesSkip)
                        .Take(_var.MessagesTake)
                        .ToList();

                    #endregion

                    #region Calculate Pages
                    if (_var.LaptimeMessagesCache.Count > 10)
                        _var.MessagesMaxPages = (int) Math.Ceiling((double) _var.LaptimeMessagesCache.Count / 10);
                    else
                        _var.MessagesMaxPages = 1;
                    #endregion

                    #region ' Paginator Button Logic '
                    if (_var.MessagesPageNumber == 1 && _var.MessagesMaxPages == 1)
                    {
                        _var.BSMessagesPagePrevious = BS.ISB_DARK;
                        _var.BSMessagesPageNext = BS.ISB_DARK;

                        _var.ColorMessagesPagePrevious = "^8";
                        _var.ColorMessagesPageNext = "^8";
                    }
                    else if (_var.MessagesPageNumber == 1)
                    {
                        _var.BSMessagesPagePrevious = BS.ISB_DARK;
                        _var.BSMessagesPageNext = BS.ISB_CLICK | BS.ISB_LIGHT;

                        _var.ColorMessagesPagePrevious = "^8";
                        _var.ColorMessagesPageNext = "^7";
                    }
                    else if (_var.MessagesPageNumber == _var.MessagesMaxPages)
                    {
                        _var.BSMessagesPagePrevious = BS.ISB_CLICK | BS.ISB_LIGHT;
                        _var.BSMessagesPageNext = BS.ISB_DARK;

                        _var.ColorMessagesPagePrevious = "^7";
                        _var.ColorMessagesPageNext = "^8";
                    }
                    else
                    {
                        _var.BSMessagesPagePrevious = BS.ISB_CLICK | BS.ISB_LIGHT;
                        _var.BSMessagesPageNext = BS.ISB_CLICK | BS.ISB_LIGHT;

                        _var.ColorMessagesPagePrevious = "^7";
                        _var.ColorMessagesPageNext = "^7";
                    }
                    #endregion

                    #region ' Variables '

                    byte Row_W = 15;
                    byte Row2_W = 45;
                    byte Row5_W = 15;
                    byte Row6_W = 39;
                    byte Col_full_W = 114;
                    byte Row_H = 4;

                    byte seperator = 1;

                    byte Cat1_L = Convert.ToByte(43);
                    byte Cat2_L = Convert.ToByte(Cat1_L + Row_W);
                    byte Cat5_L = Convert.ToByte(Cat2_L + Row2_W);
                    byte Cat6_L = Convert.ToByte(Cat5_L + Row5_W);

                    // CURRENT_CAR / TRACK
                    byte action_W = 22;
                    byte action_H = 6;
                    byte action_T = 38;
                    byte action1_L = Cat1_L;
                    byte action2_L = Convert.ToByte(action1_L + action_W + seperator);
                    byte action3_L = Convert.ToByte(action2_L + action_W + seperator);

                    byte row1_clickid = 70;
                    byte row2_clickid = 81;
                    byte row3_clickid = 92;
                    byte row4_clickid = 103;

                    byte pageinator_base_t = 96;
                    byte pageinator_label_h = Convert.ToByte(pageinator_base_t + 7);

                    byte Row1_L = 43;
                    byte Row_T = 45;

                    byte Center_T = (byte)((pageinator_base_t - Row_T) / 2 + Row_T);

                    #endregion

                    #region Background|Title
                    await _im.CreateBTN(BS.ISB_DARK, 82, 116, 42, 28, 51, ""); // Background
                    await _im.CreateBTN(BS.ISB_LIGHT, 8, 114, 43, 29, 52, "^7Laptimes"); // Title
                    #endregion

                    #region Laptimes

                    #region Actions/Sort

                    await _im.CreateBTN(_var.BSLaptimeSortVehicle, action_H, action_W, action1_L, action_T, 60,
                        $"{_var.ColorLaptimeSortVehicle}CURRENT CAR");

                    await _im.CreateBTN(_var.BSLaptimeSortTrack, action_H, action_W, action2_L, action_T, 61,
                        $"{_var.ColorLaptimeSortTrack}CURRENT TRACK");

                    await _im.CreateBTN(_var.BSLaptimeSortBestPB, action_H, action_W, action3_L, action_T, 62,
                        $"{_var.ColorLaptimeSortBestPB}BEST PB");

                    #endregion

                    #region Columns
                    await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_LEFT, Row_H, Row_W, Cat1_L, Row_T, 53,
                        $"^0Time");

                    await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_LEFT, Row_H, Row2_W, Cat2_L, Row_T, 55,
                        $"^0Splits");

                    await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_LEFT, Row_H, Row5_W, Cat5_L, Row_T, 58,
                        $"^0Laptime");

                    await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_LEFT, Row_H, Row6_W, Cat6_L, Row_T, 59,
                        $"^0Config");
                    #endregion

                    Row_T += 4;

                    #region No lap found
                    await _im.CreateBTN(BS.ISB_C2, 6, Col_full_W, Cat1_L, Center_T, row1_clickid,
                        $"^7There are no recorded laps");
                    #endregion

                    #region Content

                    var nonZeroSplits = new List<string>();
                    foreach (var x in limitedList)
                    {
                        var humanRaceTime = await _stringFormatter.getRaceTime(x.Laptime);
                        var fullVehicleName = await _carInfo.getFullVehicleName(x.Vehicle);
                        var shortVehicleName = x.Vehicle;

                        var sp1 = $"{x.Split1}";
                        var sp2 = $"{x.Split2}";
                        var sp3 = $"{x.Split3}";

                        if (sp1 != "0")
                        {
                            sp1 = await _stringFormatter.getRaceTime(x.Split1);
                            nonZeroSplits.Add(sp1);
                        }

                        if (sp2 != "0")
                        {
                            sp2 = await _stringFormatter.getRaceTime(x.Split2);
                            nonZeroSplits.Add(sp2);
                        }

                        if (sp3 != "0")
                        {
                            sp3 = await _stringFormatter.getRaceTime(x.Split3);
                            nonZeroSplits.Add(sp3);
                        }

                        string splits = string.Join(", ", nonZeroSplits);

                        await _im.CreateBTN(BS.ISB_LEFT, Row_H, Row_W, Cat1_L, Row_T, row1_clickid,
                        $"^7{x.Date.ToString("g")}");

                        await _im.CreateBTN(BS.ISB_LEFT, Row_H, Row2_W, Cat2_L, Row_T, row2_clickid,
                        $"^7{splits}");

                        await _im.CreateBTN(BS.ISB_LEFT, Row_H, Row5_W, Cat5_L, Row_T, row3_clickid,
                        $"^7{humanRaceTime}");

                        await _im.CreateBTN(BS.ISB_LEFT, Row_H, Row6_W, Cat6_L, Row_T, row4_clickid,
                        $"^7{fullVehicleName} ^8({shortVehicleName}) ^7@ {x.Track}");

                        row1_clickid++;
                        row2_clickid++;
                        row3_clickid++;
                        row4_clickid++;

                        Row_T += 4;
                        
                        nonZeroSplits.Clear();
                    }

                    #endregion

                    #endregion

                    #region Paginator
                    await _im.CreateBTN(_var.BSMessagesPagePrevious, 6, 10, 90, pageinator_base_t, 197,
                        $"{_var.ColorMessagesPagePrevious}‹");
                    await _im.CreateBTN(_var.BSMessagesPageNext, 6, 10, 100, pageinator_base_t, 198,
                        $"{_var.ColorMessagesPageNext}›");

                    await _im.CreateBTN(BS.ISB_C2, 5, 66, 67, pageinator_label_h, 199,
                        $"^7Page ^3{_var.MessagesPageNumber} ^7of ^3{string.Format(CultureInfo.InvariantCulture, "{0:n0}", _var.MessagesMaxPages)}");
                    #endregion
                    
                    #region Close Window
                    
                    await _im.CreateBTN(BS.ISB_CLICK, 8, 8, 149, 29, 200,
                        $"{_var.CloseSymbolColor}{_var.CloseSymbol}");
                    
                    #endregion
                    break;

                case WindowState.CLOSE:
                    await _im.ClearBtnRange(51, 146);
                    await _im.ClearBtnRange(194, 200);
                    break;

                case WindowState.CLOSE_CONTENT:
                    await _im.ClearBtnRange(70, 147);
                    break;
            }
        }
        #endregion

        #region ' Messages Panel '

        public async Task MessagesPanel(WindowState State)
        {
            if (_var.insim == null)
                return;
            
            #region Search Filter Text
            string SearchFilterText = "";
            if (_var.MessagesSearchWord.Length == 0)
                SearchFilterText = "Search";
            else
                SearchFilterText = _var.MessagesSearchWord;
            #endregion

            switch (State)
            {
                case WindowState.OPEN:
                    #region Messages
                    if (_var.InMessages)
                    {
                        #region ' Variables '
                        byte Col_L = 43;
                        byte Col2_L = Convert.ToByte(Col_L + 17);
                        byte Col3_L = Convert.ToByte(Col2_L + 25);

                        byte row1_clickid = 62;
                        byte row2_clickid = 73;
                        byte row3_clickid = 84;
                        #endregion

                        #region Button Style Logic
                        switch (_var.MessageTypes)
                        {
                            case GLOBAL:
                                _var.BSMessagesGlobal = BS.ISB_DARK;
                                _var.BSMessagesPlayer = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _var.BSMessagesCarContact = BS.ISB_CLICK | BS.ISB_LIGHT;

                                _var.ColorMessagesGlobal = "^8";
                                _var.ColorMessagesPlayers = "^7";
                                _var.ColorMessagesCarContact = "^7";
                                break;

                            case PLAYERS:
                                _var.BSMessagesGlobal = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _var.BSMessagesPlayer = BS.ISB_DARK;
                                _var.BSMessagesCarContact = BS.ISB_CLICK | BS.ISB_LIGHT;

                                _var.ColorMessagesGlobal = "^7";
                                _var.ColorMessagesPlayers = "^8";
                                _var.ColorMessagesCarContact = "^7";
                                break;

                            case CARCONTACT:
                                _var.BSMessagesGlobal = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _var.BSMessagesPlayer = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _var.BSMessagesCarContact = BS.ISB_DARK;

                                _var.ColorMessagesGlobal = "^7";
                                _var.ColorMessagesPlayers = "^7";
                                _var.ColorMessagesCarContact = "^8";
                                break;
                        }
                        #endregion

                        #region ' Variables '
                        byte Categories_T = 39;
                        byte Category1_L = 43;
                        byte Category2_L = Convert.ToByte(Category1_L + 15);
                        byte Category3_L = Convert.ToByte(Category2_L + 15);

                        byte pageinator_base_h = 96;
                        byte pageinator_label_h = Convert.ToByte(pageinator_base_h + 7);

                        // Relative time
                        byte relative_time_T = Convert.ToByte(pageinator_base_h + 5);

                        await _im.CreateBTN(BS.ISB_DARK, 82, 116, 42, 28, 51, ""); // Background
                        await _im.CreateBTN(BS.ISB_LIGHT, 8, 114, 43, 29, 52, "^7Messages"); // Title
                        
                        var local = _im.getLocal();
                        if (local == null)
                            return;

                        #endregion

                        #region Categories
                        await _im.CreateBTN(_var.BSMessagesGlobal, 6, 15, Category1_L, Categories_T, 53,
                            $"{_var.ColorMessagesGlobal}{GLOBAL.GetDescription()}");

                        await _im.CreateBTN(_var.BSMessagesPlayer, 6, 15, Category2_L, Categories_T, 54,
                            $"{_var.ColorMessagesPlayers}{PLAYERS.GetDescription()}");

                        await _im.CreateBTN(_var.BSMessagesCarContact, 6, 15, Category3_L, Categories_T, 55,
                            $"{_var.ColorMessagesCarContact}{CARCONTACT.GetDescription()}");
                        #endregion

                        #region Local data setting

                        var onlyLocalData = string.Empty;
                        
                        if (_var.MessagesOnlyLocalData)
                            onlyLocalData = _var.CloseSymbol;
                        
                        await _im.CreateBTN(BS.ISB_LEFT, 4, 9, 147, 37, 96,
                            $"^7Local data");
                        
                        await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_CLICK, 4, 3, 144, 37, 97,
                            $"^1{onlyLocalData}");

                        #endregion

                        await _im.ClearBtnRange(62, 94);
                        
                        #region Messages
                        #region GLOBAL
                        if (_var.MessageTypes == GLOBAL)
                        {
                            #region Variables
                            byte Row_W = 17;
                            byte Row2_W = 97;
                            byte Row_H = 4;

                            byte Row1_L = 43;
                            byte Row2_L = Convert.ToByte(Row1_L + Row_W);
                            byte Row_T = 46;

                            #endregion

                            await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_LEFT, Row_H, Row_W, Col_L, Row_T, 59,
                                $"^0Timestamp");

                            await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_LEFT, Row_H, Row2_W, Col2_L, Row_T, 60,
                                $"^0Message");

                            await _im.ClearBtnRange(62, 94);
                            
                            foreach (var x in _var.GlobalMessagesCache)
                            {
                                if (x.Message == null)
                                    return;
                                
                                #region Button Style
                                
                                var buttonStyle = BS.ISB_LEFT;

                                if (x.Message.Contains(local.PName))
                                    buttonStyle = BS.ISB_DARK | BS.ISB_LEFT;
                                
                                #endregion
                                
                                string _DateTime;
                                if (_var.MessagesRelativeTime)
                                    _DateTime = await _dateFormatter.ToRelativeTime(x.Date);
                                else
                                    _DateTime = x.Date.ToString("g");

                                Row_T += 4;

                                await _im.CreateBTN(buttonStyle, Row_H, Row_W, Row1_L, Row_T, row1_clickid,
                                $"^7{_DateTime}");

                                await _im.CreateBTN(BS.ISB_CLICK | buttonStyle, Row_H, Row2_W, Row2_L, Row_T, row3_clickid,
                                StringHelper.StripLanguage(x.Message));

                                row1_clickid++;
                                row3_clickid++;
                            }

                            await _im.ClearBtn(61);
                        }
                        #endregion
                        #region PLAYER
                        else if (_var.MessageTypes == PLAYERS)
                        {
                            #region Variables
                            byte Row_W = 17;
                            byte Row2_W = 25;
                            byte Row3_W = 72;
                            byte Row_H = 4;

                            byte Row1_L = 43;
                            byte Row2_L = Convert.ToByte(Row1_L + Row_W);
                            byte Row3_L = Convert.ToByte(Row1_L + Row_W + Row2_W);
                            byte Row_T = 46;

                            #endregion

                            await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_LEFT, Row_H, Row_W, Col_L, Row_T, 59,
                                $"^0Timestamp");

                            await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_LEFT, Row_H, Row2_W, Col2_L, Row_T, 60,
                                $"^0Player");

                            await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_LEFT, Row_H, Row3_W, Col3_L, Row_T, 61,
                                $"^0Message");

                            foreach (var x in _var.PlayerMessagesCache)
                            {
                                var buttonStyle = BS.ISB_LEFT;

                                if (local.UName.Equals(x.Username, StringComparison.CurrentCultureIgnoreCase))
                                    buttonStyle = BS.ISB_DARK | BS.ISB_LEFT;
                                
                                string _DateTime;
                                if (_var.MessagesRelativeTime)
                                    _DateTime = await _dateFormatter.ToRelativeTime(x.Date);
                                else
                                    _DateTime = x.Date.ToString("g");

                                Row_T += 4;

                                string PlayerName;

                                if (x.Username.Length == 0)
                                    PlayerName = $"^7{StringHelper.StripLanguage(x.Nickname)}";
                                else
                                    PlayerName = $"^7{StringHelper.StripLanguage(x.Nickname)} ^8({x.Username})";


                                await _im.CreateBTN(buttonStyle, Row_H, Row_W, Row1_L, Row_T, row1_clickid,
                                $"^7{_DateTime}");

                                await _im.CreateBTN(buttonStyle, Row_H, Row2_W, Row2_L, Row_T, row2_clickid,
                                PlayerName);

                                await _im.CreateBTN(BS.ISB_CLICK | buttonStyle, Row_H, Row3_W, Row3_L, Row_T, row3_clickid,
                                    StringHelper.StripLanguage(x.Message));

                                row1_clickid++;
                                row2_clickid++;
                                row3_clickid++;
                            }
                        }
                        #endregion
                        #region CAR CONTACT
                        else if (_var.MessageTypes == CARCONTACT)
                        {
                            #region Variables
                            byte Row_W = 17;
                            byte Row2_W = 97;
                            byte Row_H = 4;

                            byte Row1_L = 43;
                            byte Row2_L = Convert.ToByte(Row1_L + Row_W);
                            byte Row_T = 46;

                            #endregion

                            await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_LEFT, Row_H, Row_W, Col_L, Row_T, 59,
                                $"^0Timestamp");

                            await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_LEFT, Row_H, Row2_W, Col2_L, Row_T, 60,
                                $"^0Message");

                            await _im.ClearBtnRange(62, 94);
                            
                            foreach (var x in _var.CarContactCache)
                            {
                                var buttonStyle = BS.ISB_LEFT;
                                
                                string[] userList = [x.SourceUName.ToLower(), x.TargetUName.ToLower()];

                                if (userList.Contains(local.UName.ToLower()))
                                    buttonStyle = BS.ISB_DARK | BS.ISB_LEFT;
                                
                                string _DateTime;
                                if (_var.MessagesRelativeTime)
                                    _DateTime = await _dateFormatter.ToRelativeTime(x.Date);
                                else
                                    _DateTime = x.Date.ToString("g");

                                Row_T += 4;
                                #region Speed Conversion
                                double _Speed = Convert.ToDouble(x.Speed);
                                long Speed = Convert.ToInt64(MathHelper.MpsToKph(_Speed));

                                if (_var.SpeedoUnit == "kmh")
                                    Speed = Convert.ToInt64(MathHelper.MpsToKph(_Speed));
                                else
                                    Speed = Convert.ToInt64(MathHelper.MpsToMph(_Speed));

                                #endregion

                                await _im.CreateBTN(buttonStyle, Row_H, Row_W, Row1_L, Row_T, row1_clickid,
                                $"^7{_DateTime}");

                                await _im.CreateBTN(BS.ISB_CLICK | buttonStyle, Row_H, Row2_W, Row2_L, Row_T, row3_clickid,
                                $"^8Car Contact between ^7{StringHelper.StripLanguage(x.SourcePName)} ^7({x.SourceUName}) ^8and " +
                                    $"{StringHelper.StripLanguage(x.TargetPName)} ^7({x.TargetUName}) ^8({Speed} {_var.SpeedoUnit})");

                                row1_clickid++;
                                row3_clickid++;
                            }

                            await _im.ClearBtn(61);
                        }
                        #endregion
                        #endregion

                        #region Search

                        #region Clear Logic

                        BS ClearButton = BS.ISB_LIGHT | BS.ISB_CLICK;
                        var clearColor = "^1";

                        if (_var.MessagesSearchWord.Length == 0)
                        {
                            ClearButton = BS.ISB_DARK;
                            clearColor = "^8";
                        }

                        #endregion

                        // Clear
                        await _im.CreateBTN(ClearButton, 5, 4, 133, 41, 98,
                            $"{clearColor}{_var.CloseSymbol}");
                        
                        // Search
                        await _im.CreateBTT(BS.ISB_LIGHT | BS.ISB_LEFT | BS.ISB_CLICK, 5, 20, 137, 41, 95,
                            20, $" {SearchFilterText}", "^0Search keyword");

                        #endregion
                        
                        #region ' Messages Paginator Logic '
                        if (_var.InMessages)
                        {
                            // Lacking data, only show 1 page
                            if (_var.MessagesPageNumber == 1 && _var.MessagesMaxPages == 1)
                            {
                                _var.BSMessagesPagePrevious = BS.ISB_DARK;
                                _var.BSMessagesPageNext = BS.ISB_DARK;

                                _var.ColorMessagesPagePrevious = "^8";
                                _var.ColorMessagesPageNext = "^8";
                            }
                            // Reached first page
                            else if (_var.MessagesPageNumber == 1 && _var.MessagesMaxPages != 1)
                            {
                                _var.BSMessagesPagePrevious = BS.ISB_DARK;
                                _var.BSMessagesPageNext = BS.ISB_LIGHT | BS.ISB_CLICK;
                                
                                _var.ColorMessagesPagePrevious = "^8";
                                _var.ColorMessagesPageNext = "^7";
                            }
                            // Browse data
                            else if (_var.MessagesPageNumber < _var.MessagesMaxPages)
                            {
                                _var.BSMessagesPagePrevious = BS.ISB_LIGHT | BS.ISB_CLICK;
                                _var.BSMessagesPageNext = BS.ISB_LIGHT | BS.ISB_CLICK;
                                
                                _var.ColorMessagesPagePrevious = "^7";
                                _var.ColorMessagesPageNext = "^7";
                            }
                            // Reached max page
                            else if (_var.MessagesPageNumber == _var.MessagesMaxPages)
                            {
                                _var.BSMessagesPagePrevious = BS.ISB_LIGHT | BS.ISB_CLICK;
                                _var.BSMessagesPageNext = BS.ISB_DARK;
                                
                                _var.ColorMessagesPagePrevious = "^7";
                                _var.ColorMessagesPageNext = "^8";
                            }
                        }
                        #endregion
                        #region ' Relative Time (in Messages Panel) Logic '
                        if (_var.MessagesRelativeTime)
                        {
                            _var.BSSettingsMessagesRelativeTimeOn = BS.ISB_DARK;
                            _var.BSSettingsMessagesRelativeTimeOff = BS.ISB_CLICK | BS.ISB_LIGHT;

                            _var.ColorMessagesRelativeTimeOn = "^8";
                            _var.ColorMessagesRelativeTimeOff = "^7";
                        }
                        else
                        {
                            _var.BSSettingsMessagesRelativeTimeOn = BS.ISB_CLICK | BS.ISB_LIGHT;
                            _var.BSSettingsMessagesRelativeTimeOff = BS.ISB_DARK;

                            _var.ColorMessagesRelativeTimeOn = "^7";
                            _var.ColorMessagesRelativeTimeOff = "^8";
                        }
                        #endregion
                        #region Relative Time Setting
                        // 194,195,196

                        await _im.CreateBTN(BS.ISB_LEFT, 4, 45, 43, pageinator_base_h, 194,
                            $"^7" + "Display date as relative time ^8(02d 15h 22m)");

                        await _im.CreateBTN(_var.BSSettingsMessagesRelativeTimeOff, 5, 10, 43, relative_time_T, 195,
                            $"{_var.ColorMessagesRelativeTimeOff}OFF");

                        await _im.CreateBTN(_var.BSSettingsMessagesRelativeTimeOn, 5, 10, 53, relative_time_T, 196,
                            $"{_var.ColorMessagesRelativeTimeOn}ON");
                        #endregion
                        #region Paginator
                        await _im.CreateBTN(_var.BSMessagesPagePrevious, 6, 10, 90, pageinator_base_h, 197, 
                            $"{_var.ColorMessagesPagePrevious}‹");
                        
                        await _im.CreateBTN(_var.BSMessagesPageNext, 6, 10, 100, pageinator_base_h, 198, 
                            $"{_var.ColorMessagesPageNext}›");

                        var totalPages = string.Format(CultureInfo.InvariantCulture, "{0:n0}", _var.MessagesMaxPages);
                        var totalRecords = string.Format(CultureInfo.InvariantCulture, "{0:n0}", _var.MessagesRecords);
                        
                        await _im.CreateBTN(BS.ISB_C1, 5, 66, 67, pageinator_label_h, 199,
                            $"^7Page ^3{_var.MessagesPageNumber} ^7of ^3{totalPages} ^8({totalRecords} records)");
                        #endregion
                        #region Close Window
                        await _im.CreateBTN(BS.ISB_CLICK, 8, 8, 149, 29, 200,
                            $"{_var.CloseSymbolColor}{_var.CloseSymbol}");
                        #endregion
                    }
                    #endregion
                    break;

                case WindowState.CLOSE:
                    await _im.ClearBtnRange(51, 100);
                    await _im.ClearBtnRange(194, 200);
                    break;
            }
        }

        #endregion

        #region Side Menu

        public async Task sideMenu(WindowState state)
        {
            if (_var.insim == null)
                return;

            switch (state)
            {
                case WindowState.OPEN:
                    #region Menu Variables
                    var Menu_BasePosition_H = _btn.Location(SIDEMENU_CUSTOMIZE_UI).Top;

                    // Height variables
                    var MainMenu_H = Convert.ToByte(Menu_BasePosition_H + 1);
                    var dots_H = Convert.ToByte(Menu_BasePosition_H + 5);
                    var CustomizeUI_H = Convert.ToByte(Menu_BasePosition_H + 9);
                    var Settings_T = Convert.ToByte(Menu_BasePosition_H + 14);
                    var Messages_T = Convert.ToByte(Settings_T + 5);
                    var Laptimes_T = Convert.ToByte(Messages_T + 5);
                    var Debug_T = Convert.ToByte(Laptimes_T + 5);

                    // Width variables
                    var Menu_BasePosition_W = _btn.Location(SIDEMENU_CUSTOMIZE_UI).Left;
                    var Menu_Content_L = Convert.ToByte(Menu_BasePosition_W + 1);

                    #endregion
                    
                    await _im.CreateBTN(BS.ISB_DARK, 31, 17, Menu_BasePosition_W, Menu_BasePosition_H, 1, $"^7" + "");
                    await _im.CreateBTN(BS.ISB_C2, 5, 15, Menu_Content_L, MainMenu_H, 2, $"^7" + $@"Main Menu");
                    await _im.CreateBTN(BS.ISB_C2, 4, 15, Menu_Content_L, dots_H, 3, $"^7" + "·  ·  ·");
                    await _im.CreateBTN(_btn.Get(SIDEMENU_CUSTOMIZE_UI).Style, 5, 15, Menu_Content_L, CustomizeUI_H, _btn.Get(SIDEMENU_CUSTOMIZE_UI).PrimaryClickID,
                        $"{_btn.Get(SIDEMENU_CUSTOMIZE_UI).Color}{_btn.Get(SIDEMENU_CUSTOMIZE_UI).SelectedColor}{_btn.Get(SIDEMENU_CUSTOMIZE_UI).Body}");
                    await _im.CreateBTN(_var.BSSettings, 5, 15, Menu_Content_L, Settings_T, 5, $"{_var.ColorSettings}Settings");
                    await _im.CreateBTN(_var.BSMessages, 5, 15, Menu_Content_L, Messages_T, 6, $"{_var.ColorMessages}Messages");
                    await _im.CreateBTN(_var.BSSidemenuLaptimes, 5, 15, Menu_Content_L, Laptimes_T, 7, $"{_var.ColorMessages}Laptimes");
                    
                    if (_var.AppEnvironment == EnvironmentEnum.DEVELOPMENT)
                        await _im.CreateBTN(_var.BSSidemenuLaptimes, 5, 15, Menu_Content_L, Convert.ToByte(Laptimes_T + 5), 8, $"{_var.ColorMessages}Debug");
                    
                    break;
                
                case WindowStateEnum.CLOSE:
                    await _im.ClearBtnRange(1, 7);
                    break;
            }
        }

        #endregion
        
        #region ' Race Positions '

        public async Task RacePositions(WindowState state)
        {
            #region null check

            if (_var.insim == null)
                return;

            #endregion

            switch (state)
            {
                case WindowState.OPEN:
                {
                    var trackingList = _var.Players.Values
                        .Where(x=>x.PName != "host")
                        .OrderBy(x=>x.Racer.Position)
                        .ToList();
                            
                    // For tracking
                    foreach (var s in trackingList)
                    {
                        #region Tracking

                        var highestClickID = s.Racer.getHighestClickID(_var.Players);
                        
                        if (!s.OnTrack)
                        {
                            // prototype
                            var click = s.Racer.GetUserClickID(s.UCID);
                            
                            s.Racer.userClickIDs.Remove(s.UCID);
                            await _im.ClearBtnRange(click.Col1, click.Col3);
                            continue;
                        }

                        #region Update list with new player

                        // Add players 
                        
                        var racerObj = new RaceStats
                        {
                            Col1 = Convert.ToByte(highestClickID + 1),
                            Col2 = Convert.ToByte(highestClickID + 2),
                            Col3 = Convert.ToByte(highestClickID + 3)
                        };

                        s.Racer.AddClickID(s.UCID, racerObj);

                        #endregion

                        #endregion
                    }
                    
                    #region Variables
                    var WIDTH = _btn.Size(RACE_POSITIONS).Width;
                    var HEIGHT = (byte)4;
                    var HEIGHT_TITLE = _btn.Size(RACE_POSITIONS).Height;
                    var HEIGHT_FOOTER = Convert.ToByte(HEIGHT_TITLE - 2);
                    var TOP = _btn.Location(RACE_POSITIONS).Top;
                    var TOP_BODY = Convert.ToByte(TOP + HEIGHT_TITLE);
                    var LEFT = _btn.Location(RACE_POSITIONS).Left;

                    var C1_WIDTH = Convert.ToByte(4);
                    var C3_WIDTH = Convert.ToByte(22);
                    var C2_WIDTH = Convert.ToByte(WIDTH - (C1_WIDTH + C3_WIDTH));

                    var C2_LEFT = Convert.ToByte(LEFT + C1_WIDTH);
                    var C3_LEFT = Convert.ToByte(C2_LEFT + C2_WIDTH);

                    #endregion

                    var displayingList = trackingList
                        .Where(x => x.OnTrack)
                        .Skip(0)
                        .Take(24)
                        .ToList();
                    
                    // For displaying
                    foreach (var s in displayingList)
                    {
                        if (!s.OnTrack) //  || s.Racer.Position == 0
                            continue;
                        
                        var click = s.Racer.GetUserClickID(s.UCID);
                            
                        var strUsername = $" ^8({s.UName})";
                        var strTotalLaps = "LAPS";
                        var strPosition = $"^7P{s.Racer.Position}";
                        var diffLapsStr = $"^6SAME LAP";

                        #region Position

                        if (s.Racer.Position == 0)
                            strPosition = "^8--";

                        #endregion

                        var currentLap = s.Racer.getLocalRacePosition(_var.Players);
                        var diffLapsNum = (s.Racer.Laps - currentLap);

                        #region Lap Difference

                        if (s.Racer.Laps <= 1)
                            strTotalLaps = "LAP";
                        
                        if (diffLapsNum > 0)
                            diffLapsStr = $"^6{diffLapsNum} ^8{strTotalLaps} AHEAD";
                        else if (diffLapsNum < 0)
                            diffLapsStr = $"^6{diffLapsNum} ^8{strTotalLaps} BEHIND";
                        
                        if (s.IsLocal)
                            diffLapsStr = $"";

                        #endregion

                        if (WIDTH < 50)
                            strUsername = string.Empty;
                        
                        await _im.CreateBTN(BS.ISB_DARK, HEIGHT, C1_WIDTH, LEFT, TOP_BODY, click.Col1,
                            $@"{strPosition}");
                        
                        await _im.CreateBTN(BS.ISB_DARK | BS.ISB_LEFT, HEIGHT, C2_WIDTH, C2_LEFT, TOP_BODY, click.Col2,
                            $@"{s.PName}{strUsername}");

                        var tsComp = _var.trackedLaptime - s.Racer.trackedLapTime;
                        var comp = await _stringFormatter.getRaceTime(tsComp);
                        
                        if (tsComp.TotalMilliseconds == 0)
                            comp = "^8--";
                        
                        if (s.IsLocal || tsComp.TotalMilliseconds == 0)
                            comp = "^8--";
                        
                        await _im.CreateBTN(BS.ISB_DARK | BS.ISB_LEFT, HEIGHT, C3_WIDTH, C3_LEFT, TOP_BODY, click.Col3,
                            $@"^7{comp} ^8- {diffLapsStr.Replace("-", string.Empty)}");
                        
                        TOP_BODY += HEIGHT;
                    }
                    
                    /*await _im.CreateBTN(BS.ISB_DARK, 4, WIDTH, LEFT, TOP_BODY, 81,
                        $@"^7Cycling in ^6{_var.cycleRacers:ss}s");*/
                    break;
                }

                case WindowState.CLOSE:
                    var list = _var.Players.Values
                        .Where(x=>x.PName != "host");

                    var dict = list
                        .SelectMany(player => player.Racer.userClickIDs)
                        .Where(x => x.Value != null);

                    await _im.ClearBtnRange(
                        dict.Min(x=>x.Value!.Col1),
                        dict.Max(x=>x.Value!.Col3));
                    break;
            }
        }
        
        #endregion
    }
}
