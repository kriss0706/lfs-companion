﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LFSCompanion.Insim.Models;
using InSimDotNet.Out;
using LFSCompanion.Application.Classes;
using LFSCompanion.Application.Utils;

namespace LFSCompanion.Insim.Classes
{
    /// <summary>
    /// Everything related to OutGauge
    /// </summary>
    public class outGauge
    {
        private Variables _vars;
        private ExceptionProcessor _exception;
        private StringFormatter _formatter;
        private MathFactory _math;

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="variables"></param>
        /// <param name="exception"></param>
        public outGauge(Variables variables, ExceptionProcessor exception, StringFormatter stringFormatter, MathFactory mathFactory)
        {
            _vars = variables;
            _exception = exception;
            _formatter = stringFormatter;
            _math = mathFactory;
        }

        /// <summary>
        /// Initialize outgauge, bind event handlers and connect
        /// </summary>
        /// <returns></returns>
        public async Task Initialize()
        {
            // TimeSpan timeout = TimeSpan.FromSeconds(5);
            _vars.outgauge = new OutGauge();

            // Bind Event Handlers
            _vars.outgauge.PacketReceived += async (o, e) => await outgauge_PacketReceived(o, e);
            // _vars.outgauge.TimedOut += async (o,e) => await outgauge_TimedOut(o,e);

            // Start listening for packets from LFS.
            _vars.outgauge.Connect("127.0.0.1", _vars.Application.OutGaugePort);
            Console.WriteLine($"OutGauge has been initialized.");
        }

        /// <summary>
        /// Handle the outgauge packets
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task outgauge_PacketReceived(object sender, OutGaugeEventArgs e)
        {
            try
            {
                _vars.vehicleDashFlag = e.Packet.ShowLights;
                _vars.vehicleRpm = e.Packet.RPM;
                _vars.vehicleKmh = InSimDotNet.Helpers.MathHelper.MpsToKph(e.Packet.Speed);
                _vars.vehicleMph = InSimDotNet.Helpers.MathHelper.MpsToMph(e.Packet.Speed);
                _vars.VehicleThrottle = e.Packet.Throttle;
                
                // if (e.Packet.ShowLights.ToString())

                // Console.WriteLine(e.Packet.ShowLights.ToString());

                #region Turbo
                if (e.Packet.Turbo > 0.0)
                    _vars.vehicleTurbo = e.Packet.Turbo;
                else
                    _vars.vehicleTurbo = 0.0;
                #endregion

                // Fuel
                _vars.vehicleFuel = e.Packet.Fuel;
                _vars.vehicleFuelLitres = e.Packet.Fuel * _vars.vehicleTankSize;
                _vars.vehicleFuelGallons = await _math.LitresToGallons(_vars.vehicleFuelLitres);
                _vars.vehicleFuelPercent = _vars.vehicleFuel * 100;

                #region Gears
                string Gears = e.Packet.Gear.ToString();
                switch (Gears)
                {
                    case "0":
                        if (_vars.GearFormat == "classic")
                        {
                            _vars.vehicleGear = "R";
                        }
                        else if (_vars.GearFormat == "modern")
                        {
                            _vars.vehicleGear = "Rev";
                        }
                        break;

                    case "1":
                        if (_vars.GearFormat == "classic")
                        {
                            _vars.vehicleGear = "N";
                        }
                        else if (_vars.GearFormat == "modern")
                        {
                            _vars.vehicleGear = "N";
                        }
                        break;

                    case "2":
                        if (_vars.GearFormat == "classic")
                        {
                            _vars.vehicleGear = "1";
                        }
                        else if (_vars.GearFormat == "modern")
                        {
                            _vars.vehicleGear = "1st";
                        }
                        break;

                    case "3":
                        if (_vars.GearFormat == "classic")
                        {
                            _vars.vehicleGear = "2";
                        }
                        else if (_vars.GearFormat == "modern")
                        {
                            _vars.vehicleGear = "2nd";
                        }
                        break;

                    case "4":
                        if (_vars.GearFormat == "classic")
                        {
                            _vars.vehicleGear = "3";
                        }
                        else if (_vars.GearFormat == "modern")
                        {
                            _vars.vehicleGear = "3rd";
                        }
                        break;

                    case "5":
                        if (_vars.GearFormat == "classic")
                        {
                            _vars.vehicleGear = "4";
                        }
                        else if (_vars.GearFormat == "modern")
                        {
                            _vars.vehicleGear = "4th";
                        }
                        break;

                    case "6":
                        if (_vars.GearFormat == "classic")
                        {
                            _vars.vehicleGear = "5";
                        }
                        else if (_vars.GearFormat == "modern")
                        {
                            _vars.vehicleGear = "5th";
                        }
                        break;

                    case "7":
                        if (_vars.GearFormat == "classic")
                        {
                            _vars.vehicleGear = "6";
                        }
                        else if (_vars.GearFormat == "modern")
                        {
                            _vars.vehicleGear = "6th";
                        }
                        break;

                    case "8":
                        if (_vars.GearFormat == "classic")
                        {
                            _vars.vehicleGear = "7";
                        }
                        else if (_vars.GearFormat == "modern")
                        {
                            _vars.vehicleGear = "7th";
                        }
                        break;
                }
                #endregion
            }
            catch (Exception ex)
            {
                await _exception.Log(ExceptionProcessor.LogTypeEnum.CONSOLE, "OutGauge_PacketReceived", ex);
            }
        }

        /// <summary>
        /// Timed out event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task outgauge_TimedOut(object sender, EventArgs e)
        {
            try
            {
                Console.WriteLine("OutGauge connection timed out!");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"outgauge_TimedOut timed out: {ex.StackTrace}");
            }
        }
    }
}
