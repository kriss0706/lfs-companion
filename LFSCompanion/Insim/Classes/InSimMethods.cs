﻿using InSimDotNet;
using InSimDotNet.Helpers;
using InSimDotNet.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Runtime.Intrinsics.Arm;
using System.Net.NetworkInformation;
using LFSCompanion.Insim.Events;
using BS = InSimDotNet.Packets.ButtonStyles;
using LFSCompanion.Application.Extensions;
using System.Reflection;
using LFSCompanion.Application.Classes;
using LFSCompanion.Application.SQL.Seeders;
using LFSCompanion.Insim.Models;
using LFSCompanion.Application.SQL;
using LFSCompanion.Insim.Models.Enum;
using static LFSCompanion.Insim.Models.Enum.ButtonEnum;
using CarContact = LFSCompanion.Application.SQL.Models.CarContact;

namespace LFSCompanion.Insim.Classes
{
    /// <summary>
    /// Ancient class used to store various miscellaneous methods
    /// </summary>
    public class InSimMethods
    {
        private Variables _vars;
        private ExceptionProcessor _exc;
        private SQLContext _sql;
        private StringFormatter _formatter;
        private ButtonManager _buttonManager;

        public InSimMethods(Variables variables, ExceptionProcessor exc, SQLContext sqlContext, StringFormatter stringFormatter,
            ButtonManager buttonManager)
        {
            _vars = variables;
            _exc = exc;
            _sql = sqlContext;
            _formatter = stringFormatter;
            _buttonManager = buttonManager;
        }

        /// <summary>
        /// Send a message to LFS locally
        /// - IS_MSL
        /// </summary>
        /// <param name="Content"></param>
        /// <returns></returns>
        public async Task LMsg(string Content, string? Alias = "^3[Companion] ")
        {
            try
            {
                if (_vars.insim == null)
                    return;
                
                await _vars.insim.SendAsync(new IS_MSL
                {
                    Msg = $"{Alias}^8{Content}",
                    ReqI = 0
                });
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "LMsg()", ex);
            }
        }

        public async Task Error(string message)
        {
            try
            {
                if (_vars.insim == null)
                    return;

                await LMsg($"^1Error: ^7{message}", "^3[C] ");
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "LMsg()", ex);
            }
        }

        /// <summary>
        /// Send a message to LFS
        /// - IS_MST
        /// </summary>
        /// <param name="Content"></param>
        /// <returns></returns>
        public async Task SMsg(string Content)
        {
            try
            {
                if (_vars.insim == null)
                    return;
                
                await _vars.insim.SendAsync(new IS_MST
                {
                    Msg = $"{Content}",
                    ReqI = 0
                });
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "SMsg()", ex);
            }
        }

        public string GetMacAddress()
        {
            var networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();

            foreach (var networkInterface in networkInterfaces)
            {
                if (networkInterface.NetworkInterfaceType != NetworkInterfaceType.Ethernet &&
                    networkInterface.NetworkInterfaceType != NetworkInterfaceType.Wireless80211)
                    continue;
                
                PhysicalAddress macAddress = networkInterface.GetPhysicalAddress();
                return macAddress.ToString();
            }

            return string.Empty; // MAC address not found
        }

        /// <summary>
        /// Get setting-values from mysql and load them locally
        /// </summary>
        /// <returns></returns>
        public async Task LoadSettings()
        {
            try
            {
                _vars.ConsoleChat = await StringToBoolean(await _sql.GetSettingAsync("Settings", "Setting", "ConsoleChat"));
                _vars.AutoStart = await StringToBoolean(await _sql.GetSettingAsync("Settings", "Setting", "AutoStart"));
                _vars.SettingsMenuDisplay = await _sql.GetSettingAsync("Settings", "Setting", "MenuDisplay");
                _vars.FuelWarningValue = Convert.ToDouble(await _sql.GetSettingAsync("Settings", "Setting", "FuelWarningValue"));
                _vars.SpeedoUnit = await _sql.GetSettingAsync("Settings", "Setting", "SpeedoUnit");
                _vars.FuelUnit = await _sql.GetSettingAsync("Settings", "Setting", "FuelUnit");
                _vars.GearFormat = await _sql.GetSettingAsync("Settings", "Setting", "GearFormat");
                _vars.RpmStyle = await _sql.GetSettingAsync("Settings", "Setting", "RpmStyle");
                _vars.FuelWarningType = await _sql.GetSettingAsync("Settings", "Setting", "FuelWarningType");
                _vars.GreetingMessage = await _sql.GetSettingAsync("Settings", "Setting", "Greeting");
                _vars.GreetingsEnabled = Convert.ToBoolean(await _sql.GetSettingAsync("Settings", "Setting", "GreetingEnabled"));
                _vars.MessagesRelativeTime = Convert.ToBoolean(await _sql.GetSettingAsync("Settings", "Setting", "DateRelativeTime"));
                _vars.TotalFuel = Convert.ToBoolean(await _sql.GetSettingAsync("Settings", "Setting", "TotalFuel"));
                _vars.AutoLightsEnabled = Convert.ToBoolean(await _sql.GetSettingAsync("Settings", "Setting", "AutoLights"));
                _vars.RacingMessagesEnabled = Convert.ToBoolean(await _sql.GetSettingAsync("Settings", "Setting", "LaptimeMessages"));
                _vars.LastUsedTemplate = await _sql.GetSettingAsync("Settings", "Setting", "LastUsedTemplate");
                _vars.CarContactEnabled = Convert.ToBoolean(await _sql.GetSettingAsync("Settings", "Setting", "CarContact"));

                string DateTimeFormat = await _sql.GetSettingAsync("Settings","Setting","DateTimeFormat");
                _vars.DateTimeFormat = (DateTimeFormatEnum) Enum.Parse(typeof(DateTimeFormatEnum), DateTimeFormat);
                _vars.lfswPubstat = await _sql.GetSettingAsync("Settings", "Setting", "lfswPubstat");
                _vars.UseSiren = Convert.ToBoolean(await _sql.GetSettingAsync("Settings", "Setting", "UseSiren"));

                #region ' Button Positions '

                #region Menu
                _buttonManager.Location(SIDEMENU_CUSTOMIZE_UI).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Menu_L"));
                _buttonManager.Location(SIDEMENU_CUSTOMIZE_UI).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Menu_T"));
                #endregion

                #region Fuel
                _buttonManager.Size(FUEL).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Fuel_H"));
                _buttonManager.Size(FUEL).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Fuel_W"));
                _buttonManager.Location(FUEL).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Fuel_L"));
                _buttonManager.Location(FUEL).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Fuel_T"));
                _buttonManager.Get(FUEL).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "Fuel_IsVisible"));
                #endregion
                #region Rpm
                _buttonManager.Size(RPM).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Rpm_H"));
                _buttonManager.Size(RPM).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Rpm_W"));
                _buttonManager.Location(RPM).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Rpm_L"));
                _buttonManager.Location(RPM).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Rpm_T"));
                _buttonManager.Get(RPM).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "Rpm_IsVisible"));
                #endregion
                #region Tc
                _buttonManager.Size(TC).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Tc_H"));
                _buttonManager.Size(TC).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Tc_W"));
                _buttonManager.Location(TC).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Tc_L"));
                _buttonManager.Location(TC).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Tc_T"));
                _buttonManager.Get(TC).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "Tc_IsVisible"));
                #endregion
                #region Lights
                _buttonManager.Size(LIGHTS).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Lights_H"));
                _buttonManager.Size(LIGHTS).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Lights_W"));
                _vars.Lights_L = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Lights_L"));
                _vars.Lights_T = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Lights_T"));
                _buttonManager.Get(LIGHTS).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "Lights_IsVisible"));
                #endregion
                #region Abs
                _buttonManager.Size(ABS).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Abs_H"));
                _buttonManager.Size(ABS).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Abs_W"));
                _vars.Abs_L = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Abs_L"));
                _vars.Abs_T = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Abs_T"));
                _buttonManager.Get(ABS).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "Abs_IsVisible"));
                #endregion
                #region Gear
                _buttonManager.Size(GEAR).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Gear_H"));
                _buttonManager.Size(GEAR).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Gear_W"));
                _buttonManager.Location(GEAR).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Gear_L"));
                _buttonManager.Location(GEAR).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Gear_T"));
                _buttonManager.Get(GEAR).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "Gear_IsVisible"));
                #endregion
                #region Speed
                _buttonManager.Size(SPEED).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Speed_H"));
                _buttonManager.Size(SPEED).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Speed_W"));
                _buttonManager.Location(SPEED).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Speed_L"));
                _buttonManager.Location(SPEED).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Speed_T"));
                _buttonManager.Get(SPEED).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "Speed_IsVisible"));
                #endregion
                #region Handbrake
                _buttonManager.Size(HANDBRAKE).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Handbrake_H"));
                _buttonManager.Size(HANDBRAKE).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Handbrake_W"));
                _buttonManager.Location(HANDBRAKE).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Handbrake_L"));
                _buttonManager.Location(HANDBRAKE).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Handbrake_T"));
                _buttonManager.Get(HANDBRAKE).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "Handbrake_IsVisible"));
                #endregion
                #region BlinkerL
                _buttonManager.Size(BLINKER_LEFT).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "BlinkerL_H"));
                _buttonManager.Size(BLINKER_LEFT).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "BlinkerL_W"));
                _buttonManager.Location(BLINKER_LEFT).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "BlinkerL_L"));
                _buttonManager.Location(BLINKER_LEFT).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "BlinkerL_T"));
                _buttonManager.Get(BLINKER_LEFT).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "BlinkerL_IsVisible"));
                #endregion
                #region BlinkerR
                _buttonManager.Size(BLINKER_RIGHT).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "BlinkerR_H"));
                _buttonManager.Size(BLINKER_RIGHT).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "BlinkerR_W"));
                _buttonManager.Location(BLINKER_RIGHT).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "BlinkerR_L"));
                _buttonManager.Location(BLINKER_RIGHT).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "BlinkerR_T"));
                _buttonManager.Get(BLINKER_RIGHT).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "BlinkerR_IsVisible"));
                #endregion
                #region Turbo
                _buttonManager.Size(TURBO).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Turbo_H"));
                _buttonManager.Size(TURBO).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Turbo_W"));
                _buttonManager.Location(TURBO).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Turbo_L"));
                _buttonManager.Location(TURBO).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Turbo_T"));
                _buttonManager.Get(TURBO).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "Turbo_IsVisible"));
                #endregion
                #region DateTime
                _buttonManager.Size(DATETIME).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "DateTime_H"));
                _buttonManager.Size(DATETIME).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "DateTime_W"));
                _buttonManager.Location(DATETIME).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "DateTime_L"));
                _buttonManager.Location(DATETIME).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "DateTime_T"));
                _buttonManager.Get(DATETIME).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "DateTime_IsVisible"));
                #endregion
                #region Customize UI
                _buttonManager.Location(CUSTOMIZER_WINDOW).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "CustomizeUI_T"));
                #endregion
                #region PitSpeed
                _buttonManager.Size(PITLANE_LIMITER).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "PitSpeed_H"));
                _buttonManager.Size(PITLANE_LIMITER).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "PitSpeed_W"));
                _buttonManager.Location(PITLANE_LIMITER).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "PitSpeed_L"));
                _buttonManager.Location(PITLANE_LIMITER).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "PitSpeed_T"));
                _buttonManager.Get(PITLANE_LIMITER).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "PitSpeed_IsVisible"));
                #endregion
                #region AvgFuel
                _buttonManager.Size(AVG_FUEL).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "AvgFuel_H"));
                _buttonManager.Size(AVG_FUEL).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "AvgFuel_W"));
                _buttonManager.Location(AVG_FUEL).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "AvgFuel_L"));
                _buttonManager.Location(AVG_FUEL).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "AvgFuel_T"));
                _buttonManager.Get(AVG_FUEL).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "AvgFuel_IsVisible"));
                #endregion
                #region Laptime
                _buttonManager.Size(LAP_TIME).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Laptime_H"));
                _buttonManager.Size(LAP_TIME).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Laptime_W"));
                _buttonManager.Location(LAP_TIME).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Laptime_L"));
                _buttonManager.Location(LAP_TIME).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Laptime_T"));
                _buttonManager.Get(LAP_TIME).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "Laptime_IsVisible"));
                #endregion
                #region SplitTime
                _buttonManager.Size(LAP_SPLIT).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "SplitTime_H"));
                _buttonManager.Size(LAP_SPLIT).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "SplitTime_W"));
                _buttonManager.Location(LAP_SPLIT).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "SplitTime_L"));
                _buttonManager.Location(LAP_SPLIT).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "SplitTime_T"));
                _buttonManager.Get(LAP_SPLIT).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "SplitTime_IsVisible"));
                #endregion
                #region Track
                _buttonManager.Size(TRACK).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Track_H"));
                _buttonManager.Size(TRACK).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Track_W"));
                _buttonManager.Location(TRACK).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Track_L"));
                _buttonManager.Location(TRACK).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "Track_T"));
                _buttonManager.Get(TRACK).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "Track_IsVisible"));
                #endregion
                #region BestPB
                _buttonManager.Size(BEST_PB).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "BestPB_H"));
                _buttonManager.Size(BEST_PB).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "BestPB_W"));
                _buttonManager.Location(BEST_PB).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "BestPB_L"));
                _buttonManager.Location(BEST_PB).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "BestPB_T"));
                _buttonManager.Get(BEST_PB).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "BestPB_IsVisible"));
                #endregion
                #region Theoretical PB
                _buttonManager.Size(THEORETICAL_PB).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "TheoreticalPB_H"));
                _buttonManager.Size(THEORETICAL_PB).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "TheoreticalPB_W"));
                _buttonManager.Location(THEORETICAL_PB).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "TheoreticalPB_L"));
                _buttonManager.Location(THEORETICAL_PB).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "TheoreticalPB_T"));
                _buttonManager.Get(THEORETICAL_PB).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "TheoreticalPB_IsVisible"));
                #endregion
                #region Race Positions
                _buttonManager.Size(RACE_POSITIONS).Height = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "RacePos_H"));
                _buttonManager.Size(RACE_POSITIONS).Width = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "RacePos_W"));
                _buttonManager.Location(RACE_POSITIONS).Left = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "RacePos_L"));
                _buttonManager.Location(RACE_POSITIONS).Top = Convert.ToByte(await _sql.GetSettingAsync("ButtonPositions", "Property", "RacePos_T"));
                _buttonManager.Get(RACE_POSITIONS).IsEnabled = await StringToBoolean(await _sql.GetSettingAsync("ButtonPositions", "Property", "RacePos_IsVisible"));
                #endregion
                #endregion

            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "LoadSettings()", ex);
            }
        }
        
        /// <summary>
        /// Set button position or size
        /// </summary>
        /// <param name="addSubtract"></param>
        /// <param name="value"></param>
        /// <param name="btc"></param>
        /// <returns></returns>
        public byte buttonPosition(AddSubtract addSubtract, byte value, IS_BTC? btc)
        {
            byte Multiplier = 1;
            int ModifiedValue = value;

            if (btc == null)
                Multiplier = _vars.CustomizeMultiplier;
            else
                if (btc.CFlags == ClickFlags.ISB_LMB)
                    Multiplier = 1;
                else if (btc.CFlags == ClickFlags.ISB_RMB)
                    Multiplier = _vars.CustomizeMultiplier;
            
            if (addSubtract == AddSubtract.ADD)
                ModifiedValue = value + Multiplier;
            else if (addSubtract == AddSubtract.SUBTRACT)
                ModifiedValue = value - Multiplier;

            // Return old value if the new value is not within the button grid
            if (ModifiedValue is < 0 or > 195)
                ModifiedValue = value;

            return (byte) ModifiedValue;
        }
        
        /// <summary>
        /// Convert string (0,1) to boolean
        /// This method is used for variables retrieved from database
        /// </summary>
        /// <returns></returns>
        public async Task<bool> StringToBoolean(string? Text)
        {
            var _str = false; // Default value in case the switch case doesn't match

            try
            {
                if (Text == null)
                    return _str;
                
                switch (Text)
                {
                    case "0":
                        _str = false;
                        break;

                    case "1":
                        _str = true;
                        break;
                }

                return _str;
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "StringToBoolean() method in InSimMethods class", ex);
                return _str; // Handle the exception and return a default value if desired
            }
        }

        /// <summary>
        /// Create an InSim button
        /// </summary>
        /// <param name="ButtonStyle"></param>
        /// <param name="Height"></param>
        /// <param name="Width"></param>
        /// <param name="Left"></param>
        /// <param name="Top"></param>
        /// <param name="ClickId"></param>
        /// <param name="Text"></param>
        /// <returns></returns>
        public async Task CreateBTN(BS ButtonStyle,
            byte Height, byte Width, byte Left, byte Top, byte ClickId, string Text)
        {
            try
            {
                if (_vars.insim == null)
                    return;
                
                await _vars.insim.SendAsync(new IS_BTN
                {
                    ReqI = ClickId,
                    UCID = 0,
                    BStyle = ButtonStyle,
                    H = Height,
                    W = Width,
                    L = Left,
                    T = Top,
                    Text = Text,
                    ClickID = ClickId
                });
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET, "IS_BTN()", ex);
            }
        }

        public async Task CreateBTT(BS ButtonStyle,
            byte Height, byte Width, byte Left, byte Top, byte ClickId, byte TypeLength, string Text, string Caption, bool FillText = false)
        {
            try
            {
                if (_vars.insim == null)
                    return;

                var obj = new IS_BTN
                {
                    ReqI = ClickId,
                    UCID = 0,
                    BStyle = ButtonStyle,
                    H = Height,
                    W = Width,
                    L = Left,
                    T = Top,
                    Text = Text,
                    Caption = Caption,
                    ClickID = ClickId,
                    TypeIn = TypeLength
                };

                if (FillText)
                    obj.TypeIn = Convert.ToByte(128 + TypeLength);
                
                await _vars.insim.SendAsync(obj);
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET, "IS_BTT()", ex);
            }
        }

        /// <summary>
        /// Set local car light
        /// </summary>
        /// <param name="light"></param>
        public async Task setCarLight(long light)
        {
            try
            {
                if (_vars.insim == null)
                    return;

                var obj = new IS_SMALL
                {
                    ReqI = 0,
                    SubT = SmallType.SMALL_LCL,
                    UVal = light
                };
                
                await _vars.insim.SendAsync(obj);
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "setCarLight()", ex);
            }
        }

        /// <summary>
        /// Set local car switch
        /// </summary>
        /// <param name="light"></param>
        public async Task setCarSwitch(long light)
        {
            try
            {
                if (_vars.insim == null)
                    return;

                var obj = new IS_SMALL
                {
                    ReqI = 0,
                    SubT = SmallType.SMALL_LCS,
                    UVal = light
                };
                
                await _vars.insim.SendAsync(obj);
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "setCarSwitch()", ex);
            }
        }
        
        /// <summary>
        /// Clear a single button from the screen
        /// </summary>
        /// <param name="ClickId"></param>
        /// <returns></returns>
        public async Task ClearBtn(byte ClickId)
        {
            try
            {
                if (_vars.insim == null)
                    return;
                
                await _vars.insim.SendAsync(new IS_BFN
                {
                    ReqI = 0,
                    UCID = 0,
                    SubT = ButtonFunction.BFN_DEL_BTN,
                    ClickID = ClickId
                });
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "ClearBtn()", ex);
            }
        }

        /// <summary>
        /// Clear buttons from the screen using a range of numbers
        /// </summary>
        /// <param name="ClickId_From"></param>
        /// <param name="ClickId_To"></param>
        /// <returns></returns>
        public async Task ClearBtnRange(byte ClickId_From, byte ClickId_To)
        {
            try
            {
                if (_vars.insim == null)
                    return;
                
                await _vars.insim.SendAsync(new IS_BFN
                {
                    ReqI = 0,
                    UCID = 0,
                    SubT = ButtonFunction.BFN_DEL_BTN,
                    ClickID = ClickId_From,
                    ClickMax = ClickId_To
                });
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "ClearBtnRange()", ex);
            }
        }

        public async Task getButtonSize()
        {
            try
            {
                foreach (var x in _vars.ButtonList)
                {
                    // Primary click id is selected
                    if (_vars.LastSelectedId == x.PrimaryClickID)
                    {
                        if (x.Size == null)
                            return;

                        var sizes = x.Size.FirstOrDefault();

                        if (sizes == null)
                            return;

                        _vars.SelectedSize_W = sizes.Width;
                        _vars.SelectedSize_H = sizes.Height;
                    }

                    if (_vars.LastSelectedId != 0)
                        continue;
                    
                    _vars.SelectedSize_W = 0;
                    _vars.SelectedSize_H = 0;
                }
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    "setButtonSize() in InSimMethods class", ex);
            }
        }

        /// <summary>
        /// Used for throttle 
        /// </summary>
        /// <param name="Rpm"></param>
        /// <returns></returns>
        public string UpdateRpmOverlay(double rpm)
        {
            const int squares = 10;  // Length of the RPM meter

            // Calculate the number of filled squares based on the current RPM and maximum RPM
            int filledSquares = (int)Math.Round(rpm / _vars.MaxRpm * squares);

            // Create the RPM meter string
            string meter = "";
            for (int i = 0; i < squares; i++)
            {
                if (i < filledSquares)
                {
                    if (i >= 6 && i <= 7)
                    {
                        meter += "^3■"; // Filled square in red color
                    }
                    else if (i >= 8)
                    {
                        meter += "^1■"; // Filled square in red color
                    }
                    else
                    {
                        meter += "^2■"; // Filled square in white color
                    }
                }
                else
                {
                    meter += "^9□"; // Empty square in a different color (e.g., blue)
                }
            }

            return meter;
        }

        // A variable is provided as a parameter
        // If the variable is true, set it to false
        // If the variable is false, set it to true
        // Then return it
        public async Task<bool> ToggleBoolean(bool Variable)
        {
            try
            {
                if (Variable == true)
                    Variable = false;
                else
                    Variable = true;

                return Variable;
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "ToggleBoolean()", ex);
                return false;
            }
        }

        /// <summary>
        /// Get player object by UCID
        /// </summary>
        /// <param name="ucid"></param>
        /// <returns></returns>
        public Player? getByUcid(byte ucid)
        {
            var p = _vars.Players == null ? null : _vars.Players.ContainsKey(ucid) ? _vars.Players[ucid] : null;
            return p;
        }

        public Player? getLocal()
        {
            Player? player = null;
            
            foreach (var p in _vars.Players)
            {
                if (p.Value.IsLocal)
                    player = p.Value;
            }

            return player;
        }

        /// <summary>
        /// Get player object by PLID
        /// </summary>
        /// <param name="plid"></param>
        /// <returns></returns>
        public Player? GetByPlid(byte plid)
        {
            (byte _, Player? p) = _vars.Players.FirstOrDefault(x => x.Value.PLID == plid);
            return p;
        }

        /// <summary>
        /// Fetch data from list and display it in a message (global and player)
        /// </summary>
        /// <param name="Offset"></param>
        /// <returns></returns>
        public async Task DisplayMessage(int Offset)
        {
            if (_vars.MessageTypes == MessageTypeEnum.GLOBAL)
            {

                List<SeederModels.Messages> item = _vars.GlobalMessagesCache
                    .OrderByDescending(x => x.Date)
                    .Skip(Offset)
                    .Take(1)
                    .ToList();

                string date = await _formatter.colorizeString(item[0].Date.ToString("dd/MM/yy HH:mm"), "GlobalMessages");
                string message = await _formatter.colorizeString(item[0].Message, "GlobalMessages");
                await LMsg($"^8{date} ^2{message}", "^3[C] ");
            }
            else if (_vars.MessageTypes == MessageTypeEnum.PLAYERS)
            {
                List<SeederModels.Messages> item = _vars.PlayerMessagesCache
                    .OrderByDescending(x => x.Date)
                    .Skip(Offset)
                    .Take(1)
                    .ToList();

                string date = await _formatter.colorizeString(item[0].Date.ToString("dd/MM/yy HH:mm"),"GlobalMessages");
                string nickname = await _formatter.colorizeString(item[0].Nickname,"GlobalMessages");
                string username = await _formatter.colorizeString(item[0].Username,"GlobalMessages");
                string message = await _formatter.colorizeString(item[0].Message,"GlobalMessages");

                await LMsg($"^8{date} {nickname} ^8({username}) ^7: ^2{message}", "^3[C] ");
            }
            else if (_vars.MessageTypes == MessageTypeEnum.CARCONTACT)
            {
                List<CarContact> item = _vars.CarContactCache
                    .OrderByDescending(x => x.Date)
                    .Skip(Offset)
                    .Take(1)
                    .ToList();
                
                string nickname1 = await _formatter.colorizeString(item[0].SourcePName,"GlobalMessages");
                string nickname2 = await _formatter.colorizeString(item[0].TargetPName,"GlobalMessages");


                double speed = MathHelper.MpsToMph(Convert.ToDouble(item[0].Speed));;

                if (_vars.SpeedoUnit == "kmh")
                    speed = MathHelper.MpsToKph(Convert.ToDouble(item[0].Speed));

                await LMsg($"^8Car contact {nickname1} and {nickname2} ^8({speed} {_vars.SpeedoUnit})", "^3[C] ");
            }
        }
        
        /// <summary>
        /// Update relevant message lists to use in Message Panel
        /// </summary>
        public async Task updateLists()
        {
            var local = getLocal();
            if (local == null)
                return;
            
            var messages = _vars.MessageTypes switch
            {
                MessageTypeEnum.GLOBAL => _vars.GlobalMessages.AsEnumerable(),
                MessageTypeEnum.PLAYERS => _vars.PlayerMessages.AsEnumerable(),
                _ => Enumerable.Empty<SeederModels.Messages>()
            };

            var carcontact = _vars.CarContactMessages.AsEnumerable();
            
            var messagesBase = messages
                .OrderByDescending(x => x.Date)
                .ToList();
            
            var carcontactBase = carcontact
                .OrderByDescending(x => x.Date)
                .ToList();

            var totalPages = messagesBase.Count;

            if (_vars.MessagesOnlyLocalData)
            {
                messagesBase = messagesBase
                    .Where(x => x.Message.Contains(local.PName))
                    .ToList();
                
                carcontactBase = carcontactBase
                    .Where(x => string.Equals(x.SourceUName, local.UName, StringComparison.CurrentCultureIgnoreCase) || 
                                string.Equals(x.TargetUName, local.UName, StringComparison.CurrentCultureIgnoreCase))
                    .ToList();
            }

            if (_vars.MessagesSearchWord.Length == 0)
            {
                switch (_vars.MessageTypes)
                {
                    case MessageTypeEnum.GLOBAL:
                        
                        _vars.MessagesRecords = messagesBase.Count;
                        break;
                    
                    case MessageTypeEnum.PLAYERS:
                        
                        _vars.MessagesRecords = messagesBase.Count;
                        break;
                    
                    case MessageTypeEnum.CARCONTACT:
                        
                        _vars.MessagesRecords = carcontactBase.Count;
                        break;
                }
            }
            else
            {
                switch (_vars.MessageTypes)
                {
                    case MessageTypeEnum.GLOBAL:
                        messagesBase = messagesBase
                            .Where(x => x.Message.ToLower().Contains(_vars.MessagesSearchWord.ToLower()))
                            .ToList();
                        break;
                    
                    case MessageTypeEnum.PLAYERS:
                        messagesBase = messagesBase
                            .Where(x => x.Message.ToLower().Contains(_vars.MessagesSearchWord.ToLower()) || 
                                        x.Username.ToLower().Contains(_vars.MessagesSearchWord.ToLower()) || 
                                        x.Nickname.ToLower().Contains(_vars.MessagesSearchWord.ToLower()))
                            .ToList();
                        break;
                    
                    case MessageTypeEnum.CARCONTACT:
                        carcontactBase = carcontactBase
                            .Where(x=>x.SourceUName.Contains(_vars.MessagesSearchWord) || 
                                      x.SourcePName.Contains(_vars.MessagesSearchWord, StringComparison.OrdinalIgnoreCase) || 
                                      x.TargetUName.Contains(_vars.MessagesSearchWord, StringComparison.OrdinalIgnoreCase) || 
                                      x.TargetPName.Contains(_vars.MessagesSearchWord, StringComparison.OrdinalIgnoreCase))
                            .ToList();
                        break;
                }
            }
            
            // Run code regardless
            switch (_vars.MessageTypes)
            {
                case MessageTypeEnum.GLOBAL:
                    _vars.GlobalMessagesCache = messagesBase
                        .Skip(_vars.MessagesSkip)
                        .Take(_vars.MessagesTake)
                        .ToList();
                    
                    _vars.MessagesRecords = messagesBase.Count;
                    totalPages = messagesBase.Count;
                    break;
                
                case MessageTypeEnum.PLAYERS:
                    _vars.PlayerMessagesCache = messagesBase
                        .Skip(_vars.MessagesSkip)
                        .Take(_vars.MessagesTake)
                        .ToList();
                    
                    totalPages = messagesBase.Count;
                    _vars.MessagesRecords = messagesBase.Count;
                    break;
                
                case MessageTypeEnum.CARCONTACT:
                    _vars.CarContactCache = carcontactBase
                        .Skip(_vars.MessagesSkip)
                        .Take(_vars.MessagesTake)
                        .ToList();
                    
                    totalPages = carcontactBase.Count;
                    _vars.MessagesRecords = carcontactBase.Count;
                    break;
            }
            
            // Calculate paginator: Total pages
            if (totalPages > 10)
                _vars.MessagesMaxPages = (int) Math.Ceiling((double) totalPages / 10);
            else
                _vars.MessagesMaxPages = 1;
        }
    }
}
