namespace LFSCompanion.Insim.Classes;

public class LapGui
{
    private TimeSpan lastSplit;
    private byte lastSplitType;
    private TimeSpan Laptime;
    
    private double bestPB;
    private double theoreticalPB;

    #region Getters

    /// <summary>
    /// Display information in the User Interface (SPLIT_VALUE)
    /// </summary>
    /// <returns></returns>
    public TimeSpan getLastSplit()
    {
        return lastSplit;
    }

    /// <summary>
    /// Display information in the User Interface (SPLIT_TYPE in the Title)
    /// </summary>
    /// <returns></returns>
    public byte getLastSplitType()
    {
        return lastSplitType;
    }
    
    /// <summary>
    /// Fetch the lap time
    /// </summary>
    /// <returns></returns>
    public TimeSpan getLaptime()
    {
        return Laptime;
    }
    
    public double getBestPb()
    {
        return bestPB;
    }

    public double getTheoreticalPb()
    {
        return theoreticalPB;
    }

    #endregion

    #region Setters

    /// <summary>
    /// Set the last Split
    /// </summary>
    /// <param name="value"></param>
    public void setLastSplit(TimeSpan value)
    {
        lastSplit = value;
    }

    /// <summary>
    /// Which split type (1-3)
    /// </summary>
    /// <param name="value"></param>
    public void setLastSplitType(byte value)
    {
        lastSplitType = value;
    }
    
    /// <summary>
    /// Set the laptime
    /// </summary>
    /// <param name="value"></param>
    public void setLaptime(TimeSpan value)
    {
        Laptime = value;
    }
    
    public void setBestPb(double bestPB)
    {
        this.bestPB = bestPB;
    }

    public void setTheoreticalPb(double theoreticalPb)
    {
        this.theoreticalPB = theoreticalPb;
    }

    #endregion
    
    
}