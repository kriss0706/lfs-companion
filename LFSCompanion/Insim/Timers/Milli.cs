using LFSCompanion.Application.SQL.Models;
using LFSCompanion.Insim.Events;
using InSimDotNet.Helpers;
using InSimDotNet.Packets;
using LFSCompanion.Application.Classes;
using LFSCompanion.Application.Extensions;
using LFSCompanion.Application.Utils;
using LFSCompanion.Insim.Classes;
using LFSCompanion.Insim.Models;
using LFSCompanion.Insim.Models.Enum;
using Microsoft.VisualBasic;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using InSimDotNet.Out;
using static LFSCompanion.Insim.Models.Enum.ButtonEnum;
using static LFSCompanion.Insim.Models.Enum.WindowStateEnum;
using BS = InSimDotNet.Packets.ButtonStyles;
using SeederModels = LFSCompanion.Application.SQL.Seeders.SeederModels;

namespace LFSCompanion.Insim.Timers
{
    /// <summary>
    /// Anything related to millisecond class
    /// </summary>
    public class Milli
    {
        private Variables _vars;
        private ExceptionProcessor _exceptionP;
        private InSimMethods _im;
        private StringFormatter _stringFormatter;
        private Main _main;
        private DateFormatter _dateFormatter;
        private ButtonManager _btn;
        private MathFactory _math;
        private LapManager _lapManager;
        private WindowFactory _window;

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="variables"></param>
        /// <param name="exceptionP"></param>
        /// <param name="insimMethods"></param>
        /// <param name="stringFormatter"></param>
        public Milli(Variables variables, ExceptionProcessor exceptionP, InSimMethods insimMethods, StringFormatter stringFormatter,
            Main main, DateFormatter dateFormatter, ButtonManager clickIdManager, MathFactory mathFactory,
            LapManager lapManager, WindowFactory window)
        {
            _vars = variables;
            _exceptionP = exceptionP;
            _im = insimMethods;
            _stringFormatter = stringFormatter;
            _main = main;
            _dateFormatter = dateFormatter;
            _btn = clickIdManager;
            _math = mathFactory;
            _lapManager = lapManager;
            _window = window;
        }

        /// <summary>
        /// Eventhandler for Milliseconds (200ms)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public async void MilliSecondTimer_EventHandler(object? sender, ElapsedEventArgs e)
        {
            try
            {
                if (!_vars.ApiTestPassed)
                    return;

                if (_vars.insim == null)
                    return;
                
                if (_vars.StartButtonsCounter.TotalMilliseconds < 100)
                    _vars.StartButtonsCounter.Add(TimeSpan.FromMilliseconds(20));
                else
                {
                    if (_vars.InSimView.HasFlag(StateFlags.ISS_GAME))
                    {
                        {
                            #region ' Strobe Visuals '

                            if (_vars.StrobeCounter < 31 && _vars.StrobeActive)
                            {
                                #region FogFront/Extra Stage 1

                                if (_vars.StrobeCounter == 0)
                                {
                                    await _im.setCarLight(LocalCarLights.LCL_FOG_FRONT_ON);
                                    await _im.setCarLight(LocalCarLights.LCL_EXTRA_ON);
                                }
                                else if (_vars.StrobeCounter == 2)
                                {
                                    await _im.setCarLight(LocalCarLights.LCL_FOG_FRONT_OFF);
                                    await _im.setCarLight(LocalCarLights.LCL_EXTRA_OFF);
                                }
                                else if (_vars.StrobeCounter == 4)
                                {
                                    await _im.setCarLight(LocalCarLights.LCL_FOG_FRONT_ON);
                                }
                                else if (_vars.StrobeCounter == 6)
                                {
                                    await _im.setCarLight(LocalCarLights.LCL_FOG_FRONT_OFF);
                                }

                                #endregion
                                #region Headlights/Extra Stage 2

                                else if (_vars.StrobeCounter == 8)
                                {
                                    await _im.setCarSwitch(LocalCarSwitches.LCS_HEADLIGHTS_ON);
                                    await _im.setCarLight(LocalCarLights.LCL_EXTRA_ON);
                                }
                                else if (_vars.StrobeCounter == 10)
                                {
                                    await _im.setCarSwitch(LocalCarSwitches.LCS_HEADLIGHTS_OFF);
                                    await _im.setCarLight(LocalCarLights.LCL_EXTRA_OFF);
                                }
                                else if (_vars.StrobeCounter == 12)
                                {
                                    await _im.setCarSwitch(LocalCarSwitches.LCS_HEADLIGHTS_ON);
                                    await _im.setCarLight(LocalCarLights.LCL_EXTRA_ON);
                                }
                                else if (_vars.StrobeCounter == 14)
                                {
                                    await _im.setCarSwitch(LocalCarSwitches.LCS_HEADLIGHTS_OFF);
                                    await _im.setCarLight(LocalCarLights.LCL_EXTRA_OFF);
                                }

                                #endregion
                                #region FogRear/Extra Stage 3

                                else if (_vars.StrobeCounter == 16)
                                {
                                    await _im.setCarLight(LocalCarLights.LCL_FOG_REAR_ON);
                                    await _im.setCarLight(LocalCarLights.LCL_EXTRA_ON);
                                }
                                else if (_vars.StrobeCounter == 18)
                                {
                                    await _im.setCarLight(LocalCarLights.LCL_FOG_REAR_OFF);
                                    await _im.setCarLight(LocalCarLights.LCL_EXTRA_OFF);
                                }
                                else if (_vars.StrobeCounter == 20)
                                {
                                    await _im.setCarLight(LocalCarLights.LCL_FOG_REAR_ON);
                                }
                                else if (_vars.StrobeCounter == 22)
                                {
                                    await _im.setCarLight(LocalCarLights.LCL_FOG_REAR_OFF);
                                }

                                #endregion
                                #region Headlight/Extra Stage 4

                                else if (_vars.StrobeCounter == 24)
                                {
                                    await _im.setCarSwitch(LocalCarSwitches.LCS_HEADLIGHTS_ON);
                                    await _im.setCarLight(LocalCarLights.LCL_EXTRA_ON);
                                }
                                else if (_vars.StrobeCounter == 26)
                                {
                                    await _im.setCarSwitch(LocalCarSwitches.LCS_HEADLIGHTS_OFF);
                                    await _im.setCarLight(LocalCarLights.LCL_EXTRA_OFF);
                                }
                                else if (_vars.StrobeCounter == 28)
                                {
                                    await _im.setCarSwitch(LocalCarSwitches.LCS_HEADLIGHTS_ON);
                                    await _im.setCarLight(LocalCarLights.LCL_EXTRA_ON);
                                }
                                else if (_vars.StrobeCounter == 30)
                                {
                                    await _im.setCarSwitch(LocalCarSwitches.LCS_HEADLIGHTS_OFF);
                                    await _im.setCarLight(LocalCarLights.LCL_EXTRA_OFF);
                                }

                                #endregion

                                _vars.StrobeCounter++;
                            }
                            else
                            {
                                _vars.StrobeCounter = 0;
                            }

                            #region ' Turn off lights and siren '

                            #region ' Turn off remaining lights & siren '

                            if (_vars.StrobeOffCounter > TimeSpan.FromSeconds(0))
                            {
                                if (_vars.StrobeOffCounter < TimeSpan.FromSeconds(2))
                                {
                                    await _im.setCarSwitch(LocalCarSwitches.LCS_SIREN_OFF);
                                    await _im.setCarSwitch(LocalCarSwitches.LCS_HEADLIGHTS_OFF);
                                }
                                else if (_vars.StrobeOffCounter < TimeSpan.FromSeconds(3))
                                {
                                    await _im.setCarLight(LocalCarLights.LCL_EXTRA_OFF);
                                }
                                else if (_vars.StrobeOffCounter < TimeSpan.FromSeconds(4))
                                {
                                    await _im.setCarLight(LocalCarLights.LCL_FOG_REAR_OFF);
                                    await _im.setCarLight(LocalCarLights.LCL_FOG_FRONT_OFF);
                                }

                                _vars.StrobeOffCounter -= TimeSpan.FromMilliseconds(200);
                            }

                            #endregion

                            #endregion

                            #endregion

                            #region Connection

                            var conn = _im.getByUcid(_vars.UCID);

                            if (conn == null)
                                return;

                            foreach (var g in _vars.Players.Values)
                            {
                                g.Racer.ETime += TimeSpan.FromMilliseconds(50);
                            }

                            if (!conn.IsLocal)
                                return;

                            #endregion

                            #region Global Button Logic

                            foreach (var x in _vars.ButtonList)
                            {
                                if (!conn.IsLocal)
                                    continue;
                                
                                #region Selection Color

                                if (_vars.SelectedClickIDs.Contains(x.PrimaryClickID))
                                    x.SelectedColor = "^2";
                                else
                                    x.SelectedColor = "";

                                #endregion

                                if (_btn.getTitleClickId(x.Name) == null)
                                    continue;

                                if (_vars.InCustomizeUI)
                                {
                                    #region Enabled

                                    if (x.IsEnabled)
                                    {
                                        x.Style = BS.ISB_CLICK | BS.ISB_LIGHT;
                                        x.Color = "^7";
                                    }
                                    else
                                    {
                                        x.Style = BS.ISB_CLICK | BS.ISB_DARK;
                                        x.Color = "^8";
                                    }

                                    #endregion
                                }
                                else
                                {
                                    #region Enabled

                                    if (x.IsEnabled)
                                    {
                                        x.Style = BS.ISB_DARK;

                                        #region Special Logic

                                        #region Pit Limiter Logic

                                        // Pit Limiter Logic
                                        if (x.Name == PITLANE_LIMITER)
                                            if (_vars.vehicleDashFlag.HasFlag(DashLightFlags.DL_PITSPEED))
                                                x.IsVisible = true;
                                            else
                                                x.IsVisible = false;

                                        #endregion

                                        #region Blinker Logic

                                        if (x.Name == BLINKER_LEFT || x.Name == BLINKER_RIGHT)
                                            x.Style = BS.ISB_DARK | ButtonStyles.ISB_C4;

                                        #endregion

                                        #endregion
                                    }

                                    #endregion
                                }

                                #region Button Body

                                switch (x.Name)
                                {
                                    case TRACK:
                                        x.Body = _vars.TrackName;
                                        break;

                                    case AVG_FUEL:

                                        #region Average Fuel
                                        
                                        var value = "";
                                        var valueType = "L/100 km";

                                        value = $"{conn.TotalAvgFuel:F2}";

                                        if (_vars.SpeedoUnit == "mph")
                                            valueType = "G/100 mi";

                                        string avgFuelTitleText = "Fuel Consumption";
                                        if (_btn.Size(AVG_FUEL).Width < 19)
                                            avgFuelTitleText = "Fuel Cons.";

                                        x.Title = avgFuelTitleText;
                                        x.Body = $"{value} {valueType}";
                                        x.Color = "^7";

                                        if (_vars.vehicleRpm == 0)
                                        {
                                            x.Body = "--";
                                            x.Color = "^8";
                                        }

                                        #endregion

                                        break;

                                    case PITLANE_LIMITER:

                                        #region Pitlane Limiter

                                        x.Body = "^2ACTIVE";

                                        #endregion

                                        break;

                                    case LAP_SPLIT:

                                        #region Split

                                        TimeSpan splitTimeObj = _lapManager.Gui.getLastSplit();
                                        var splitValue =
                                            await _stringFormatter.getRaceTime(splitTimeObj.TotalMilliseconds);

                                        var splitType = $" {_lapManager.Gui.getLastSplitType()}";

                                        if (splitType.Contains('0'))
                                            splitType = $"";

                                        if (splitValue == "--")
                                            x.Color = "^8";
                                        else
                                            x.Color = "^7";

                                        x.Title = $"Split {splitType}";
                                        x.Body = $"{splitValue}";

                                        #endregion

                                        break;

                                    case LAP_TIME:

                                        #region LapTime

                                        var lapValue = _lapManager.Gui.getLaptime();
                                        var lapText = await _stringFormatter.getRaceTime(lapValue.TotalMilliseconds);

                                        if (lapText == "--")
                                            x.Color = "^8";
                                        else
                                            x.Color = "^7";

                                        x.Title = $"LapTime";
                                        x.Body = $"{lapText}";

                                        #endregion

                                        break;

                                    case BEST_PB:

                                        #region Best PB

                                        var pb = _lapManager.Gui.getBestPb();
                                        var humanPb = "--";

                                        if (pb > 0)
                                            humanPb = await _stringFormatter.getRaceTime(pb);

                                        if (humanPb == "--")
                                            x.Color = "^8";
                                        else
                                            x.Color = "^7";

                                        x.Title = "Best PB";
                                        x.Body = $"{humanPb}";

                                        #endregion

                                        break;

                                    case THEORETICAL_PB:

                                        #region Theoretical PB

                                        var predictedPb = _lapManager.Gui.getTheoreticalPb();
                                        var humanTpb = "--";

                                        if (predictedPb > 0)
                                            humanTpb = await _stringFormatter.getRaceTime(predictedPb);

                                        x.Title = "Theoretical PB";

                                        if (_btn.Size(x.Name).Width < 15)
                                            x.Title = "TPB";

                                        if (humanTpb == "--")
                                            x.Color = "^8";
                                        else
                                            x.Color = "^7";

                                        x.Body = $"{humanTpb}";

                                        #endregion

                                        break;

                                    case FUEL:

                                        #region Fuel

                                        string fuelSeperatorColor;
                                        string fuelColor;
                                        string fuelValue = string.Empty;

                                        if (_vars.InCustomizeUI)
                                        {
                                            fuelColor = "";
                                            fuelSeperatorColor = "";
                                        }
                                        else
                                        {
                                            fuelColor = "^7";
                                            fuelSeperatorColor = "^3";
                                        }

                                        if (_vars.FuelUnit == "litres")
                                        {
                                            var totalFuel = String.Empty;
                                            _vars.FuelUnitShort = "L";

                                            if (_vars.TotalFuel)
                                                totalFuel =
                                                    $" {fuelSeperatorColor}/ {fuelColor}{await _stringFormatter.toDoubleDigits(_vars.vehicleTankSize)} {_vars.FuelUnitShort}";

                                            fuelValue =
                                                $"{await _stringFormatter.toDoubleDigits(_vars.vehicleFuelLitres)} {_vars.FuelUnitShort}{totalFuel}";

                                            // Fuel warning
                                            if (_vars.vehicleFuelLitres < _vars.FuelWarningValue)
                                                _vars.FuelStrobeActive = true;
                                            else
                                                _vars.FuelStrobeActive = false;
                                        }
                                        else if (_vars.FuelUnit == "percent")
                                        {
                                            var totalFuel = string.Empty;
                                            _vars.FuelUnitShort = "%";

                                            if (_vars.TotalFuel)
                                                totalFuel = $" {fuelSeperatorColor}/ {fuelColor}100%";

                                            fuelValue =
                                                $"{await _stringFormatter.toDoubleDigits(_vars.vehicleFuelPercent)}{_vars.FuelUnitShort}{totalFuel}";

                                            // Fuel warning
                                            if (_vars.vehicleFuelPercent < _vars.FuelWarningValue)
                                                _vars.FuelStrobeActive = true;
                                            else
                                                _vars.FuelStrobeActive = false;
                                        }

                                        x.Title = "Fuel";
                                        x.Body = fuelValue;

                                        #endregion

                                        break;

                                    case DATETIME:

                                        #region DateTime

                                        var formatMappings = new Dictionary<DateTimeFormatEnum, string>
                                        {
                                            { DateTimeFormatEnum.DATETIME, "g" },
                                            { DateTimeFormatEnum.DATE, "d" },
                                            { DateTimeFormatEnum.TIME, "t" },
                                            { DateTimeFormatEnum.LONG_DATE, "ddd d MMM yyy" },
                                            { DateTimeFormatEnum.LONG_DATE_TIME, "ddd d MMM yyy HH:mm" }
                                        };

                                        x.Body = DateTime.Now.ToString(formatMappings[_vars.DateTimeFormat]);

                                        #endregion

                                        break;

                                    case RPM:

                                        #region Engine Type

                                        string EngineType = "RPM";
                                        var rpmClassicText =
                                            $"{await _stringFormatter.toSingleDigit(_vars.vehicleRpm / 1000)} K";
                                        if (_vars.vehicleShiftType == ShiftTypes.ELECTRIC)
                                        {
                                            EngineType = "EV";
                                            rpmClassicText =
                                                $"{await _stringFormatter.toSingleDigit((_vars.VehicleThrottle * 3))} Kw";
                                        }

                                        #endregion

                                        #region MyRegion

                                        x.Color = "^7";

                                        x.Title = EngineType;

                                        if (_vars.RpmStyle == "classic")
                                        {
                                            x.Body = rpmClassicText;

                                            if (_vars.vehicleShiftType != ShiftTypes.ELECTRIC)
                                                x.Body = rpmClassicText;
                                        }
                                        else
                                            x.Body = _im.UpdateRpmOverlay(_vars.vehicleRpm);

                                        #region Color

                                        // Idle mode
                                        if (_vars.vehicleRpm == 0)
                                        {
                                            x.Body = "--";
                                            x.Color = "^8";
                                        }

                                        // Max RPM
                                        if (_vars.vehicleRpm > _vars.MaxRpm)
                                            x.Color = "^1";
                                        else
                                            x.Color = "^7";

                                        #endregion

                                        #endregion

                                        break;

                                    case SPEED:

                                        #region Speed

                                        var buttonSpeed = await _stringFormatter.toNumber(_vars.vehicleKmh);
                                        ;

                                        if (_vars.SpeedoUnit == "mph")
                                            buttonSpeed = await _stringFormatter.toNumber(_vars.vehicleMph);

                                        if (_vars.vehicleRpm == 0)
                                        {
                                            x.Color = "^8";
                                            buttonSpeed = $"--";
                                        }
                                        else
                                        {
                                            x.Color = "^7";
                                        }

                                        x.Title = $"Speed ^8({_vars.SpeedoUnit.ToUpper()})";

                                        if (_btn.Size(x.Name).Width < 11)
                                            x.Title = $"{_vars.SpeedoUnit.ToUpper()}";

                                        x.Body = buttonSpeed;

                                        #endregion

                                        break;

                                    case GEAR:

                                        #region Gear

                                        string buttonGear;

                                        if (_vars.vehicleRpm > 10 || _vars.vehicleShiftType == ShiftTypes.ELECTRIC)
                                        {
                                            buttonGear = _vars.vehicleGear;
                                            x.Color = "^7";
                                        }
                                        else
                                        {
                                            buttonGear = "---";
                                            x.Color = "^8";
                                        }

                                        x.Body = buttonGear;

                                        #endregion

                                        break;

                                    case BLINKER_LEFT:
                                        x.Body = "‹";

                                        x.Color = "^9";

                                        if (_vars.vehicleDashFlag.HasFlag(DashLightFlags.DL_SIGNAL_L))
                                            x.IsVisible = true;
                                        else
                                            x.IsVisible = false;
                                        break;

                                    case BLINKER_RIGHT:
                                        x.Body = "›";

                                        x.Color = "^9";

                                        if (_vars.vehicleDashFlag.HasFlag(DashLightFlags.DL_SIGNAL_R))
                                            x.IsVisible = true;
                                        else
                                            x.IsVisible = false;
                                        break;

                                    case TURBO:
                                        x.Body = $"{_vars.vehicleTurbo:F1}";
                                        break;

                                    case RACE_POSITIONS:

                                        #region Body

                                        if (x.IsEnabled && !_vars.guiBusy && _vars.DisplayGui)
                                            if (!_vars.TrackName.Contains('X') && !_vars.TrackName.Contains('Y'))
                                                await _window.RacePositions(OPEN);
                                        #endregion

                                        break;

                                    default:
                                        x.Body = "[body]";
                                        break;
                                }

                                #endregion

                                if (_vars.InSettings || _vars.InMessages || _vars.InCustomizeHelp ||
                                    _vars.InImportTemplate || _vars.InManageTemplate || _vars.InTemplatesOverview ||
                                    _vars.InLaptimes)
                                {
                                    // Hide on-screen buttons
                                    await _im.ClearBtnRange(11, 48);
                                    _vars.guiBusy = true;
                                }
                                else
                                {
                                    _vars.guiBusy = false;

                                    #region Display Widgets

                                    if (!_vars.DisplayGui)
                                        continue;

                                    if (_vars.InCustomizeUI)
                                    {
                                        // Always display the buttons in CustomizeUI

                                        #region Race Positions

                                        if (x.Name == RACE_POSITIONS)
                                        {
                                            await _im.CreateBTN(x.Style, _btn.Size(x.Name).Height,
                                                _btn.Size(x.Name).Width,
                                                _btn.Location(x.Name).Left,
                                                _btn.Location(x.Name).Top,
                                                Convert.ToByte(_btn.getTitleClickId(x.Name)),
                                                $"{_btn.Get(x.Name).TitleColor}{_btn.Get(x.Name).SelectedColor}{x.Title}");
                                            continue;
                                        }

                                        #endregion

                                        #region Buttons

                                        await _im.CreateBTN(x.Style, 4,
                                            _btn.Size(x.Name).Width,
                                            _btn.Location(x.Name).Left,
                                            _btn.Location(x.Name).Top,
                                            Convert.ToByte(_btn.getTitleClickId(x.Name)),
                                            $"{_btn.Get(x.Name).TitleColor}{_btn.Get(x.Name).SelectedColor}{x.Title}");

                                        await _im.CreateBTN(x.Style,
                                            _btn.Size(x.Name).Height, _btn.Size(x.Name).Width,
                                            _btn.Location(x.Name).Left,
                                            Convert.ToByte(_btn.Location(x.Name).Top + 4),
                                            _btn.Get(x.Name).PrimaryClickID,
                                            $"{_btn.Get(x.Name).Color}{_btn.Get(x.Name).SelectedColor}{x.Body}");

                                        #endregion
                                    }
                                    else
                                    {
                                        if (x is { IsEnabled: true, IsVisible: true })
                                        {
                                            #region Race Positions

                                            if (x.Name == RACE_POSITIONS)
                                            {
                                                await _im.CreateBTN(x.Style, _btn.Size(x.Name).Height,
                                                    _btn.Size(x.Name).Width,
                                                    _btn.Location(x.Name).Left,
                                                    _btn.Location(x.Name).Top,
                                                    Convert.ToByte(_btn.getTitleClickId(x.Name)),
                                                    $"{_btn.Get(x.Name).TitleColor}{_btn.Get(x.Name).SelectedColor}{x.Title}");
                                                continue;
                                            }

                                            #endregion

                                            #region Buttons

                                            await _im.CreateBTN(x.Style, 4,
                                                _btn.Size(x.Name).Width,
                                                _btn.Location(x.Name).Left,
                                                _btn.Location(x.Name).Top,
                                                Convert.ToByte(_btn.getTitleClickId(x.Name)),
                                                $"{_btn.Get(x.Name).TitleColor}{_btn.Get(x.Name).SelectedColor}{x.Title}");

                                            await _im.CreateBTN(x.Style,
                                                _btn.Size(x.Name).Height,
                                                _btn.Size(x.Name).Width,
                                                _btn.Location(x.Name).Left,
                                                Convert.ToByte(_btn.Location(x.Name).Top + 4),
                                                _btn.Get(x.Name).PrimaryClickID,
                                                $"{_btn.Get(x.Name).Color}{_btn.Get(x.Name).SelectedColor}{x.Body}");

                                            #endregion
                                        }
                                        else
                                        {
                                            #region Hide disabled buttons

                                            var primaryClickId = _btn.Get(x.Name).PrimaryClickID;
                                            await _im.ClearBtn(primaryClickId);

                                            var secondaryClickId = _btn.getTitleClickId(x.Name);
                                            await _im.ClearBtn(secondaryClickId.Value);

                                            if (x.Name == RACE_POSITIONS)
                                                await _im.ClearBtnRange(80, 150);

                                            #endregion
                                        }
                                    }

                                    #endregion
                                }
                            }

                            #endregion

                            #region Get Button Size

                            // await _im.SetButtonSizeOld();
                            await _im.getButtonSize();

                            #endregion

                            #region ' Settings Paginator Logic '

                            if (_vars.SettingsPageNumber == _vars.SettingsFirstPage)
                            {
                                _vars.BSSettingsPagePrevious = BS.ISB_DARK;
                                _vars.BSSettingsPageNext = BS.ISB_CLICK | BS.ISB_LIGHT;

                                _vars.ColorSettingsPagePrevious = "^8";
                                _vars.ColorSettingsPageNext = "^7";
                            }
                            else if (_vars.SettingsPageNumber == _vars.SettingsLastPage)
                            {
                                _vars.BSSettingsPagePrevious = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSettingsPageNext = BS.ISB_DARK;
                                _vars.ColorSettingsPagePrevious = "^7";
                                _vars.ColorSettingsPageNext = "^8";
                            }
                            else
                            {
                                _vars.BSSettingsPagePrevious = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSettingsPageNext = BS.ISB_CLICK | BS.ISB_LIGHT;

                                _vars.ColorSettingsPagePrevious = "^7";
                                _vars.ColorSettingsPageNext = "^7";
                            }

                            #endregion

                            #region ' Customize UI Logic '

                            #region Arrows

                            // Clickable if there's a selected_id set
                            if (_vars.SelectedClickIDs.Count == 0)
                            {
                                _vars.BSArrowsCustomize = BS.ISB_DARK;
                                _vars.ColorCustomizeArrows = "^8";
                            }
                            else
                            {
                                _vars.BSArrowsCustomize = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.ColorCustomizeArrows = "^7";
                            }

                            #endregion

                            #endregion

                            #region Internal Settings Logic

                            #region ' Page 1 '

                            #region ' Menu Display Logic '

                            if (_vars.SettingsMenuDisplay == "parked")
                            {
                                _vars.BSSettingsMenuDisplayParked = BS.ISB_DARK;
                                _vars.BSSettingsMenuDisplayAlways = BS.ISB_CLICK | BS.ISB_LIGHT;

                                _vars.ColorDisplayParked = "^8";
                                _vars.ColorDisplayAlways = "^7";
                            }
                            else if (_vars.SettingsMenuDisplay == "always")
                            {
                                _vars.BSSettingsMenuDisplayParked = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSettingsMenuDisplayAlways = BS.ISB_DARK;

                                _vars.ColorDisplayParked = "^7";
                                _vars.ColorDisplayAlways = "^8";
                            }

                            #endregion

                            #region ' Speedo Logic '

                            if (_vars.SpeedoUnit == "kmh")
                            {
                                _vars.BSSettingsSpeedUnitKmh = BS.ISB_DARK;
                                _vars.BSSettingsSpeedUnitMph = BS.ISB_CLICK | BS.ISB_LIGHT;

                                _vars.ColorSpeedUnitKmh = "^8";
                                _vars.ColorSpeedUnitMph = "^7";
                            }
                            else if (_vars.SpeedoUnit == "mph")
                            {
                                _vars.BSSettingsSpeedUnitKmh = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSettingsSpeedUnitMph = BS.ISB_DARK;

                                _vars.ColorSpeedUnitKmh = "^7";
                                _vars.ColorSpeedUnitMph = "^8";
                            }

                            #endregion

                            #region ' Fuel Logic '

                            if (_vars.FuelUnit == "litres")
                            {
                                _vars.BSSettingsFuelUnitLitres = BS.ISB_DARK;
                                _vars.BSSettingsFuelUnitPercent = BS.ISB_CLICK | BS.ISB_LIGHT;

                                _vars.ColorFuelUnitLitres = "^8";
                                _vars.ColorFuelUnitPercent = "^7";
                            }
                            else if (_vars.FuelUnit == "percent")
                            {
                                _vars.BSSettingsFuelUnitLitres = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSettingsFuelUnitPercent = BS.ISB_DARK;

                                _vars.ColorFuelUnitLitres = "^7";
                                _vars.ColorFuelUnitPercent = "^8";
                            }

                            #endregion

                            #region ' Total Fuel Logic '

                            if (_vars.TotalFuel == true)
                            {
                                _vars.BSSettingsTotalFuelON = BS.ISB_DARK;
                                _vars.BSSettingsTotalFuelOFF = BS.ISB_CLICK | BS.ISB_LIGHT;

                                _vars.ColorTotalFuelON = "^8";
                                _vars.ColorTotalFuelOFF = "^7";
                            }
                            else
                            {
                                _vars.BSSettingsTotalFuelON = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSettingsTotalFuelOFF = BS.ISB_DARK;

                                _vars.ColorTotalFuelON = "^7";
                                _vars.ColorTotalFuelOFF = "^8";
                            }

                            #endregion

                            #region ' Gear Format Logic '

                            if (_vars.GearFormat == "classic")
                            {
                                _vars.BSSettingsGearFormatClassic = BS.ISB_DARK;
                                _vars.BSSettingsGearFormatModern = BS.ISB_CLICK | BS.ISB_LIGHT;

                                _vars.ColorGearFormatClassic = "^8";
                                _vars.ColorGearFormatModern = "^7";
                            }
                            else if (_vars.GearFormat == "modern")
                            {
                                _vars.BSSettingsGearFormatClassic = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSettingsGearFormatModern = BS.ISB_DARK;

                                _vars.ColorGearFormatClassic = "^7";
                                _vars.ColorGearFormatModern = "^8";
                            }

                            #endregion

                            #region ' Rpm Style Logic '

                            if (_vars.RpmStyle == "classic")
                            {
                                _vars.BSSettingsRpmStyleClassic = BS.ISB_DARK;
                                _vars.BSSettingsRpmStyleModern = BS.ISB_CLICK | BS.ISB_LIGHT;

                                _vars.ColorRpmStyleClassic = "^8";
                                _vars.ColorRpmStyleModern = "^7";
                            }
                            else if (_vars.RpmStyle == "modern")
                            {
                                _vars.BSSettingsRpmStyleClassic = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSettingsRpmStyleModern = BS.ISB_DARK;

                                _vars.ColorRpmStyleClassic = "^7";
                                _vars.ColorRpmStyleModern = "^8";
                            }

                            #endregion

                            #region ' Greeting Logic '

                            if (_vars.GreetingsEnabled == true)
                            {
                                _vars.BSSettingsGreetingMessageOn = BS.ISB_DARK;
                                _vars.BSSettingsGreetingMessageOff = BS.ISB_CLICK | BS.ISB_LIGHT;

                                _vars.ColorGreetingMessageOn = "^8";
                                _vars.ColorGreetingMessageOff = "^7";
                            }
                            else
                            {
                                _vars.BSSettingsGreetingMessageOn = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSettingsGreetingMessageOff = BS.ISB_DARK;

                                _vars.ColorGreetingMessageOn = "^7";
                                _vars.ColorGreetingMessageOff = "^8";
                            }

                            #endregion

                            #endregion

                            #region ' Page 2 '

                            #region ' Autolights Logic '

                            if (_vars.AutoLightsEnabled == true)
                            {
                                _vars.BSSettingsAutoLightsOff = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSettingsAutoLightsOn = BS.ISB_DARK;

                                _vars.ColorAutoLightsOff = "^7";
                                _vars.ColorAutoLightsOn = "^8";
                            }
                            else if (_vars.AutoLightsEnabled == false)
                            {
                                _vars.BSSettingsAutoLightsOff = BS.ISB_DARK;
                                _vars.BSSettingsAutoLightsOn = BS.ISB_CLICK | BS.ISB_LIGHT;

                                _vars.ColorAutoLightsOff = "^8";
                                _vars.ColorAutoLightsOn = "^7";
                            }

                            #endregion

                            #region ' LaptimeMessages Logic '

                            if (_vars.RacingMessagesEnabled == true)
                            {
                                _vars.BSSettingsLaptimeMessagesOff = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSettingsLaptimeMessagesOn = BS.ISB_DARK;

                                _vars.ColorLaptimeMessagesOff = "^7";
                                _vars.ColorLaptimeMessagesOn = "^8";
                            }
                            else if (_vars.RacingMessagesEnabled == false)
                            {
                                _vars.BSSettingsLaptimeMessagesOff = BS.ISB_DARK;
                                _vars.BSSettingsLaptimeMessagesOn = BS.ISB_CLICK | BS.ISB_LIGHT;

                                _vars.ColorLaptimeMessagesOff = "^8";
                                _vars.ColorLaptimeMessagesOn = "^7";
                            }

                            #endregion

                            #endregion

                            #region ' Page 3 '

                            #region ' Console Chat Logic '

                            if (_vars.ConsoleChat == true)
                            {
                                _vars.BSSettingsConsoleChatOn = BS.ISB_DARK;
                                _vars.BSSettingsConsoleChatOff = BS.ISB_CLICK | BS.ISB_LIGHT;

                                _vars.ColorConsoleChatOn = "^8";
                                _vars.ColorConsoleChatOff = "^7";
                            }
                            else if (_vars.ConsoleChat == false)
                            {
                                _vars.BSSettingsConsoleChatOn = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSettingsConsoleChatOff = BS.ISB_DARK;

                                _vars.ColorConsoleChatOn = "^7";
                                _vars.ColorConsoleChatOff = "^8";
                            }

                            #endregion

                            #region ' Automatic Updates Logic '

                            // Automatic updates are removed due to open source

                            #endregion

                            #endregion

                            #region ' Page 4 '

                            #region ' Module: Car Contact '

                            if (_vars.CarContactEnabled)
                            {
                                _vars.BSSettingsCarContactOff = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSettingsCarContactOn = BS.ISB_DARK;

                                _vars.ColorCarContactOff = "^7";
                                _vars.ColorCarContactOn = "^8";
                            }
                            else
                            {
                                _vars.BSSettingsCarContactOff = BS.ISB_DARK;
                                _vars.BSSettingsCarContactOn = BS.ISB_CLICK | BS.ISB_LIGHT;

                                _vars.ColorCarContactOff = "^8";
                                _vars.ColorCarContactOn = "^7";
                            }

                            #endregion

                            #endregion

                            #endregion

                            #region Sidepanel Buttons Logic

                            if (_vars.InCustomizeUI)
                            {
                                _btn.Get(SIDEMENU_CUSTOMIZE_UI).Style = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSettings = BS.ISB_DARK;
                                _vars.BSMessages = BS.ISB_DARK;
                                _vars.BSSidemenuLaptimes = BS.ISB_DARK;
                                _btn.Get(SIDEMENU_CUSTOMIZE_UI).Color = "^7";
                                _vars.ColorSettings = "^8";
                                _vars.ColorMessages = "^8";

                                if (_vars.SelectedClickIDs.Contains(_btn.Get(SIDEMENU_CUSTOMIZE_UI).PrimaryClickID))
                                    _btn.Get(SIDEMENU_CUSTOMIZE_UI).Body = "[edit mode]";
                                else
                                    _btn.Get(SIDEMENU_CUSTOMIZE_UI).Body = "[click to edit]";
                            }
                            else if (_vars.InSettings)
                            {
                                _btn.Get(SIDEMENU_CUSTOMIZE_UI).Body = "Customize UI";
                                _btn.Get(SIDEMENU_CUSTOMIZE_UI).Style = BS.ISB_DARK;
                                _vars.BSSettings = BS.ISB_DARK;
                                _vars.BSMessages = BS.ISB_DARK;
                                _vars.BSSidemenuLaptimes = BS.ISB_DARK;
                                _btn.Get(SIDEMENU_CUSTOMIZE_UI).Color = "^8";
                                _vars.ColorSettings = "^8";
                                _vars.ColorMessages = "^8";
                            }
                            else if (_vars.InMessages)
                            {
                                _btn.Get(SIDEMENU_CUSTOMIZE_UI).Body = "Customize UI";
                                _btn.Get(SIDEMENU_CUSTOMIZE_UI).Style = BS.ISB_DARK;
                                _vars.BSSettings = BS.ISB_DARK;
                                _vars.BSMessages = BS.ISB_DARK;
                                _vars.BSSidemenuLaptimes = BS.ISB_DARK;
                                _btn.Get(SIDEMENU_CUSTOMIZE_UI).Color = "^8";
                                _vars.ColorSettings = "^8";
                                _vars.ColorMessages = "^8";
                            }
                            else if (_vars.InTemplatesOverview || _vars.InManageTemplate || _vars.InAddTemplate ||
                                     _vars.InImportTemplate || _vars.InLaptimes)
                            {
                                _btn.Get(SIDEMENU_CUSTOMIZE_UI).Body = "Customize UI";

                                _btn.Get(SIDEMENU_CUSTOMIZE_UI).Style = BS.ISB_DARK;
                                _vars.BSSettings = BS.ISB_DARK;
                                _vars.BSMessages = BS.ISB_DARK;
                                _vars.BSSidemenuLaptimes = BS.ISB_DARK;

                                _btn.Get(SIDEMENU_CUSTOMIZE_UI).Color = "^8";
                                _vars.ColorSettings = "^8";
                                _vars.ColorMessages = "^8";
                            }
                            else
                            {
                                _btn.Get(SIDEMENU_CUSTOMIZE_UI).Body = "Customize UI";
                                _btn.Get(SIDEMENU_CUSTOMIZE_UI).Style = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSettings = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSMessages = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _vars.BSSidemenuLaptimes = BS.ISB_CLICK | BS.ISB_LIGHT;
                                _btn.Get(SIDEMENU_CUSTOMIZE_UI).Color = "^7";
                                _vars.ColorSettings = "^7";
                                _vars.ColorMessages = "^7";
                            }


                            #endregion

                            if (_vars.InCustomizeUI)
                            {
                                #region Customize UI

                                #region ' Button Styles / Colors '

                                #region ' Move or Resize '

                                if (_vars.MoveCustomizerWindow == MoveCustomizerEnum.MOVE)
                                {
                                    _vars.BSCustomizeMove = BS.ISB_DARK;
                                    _vars.ActionMove_Sel = "^8";

                                    _vars.BSCustomizeResize = BS.ISB_DARK;
                                    _vars.ActionResize_Sel = "^8";
                                }
                                else
                                {
                                    switch (_vars.MoveOrResize)
                                    {
                                        case MoveOrResizeEnum.MOVE:
                                            _vars.BSCustomizeMove = BS.ISB_DARK;
                                            _vars.ActionMove_Sel = "^8";

                                            _vars.BSCustomizeResize = BS.ISB_CLICK | BS.ISB_LIGHT;
                                            _vars.ActionResize_Sel = "^7";
                                            break;

                                        case MoveOrResizeEnum.RESIZE:
                                            _vars.BSCustomizeResize = BS.ISB_DARK;
                                            _vars.ActionResize_Sel = "^8";

                                            _vars.BSCustomizeMove = BS.ISB_CLICK | BS.ISB_LIGHT;
                                            _vars.ActionMove_Sel = "^7";
                                            break;
                                    }
                                }


                                #endregion

                                #region ' Move Customizer Window '

                                if (_vars.SelectedClickIDs.Count == 0 ||
                                    _vars.MoveCustomizerWindow == MoveCustomizerEnum.MOVE)
                                {
                                    _vars.BSMoveCustomizerWindow = BS.ISB_LIGHT | BS.ISB_CLICK;

                                    if (_vars.MoveCustomizerWindow == MoveCustomizerEnum.MOVE)
                                        _vars.ColorMoveCustomizerWindow = "^2";
                                    else
                                        _vars.ColorMoveCustomizerWindow = "^7";

                                    #region ClickID Check

                                    if (_vars.SelectedClickIDs.Contains(_btn.Get(CUSTOMIZER_WINDOW).PrimaryClickID) ==
                                        false)
                                    {
                                        _vars.MoveCustomizerWindow = MoveCustomizerEnum.NONE;
                                    }

                                    #endregion

                                }
                                else
                                {
                                    _vars.BSMoveCustomizerWindow = BS.ISB_DARK;
                                    _vars.ColorMoveCustomizerWindow = "^8";
                                }


                                #endregion

                                #endregion

                                #region > Customize UI <

                                #region ' Position Variables '

                                // Background
                                var top = _btn.Location(CUSTOMIZER_WINDOW).Top;

                                byte backg_t = top;

                                // Customize User Interface (title)
                                byte title_t = Convert.ToByte(backg_t + 1);
                                byte selected_label = Convert.ToByte(title_t + 9);

                                // Arrows
                                byte arrow_upper = Convert.ToByte(top + 21);
                                byte arrow_lower = Convert.ToByte(top + 29);

                                // Move/Resize
                                byte moveResize_base = Convert.ToByte(top + 22);
                                byte moveResize_Resize = Convert.ToByte(moveResize_base + 8);

                                byte Help_Top = Convert.ToByte(top + 40);

                                // Bottom Bar
                                byte BottomBar = Convert.ToByte(moveResize_Resize + 9);

                                #endregion

                                #region ' Buttons '

                                #region ' Background & Title '

                                // Background
                                await _im.CreateBTN(BS.ISB_DARK, 47, 57, 72, backg_t, 51, "");

                                // Customize User Interface (title)
                                await _im.CreateBTN(BS.ISB_LIGHT, 8, 55, 73, title_t, 52,
                                    "^7" + "Customize User Interface");

                                #endregion

                                #region ' Selected: ## - Selected Mode: #### '

                                // Selected: ## - Selected Mode: ####
                                await _im.CreateBTN(BS.ISB_C2, 5, 48, 76, selected_label, 53,
                                    $"^7Selected ^3{_vars.SelectedClickIDs.Count} ^7buttons        " +
                                    $"^7Mode: ^3{_vars.MoveOrResize.GetDescription()}");

                                #endregion

                                #region ' Arrows '

                                // Arrows

                                byte[] uparrow = [94, 66, 226, 134, 145];

                                await _im.CreateBTN(_vars.BSArrowsCustomize, 8, 8, 97, arrow_upper, 54,
                                    $"{_vars.ColorCustomizeArrows}" + Encoding.UTF8.GetString(uparrow));
                                await _im.CreateBTN(_vars.BSArrowsCustomize, 8, 8, 89, arrow_lower, 55,
                                    $"{_vars.ColorCustomizeArrows}←");
                                await _im.CreateBTN(_vars.BSArrowsCustomize, 8, 8, 97, arrow_lower, 56,
                                    $"{_vars.ColorCustomizeArrows}↓");
                                await _im.CreateBTN(_vars.BSArrowsCustomize, 8, 8, 105, arrow_lower, 57,
                                    $"{_vars.ColorCustomizeArrows}→");

                                #endregion

                                #region ' Selected Height/Width '

                                await _im.CreateBTN(BS.ISB_C2, 5, 24, 89, BottomBar, 61,
                                    $"^7Width: ^3{_vars.SelectedSize_W}   ^7Height: ^3{_vars.SelectedSize_H}");

                                #endregion

                                #region ' Move / Resize '

                                await _im.CreateBTN(_vars.BSCustomizeMove, 6, 13, 74, moveResize_base, 58,
                                    $"{_vars.ActionMove_Sel}Move");
                                await _im.CreateBTN(_vars.BSCustomizeResize, 6, 13, 74, moveResize_Resize, 59,
                                    $"{_vars.ActionResize_Sel}Resize");

                                #endregion

                                #region ' Multiplier '

                                await _im.CreateBTN(BS.ISB_CLICK | BS.ISB_LIGHT, 5, 16, 111, moveResize_base, 60,
                                    "^7" + $"Multiplier: ^3{_vars.CustomizeMultiplier}X");

                                #endregion

                                #region ' Click to move '

                                await _im.CreateBTN(_vars.BSMoveCustomizerWindow, 5, 15, 74, BottomBar,
                                    _btn.Get(CUSTOMIZER_WINDOW).PrimaryClickID,
                                    $"{_vars.ColorMoveCustomizerWindow}{_vars.MoveCustomizerWindow.GetDescription()}");

                                #endregion

                                #region ' Help '

                                await _im.CreateBTN(BS.ISB_CLICK | BS.ISB_LIGHT, 5, 5, 122, Help_Top, 63,
                                    "^7" + $"?");

                                #endregion

                                #region ' Templates '

                                await _im.CreateBTN(BS.ISB_CLICK | BS.ISB_LIGHT, 5, 16, 111,
                                    Convert.ToByte(moveResize_base - 5), 64,
                                    "^7" + $"Templates");

                                #endregion

                                #region ' Save '

                                await _im.CreateBTN(BS.ISB_CLICK, 8, 8, 120, title_t, 200,
                                    $"{_vars.CloseSymbolColor}{_vars.CloseSymbol}");

                                #endregion

                                #endregion

                                #endregion

                                #endregion
                            }
                            else
                            {
                                #region Customize Help

                                if (_vars.InCustomizeHelp)
                                {
                                    #region ' Position Variables '

                                    // Background
                                    byte backg_t = _btn.Location(CUSTOMIZER_WINDOW).Top;

                                    // Customize User Interface (title)
                                    byte title_t = Convert.ToByte(backg_t + 1);

                                    byte Content_base_t = Convert.ToByte(title_t + 9);

                                    byte Shortcuts_spacing = 6;

                                    byte Shortcuts_base_t = Convert.ToByte(Content_base_t + 7);
                                    byte Shortcuts_t = Convert.ToByte(Shortcuts_base_t + Shortcuts_spacing);

                                    #endregion

                                    #region ' Background & Title '

                                    // Background
                                    await _im.CreateBTN(BS.ISB_DARK, 70, 57, 72, backg_t, 51, "");

                                    // Customize User Interface (title)
                                    await _im.CreateBTN(BS.ISB_LIGHT, 8, 55, 73, title_t, 52,
                                        "^7" + "Help/Keyboard Shortcuts");

                                    #endregion

                                    #region Tip

                                    await _im.CreateBTN(BS.ISB_C2, 4, 55, 73, Content_base_t, 53,
                                        "^6Tip: ^7You can move/resize multiple buttons at the same time");

                                    #endregion

                                    #region Shortcuts

                                    await _im.CreateBTN(BS.ISB_LEFT, 6, 55, 73, Shortcuts_base_t, 54, "^7Shortcuts");

                                    // List all shortcuts
                                    await _im.CreateBTN(BS.ISB_LEFT, 5, 54, 74, Shortcuts_t, 55,
                                        "^3" + "C ^7- Deselect all buttons");

                                    await _im.CreateBTN(BS.ISB_LEFT, 5, 54, 74,
                                        Convert.ToByte(Shortcuts_t + Shortcuts_spacing), 56,
                                        "^3" + "Page Down ^7- Toggle strobe (visual)");

                                    await _im.CreateBTN(BS.ISB_LEFT, 5, 54, 74,
                                        Convert.ToByte(Shortcuts_t + Shortcuts_spacing * 2), 57,
                                        "^3" + "Page Up ^7- Toggle siren (audible)");

                                    #region Arrows

                                    await _im.CreateBTN(BS.ISB_LEFT, 5, 54, 74,
                                        Convert.ToByte(Shortcuts_t + Shortcuts_spacing * 3), 58,
                                        "^3" + "Arrow Up ^7- Decrease position or decrease height of a button");

                                    await _im.CreateBTN(BS.ISB_LEFT, 5, 54, 74,
                                        Convert.ToByte(Shortcuts_t + Shortcuts_spacing * 4), 59,
                                        "^3" + "Arrow Down ^7- Increase position or increase height of a button");

                                    await _im.CreateBTN(BS.ISB_LEFT, 5, 54, 74,
                                        Convert.ToByte(Shortcuts_t + Shortcuts_spacing * 5), 60,
                                        "^3" + "Arrow Left ^7- Decrease position or decrease height of a button");

                                    await _im.CreateBTN(BS.ISB_LEFT, 5, 54, 74,
                                        Convert.ToByte(Shortcuts_t + Shortcuts_spacing * 6), 61,
                                        "^3" + "Arrow Right ^7- Increase position or Increase height of a button");

                                    #endregion

                                    #endregion

                                    #region Clear Un-used Buttons

                                    await _im.ClearBtnRange(62, 64);

                                    #endregion

                                    #region ' Save '

                                    await _im.CreateBTN(BS.ISB_CLICK, 8, 8, 73, title_t, 200,
                                        $"{_vars.CloseSymbolColor}«");

                                    #endregion
                                }

                                #endregion

                                #region Settings

                                if (_vars.InSettings == true)
                                {
                                    #region ' Variables '

                                    byte SubTitle_T = 39;
                                    byte setting1_T = Convert.ToByte(SubTitle_T + 8);
                                    byte setting2_T = Convert.ToByte(setting1_T + 6);
                                    byte setting3_T = Convert.ToByte(setting2_T + 6);
                                    byte setting4_T = Convert.ToByte(setting3_T + 6);
                                    byte setting5_T = Convert.ToByte(setting4_T + 6);
                                    byte setting4_T2 = Convert.ToByte(setting4_T + 6);
                                    byte setting6_T = Convert.ToByte(setting5_T + 6);
                                    byte hint1_T = Convert.ToByte(setting4_T2 + 6);
                                    byte hint2_T = Convert.ToByte(hint1_T + 4);

                                    byte setting1_L = 55;
                                    byte setting2_L = 79;
                                    byte setting3_L = 89;

                                    byte setting4_L = 101;
                                    byte setting5_L = 124;
                                    byte setting6_L = 134;

                                    // Window Properties
                                    byte window_width = 92;
                                    byte window_left = 54;

                                    byte title_t = 29;

                                    byte title_width = Convert.ToByte(window_width - 2);
                                    byte title_left = Convert.ToByte(window_left + 1);


                                    #endregion

                                    #region ' Global Buttons '

                                    await _im.CreateBTN(BS.ISB_DARK, 64, window_width, window_left, 28, 51, "");
                                    await _im.CreateBTN(BS.ISB_LIGHT, 8, title_width, title_left, title_t, 52,
                                        "^7Settings");
                                    await _im.CreateBTN(BS.ISB_RIGHT, 4, title_width, title_left,
                                        Convert.ToByte(title_t + 9), 81, $"v{_vars.Version}{_vars.DevBuildString}");

                                    #endregion

                                    #region ' Pages '

                                    #region InSim Settings - Page 1

                                    if (_vars.SettingsPageNumber == 1)
                                    {
                                        // Subtitle
                                        await _im.CreateBTN(BS.ISB_C2, 6, 66, 67, SubTitle_T, 53,
                                            "^7InSim Settings");

                                        #region ' Menu Display '

                                        // Menu Display Label
                                        await _im.CreateBTN(BS.ISB_LEFT, 5, 15, setting4_L, setting1_T, 54,
                                            "^7Menu display");

                                        // Parked Setting
                                        await _im.CreateBTN(_vars.BSSettingsMenuDisplayParked, 5, 10, setting5_L,
                                            setting1_T, 55,
                                            $"{_vars.ColorDisplayParked}Parked");

                                        // Always Setting
                                        await _im.CreateBTN(_vars.BSSettingsMenuDisplayAlways, 5, 10, setting6_L,
                                            setting1_T, 56,
                                            $"{_vars.ColorDisplayAlways}Always");

                                        #endregion

                                        #region ' Fuel Warning '

                                        await _im.CreateBTN(BS.ISB_LEFT, 5, 15, setting1_L, setting1_T, 57,
                                            "^7Fuel warning");

                                        var Fuel_Text = $"{_vars.FuelWarningValue.ToString("0.00", CultureInfo.InvariantCulture)} %";

                                        if (_vars.FuelWarningType == "litres")
                                            Fuel_Text = $"{_vars.FuelWarningValue.ToString("0.00", CultureInfo.InvariantCulture)} L";

                                        await _im.CreateBTT(BS.ISB_CLICK | BS.ISB_LIGHT, 5, 20, setting2_L, setting1_T,
                                            58,
                                            8, $"^3{Fuel_Text}",
                                            $"^0Fuel Warning (Percent or Litres)", true);

                                        #endregion

                                        #region ' Display Total Fuel '

                                        await _im.CreateBTN(BS.ISB_LEFT, 5, 23, setting1_L, setting2_T, 78,
                                            "^7Display total fuel");

                                        // Total Fuel ON
                                        await _im.CreateBTN(_vars.BSSettingsTotalFuelON, 5, 10, setting2_L, setting2_T,
                                            79,
                                            $"{_vars.ColorTotalFuelON}ON");

                                        // Total Fuel OFF
                                        await _im.CreateBTN(_vars.BSSettingsTotalFuelOFF, 5, 10, setting3_L, setting2_T,
                                            80,
                                            $"{_vars.ColorTotalFuelOFF}OFF");

                                        #endregion

                                        #region ' Rpm Style '

                                        // Rpm Style
                                        await _im.CreateBTN(BS.ISB_LEFT, 5, 15, setting4_L, setting4_T, 69,
                                            "^7Rpm style");

                                        // Classic Setting
                                        await _im.CreateBTN(_vars.BSSettingsRpmStyleClassic, 5, 10, setting5_L,
                                            setting4_T, 70,
                                            $"{_vars.ColorRpmStyleClassic}Classic");

                                        // Modern Setting
                                        await _im.CreateBTN(_vars.BSSettingsRpmStyleModern, 5, 10, setting6_L,
                                            setting4_T, 71,
                                            $"{_vars.ColorRpmStyleModern}Modern");

                                        #endregion

                                        #region ' Speedo Unit '

                                        // Speedometer unit label
                                        await _im.CreateBTN(BS.ISB_LEFT, 5, 15, setting4_L, setting3_T, 60,
                                            $"^7Speedo unit");

                                        // Kmh
                                        await _im.CreateBTN(_vars.BSSettingsSpeedUnitKmh, 5, 10, setting5_L, setting3_T,
                                            61,
                                            $"{_vars.ColorSpeedUnitKmh}Kmh");

                                        // Mph
                                        await _im.CreateBTN(_vars.BSSettingsSpeedUnitMph, 5, 10, setting6_L, setting3_T,
                                            62,
                                            $"{_vars.ColorSpeedUnitMph}Mph");

                                        #endregion

                                        #region ' Fuel Unit '

                                        // Menu Display Label
                                        await _im.CreateBTN(BS.ISB_LEFT, 5, 15, setting1_L, setting3_T, 63,
                                            "^7Fuel unit");

                                        // Parked Setting
                                        await _im.CreateBTN(_vars.BSSettingsFuelUnitLitres, 5, 10, setting2_L,
                                            setting3_T, 64,
                                            $"{_vars.ColorFuelUnitLitres}Litres");

                                        // Always Setting
                                        await _im.CreateBTN(_vars.BSSettingsFuelUnitPercent, 5, 10, setting3_L,
                                            setting3_T, 65,
                                            $"{_vars.ColorFuelUnitPercent}Percent");

                                        #endregion

                                        #region ' Gear Format '

                                        // Gear format label
                                        await _im.CreateBTN(BS.ISB_LEFT, 5, 15, setting4_L, setting2_T, 66,
                                            $"^7Gear format");

                                        // Gears: Classic
                                        await _im.CreateBTN(_vars.BSSettingsGearFormatClassic, 5, 10, setting5_L,
                                            setting2_T, 67,
                                            $"{_vars.ColorGearFormatClassic}Classic");

                                        // Gears: Modern
                                        await _im.CreateBTN(_vars.BSSettingsGearFormatModern, 5, 10, setting6_L,
                                            setting2_T, 68,
                                            $"{_vars.ColorGearFormatModern}Modern");

                                        #endregion

                                        #region ' Welcome Message '

                                        // Greeting Message
                                        await _im.CreateBTN(BS.ISB_LEFT, 5, 15, setting1_L, setting4_T, 72,
                                            "^7Greeting");


                                        await _im.CreateBTN(_vars.BSSettingsGreetingMessageOn, 5, 10, setting2_L,
                                            setting4_T, 73,
                                            $"{_vars.ColorGreetingMessageOn}ON");

                                        await _im.CreateBTN(_vars.BSSettingsGreetingMessageOff, 5, 10, setting3_L,
                                            setting4_T, 74,
                                            $"{_vars.ColorGreetingMessageOff}OFF");

                                        await _im.CreateBTT(BS.ISB_CLICK | BS.ISB_LIGHT, 5, 44, setting1_L, setting4_T2,
                                            75,
                                            64, $"^8{_vars.GreetingMessage}",
                                            $"^0Greeting Message",
                                            true);

                                        #region Format Hints

                                        await _im.CreateBTN(BS.ISB_LEFT, 4, 25, setting1_L, hint1_T, 76,
                                            $"^7" + "Player name  ^8%playername%");

                                        await _im.CreateBTN(BS.ISB_LEFT, 4, 25, setting1_L, hint2_T, 77,
                                            $"^7" + "User name     ^8%username%");

                                        #endregion


                                        #endregion
                                    }

                                    #endregion

                                    #region InSim Settings - Page 2

                                    else if (_vars.SettingsPageNumber == 2)
                                    {
                                        // Subtitle
                                        await _im.CreateBTN(BS.ISB_C2, 6, 66, 67, SubTitle_T, 53,
                                            "^7InSim Settings");

                                        #region ' Autolights '

                                        // Autolights label
                                        await _im.CreateBTN(BS.ISB_LEFT, 5, 23, setting1_L, setting1_T, 54,
                                            "^7Auto-toggle lights");

                                        // ON
                                        await _im.CreateBTN(_vars.BSSettingsAutoLightsOn, 5, 10, setting2_L, setting1_T,
                                            55,
                                            $"{_vars.ColorAutoLightsOn}ON");

                                        // OFF
                                        await _im.CreateBTN(_vars.BSSettingsAutoLightsOff, 5, 10, setting3_L,
                                            setting1_T, 56,
                                            $"{_vars.ColorAutoLightsOff}OFF");

                                        #endregion

                                        #region ' DateTime '

                                        // Autolights label
                                        await _im.CreateBTN(BS.ISB_LEFT, 5, 23, setting4_L, setting1_T, 57,
                                            "^7DateTime format");

                                        await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_CLICK, 5, 19, setting5_L, setting1_T,
                                            58,
                                            $"^3{_vars.DateTimeFormat.GetDescription()}");

                                        #endregion

                                        #region Laptime Messages

                                        await _im.CreateBTN(BS.ISB_LEFT, 5, 23, setting1_L, setting2_T, 60,
                                            "^7Racing Messages");

                                        // ON
                                        await _im.CreateBTN(_vars.BSSettingsLaptimeMessagesOn, 5, 10, setting2_L,
                                            setting2_T, 61,
                                            $"{_vars.ColorLaptimeMessagesOn}ON");

                                        // OFF
                                        await _im.CreateBTN(_vars.BSSettingsLaptimeMessagesOff, 5, 10, setting3_L,
                                            setting2_T, 62,
                                            $"{_vars.ColorLaptimeMessagesOff}OFF");

                                        #endregion

                                    }

                                    #endregion

                                    #region Application Settings - Page 3

                                    else if (_vars.SettingsPageNumber == 3)
                                    {
                                        await _im.CreateBTN(BS.ISB_C2, 6, 66, 67, SubTitle_T, 53,
                                            "^7Application Settings");

                                        #region ' Console Chat '

                                        // Menu Display Label
                                        await _im.CreateBTN(BS.ISB_LEFT, 5, 15, setting1_L, setting1_T, 54,
                                            "^7Console chat");

                                        // ON
                                        await _im.CreateBTN(_vars.BSSettingsConsoleChatOn, 5, 10, setting2_L,
                                            setting1_T, 55,
                                            $"{_vars.ColorConsoleChatOn}ON");

                                        // OFF
                                        await _im.CreateBTN(_vars.BSSettingsConsoleChatOff, 5, 10, setting3_L,
                                            setting1_T, 56,
                                            $"{_vars.ColorConsoleChatOff}OFF");

                                        #endregion

                                    }

                                    #endregion

                                    #region Modules - Page 4

                                    else if (_vars.SettingsPageNumber == 4)
                                    {
                                        await _im.CreateBTN(BS.ISB_C2, 6, 66, 67, SubTitle_T, 53, "^7Modules");

                                        #region ' Car Contact '

                                        await _im.CreateBTN(BS.ISB_LEFT, 5, 23, setting1_L, setting1_T, 78,
                                            "^7Car Contact");

                                        // Car Contact ON
                                        await _im.CreateBTN(_vars.BSSettingsCarContactOn, 5, 10, setting2_L, setting1_T,
                                            79,
                                            $"{_vars.ColorCarContactOn}ON");

                                        // Car Contact OFF
                                        await _im.CreateBTN(_vars.BSSettingsCarContactOff, 5, 10, setting3_L,
                                            setting1_T, 80,
                                            $"{_vars.ColorCarContactOff}OFF");

                                        #endregion

                                        #region Pubstat Key

                                        // Menu Display Label
                                        await _im.CreateBTN(BS.ISB_LEFT, 5, 15, setting1_L, setting2_T, 54,
                                            "^7Pubstat Key");

                                        // Parked Setting
                                        await _im.CreateBTT(BS.ISB_LEFT | BS.ISB_LIGHT | ButtonStyles.ISB_CLICK, 5, 62,
                                            setting2_L, setting2_T, 55, 64,
                                            $"^3{_vars.lfswPubstat}", $"^0Enter your lfsw pubstat key", true);

                                        // click ID 56 reserved

                                        #endregion
                                    }

                                    #endregion

                                    #endregion

                                    #region ' Paginator/Close Window '

                                    #region Paginator

                                    byte pageinator_base_h = 78;
                                    byte pageinator_label_h = Convert.ToByte(pageinator_base_h + 7);

                                    await _im.CreateBTN(_vars.BSSettingsPagePrevious, 6, 10, 90, pageinator_base_h, 197,
                                        $"{_vars.ColorSettingsPagePrevious}‹");
                                    await _im.CreateBTN(_vars.BSSettingsPageNext, 6, 10, 100, pageinator_base_h, 198,
                                        $"{_vars.ColorSettingsPageNext}›");

                                    await _im.CreateBTN(BS.ISB_C2, 5, 66, 67, pageinator_label_h, 199,
                                        $"^7Page ^3{_vars.SettingsPageNumber} ^7of ^3{_vars.SettingsLastPage}");

                                    #endregion

                                    #region Close Window

                                    await _im.CreateBTN(BS.ISB_CLICK, 8, 8, 137, title_t, 200,
                                        $"{_vars.CloseSymbolColor}{_vars.CloseSymbol}");

                                    #endregion

                                    #endregion
                                }

                                #endregion
                            }

                            #region Menu

                            if (_vars.DisplayGui)
                            {
                                if (conn.Kmh > 3 && _vars.SettingsMenuDisplay == "parked" && !_vars.OverwriteSidemenuVisibility && conn.OnTrack && conn.PlayerType != PlayerTypes.PLT_AI)
                                {
                                    await _window.sideMenu(CLOSE);
                                    return;
                                }

                                // conn.Kmh > 3 && _vars.SettingsMenuDisplay == "parked" && !_vars.OverwriteSidemenuVisibility && conn.OnTrack
                                await _window.sideMenu(OPEN);
                            }
                        }

                        #endregion
                    }
                    else if (_vars.InSimView.HasFlag(StateFlags.ISS_FRONT_END))
                    {
                        // Display some text in Main Menu of LFS
                        await _im.ClearBtnRange(3, 254);
                        await _im.CreateBTN(BS.ISB_C2, 9, 200, 0, 0, 1, $"^7LFS Companion");
                        await _im.CreateBTN(BS.ISB_C2, 4, 200, 0, 8, 2, $"^7Version ^3{_vars.Version.Major}.{_vars.Version.Minor}.{_vars.Version.Build} ^7by kristofferandersen");
                    }
                    else
                    {
                        await _im.ClearBtnRange(0, 254);
                    }
                }
            }
            catch (Exception ex)
            {
                await _exceptionP.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"MillisecondTimer()", ex);
            }
        }
    }
}
