﻿using LFSCompanion.Insim.Events;
using InSimDotNet.Helpers;
using InSimDotNet.Out;
using InSimDotNet.Packets;
using LFSCompanion.Application.Classes;
using LFSCompanion.Application.SQL;
using LFSCompanion.Application.Utils;
using LFSCompanion.Insim.Classes;
using LFSCompanion.Insim.Models;
using LFSCompanion.Insim.Models.Enum;
using SQLitePCL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using static LFSCompanion.Insim.Models.Enum.ButtonEnum;
using BS = InSimDotNet.Packets.ButtonStyles;

namespace LFSCompanion.Insim.Timers
{
    /// <summary>
    /// Anything related to second class
    /// </summary>
    public class Second
    {
        private Variables _var;
        private ExceptionProcessor _exceptionP;
        private InSimMethods _im;
        private StringFormatter _stringFormatter;
        private SQLContext _sql;
        private REST_API _api;
        private WindowFactory _window;
        private MathFactory _math;
        private LapManager _lapManager;
        private TimeSpan warnFuel = TimeSpan.Zero;
        private ButtonManager _btn;

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="variables"></param>
        /// <param name="exceptionP"></param>
        /// <param name="insimMethods"></param>
        public Second(Variables variables, ExceptionProcessor exceptionP, InSimMethods insimMethods,
            StringFormatter stringFormatter, SQLContext sql, REST_API api, WindowFactory window, MathFactory mathFactory,
            LapManager lapManager, ButtonManager btn)
        {
            _var = variables;
            _exceptionP = exceptionP;
            _im = insimMethods;
            _stringFormatter = stringFormatter;
            _sql = sql;
            _api = api;
            _window = window;
            _math = mathFactory;
            _lapManager = lapManager;
            _btn = btn;
        }

        /// <summary>
        /// Global Second Timer Tick - 1000ms / 1 second
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public async void SecondTimer_EventHandler(object? sender, ElapsedEventArgs e)
        {
            try
            {
                {
                    #region vars

                    if (_var.insim == null)
                        return;
                    
                    var conn = _im.getByUcid(_var.UCID);

                    if (conn == null)
                        return;
                    
                    if (!conn.IsLocal)
                        return;
                    
                    // await _im.LMsg($"Position: {conn.RacePosition}");
                    
                    #endregion
                    
                    #region Racing

                    LapAvgSpeed(conn);

                    #region Set Best PB Gadget
                    
                    setRaceGadgets(conn);
                    
                    #endregion

                    #region Racer Positions Page Counter

                    if (_var.cycleRacers.TotalSeconds > 1)
                        _var.cycleRacers -= TimeSpan.FromSeconds(1);
                    else
                        _var.cycleRacers = TimeSpan.FromSeconds(30);

                    #endregion

                    #endregion

                    #region Car Parked

                    if (conn.Kmh <= 1)
                        _var.Counters.CarParkedCounter += TimeSpan.FromSeconds(1);
                    else
                        _var.Counters.CarParkedCounter = TimeSpan.Zero;
                    
                    #endregion
                    
                    #region ' Avg. Fuel Consumption '
                    if (_var.FuelConsumptionCounter < 10)
                        _var.FuelConsumptionCounter++;
                    else
                    {
                        conn.TotalAvgFuel = _math.getFuelConsumption(conn);

                        #region Reset
                        conn.AvgMeters = 0;
                        conn.AvgFuelValue = _var.vehicleFuelLitres;
                        #endregion

                        if (conn.TotalAvgFuel < 0)
                            conn.TotalAvgFuel = 0;

                        _var.FuelConsumptionCounter = 0;
                    }
                    #endregion

                    #region Delay Greeting
                    if (_var.GreetingsTemporarilyDisabled == true)
                    {
                        if (_var.GreetingDisabledCounter <= 5)
                        {
                            _var.GreetingDisabledCounter++;
                        }
                        else
                        {
                            _var.GreetingsTemporarilyDisabled = false;
                            _var.GreetingDisabledCounter = 0;
                        }
                    }
                    #endregion

                    #region OutGauge Warning
                    if (_var.InSimView.HasFlag(StateFlags.ISS_GAME))
                    {
                        if (_var.outgauge == null)
                        {
                            if (_var.OutGaugeWarningCounter <= 5)
                            {
                                _var.OutGaugeWarningCounter++;
                            }
                            else
                            {
                                await _im.LMsg("^8Failed to connect to OutGauge.");
                                await _im.LMsg("^8Please verify your ^3OutGauge Port ^8and ^3OutGauge Mode");
                                _var.OutGaugeWarningCounter = 0;
                            }
                        }
                    }
                    #endregion

                    #region ' Incrementers '
                    _var.startupMessageCounter++;
                    _var.LoadSettingsCounter++;
                    
                    // Prevent sending pubstat messages when local user joins a server
                    if (_var.InSimView.HasFlag(StateFlags.ISS_GAME))
                        _var.pubstatTimer += TimeSpan.FromSeconds(1);
                    
                    // Console.WriteLine($@"pubstat Timer : {_var.pubstatTimer.TotalSeconds}");

                    #endregion

                    #region Export Button
                    if (_var.ExportDisabledCounter > 0)
                    {
                        _var.ExportDisabledCounter--;
                        _var.BSExportButton = BS.ISB_DARK;
                    }
                    else
                        _var.BSExportButton = BS.ISB_LIGHT | BS.ISB_CLICK;
                    #endregion
                    
                    #region 10 second loop
                    if (_var.ListSaveCounter < 10)
                    {
                        _var.ListSaveCounter++;
                    }
                    else
                    {
                        if (_var.insim == null)
                            return;

                        lock (_var.PendingPlayerMessages)
                        {
                            // Check if list has any items
                            if (_var.PendingPlayerMessages.Count > 0)
                            {
                                string[] ColumnNames = new string[] { "Nickname","Username","Message","Date" };
                                string[] ColumnValues = new string[] { "@nickname","@username","@message","@date" };

                                // Save them
                                _sql.InsertData("PlayerMessages", ColumnNames, _var.PendingPlayerMessages);
                                _var.PendingPlayerMessages.Clear();
                            }
                        }

                        lock (_var.PendingGlobalMessages)
                        {
                            if (_var.PendingGlobalMessages.Count > 0)
                            {
                                string[] ColumnNames = new string[] { "Date","Message" };

                                _sql.InsertData("GlobalMessages", ColumnNames, _var.PendingGlobalMessages);

                                _var.PendingGlobalMessages.Clear();
                            }
                        }

                        lock (_var.PendingCarContactMessages)
                        {
                            if (_var.PendingCarContactMessages.Count > 0)
                            {
                                _sql.InsertCarContact(_var.PendingCarContactMessages);
                                _var.PendingCarContactMessages.Clear();
                            }
                        }

                        lock (_var.PendingLaptimeMessages)
                        {
                            if (_var.PendingLaptimeMessages.Count > 0)
                            {
                                string[] ColumnNames = new string[]
                                    { "Username","Playername","Split1","Split2","Split3","Laptime","Vehicle","Track","Date" };

                                _sql.InsertData("Laptime", ColumnNames, _var.PendingLaptimeMessages);
                                _var.PendingLaptimeMessages.Clear();
                            }
                        }

                        _var.ListSaveCounter = 1;
                    }
                    #endregion

                    if (_var.InCustomizeUI == false)
                    {
                        #region Fuel Warning

                        foreach (var x in _var.ButtonList)
                        {
                            if (x.Name != FUEL)
                                continue;

                            _var.FuelStrobe = _var.FuelStrobe.Add(TimeSpan.FromSeconds(1));
                            
                            if (_var.FuelStrobeActive)
                            {
                                if (_var.FuelStrobe.Seconds == 1)
                                    x.Color = "^7";
                                else if (_var.FuelStrobe.Seconds >= 2)
                                {
                                    x.Color = "^1";
                                    _var.FuelStrobe = TimeSpan.Zero;
                                }
                            }
                            else
                            {
                                x.Color = "^7";
                            }
                        }
                        #endregion

                        #region Greet
                        if (_var.GreetingMessagesCounter <= 5)
                            _var.GreetingMessagesCounter++;
                        else
                            _var.CanGreet = true;
                        #endregion

                        #region AutoLights
                        // Initiate AutoLights every 2 seconds
                        if (_var.AutoLightsCounter < 2)
                            _var.AutoLightsCounter++;
                        else
                        {
                            if (_var.AutoLightsEnabled && _var.StrobeActive == false)
                            {
                                if (_var.insim == null)
                                    return;

                                if (_var.Counters.CarParkedCounter > TimeSpan.FromSeconds(30))
                                {
                                    await _var.insim.SendAsync(new IS_SMALL
                                    {
                                        SubT = SmallType.SMALL_LCS,
                                        UVal = LocalCarSwitches.LCS_HEADLIGHTS_OFF
                                    });
                                }
                                else
                                {
                                    await _var.insim.SendAsync(new IS_SMALL
                                    {
                                        SubT = SmallType.SMALL_LCS,
                                        UVal = LocalCarSwitches.LCS_HEADLIGHTS_ON
                                    });
                                }
                            }

                            _var.AutoLightsCounter = 0;
                        }
                        #endregion
                    }

                    #region Siren Slow/Fast Rotation
                    
                    // Counter logic
                    {
                        const int MAX = 30;
                        const int HALF = MAX / 2;

                        if (_var.UseSiren && _var.StrobeActive)
                        {
                            // Reset to max value
                            if (_var.Counters.Siren.TotalSeconds <= 0)
                                _var.Counters.Siren = TimeSpan.FromSeconds(MAX);

                            // Remove 1 each second
                            if (_var.Counters.Siren <= TimeSpan.FromSeconds(MAX))
                                _var.Counters.Siren -= TimeSpan.FromSeconds(1);
                        }
                        
                        // Change to slow
                        if (_var.Counters.Siren == TimeSpan.FromSeconds(MAX - 1))
                            await _im.setCarSwitch(LocalCarSwitches.LCS_SIREN_SLOW);
                        
                        // Change to fast
                        if (_var.Counters.Siren == TimeSpan.FromSeconds(HALF))
                            await _im.setCarSwitch(LocalCarSwitches.LCS_SIREN_FAST);
                    }

                    #endregion
                    
                    switch (_var.startupMessageCounter)
                    {
                        // Welcome messages
                        case 1:
                            if (_var.AppEnvironment == EnvironmentEnum.PRODUCTION)
                            {
                                await _im.LMsg($"Connected to insim protocol ^2successfully");
                            }
                            
                            #region ' LFS API Test '
                            
                            var entry = await _api.GetVehicleBase();
                            if (entry.FirstOrDefault() == null)
                            { 
                                if (!_var.ApiTestPassed)
                                {
                                    #region MyRegion

                                    byte top = 50;
                                    byte height = 5;

                                    byte title_H = 10;
                                    byte ClickID = 2;

                                    byte bkg_left = Convert.ToByte(70 - 1);
                                    byte bkg_top = Convert.ToByte(top - 1);
                                    
                                    await _im.CreateBTN(BS.ISB_DARK | BS.ISB_C1 | BS.ISB_C4, 105, Convert.ToByte(60 + 2), bkg_left, bkg_top, ClickID, "");
                                    ClickID++;
                                    await _im.CreateBTN(BS.ISB_LIGHT | BS.ISB_C1 | BS.ISB_C4, title_H, 60, 70, top, ClickID, 
                                        $"^8MODS API: UNAUTHORISED");

                                    top += Convert.ToByte(title_H + 3);
                                    ClickID++;
                                    await _im.CreateBTN(BS.ISB_C1, 5, 60, 70, top, ClickID, 
                                        $"^1The InSim was not able to connect to LFS Mods API.");
                                    
                                    top += Convert.ToByte(height + 3);
                                    ClickID++;
                                    
                                    await _im.CreateBTN(BS.ISB_C1 | BS.ISB_C4 | BS.ISB_LEFT, height, 60, 70, top, ClickID, 
                                        $"^8Troubleshooting:");

                                    height = 4;
                                    top += Convert.ToByte(height + 1);
                                    ClickID++;
                                    
                                    await _im.CreateBTN(BS.ISB_LEFT, height, 60, 70, top, ClickID, 
                                        $"^7Its likely you have installed a new version of the application.");
                                    
                                    top += Convert.ToByte(height);
                                    ClickID++;
                                    
                                    await _im.CreateBTN(BS.ISB_LEFT, height, 60, 70, top, ClickID, 
                                        $"^7Please verify the API fields in the ^3Connection.ini^8:");
                                    
                                    top += Convert.ToByte(height);
                                    ClickID++;
                                    
                                    await _im.CreateBTN(BS.ISB_LEFT, height, 60, 70, top, ClickID, 
                                        $"   ^3• ^8LFS_API_CLIENT_ID");
                                    
                                    top += Convert.ToByte(height);
                                    ClickID++;
                                    
                                    await _im.CreateBTN(BS.ISB_LEFT, height, 60, 70, top, ClickID, 
                                        $"   ^3• ^8LFS_API_CLIENT_SECRET");
                                    
                                    top += Convert.ToByte(height * 2);
                                    ClickID++;
                                    
                                    await _im.CreateBTN(BS.ISB_LEFT, height, 60, 70, top, ClickID, 
                                        $"^7To generate the ^3CLIENT_ID ^7and ^3CLIENT_SECRET^7, you need to fill a form on ^3lfs.net^7:");
                                    
                                    top += Convert.ToByte(height + 2);
                                    ClickID++;
                                    
                                    await _im.CreateBTT(BS.ISB_C1 | BS.ISB_CLICK | ButtonStyles.ISB_LIGHT, height, 28, 70, top, ClickID, 80,
                                        $"^8https://www.lfs.net/account/api","^0Paste this URL to your web browser", true);
                                    
                                    top += Convert.ToByte(height + 2);
                                    ClickID++;
                                    
                                    await _im.CreateBTN(BS.ISB_LEFT, height, 60, 70, top, ClickID, 
                                        $"^7Please fill the form with the following values:");
                                    
                                    top += Convert.ToByte(height);
                                    ClickID++;
                                    
                                    await _im.CreateBTN(BS.ISB_LEFT, height, 60, 70, top, ClickID, 
                                        $"   ^3• ^8Application name: ^3LFS Companion");
                                    
                                    top += Convert.ToByte(height);
                                    ClickID++;
                                    
                                    await _im.CreateBTN(BS.ISB_LEFT, height, 60, 70, top, ClickID, 
                                        $"   ^3• ^8Application display name: ^3LFS Companion");
                                    
                                    top += Convert.ToByte(height);
                                    ClickID++;
                                    
                                    await _im.CreateBTN(BS.ISB_LEFT, height, 15, 70, top, ClickID, 
                                        $"   ^3• ^8Redirect URIs:");
                                    
                                    ClickID++;
                                    
                                    await _im.CreateBTT(BS.ISB_C1 | BS.ISB_CLICK  | ButtonStyles.ISB_LIGHT, height, 17, 84, top, ClickID, 100,
                                        $"^8https://www.lfs.net","Press CTRL+C to copy URL", true);
                                    
                                    top += Convert.ToByte(height);
                                    ClickID++;
                                    
                                    await _im.CreateBTN(BS.ISB_LEFT, height, 60, 70, top, ClickID, 
                                        $"   ^3• ^8Single page application: ^3unchecked");
                                    
                                    top += Convert.ToByte(height + 2);
                                    ClickID++;
                                    
                                    await _im.CreateBTN(BS.ISB_LEFT, height, 60, 70, top, ClickID, 
                                        $"^7Copy both of them to the ^3Connection.ini^7.");
                                    
                                    top += Convert.ToByte(height + 2);
                                    ClickID++;
                                    
                                    await _im.CreateBTN(BS.ISB_LEFT, height, 60, 70, top, ClickID, 
                                        $"^7Where is the ^3Connection.ini^7?");
                                    
                                    top += Convert.ToByte(height + 3);
                                    ClickID++;
                                    
                                    var configPath = Path.Combine("[LFS_Companion_Folder]", "Config", "Connection.ini");
                                    
                                    await _im.CreateBTT(BS.ISB_C1 | BS.ISB_CLICK | ButtonStyles.ISB_LIGHT, height, 37, 70, top, ClickID, 100,
                                        $"^8{configPath}","Press CTRL+C to copy the path", true);

                                    #endregion
                                }
                                
                                // await _im.Error($"LFS MODS API TEST FAILED. VERIFY CONFIG FILE.");
                                _var.ApiTestPassed = false;
                            }
                            else
                                _var.ApiTestPassed = true;
                            
                            #endregion
                            
                            break;

                        // Load config files
                        case 2:

                            break;

                        // Connect database
                        case 30:

                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                await _exceptionP.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"SecondTimer()", ex);
            }
        }

        private void LapAvgSpeed(Player conn)
        {
            #region Laptime Avg. Speed

            if (!(_var.vehicleKmh > 2))
                return;
            
            double _speed;

            #region Speed Unit
            _speed = _var.SpeedoUnit == "kmh" ? _var.vehicleKmh : _var.vehicleMph;
            #endregion

            _var.LaptimeAvgSpeed.Add(new LaptimeAvgSpeedModel
            {
                Speed = _speed
            });
            #endregion
        }

        private void setRaceGadgets(Player conn)
        {
            if (_var.fetchPbInterval > 0 && conn.IsLocal)
            {
                _var.fetchPbInterval--;
            }
            else
            {
                if (conn.CarId == null)
                    return;
                            
                var pb = _lapManager.getPb(_var.TrackName, conn.CarId).Result;
                            
                if (pb == null)
                    _lapManager.Gui.setBestPb(int.MinValue);
                else
                    _lapManager.Gui.setBestPb(pb.Laptime);
                
                var tpb = _lapManager.getPredictedPb(_var.TrackName, conn);
                
                if (tpb.Result == 0)
                    _lapManager.Gui.setTheoreticalPb(int.MinValue);
                else
                    _lapManager.Gui.setTheoreticalPb(tpb.Result);

                _var.fetchPbInterval = 5;
            }
        }
    }
}
