﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Insim.Models
{
    public class LaptimeModel
    {
        public string? Username { get; set; }
        public string? Playername { get; set; }
        public double Split1 { get; set; }
        public double Split2 { get; set; }
        public double Split3 { get; set; }
        public double Laptime { get; set; }
        public string? Vehicle { get; set; }
        public string? Track { get; set; }
        public DateTime Date { get; set; }
    }
}
