﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Insim.Models
{
    public class LaptimeAvgSpeedModel
    {
        public double Speed { get; set; }
    }
}
