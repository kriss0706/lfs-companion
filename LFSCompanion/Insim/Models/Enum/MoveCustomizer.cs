﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Insim.Models.Enum
{
    public enum MoveCustomizerEnum
    {
        [Description("[move mode]")]
        MOVE,

        [Description("[click to move]")]
        NONE
    }
}
