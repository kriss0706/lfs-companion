﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Insim.Models.Enum
{
    public enum DateTimeFormatEnum
    {
        [Description("Date and time")]
        DATETIME,

        [Description("Date")]
        DATE,

        [Description("Time")]
        TIME,

        [Description("Long date")]
        LONG_DATE,

        [Description("Long date and time")]
        LONG_DATE_TIME
    }
}
