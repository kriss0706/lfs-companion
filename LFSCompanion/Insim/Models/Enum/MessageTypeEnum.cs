﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Insim.Models.Enum
{
    public enum MessageTypeEnum
    {
        [Description("Global")]
        GLOBAL,

        [Description("Players")]
        PLAYERS,

        [Description("Car Contact")]
        CARCONTACT
    }
}
