﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Insim.Models.Enum
{
    public enum ButtonEnum
    {
        [Description("Rpm")]
        RPM,

        [Description("Fuel")]
        FUEL,

        [Description("Gear")]
        GEAR,

        [Description("Speed")]
        SPEED,

        [Description("Turbo")]
        TURBO,

        [Description("Lights")]
        LIGHTS,

        [Description("Handbrake")]
        HANDBRAKE,

        [Description("Abs")]
        ABS,

        [Description("Tc")]
        TC,

        SIDEMENU_CUSTOMIZE_UI,

        [Description("BlinkerL")]
        BLINKER_LEFT,

        [Description("BlinkerR")]
        BLINKER_RIGHT,

        [Description("PitSpeed")]
        PITLANE_LIMITER,

        [Description("DateTime")]
        DATETIME,

        CUSTOMIZER_WINDOW,

        [Description("AvgFuel")]
        AVG_FUEL,

        [Description("Laptime")]
        LAP_TIME,

        [Description(description: "SplitTime")]
        LAP_SPLIT,

        [Description(description: "Track")]
        TRACK,

        [Description(description: "BestPB")]
        BEST_PB,
        
        [Description(description: "TheoreticalPB")]
        THEORETICAL_PB,
        
        [Description("Race Positions")]
        RACE_POSITIONS
    }

    public enum ButtonChild
    {
        RPM_TITLE,
        RPM_TEXT,
        TURBO_TITLE,
        GEAR_TITLE,
        SPEED_TITLE,
        FUEL_TITLE,
        PITLANE_LIMITER_TITLE,
        DATETIME_TITLE,
        AVG_FUEL_CONSUMPTION_TITLE,
        LAP_TIME_TITLE,
        LAP_SPLIT_TITLE,
        TRACK_TITLE,
        BEST_PB_TITLE,
        THEORETICAL_PB_TITLE,
        BLINKER_LEFT_TITLE,
        BLINKER_RIGHT_TITLE,
        RACE_POSITIONS_TITLE
    }
}
