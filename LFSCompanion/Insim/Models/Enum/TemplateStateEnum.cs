﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Insim.Models.Enum
{
    public enum WindowStateEnum
    {
        OPEN,
        CLOSE,
        CLOSE_CONTENT
    }
}
