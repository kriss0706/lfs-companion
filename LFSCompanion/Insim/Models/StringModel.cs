﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Insim.Models
{
    public class StringModel
    {
        /// <summary>
        /// Enum model used by getString() method in InSimMethods class.
        /// </summary>
        public enum StringEnum
        {
            ERROR_MESSAGE,
            MESSAGE_PREFIX,
            BTC_EXCEED_RANGE_MIN,
            BTC_EXCEED_RANGE_MAX
        }
    }
}
