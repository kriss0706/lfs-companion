﻿using LFSCompanion.Insim.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InSimDotNet.Packets;

namespace LFSCompanion.Insim.Models
{
    public class ButtonGroup
    {
        public ButtonEnum Name { get; set; }
        public byte PrimaryClickID { get; set; }
        public List<ButtonGroupChild>? ClickIDs { get; set; }
        public List<ButtonGroupSize>? Size { get; set; }
        public List<ButtonGroupLocation>? Location { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsVisible { get; set; }
        public string TitleColor { get; set; }
        public string Color { get; set; }
        public string SelectedColor { get; set; }
        public ButtonStyles Style { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }

    public class ButtonGroupChild
    {
        public ButtonChild Name { get; set; }
        public byte? ClickID { get; set; }
    }

    public class ButtonGroupSize
    {
        public byte Width { get; set; }
        public byte Height { get; set; }
    }

    public class ButtonGroupLocation
    {
        public byte Left { get; set; }
        public byte Top { get; set; }
    }
}
