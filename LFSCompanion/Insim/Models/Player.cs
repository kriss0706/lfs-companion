﻿using InSimDotNet.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LFSCompanion.Application.Classes;

namespace LFSCompanion.Insim.Models
{
    public class Player
    {
        public Racer Racer { get; private set; }
        
        public Player()
        {
            Racer = new Racer();
        }
         
        public int UCID { get; set; }
        public byte PLID { get; set; }
        public string UName { get; set; }
        public string PName { get; set; }
        public string? CarId { get; set; }
        public string? Plate { get; set; }
        public bool IsLocal { get; set; }
        public bool OnTrack { get; set; }
        public PlayerTypes PlayerType { get; set; }

        // Average Fuel Consumption
        public double TotalAvgFuel { get; set; }
        public double AvgMeters { get; set; }
        public double AvgFuelValue { get; set; }
        public double SessionKm { get; set; }
        public double Kmh { get; set; }
        public double Mph { get; set; }
    }

    public class Racer
    {
        public byte Position { get; set; }
        public int Laps { get; set; }
        public TimeSpan ETime { get; set; } = TimeSpan.Zero;
        public TimeSpan trackedLapTime { get; set; } = TimeSpan.Zero;
        public Dictionary<int, RaceStats?> userClickIDs { get; set; } = new Dictionary<int, RaceStats?>
        {
            { 0, new RaceStats
                {
                    Col1 = 80, // reserved for the Race Positions Title
                    Col2 = 81, // reserved for the Race Positions Footer
                    Col3 = 82
                }
            }
        };
        public bool renderPosition { get; set; } = true;
        
        public void AddClickID(int UCID, RaceStats? RacerObj)
        {
            /*var highest = userClickIDs
                .Values
                .OrderBy(x => x.Col3)
                .FirstOrDefault().Col3;

            var RacerObj = new RaceStats
            {
                Col1 = Convert.ToByte(highest + 1),
                Col2 = Convert.ToByte(highest + 2),
                Col3 = Convert.ToByte(highest + 3)
            };*/
            
            userClickIDs.TryAdd(UCID, RacerObj);
        }

        public int getLocalRacePosition(IDictionary<byte, Player> players)
        {
            var pl = players.Values
                .FirstOrDefault(x => x.IsLocal);

            if (pl == null)
                return 0;
            
            return pl.Racer.Laps;
        }
        
        public byte getHighestClickID(IDictionary<byte,Player> players)
        {
            var pl = players.Values
                .Where(x => x.PName != "host");
            
            var cl = pl
                .SelectMany(player => player.Racer.userClickIDs)
                .Where(x => x.Value != null)
                .Max(x => x.Value!.Col3);

            return cl;
        }

        public RaceStats GetUserClickID(int key)
        {
            if (userClickIDs.TryGetValue(key, out RaceStats? racerObj))
            {
                if (racerObj == null)
                    return new RaceStats();
                
                return racerObj;
            }
            
            return new RaceStats();

        }
    }

    public class RaceStats
    {
        public byte Col1 { get; set; }
        public byte Col2 { get; set; }
        public byte Col3 { get; set; }
    }
}
