﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using LFSCompanion.Insim.Models;
using LFSCompanion.Application.Classes;
using LFSCompanion.Application.SQL;
using LFSCompanion.Insim.Classes;
using LFSCompanion.Insim.Models.Enum;

namespace LFSCompanion.Insim.Events
{
    internal class TOC
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _insimMethods;
        private SQLContext _sqlContext;
        private REST_API _api;
        private LapManager _lapManager;
        private CarInfo _carInfo;
        private WindowFactory _window;
        public TOC(Variables variables,
            ExceptionProcessor exc,
            InSimMethods insimMethods,
            SQLContext sqlContext,
            LapManager lapManager,
            CarInfo carInfo,
            WindowFactory window)
        {
            _var = variables;
            _exc = exc;
            _insimMethods = insimMethods;
            _sqlContext = sqlContext;
            _api = new(_var, _exc);
            _lapManager = lapManager;
            _carInfo = carInfo;
            _window = window;
        }

        /// <summary>
        /// Player take over car
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task TakeOverCar(object o, PacketEventArgs<IS_TOC> e)
        {
            try
            {
                var oldConn = _insimMethods.getByUcid(e.Packet.OldUCID);
                var newConn = _insimMethods.getByUcid(e.Packet.NewUCID);
                
                if (oldConn == null || newConn == null)
                    return;

                newConn.PLID = oldConn.PLID;
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET,
                    $"IS_TOC", ex);
            }
        }
    }
}
