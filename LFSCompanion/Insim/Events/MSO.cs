﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using InSimDotNet.Helpers;
using static LFSCompanion.Application.SQL.Seeders.SeederModels;
using LFSCompanion.Application.SQL;
using LFSCompanion.Application.Classes;
using LFSCompanion.Insim.Classes;
using static LFSCompanion.Insim.Models.Enum.WindowStateEnum;

namespace LFSCompanion.Insim.Events
{
    public class MSO
    {
        private volatile Variables _vars;
        private SQLContext _sql;
        private InSimMethods _insimMethods;
        private ExceptionProcessor _exc;
        private WindowFactory _windowFactory;
        private readonly object varsLock = new();
        private readonly object globalMessagesLock = new();
        private readonly object pendingglobalMessagesLock = new();
        private readonly object playerMessagesLock = new();
        private readonly object pendingplayerMessagesLock = new();

        public MSO(Variables vars, SQLContext sql, InSimMethods insimMethods, ExceptionProcessor exceptionProcessor, WindowFactory windowFactory)
        {
            _vars = vars;
            _sql = sql;
            _insimMethods = insimMethods;
            _exc = exceptionProcessor;
            _windowFactory = windowFactory;
        }

        /// <summary>
        /// Manage chat messages
        /// </summary>
        /// <param name="o"></param>
        /// <param name="mso"></param>
        /// <returns></returns>
        public async Task MessageOut(object o, PacketEventArgs<IS_MSO> mso)
        {
            try
            {
                var conn = _insimMethods.getByUcid(mso.Packet.UCID);
                
                string[] off_alias = ["off", "false", "disable", "deny", "no"];
                string[] on_alias = ["on", "true", "enable", "accept", "yes"];

                DateTime date = DateTime.Now;
                string dateString = date.ToString("dd/MM/yy HH:mm");

                string FullMessage = mso.Packet.Msg;

                List<string> aliasList = new()
                {
                    "^3[C]",
                    "^3[Companion]"
                };

                bool ContainsAlias = false;
                foreach (var item in aliasList)
                {
                    if (FullMessage.StartsWith(item))
                    {
                        ContainsAlias = true;
                    }
                }

                if (ContainsAlias == false)
                {
                    lock (_vars.PendingGlobalMessages)
                    {
                        _vars.PendingGlobalMessages.Add(new Messages
                        {
                            Message = FullMessage,
                            Date = DateTime.Now
                        });
                    }

                    lock (_vars.GlobalMessages)
                    {
                        _vars.GlobalMessages.Add(new Messages
                        {
                            Message = FullMessage,
                            Date = DateTime.Now
                        });
                    }
                    
                    // Handle commands starting with /o
                    if (mso.Packet.UserType == UserType.MSO_O)
                    {
                        var args = mso.Packet.Msg.Split(" ");
                        
                        switch (args[0])
                        {
                            case "help":
                                await _insimMethods.LMsg($"^7Application Help Commands:");
                                await _insimMethods.LMsg($" ^7» ^3/o help ^7- Display this help page");
                                await _insimMethods.LMsg($" ^7» ^3/o menu ^7- Toggle the side menu visibility");
                                await _insimMethods.LMsg($" ^7» ^3/o rp ^7- Display help commands for Race Positions window");
                                await _insimMethods.LMsg($" ^7» ^3/o rp next ^7- Go to the next page of racers");
                                await _insimMethods.LMsg($" ^7» ^3/o rp prev ^7- Go to the previous page of racers");
                                await _insimMethods.LMsg($" ^7» ^3/o strobe ^7- Toggle the vehicle strobe with a single command");
                                await _insimMethods.LMsg($" ^7» ^3/o strobe [off|on] ^7- Toggle the vehicle strobe");
                                await _insimMethods.LMsg($" ^7» ^3/o siren ^7- Toggle the vehicle siren with a single command");
                                await _insimMethods.LMsg($" ^7» ^3/o siren [off|on] ^7- Toggle the vehicle siren");
                                break;
                            
                            case "menu":
                                lock (varsLock)
                                    _vars.OverwriteSidemenuVisibility = !_vars.OverwriteSidemenuVisibility;
                                
                                await _insimMethods.LMsg($"^8Overwriting sidemenu visibility is now ^3{_vars.OverwriteSidemenuVisibility}");
                                break;
                            
                            case "rp":
                                if (args.Length == 1)
                                {
                                    await _insimMethods.LMsg($"^7Race Positions Help Commands:");
                                    await _insimMethods.LMsg($" ^7» ^3/o rp ^7- Display this help page");
                                    await _insimMethods.LMsg($" ^7» ^3/o rp next ^7- Go to the next page of racers");
                                    await _insimMethods.LMsg($" ^7» ^3/o rp prev ^7- Go to the previous page of racers");
                                }
                                
                                if (args[1] == "prev" || args[1] == "next")
                                    lock (varsLock)
                                        _vars.cycleRacers = TimeSpan.FromSeconds(30);
                                break;
                            
                            case "strobe":

                                lock (varsLock)
                                {
                                    if (_vars.StrobeActive)
                                    {
                                        // Turn off all lights automatically
                                        _vars.StrobeOffCounter = TimeSpan.FromSeconds(5);
                                    }
                                }
                                
                                // Toggle strobe
                                if (args.Length == 1)
                                {
                                    lock (varsLock)
                                    {
                                        _vars.StrobeActive = !_vars.StrobeActive;
                                        
                                        if (_vars.UseSiren)
                                            _vars.Counters.Siren = TimeSpan.FromSeconds(30);
                                    }
                                    
                                    return;
                                }

                                // Turn off strobe
                                // Command: '/o strobe off'
                                if (off_alias.Contains(args[1]))
                                    lock (varsLock)
                                        _vars.StrobeActive = false;
                                    
                                
                                // Turn on strobe
                                // Command: '/o strobe on'
                                if (on_alias.Contains(args[1]))
                                {
                                    lock (varsLock)
                                    {
                                        _vars.StrobeActive = true;

                                        if (_vars.UseSiren)
                                        {
                                            _vars.Counters.Siren = TimeSpan.FromSeconds(30);
                                        }
                                    }
                                }
                                break;
                            
                            case "siren":
                                if (args.Length == 1)
                                {
                                    lock (varsLock)
                                        _insimMethods.LMsg($"Use Siren : {_vars.UseSiren}");
                                    
                                    // TODO: Rewrite this to toggle setting (save to DB too)
                                }
                                
                                // Turn off siren
                                if (off_alias.Contains(args[1]))
                                    lock (varsLock)
                                    {
                                        _vars.UseSiren = false;
                                        _insimMethods.LMsg($"^7Audible ^3siren ^7disabled!");
                                        _sql.SaveSettingAsync("UseSiren", _vars.UseSiren.ToString());
                                    }
                                
                                // Turn on siren
                                if (on_alias.Contains(args[1]))
                                    lock (varsLock)
                                    {
                                        _vars.UseSiren = true;
                                        _insimMethods.LMsg($"^7Audible ^3siren ^7enabled! It will be used in combination with the strobe.");
                                        _sql.SaveSettingAsync("UseSiren", _vars.UseSiren.ToString());
                                    }
                                break;
                            
                            default:
                                await _insimMethods.LMsg($"^1Command not found, type ^7!help ^1for a list of commands.");
                                break;
                        }
                    }

                    if (FullMessage.Contains(" ^7: "))
                    {
                        var msgSplit = FullMessage.Split(" ^7: ");

                        var nickname = msgSplit[0];
                        var msg = msgSplit[1];

                        lock (_vars.PendingPlayerMessages)
                        {
                            // List ready to be saved to database, but not yet saved
                            _vars.PendingPlayerMessages.Add(new Messages
                            {
                                Nickname = nickname,
                                Username = conn.UName,
                                Message = msg.Replace("^8", ""),
                                Date = DateTime.Now
                            });
                        }

                        lock (_vars.PlayerMessages)
                        {
                            // Populate local list (does not get cleared)
                            _vars.PlayerMessages.Add(new Messages
                            {
                                Nickname = nickname,
                                Username = conn.UName,
                                Message = msg.Replace("^8", ""),
                                Date = DateTime.Now
                            });
                        }

                        lock(varsLock)
                            if (_vars.ConsoleChat)
                                Console.WriteLine($@"{dateString} - {StringHelper.Escape(StringHelper.StripColors(nickname))} : {StringHelper.StripColors(msg)}");
                    }
                    else if (FullMessage.Contains("^7GC"))
                    {
                        var gcSplit = FullMessage.Split("^3» ");

                        var gcNickname = gcSplit[0].Remove(2,3);
                        var gcMessage = gcSplit[1];

                        lock (_vars.PendingPlayerMessages)
                        {
                            // Save group chat messages to local database
                            _vars.PendingPlayerMessages.Add(new Messages
                            {
                                Nickname = $"^3[GC] ^7{gcNickname}",
                                Username = conn.UName,
                                Message = gcMessage.Replace("^8", ""),
                                Date = DateTime.Now
                            });
                        }
                        
                        lock (_vars.PlayerMessages)
                        {
                            // Populate local list (does not get cleared)
                            _vars.PlayerMessages.Add(new Messages
                            {
                                Nickname = $"^3[GC] ^7{gcNickname}",
                                Username = conn.UName,
                                Message = gcMessage.Replace("^8", ""),
                                Date = DateTime.Now
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET, "IS_MSO", ex);
            }
        }
    }
}
