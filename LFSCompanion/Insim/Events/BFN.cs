﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LFSCompanion.Application.SQL;
using LFSCompanion.Application.Classes;
using LFSCompanion.Insim.Classes;

namespace LFSCompanion.Insim.Events
{
    internal class BFN
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _insimMethods;
        private SQLContext _sqlContext;

        public BFN(Variables variables,
            ExceptionProcessor exc,
            InSimMethods insimMethods,
            SQLContext sqlContext)
        {
            _var = variables;
            _exc = exc;
            _insimMethods = insimMethods;
            _sqlContext = sqlContext;
        }

        /// <summary>
        /// Manage insim states
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task ButtonFunction(object o, PacketEventArgs<IS_BFN> e)
        {
            try
            {
                #region Window States
                _var.InCustomizeUI = false;
                _var.InSettings = false;
                _var.InMessages = false;
                _var.InCustomizeHelp = false;
                _var.InTemplatesOverview = false;
                _var.InAddTemplate = false;
                _var.InManageTemplate = false;
                _var.InImportTemplate = false;
                _var.InLaptimes = false;
                #endregion

                if (_var.DisplayGui)
                {
                    await _insimMethods.LMsg($"InSim buttons are now hidden. Press ^3SHIFT+B ^8to toggle.");
                    _var.DisplayGui = false;
                }
                else
                {
                    await _insimMethods.LMsg($"InSim buttons are now visible. Press ^3SHIFT+B ^8to toggle.");
                    _var.DisplayGui = true;
                }


            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET,
                    $"IS_BFN", ex);
            }
        }
    }
}
