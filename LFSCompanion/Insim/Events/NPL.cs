﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using LFSCompanion.Insim.Models;
using LFSCompanion.Application.Classes;
using LFSCompanion.Application.SQL;
using LFSCompanion.Insim.Classes;

namespace LFSCompanion.Insim.Events
{
    internal class NPL
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _insimMethods;
        private SQLContext _sqlContext;
        private REST_API _api;
        private LapManager _lapManager;
        private CarInfo _carInfo;

        public NPL(Variables variables,
            ExceptionProcessor exc,
            InSimMethods insimMethods,
            SQLContext sqlContext,
            LapManager lapManager,
            CarInfo carInfo)
        {
            _var = variables;
            _exc = exc;
            _insimMethods = insimMethods;
            _sqlContext = sqlContext;
            _api = new(_var, _exc);
            _lapManager = lapManager;
            _carInfo = carInfo;
        }

        /// <summary>
        /// Manage new car on the track
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task NewPlayer(object o, PacketEventArgs<IS_NPL> e)
        {
            try
            {
                var npl = e.Packet;
                var conn = _insimMethods.getByUcid(npl.UCID);

                if (conn == null)
                    return;
                
                conn.UCID = npl.UCID;
                conn.PLID = npl.PLID;
                conn.OnTrack = true;
                conn.Racer.renderPosition = true;
                conn.CarId = npl.CName;
                conn.PlayerType = e.Packet.PType;
                conn.Racer.ETime = TimeSpan.Zero;
                conn.Plate = npl.Plate;

                _var.trackedLaptime = TimeSpan.Zero;
                
                if (!conn.IsLocal)
                    return;
                
                // Clear racing related values
                _lapManager.setSplit(1, TimeSpan.Zero);
                _lapManager.setSplit(2, TimeSpan.Zero);
                _lapManager.setSplit(3, TimeSpan.Zero);
                _lapManager.Gui.setLastSplit(TimeSpan.Zero);
                _lapManager.Gui.setLaptime(TimeSpan.Zero);
                
                _var.UCID = npl.UCID;
                conn.AvgFuelValue = 0;
                conn.AvgMeters = 0;
                conn.TotalAvgFuel = 0;

                _var.LaptimeAvgSpeed.Clear();

                try
                {
                    // Get vehicle details from object
                    var entry = await _api.GetVehicleDetails(npl.CName);

                    if (entry.Vehicle?.MaxPowerRpm == null)
                        return;

                    _var.vehicleTankSize = entry.Vehicle.FuelTankSize;
                    _var.vehicleShiftType = entry.Vehicle.ShiftType;
                    _var.MaxRpm = entry.Vehicle.MaxPowerRpm.Value;
                }
                catch (Exception ex)
                {
                    await _insimMethods.LMsg($"^1Could not fetch vehicle data. API Service unavailable?");
                    await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, $"GetVehicleDetails in NPL.cs", ex);
                }
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET,
                    $"IS_NPL", ex);
            }
        }
    }
}
