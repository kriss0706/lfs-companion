﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InSimDotNet.Helpers;
using static LFSCompanion.Application.SQL.Seeders.SeederModels;
using LFSCompanion.Application.SQL;
using LFSCompanion.Application.Classes;
using LFSCompanion.Insim.Classes;

namespace LFSCompanion.Insim.Events
{
    public class MCI
    {
        private Variables _vars;
        private SQLContext _sql;
        private InSimMethods _insimMethods;

        public MCI(Variables vars, SQLContext sql, InSimMethods insimMethods)
        {
            _vars = vars;
            _sql = sql;
            _insimMethods = insimMethods;
        }

        private ExceptionProcessor exc = new();

        /// <summary>
        /// Manage chat messages
        /// </summary>
        /// <param name="o"></param>
        /// <param name="mso"></param>
        /// <returns></returns>
        public async Task MultiInfo(object o, PacketEventArgs<IS_MCI> e)
        {
            try
            {
                if (_vars.insim == null)
                    return;
                
                var speedMulti = 1 / _vars.insim.Settings.Interval;
                
                foreach (var car in e.Packet.Info)
                {
                    var conn = _insimMethods.GetByPlid(car.PLID);
                    
                    if (conn == null)
                        return;
                    
                    conn.PLID = car.PLID;
                    conn.Racer.Laps = car.Lap;
                    conn.Racer.Position = car.Position;
                    
                    var mps = MathHelper.SpeedToMps(car.Speed) * speedMulti;
                    
                    // Distance Trackers
                    conn.AvgMeters += MathHelper.LengthToMeters(mps);
                    conn.SessionKm += MathHelper.LengthToKilometers(mps);
                    
                    // Speed Trackers
                    conn.Kmh = MathHelper.SpeedToKph(car.Speed);
                    conn.Mph = MathHelper.SpeedToMph(car.Speed);
                    
                    /*if (conn.IsLocal)
                        await _insimMethods.LMsg($"value : {conn.AvgMeters}");*/
                    
                    
                }
            }
            catch (Exception ex)
            {
                await exc.Log(ExceptionProcessor.LogTypeEnum.PACKET, "IS_MCI", ex);
            }
        }
    }
}
