﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InSimDotNet.Helpers;
using LFSCompanion.Insim.Models.Enum;
using LFSCompanion.Application.SQL.Models;
using LFSCompanion.Application.SQL;
using LFSCompanion.Application.Classes;
using LFSCompanion.Insim.Classes;

namespace LFSCompanion.Insim.Events
{
    internal class CON
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _insimMethods;
        private SQLContext _sqlContext;

        public CON(Variables variables,
            ExceptionProcessor exc,
            InSimMethods insimMethods,
            SQLContext sqlContext)
        {
            _var = variables;
            _exc = exc;
            _insimMethods = insimMethods;
            _sqlContext = sqlContext;
        }

        /// <summary>
        /// Manage car contact
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task CarContact(object o, PacketEventArgs<IS_CON> e)
        {
            try
            {
                var con = e.Packet;
                var a = _insimMethods.GetByPlid(con.A.PLID);
                var b = _insimMethods.GetByPlid(con.B.PLID);

                // A and B
                if (a == null || b == null)
                    return;

                if (a.PlayerType.HasFlag(PlayerTypes.PLT_AI) || b.PlayerType.HasFlag(PlayerTypes.PLT_AI))
                    return;

                if (_var.CarContactEnabled && con.A.PLID == a.PLID && con.B.PLID == b.PLID)
                {
                    #region Display Highest Speed

                    double contactSpeedRaw;
                    double contactSpeed;
                    if (con.A.Speed > con.B.Speed)
                    {
                        contactSpeed = con.A.Speed;
                        contactSpeedRaw = con.A.Speed;
                    }
                    else
                    {
                        contactSpeed = con.B.Speed;
                        contactSpeedRaw = con.B.Speed;
                    }
                    #endregion

                    #region Convert speed to speed unit
                    if (_var.SpeedoUnit == "kmh")
                        contactSpeed = MathHelper.MpsToKph(contactSpeed);
                    else
                        contactSpeed = MathHelper.MpsToMph(contactSpeed);
                    #endregion

                    // Add to pending list
                    _var.PendingCarContactMessages.Add(new Application.SQL.Models.CarContact
                    {
                        SourceUName = a.UName,
                        SourcePName = StringHelper.StripLanguage(a.PName),
                        TargetUName = b.UName,
                        TargetPName = StringHelper.StripLanguage(b.PName),
                        Speed = Convert.ToInt32(contactSpeedRaw),
                        Date = DateTime.Now
                    });

                    // Add to live list
                    _var.CarContactMessages.Add(new Application.SQL.Models.CarContact
                    {
                        SourceUName = a.UName,
                        SourcePName = StringHelper.StripLanguage(a.PName),
                        TargetUName = b.UName,
                        TargetPName = StringHelper.StripLanguage(b.PName),
                        Speed = Convert.ToInt32(contactSpeedRaw),
                        Date = DateTime.Now
                    });
                    
                    if (a.IsLocal || b.IsLocal)
                        await _insimMethods.LMsg($"^7Car Contact: " +
                                                 $"{StringHelper.StripLanguage(a.PName)} ^7& {StringHelper.StripLanguage(b.PName)} " +
                                                 $"^7({contactSpeed:F0} {_var.SpeedoUnit})");
                }
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"IS_CON", ex);
            }
        }
    }
}
