﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LFSCompanion.Application.SQL;
using LFSCompanion.Application.Classes;
using LFSCompanion.Insim.Classes;
using LFSCompanion.Insim.Models.Enum;

namespace LFSCompanion.Insim.Events
{
    internal class STA
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _insimMethods;
        private SQLContext _sqlContext;

        public STA(Variables variables,
            ExceptionProcessor exc,
            InSimMethods insimMethods,
            SQLContext sqlContext)
        {
            _var = variables;
            _exc = exc;
            _insimMethods = insimMethods;
            _sqlContext = sqlContext;
        }

        /// <summary>
        /// Manage insim states
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task StateChanged(object o, PacketEventArgs<IS_STA> e)
        {
            try
            {
                _var.InSimView = e.Packet.Flags;

                #region TrackName
                if (_var.InSimView.HasFlag(StateFlags.ISS_FRONT_END))
                    _var.TrackName = "Menu";
                else
                    _var.TrackName = e.Packet.Track;
                #endregion
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET,
                    $"IS_STA", ex);
            }
        }
    }
}
