﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Globalization;
using static LFSCompanion.Application.Classes.Variables;
using InSimDotNet.Helpers;
using LFSCompanion.Application.Classes;
using LFSCompanion.Insim.Models;
using LFSCompanion.Application.SQL;
using LFSCompanion.Insim.Classes;

namespace LFSCompanion.Insim.Events
{
    public class LAP
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _insimMethods;
        private SQLContext _sqlContext;
        private StringFormatter _stringFormatter;
        private LapManager _lapManager;

        public LAP(Variables variables,
            ExceptionProcessor exc,
            InSimMethods insimMethods,
            SQLContext sqlContext,
            StringFormatter stringFormatter,
            LapManager lapManager)
        {
            _var = variables;
            _exc = exc;
            _insimMethods = insimMethods;
            _sqlContext = sqlContext;
            _stringFormatter = stringFormatter;
            _lapManager = lapManager;
        }

        /// <summary>
        /// Manage laptimes
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task LapTime(object o, PacketEventArgs<IS_LAP> e)
        {
            try
            {
                var l = e.Packet;
                var conn = _insimMethods.GetByPlid(e.Packet.PLID);

                if (conn == null)
                    return;

                #region Racer
                
                // Compare times
                if (conn.IsLocal)
                    _var.trackedLaptime += l.LTime;
                else
                    conn.Racer.trackedLapTime += l.LTime;

                #endregion

                if (!conn.IsLocal)
                    return;
                
                if (conn.PlayerType.HasFlag(PlayerTypes.PLT_AI))
                    return;

                if (conn.CarId == null)
                    return;
                
                var lastSplit = new TimeSpan[4];
                
                lastSplit[1] = _lapManager.getSplit(1);
                lastSplit[2] = _lapManager.getSplit(2);
                lastSplit[3] = _lapManager.getSplit(3);
                
                conn.PLID = e.Packet.PLID;
                
                if (e.Packet.LTime.TotalHours >= 1 || lastSplit[1].TotalMilliseconds == 0 || e.Packet.LTime.TotalMilliseconds == 0)
                    return;
                
                // Store the value for DB Context
                _lapManager.setLaptime(e.Packet.LTime);
                
                // Used to display the widget
                _lapManager.Gui.setLaptime(e.Packet.LTime);

                #region Compare Laptimes

                #region Avg Speed
                var avgSpeed = _var.LaptimeAvgSpeed.Average(x => x.Speed)
                    .ToString("0.0", CultureInfo.InvariantCulture);
                #endregion

                // Fetch the fastest time
                var getPb = _var.LaptimeMessages
                    .Where(x => x.Track == _var.TrackName && x.Vehicle == conn.CarId)
                    .OrderBy(x => x.Laptime)
                    .FirstOrDefault();
                
                if (getPb == null)
                {
                    // First recorded lap with track+car combination
                    await _insimMethods.LMsg($"^8This is your first recorded lap with this vehicle");
                    
                    var tsLap = _lapManager.getLaptime();
                    var textLap = $@"{tsLap:%m\:ss\.ff}";

                    if (_var.RacingMessagesEnabled)
                        await _insimMethods.LMsg($"^2{textLap} ^8- Average speed: ^3{avgSpeed} {_var.SpeedoUnit}");
                }
                else
                {
                    // Vehicle already has lap records
                    var _lap = _lapManager.getLaptime();
                    var LaptimeCompare = $@"{_lap:mm\:ss\.ff}";
                    
                    var lapDiff = await _stringFormatter.getRaceTimeDifference(getPb.Laptime, e.Packet.LTime.TotalMilliseconds);

                    if (_var.RacingMessagesEnabled)
                        await _insimMethods.LMsg($"^8{LaptimeCompare}{lapDiff} " +
                            $"^8- Average speed: ^3{avgSpeed} {_var.SpeedoUnit}");
                }
                
                _var.LaptimeAvgSpeed.Clear();
                #endregion

                #region Update storage
                var lap = new LaptimeModel
                {
                    Username = conn.UName,
                    Playername = conn.PName,
                    Laptime = _lapManager.getLaptime().TotalMilliseconds,
                    Split1 = lastSplit[1].TotalMilliseconds,
                    Split2 = lastSplit[2].TotalMilliseconds,
                    Split3 = lastSplit[3].TotalMilliseconds,
                    Track = _var.TrackName,
                    Vehicle = conn.CarId,
                    Date = DateTime.Now
                };

                _var.LaptimeMessages.Add(lap);
                _var.PendingLaptimeMessages.Add(lap);
                #endregion
                
                #region Best PB Check
                var personalBest = _lapManager.getPb(_var.TrackName, conn.CarId).Result;

                // Update the PB Gadget
                if (personalBest == null)
                    return;
                
                _lapManager.Gui.setBestPb(personalBest.Laptime);
                _lapManager.Gui.setTheoreticalPb(await _lapManager.getPredictedPb(_var.TrackName, conn));

                #endregion
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET,
                    $"IS_LAP", ex);
            }
        }
    }
}
