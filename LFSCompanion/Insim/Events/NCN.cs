﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InSimDotNet.Helpers;
using LFSCompanion.Insim.Models;
using LFSCompanion.Application.SQL;
using LFSCompanion.Application.Classes;
using LFSCompanion.Application.Models;
using LFSCompanion.Insim.Classes;

namespace LFSCompanion.Insim.Events
{
    public class NCN
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _insimMethods;
        private SQLContext _sqlContext;
        private StringFormatter _stringFormatter;
        private LFSW _lfsw;

        public NCN(Variables variables,
            ExceptionProcessor exc,
            InSimMethods insimMethods,
            SQLContext sqlContext,
            StringFormatter stringFormatter,
            LFSW lfsw)
        {
            _var = variables;
            _exc = exc;
            _insimMethods = insimMethods;
            _sqlContext = sqlContext;
            _stringFormatter = stringFormatter;
            _lfsw = lfsw;
        }

        /// <summary>
        /// Mange new connection
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task NewConnection(object o, PacketEventArgs<IS_NCN> e)
        {
            try
            {
                var ncn = e.Packet;

                _var.Players[ncn.UCID] = new Player
                {
                    UCID = ncn.UCID,
                    UName = ncn.UName,
                    PName = ncn.PName,
                    OnTrack = false,
                    IsLocal = ncn.Remote == false
                };

                if (!ncn.Remote)
                {
                    _var.UCID = ncn.UCID;
                    _var.IsLocal = false;
                }
                
                
                
                // Only send pubstat message when user joining is not local user
                if (ncn.Remote && _var.pubstatTimer.TotalSeconds != 0 && _var.pubstatTimer.TotalSeconds > 2 && _var.lfswPubstat != "none")
                {
                    await displayPubstat(ncn);
                }
                
                #region Greet
                if (_var is { CanGreet: true, GreetingsEnabled: true })
                {
                    if (ncn.UCID != 0)
                    {
                        string[] syntax =
                        [
                            "%playername%",
                            "%username%"
                        ];

                        string greetMessage;

                        if (_var.GreetingMessage.Contains(syntax[0]) || _var.GreetingMessage.Contains(syntax[1]))
                        {
                            greetMessage = _var.GreetingMessage
                                .Replace(syntax[0], ncn.PName)
                                .Replace(syntax[1], ncn.UName);
                        }
                        else
                        {
                            greetMessage = _var.GreetingMessage;
                        }

                        if (_var.GreetingsTemporarilyDisabled == false)
                        {
                            await Task.Delay(1000);
                            await _insimMethods.SMsg(greetMessage);
                        }
                    }
                }
                #endregion
                
                /*await _var.insim.SendAsync(new IS_TINY
                {
                    ReqI = 255,
                    SubT = TinyType.TINY_MCI
                });*/
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET,
                    $"IS_NCN", ex);
            }
        }

        private async Task displayPubstat(IS_NCN ncn)
        {
            var obj = await _lfsw.getPubstat(ncn.UName);
            var status = obj.FirstOrDefault().Key;
            var value = obj.FirstOrDefault().Value;
            
            if (status == PubstatStatus.HIDDEN_STATS)
            {     
                await _insimMethods.LMsg($" ^6User ^7{ncn.PName} ^8({ncn.UName}) ^6has hidden statistics", "^3[C]");
                return;
            }
            
            if (status == PubstatStatus.BAD_GATEWAY)
            {     
                await _insimMethods.LMsg($" ^6Request for ^7{ncn.PName} ^8({ncn.UName}): ^6Bad Gateway (502)", "^3[C]");
                return;
            }

            if (status == PubstatStatus.AUTH_ERROR)
            {
                await _insimMethods.LMsg($" ^6Unauthorized access for pubstat request. Vpn?", "^3[C]");
                return;
            }
                    
            if (value == null)
            {
                Console.WriteLine($@"Received a status but no valid Pubstat data");
                return;
            }
            
            var dist = MathHelper.MetersToKilometers(Convert.ToInt64(value.distance));
            var laps = Convert.ToInt32(value.laps);
            var wins = Convert.ToInt32(value.win);
            var distanceUnit = "km";

            string country;
            
            if (value.country.Contains(','))
            {
                var countryRaw = value.country.Split(", ");
                country = countryRaw[0];
            }
            else
            {
                country = value.country;
            }
            
            if (_var.SpeedoUnit == "mph")
            {
                dist = MathHelper.MetersToKilometers(Convert.ToInt32(value.distance));
                distanceUnit = "mi";
            }
            
            await _insimMethods.LMsg($" ^7{ncn.PName} ^8({ncn.UName}) ^6is from ^7{country}", "^3[C]");
            await _insimMethods.LMsg($" ^7{await _stringFormatter.toNumber(dist)} ^6{distanceUnit}, ^7" +
                        $"{await _stringFormatter.toNumber(laps)} ^6laps, ^7{await _stringFormatter.toNumber(wins)} ^6wins", "^3[C]");
        }
    }
}
