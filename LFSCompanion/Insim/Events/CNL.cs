﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LFSCompanion.Insim.Models;
using LFSCompanion.Application.SQL;
using LFSCompanion.Application.Classes;
using LFSCompanion.Insim.Classes;

namespace LFSCompanion.Insim.Events
{
    public class CNL
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _im;
        private SQLContext _sqlContext;

        public CNL(Variables variables,
            ExceptionProcessor exc,
            InSimMethods im,
            SQLContext sqlContext)
        {
            _var = variables;
            _exc = exc;
            _im = im;
            _sqlContext = sqlContext;
        }

        /// <summary>
        /// Manage connection leaving the server
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task connLeave(object o, PacketEventArgs<IS_CNL> e)
        {
            try
            {
                var conn = _im.getByUcid(e.Packet.UCID);

                if (conn == null)
                    return;

                #region Race Positions - Clear buttons

                // Clear the 3 buttons about the user on the Race Positions Window
                var click = conn.Racer.GetUserClickID(conn.UCID);
                conn.Racer.userClickIDs.Remove(conn.UCID);
                await _im.ClearBtnRange(click.Col1, click.Col3);

                #endregion
                
                if (conn.IsLocal)
                    _var.pubstatTimer = TimeSpan.Zero;

                _var.Players.Remove(e.Packet.UCID);
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET,
                    $"IS_CNL", ex);
            }
        }
    }
}
