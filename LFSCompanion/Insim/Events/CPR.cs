﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InSimDotNet.Helpers;
using LFSCompanion.Insim.Models.Enum;
using LFSCompanion.Application.SQL.Models;
using LFSCompanion.Application.SQL;
using LFSCompanion.Application.Classes;
using LFSCompanion.Insim.Classes;

namespace LFSCompanion.Insim.Events
{
    internal class CPR
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _im;
        private SQLContext _sqlContext;

        public CPR(Variables variables,
            ExceptionProcessor exc,
            InSimMethods im,
            SQLContext sqlContext)
        {
            _var = variables;
            _exc = exc;
            _im = im;
            _sqlContext = sqlContext;
        }

        /// <summary>
        /// Connection renamed
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task ConnectionRename(object o, PacketEventArgs<IS_CPR> e)
        {
            try
            {
                var cpr = e.Packet;
                var conn = _im.getByUcid(cpr.UCID);

                if (conn == null)
                    return;
                
                conn.PName = cpr.PName;
                conn.Plate = cpr.Plate;
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"IS_CPR", ex);
            }
        }
    }
}
