﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Globalization;
using static LFSCompanion.Application.Classes.Variables;
using InSimDotNet.Helpers;
using LFSCompanion.Application.Classes;
using LFSCompanion.Insim.Models;
using LFSCompanion.Application.SQL;
using LFSCompanion.Insim.Classes;

namespace LFSCompanion.Insim.Events
{
    public class RST
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _insimMethods;
        private SQLContext _sqlContext;
        private StringFormatter _stringFormatter;
        private LapManager _lapManager;

        public RST(Variables variables,
            ExceptionProcessor exc,
            InSimMethods insimMethods,
            SQLContext sqlContext,
            StringFormatter stringFormatter,
            LapManager lapManager)
        {
            _var = variables;
            _exc = exc;
            _insimMethods = insimMethods;
            _sqlContext = sqlContext;
            _stringFormatter = stringFormatter;
            _lapManager = lapManager;
        }

        /// <summary>
        /// The race is starting
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task RaceSTart(object o, PacketEventArgs<IS_RST> e)
        {
            try
            {
                foreach (var conn in _var.Players.Values)
                {
                    conn.Racer.trackedLapTime = TimeSpan.Zero;
                }
                
                _var.trackedLaptime = TimeSpan.Zero;
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET,
                    $"IS_RST", ex);
            }
        }
    }
}
