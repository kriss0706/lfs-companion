﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InSimDotNet.Helpers;
using BS = InSimDotNet.Packets.ButtonStyles;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Diagnostics.SymbolStore;
using System.Reflection;
using static System.Formats.Asn1.AsnWriter;
using System.Xml.Linq;
using LFSCompanion.Application.SQL.Models;
using Microsoft.Data.Sqlite;
using LFSCompanion.Application.Extensions;
using LFSCompanion.Application.Models.Enum;
using LFSCompanion.Application.Classes;
using LFSCompanion.Application.Utils;
using LFSCompanion.Insim.Classes;
using LFSCompanion.Application.SQL;
using LFSCompanion.Insim.Models;
using LFSCompanion.Insim.Models.Enum;
using static LFSCompanion.Application.SQL.Seeders.SeederModels;
using static LFSCompanion.Insim.Models.Enum.MoveOrResizeEnum;
using static LFSCompanion.Insim.Models.Enum.WindowStateEnum;
using CarContact = LFSCompanion.Application.SQL.Models.CarContact;

namespace LFSCompanion.Insim.Events
{
    internal class BTC
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _im;
        private SQLContext _sqlContext;
        private ButtonManager _btn;
        private WindowFactory _window;
        private Migration _migration;
        private TemplateHelper _template;
        private DirectoryFactory _dir;
        private StringFormatter _stringFormatter;
        private CarInfo _carInfo;
        private LapManager _lapManager;
        private LFSW _lfsw;

        public BTC(Variables variables, ExceptionProcessor exc, InSimMethods im,
            SQLContext sqlContext, ButtonManager btn, WindowFactory window,
            Migration migration, TemplateHelper template, DirectoryFactory directoryFactory,
            StringFormatter stringFormatter, CarInfo carInfo, LapManager lapManager, LFSW lfsw)
        {
            _var = variables;
            _exc = exc;
            _im = im;
            _sqlContext = sqlContext;
            _btn = btn;
            _window = window;
            _migration = migration;
            _template = template;
            _dir = directoryFactory;
            _stringFormatter = stringFormatter;
            _carInfo = carInfo;
            _lapManager = lapManager;
            _lfsw = lfsw;
        }

        /// <summary>
        /// Manage button clicks within the insim
        /// </summary>
        /// <param name="o"></param>
        /// <param name="btc"></param>
        /// <returns></returns>
        public async Task buttonClick(object o, PacketEventArgs<IS_BTC> btc)
        {
            try
            {
                var conn = _im.getByUcid(_var.UCID);

                if (conn == null)
                    return;

                if (!conn.IsLocal)
                    return;
                
                byte[] buttonClickWhitelist = { 54,55,56,57,58,59,60,200 };
                var rmb = btc.Packet.CFlags.HasFlag(ClickFlags.ISB_RMB);
                var lmb = btc.Packet.CFlags.HasFlag(ClickFlags.ISB_LMB);
                var ctrl = btc.Packet.CFlags.HasFlag(ClickFlags.ISB_CTRL);
                var shift = btc.Packet.CFlags.HasFlag(ClickFlags.ISB_SHIFT);
                var clickedId = btc.Packet.ClickID;

                // Automatically set selected IDs
                #region ' Set selected id automatically '
                if (_var.InCustomizeUI)
                {
                    if (lmb)
                    {
                        #region Detect Button Click
                        foreach (var item in _var.ButtonList)
                        {
                            // Check if the primary click ID matches the clicked ID
                            if (item.PrimaryClickID == clickedId)
                            {
                                // Handle the case where the primary click ID matches
                                if (_var.SelectedClickIDs.Contains(item.PrimaryClickID))
                                {
                                    _var.SelectedClickIDs.Remove(item.PrimaryClickID);
                                    _var.LastSelectedId = 0;
                                }
                                else
                                {
                                    _var.SelectedClickIDs.Add(item.PrimaryClickID);
                                    _var.LastSelectedId = item.PrimaryClickID;
                                }

                                if (_var.AppEnvironment == EnvironmentEnum.DEVELOPMENT)
                                    Console.WriteLine($"Primary Click ID Match - Name: [{item.Name}], Click ID: [{item.PrimaryClickID}]");
                            }

                            // Check if any of the children's click IDs match the clicked ID
                            if (item.ClickIDs == null || item.ClickIDs.All(child => child.ClickID != clickedId))
                                continue;
                            
                            // Handle the case where a child's click ID matches
                            var matchingChildren = item.ClickIDs.Where(child => child.ClickID == clickedId);
                            foreach (var child in matchingChildren)
                            {
                                if (_var.SelectedClickIDs.Contains(item.PrimaryClickID))
                                {
                                    _var.SelectedClickIDs.Remove(item.PrimaryClickID);
                                    _var.LastSelectedId = 0;
                                }
                                else
                                {
                                    _var.SelectedClickIDs.Add(item.PrimaryClickID);
                                    _var.LastSelectedId = item.PrimaryClickID;
                                }

                                if (_var.AppEnvironment == EnvironmentEnum.DEVELOPMENT)
                                    Console.WriteLine($@"Child Click ID Match - Name: [{item.Name}], Child Name: [{child.Name}], Click ID: [{child.ClickID}]");
                            }
                        }
                        #endregion
                        #region Width/Height Check

                        /// <summary>
                        /// Set dimensions to 0 if the list is empty
                        /// </summary>
                        if (_var.SelectedClickIDs.Count == 0)
                        {
                            _var.SelectedSize_W = 0;
                            _var.SelectedSize_H = 0;
                        }
                        #endregion
                    }
                    else if (rmb)
                    {
                        #region Toggle Visibility
                        
                        foreach (var item in _var.ButtonList)
                        {
                            // Check if the primary click ID matches the clicked ID
                            if (item.PrimaryClickID == clickedId)
                                item.IsEnabled = !item.IsEnabled;
                            
                            // Check if any of the children's click IDs match the primary click ID
                            if (item.ClickIDs == null || item.ClickIDs.All(child => child.ClickID != clickedId))
                                continue;
                            
                            #region Handle Children
                            // Handle the case where a child's click ID matches
                            var matchingChildren = item.ClickIDs
                                .Where(child => child.ClickID == clickedId);

                            foreach (var child in matchingChildren)
                                item.IsEnabled = !item.IsEnabled;
                            #endregion
                        }
                        #endregion
                    }
                }

                // Initialization is specified in millisecond timer method
                #endregion

                switch (btc.Packet.ClickID)
                {
                    // Customize UI button is clicked
                    case 4:
                        if (_var.InCustomizeUI == false)
                        {
                            #region Open Customize UI
                            _var.InCustomizeUI = true;
                            _var.MoveOrResize = MOVE;

                            #endregion
                        }
                        break;

                    case 5:
                        // Settings button is clicked
                        if (_var.InSettings == false)
                        {
                            #region Open Settings

                            await _window.RacePositions(CLOSE);
                            _var.InSettings = true;
                            _var.SettingsPageNumber = 1;
                            #endregion
                        }
                        break;

                    case 6:
                        {
                            // Messages button
                            #region Sidemenu => Messages

                            _var.InMessages = true;
                            
                            await _window.RacePositions(CLOSE);
                                
                            // Global messages button is clicked in Messages Panel
                            _var.MessageTypes = MessageTypeEnum.GLOBAL;
                            _var.MessagesSkip = 0;
                            _var.MessagesPageNumber = 1;
                            _var.MessagesSearchWord = string.Empty;
                            _var.MessagesOnlyLocalData = false;

                            await _im.updateLists();

                            _var.MessagesRecords = _var.GlobalMessages.Count;
                            
                            await _window.MessagesPanel(OPEN);

                            #endregion
                        }
                        break;

                    case 7:
                        // Laptimes button
                        #region Sidemenu - Laptimes button
                        if (lmb && !_var.InLaptimes)
                        {
                            _var.InLaptimes = true;
                            await _window.RacePositions(CLOSE);
                            
                            _var.MessagesSkip = 0;
                            _var.MessagesPageNumber = 1;
                            await _window.laptimePanel(OPEN);
                        }

                        if (rmb)
                        {
                            if (conn.CarId == null || _var.TrackName == string.Empty)
                                return;
                            
                            #region Personal Best
                            
                            var lap = _lapManager.getPb(_var.TrackName, conn.CarId).Result;

                            if (lap == null)
                            {
                                await _im.LMsg($"No laptime found for {conn.CarId} on {_var.TrackName}");
                                return;
                            }

                            // Vehicle check if the record doesnt have a vehicle
                            if (lap.Vehicle == null)
                                return;

                            var pb = await _stringFormatter.getRaceTime(lap.Laptime);
                            var pbVehicle = await _carInfo.getFullVehicleName(lap.Vehicle);

                            await _im.LMsg($"^8PB: ^7{pb} ^8with ^7{pbVehicle} @ {lap.Track}", "^3[C] ");

                            #endregion
                            
                            #region Theoretical PB

                            var tPb = _lapManager.getPredictedPb(_var.TrackName, conn).Result;
                            await _im.LMsg($"^8Theoretical PB: ^3{await _stringFormatter.getRaceTime(tPb)}");

                            #endregion
                        }
                        #endregion
                        break;

                    // DEBUG BUTTON IN THE SIDEMENU
                    case 8:
                        if (lmb)
                        {
                            await _im.LMsg("Sent to console!");
                            
                            foreach (var player in _var.Players.Values)
                            {
                                Console.WriteLine($@"{StringHelper.StripColors(player.PName)} ({player.UName})");
                            }
                        }

                        if (rmb)
                        {
                            
                        }
                        break;

                    case 53:
                        #region Messages
                        if (_var.InMessages)
                        {
                            // Global messages button is clicked in Messages Panel
                            _var.MessageTypes = MessageTypeEnum.GLOBAL;
                            _var.MessagesSkip = 0;
                            _var.MessagesPageNumber = 1;

                            await _im.updateLists();
                            
                            await _window.MessagesPanel(OPEN);
                        }
                        #endregion
                        break;

                    case 54:
                        if (_var.InCustomizeUI)
                        {
                            // Arrow up is clicked

                            #region Move/Resize
                            
                            foreach (var x in _var.ButtonList)
                            {
                                if (!_var.SelectedClickIDs.Contains(x.PrimaryClickID)) continue;

                                if (_var.MoveOrResize == MOVE)
                                {
                                    var loc = _btn.Location(x.Name);
                                    loc.Top = _im.buttonPosition(AddSubtract.SUBTRACT, loc.Top, btc.Packet);
                                }
                                else
                                {
                                    var loc = _btn.Size(x.Name);
                                    loc.Height = _im.buttonPosition(AddSubtract.SUBTRACT, loc.Height, btc.Packet);
                                }
                            }

                            #endregion
                        }
                        else if (_var.InMessages)
                        {
                            #region Players > Messages Panel
                            // PLAYERS button is clicked in Messages Panel
                            _var.MessageTypes = MessageTypeEnum.PLAYERS;
                            _var.MessagesSkip = 0;
                            _var.MessagesPageNumber = 1;

                            await _im.updateLists();
                            
                            await _window.MessagesPanel(OPEN);
                            #endregion
                        }
                        break;
                    
                    case 55:
                        if (_var.InCustomizeUI)
                        {
                            // Arrow left
                            
                            #region Move/Resize
                            
                            foreach (var x in _var.ButtonList)
                            {
                                if (!_var.SelectedClickIDs.Contains(x.PrimaryClickID)) continue;

                                if (_var.MoveOrResize == MOVE)
                                {
                                    var loc = _btn.Location(x.Name);
                                    loc.Left = _im.buttonPosition(AddSubtract.SUBTRACT, loc.Left, btc.Packet);
                                }
                                else
                                {
                                    var loc = _btn.Size(x.Name);
                                    loc.Width = _im.buttonPosition(AddSubtract.SUBTRACT, loc.Width, btc.Packet);
                                }
                            }

                            #endregion
                        }
                        else if (_var.InSettings)
                        {
                            #region In Settings
                            if (_var.SettingsPageNumber == 1)
                            {
                                // Set Menu Display to Parked
                                await _sqlContext.SaveSettingAsync("MenuDisplay", "parked");
                                _var.SettingsMenuDisplay = "parked";
                            }
                            else if (_var.SettingsPageNumber == 2)
                            {
                                await _sqlContext.SaveSettingAsync("AutoLights", "true");
                                _var.AutoLightsEnabled = true;
                            }
                            else if (_var.SettingsPageNumber == 3)
                            {
                                await _sqlContext.SaveSettingAsync("ConsoleChat", "true");
                                _var.ConsoleChat = true;
                            }
                            #endregion
                        }
                        else if (_var.InMessages)
                        {
                            #region Car Contact > Messages Panel
                            // CAR_CONTACT button is clicked in Messages Panel
                            _var.MessageTypes = MessageTypeEnum.CARCONTACT;
                            _var.MessagesSkip = 0;
                            _var.MessagesPageNumber = 1;
                            
                            await _im.updateLists();
                            
                            await _window.MessagesPanel(OPEN);
                            
                            #endregion
                        }
                        else if (_var.InImportTemplate)
                        {
                            #region Directory Variables
                            string importFolder = await _dir.GetDirectory(InsimDirectoryEnum.TEMPLATE_IMPORTS);
                            #endregion
                            #region Check folder existence
                            if (!Directory.Exists(importFolder))
                            {
                                Console.WriteLine($"Import folder was not found, creating..");
                                Directory.CreateDirectory(importFolder);
                            }
                            #endregion
                            #region MyRegion
                            DirectoryInfo dirInfo = new DirectoryInfo(importFolder);

                            // Get all csv files
                            FileInfo[] Files = dirInfo.GetFiles("*.csv");
                            string str = "";

                            if (Files.Length == 0)
                            {
                                await _im.LMsg($"^6Please move the file you would like to import into");
                                await _im.LMsg($"^6the ^7Template Imports ^6folder in the insim directory.");
                            }
                            else
                            {
                                byte ImportedFiles = 0;
                                string PluralText = "";

                                // Import all items in the folder
                                foreach (FileInfo file in Files)
                                {
                                    if (_var.TemplateSlots + Files.Count() > 5)
                                    {
                                        await _im.LMsg($"^1Exceeding template slot limit. Import aborted.");
                                        return;
                                    }

                                    #region Format File Name
                                    // Format from 'Template_Test.csv' to 'Test'
                                    // Pass value to ImportCSV() method for processing
                                    string[] FileName = file.Name.Split(".");

                                    if (file.Name.StartsWith("Template"))
                                    {
                                        string[] TemplateName = FileName[0].Split("_");
                                        FileName[0] = TemplateName[1];
                                    }
                                    #endregion

                                    await _sqlContext.ImportCSV(FileName[0], file.FullName);

                                    await _sqlContext.AddTemplate(FileName[0]);

                                    ImportedFiles++;
                                    file.Delete();
                                }

                                if (ImportedFiles > 1)
                                    PluralText = "s";

                                await _im.LMsg($"^2Imported ^7{ImportedFiles} ^2template{PluralText}");

                                _var.InImportTemplate = false;
                                await _window.ImportTemplate(CLOSE);
                                _var.InCustomizeUI = true;
                            }
                            #endregion
                        }
                        break;

                    case 56:
                        if (_var.InCustomizeUI)
                        {
                            // Arrow down
                            #region Move/Resize
                            
                            foreach (var x in _var.ButtonList)
                            {
                                if (!_var.SelectedClickIDs.Contains(x.PrimaryClickID)) continue;

                                if (_var.MoveOrResize == MOVE)
                                {
                                    var loc = _btn.Location(x.Name);
                                    loc.Top = _im.buttonPosition(AddSubtract.ADD, loc.Top, btc.Packet);
                                }
                                else
                                {
                                    var loc = _btn.Size(x.Name);
                                    loc.Height = _im.buttonPosition(AddSubtract.ADD, loc.Height, btc.Packet);
                                }
                            }

                            #endregion
                        }
                        else if (_var.InSettings)
                        {
                            // Apply setting per page
                            #region Setting
                            if (_var.SettingsPageNumber == 1)
                            {
                                await _sqlContext.SaveSettingAsync("MenuDisplay", "always");
                                _var.SettingsMenuDisplay = "always";
                            }
                            else if (_var.SettingsPageNumber == 2)
                            {
                                await _sqlContext.SaveSettingAsync("AutoLights", "false");
                                _var.AutoLightsEnabled = false;
                            }
                            else if (_var.SettingsPageNumber == 3)
                            {
                                await _sqlContext.SaveSettingAsync("ConsoleChat", "false");
                                _var.ConsoleChat = false;
                            }
                            #endregion
                        }
                        break;

                    case 57:
                        if (_var.InCustomizeUI)
                        {
                            // Arrow right
                            
                            #region Move/Resize
                            
                            foreach (var x in _var.ButtonList)
                            {
                                if (!_var.SelectedClickIDs.Contains(x.PrimaryClickID)) continue;

                                if (_var.MoveOrResize == MOVE)
                                {
                                    var loc = _btn.Location(x.Name);
                                    loc.Left = _im.buttonPosition(AddSubtract.ADD, loc.Left, btc.Packet);
                                }
                                else
                                {
                                    var loc = _btn.Size(x.Name);
                                    loc.Width = _im.buttonPosition(AddSubtract.ADD, loc.Width, btc.Packet);
                                }
                            }

                            #endregion
                        }
                        else if (_var.InAddTemplate)
                        {
                            // Proceed button is clicked in Add Template Panel
                            try
                            {
                                #region Proceed Button
                                if (_var.TemplateName == "")
                                {
                                    await _im.Error($"Please enter a name for the template.");
                                    return;
                                }
                                
                                var sqlTemplateName = $"Template_{StringHelper.StripColors(_var.TemplateName).Replace(' ', '_')}";

                                // Name is already in use
                                if (await _sqlContext.TableExist(sqlTemplateName))
                                {
                                    await _im.Error($"Could not create template, name is already in use!");
                                    return;
                                }

                                await _sqlContext.AddTemplate(_var.TemplateName);
                                _var.InAddTemplate = false;
                                _var.InCustomizeUI = true;
                                
                                await _migration.CreateTable(sqlTemplateName, _var.SQLColumns_ButtonPositions);
                                await _migration.MergeTables("ButtonPositions", sqlTemplateName);

                                #region ' Operation complete, go back to customize_ui '
                                await _window.AddTemplate(CLOSE);
                                #endregion

                                await _im.LMsg($"^2Template created.");
                                _var.TemplateName = string.Empty;

                                #endregion
                            }
                            catch (Exception ex)
                            {
                                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET,
                                    $"IS_BTC - AddTemplate", ex);
                            }
                        }
                        else if (_var.InManageTemplate)
                        {
                            #region Delete Template

                            if (_var.SelectedTemplate == null || _var.SelectedTemplate.Name == null)
                                return;

                            // Remove row from Templates table
                            await _sqlContext.DeleteRow("Templates", "Name", _var.SelectedTemplate.Name);

                            // Drop table
                            await _migration.DeleteTable($"Template_{_var.SelectedTemplate.Name}");

                            _var.InManageTemplate = false;
                            await _window.ManageTemplate(CLOSE);
                            _var.InCustomizeUI = true;

                            #endregion
                        }
                        break;

                    case 58:
                        // Move button in customize UI
                        if (_var.InCustomizeUI == true)
                        {
                            _var.MoveOrResize = MOVE;
                        }
                        else if (_var.InSettings)
                        {
                            #region In Settings
                            if (_var.SettingsPageNumber == 2)
                            {
                                byte minValue = 0;
                                byte maxValue = 4;

                                if (lmb)
                                {
                                    if (_var.SettingsDatetimeFormatIncrement < maxValue)
                                        _var.SettingsDatetimeFormatIncrement++;
                                    else
                                        _var.SettingsDatetimeFormatIncrement = minValue;
                                }
                                else if (rmb)
                                {
                                    if (_var.SettingsDatetimeFormatIncrement > minValue)
                                        _var.SettingsDatetimeFormatIncrement--;
                                    else
                                        _var.SettingsDatetimeFormatIncrement = maxValue;
                                }

                                #region DateTime
                                switch (_var.SettingsDatetimeFormatIncrement)
                                {
                                    case 0: // DATETIME
                                        _var.DateTimeFormat = DateTimeFormatEnum.DATE;
                                        await _sqlContext.SaveSettingAsync("DateTimeFormat", DateTimeFormatEnum.DATE.ToString());
                                        break;

                                    case 1: // DATE
                                        _var.DateTimeFormat = DateTimeFormatEnum.TIME;
                                        await _sqlContext.SaveSettingAsync("DateTimeFormat", DateTimeFormatEnum.TIME.ToString());
                                        break;

                                    case 2: // TIME
                                        _var.DateTimeFormat = DateTimeFormatEnum.LONG_DATE;
                                        await _sqlContext.SaveSettingAsync("DateTimeFormat", DateTimeFormatEnum.LONG_DATE.ToString());
                                        break;

                                    case 3: // LONG_DATE
                                        _var.DateTimeFormat = DateTimeFormatEnum.LONG_DATE_TIME;
                                        await _sqlContext.SaveSettingAsync("DateTimeFormat", DateTimeFormatEnum.LONG_DATE_TIME.ToString());
                                        break;

                                    case 4: // LONG_DATE_TIME
                                        _var.DateTimeFormat = DateTimeFormatEnum.DATETIME;
                                        await _sqlContext.SaveSettingAsync("DateTimeFormat", DateTimeFormatEnum.DATETIME.ToString());
                                        break;
                                }
                                #endregion
                            }
                            else if (_var.SettingsPageNumber == 3)
                            {
                                await _sqlContext.SaveSettingAsync("AutomaticUpdates", "true");
                            }
                            #endregion
                        }
                        else if (_var.InTemplatesOverview)
                        {
                            // Add Template button is clicked in Templates Panel
                            _var.InTemplatesOverview = false;
                            _var.InAddTemplate = true;

                            await _window.Templates(CLOSE);
                            await _window.AddTemplate(OPEN);
                        }
                        else if (_var.InManageTemplate)
                        {
                            #region Overwrite Template

                            if (_var.SelectedTemplate == null || _var.SelectedTemplate.Name == null)
                                return;

                            await _sqlContext.ClearTable($"Template_{_var.SelectedTemplate.Name}");

                            await _migration.MergeTables("ButtonPositions", $"Template_{_var.SelectedTemplate.Name}");

                            _var.InManageTemplate = false;
                            await _window.ManageTemplate(CLOSE);

                            _var.InCustomizeUI = true;

                            #endregion
                        }
                        break;

                    case 59:
                        // Resize button in customize UI
                        #region Resize Button
                        if (_var.InCustomizeUI == true)
                            _var.MoveOrResize = RESIZE;
                        else
                        {
                            if (_var.InSettings == true)
                            {
                                #region In Settings
                                if (_var.SettingsPageNumber == 3)
                                {
                                    await _sqlContext.SaveSettingAsync("AutomaticUpdates", "false");
                                }
                                #endregion
                            }
                            else if (_var.InManageTemplate)
                            {
                                #region Export Template
                                if (_var.SelectedTemplate == null || _var.SelectedTemplate.Name == null)
                                    return;

                                bool hasCompleted = await _sqlContext.ExportTableCSV($"Template_{_var.SelectedTemplate.Name}");

                                if (hasCompleted)
                                {
                                    await _im.LMsg($"^2Export complete, file path: " +
                                                             $"^7<InSim Folder>/{InsimDirectoryEnum.TEMPLATE_EXPORTS.GetDescription()}/Template_{_var.SelectedTemplate.Name}.csv");

                                    _var.BSExportButton = BS.ISB_DARK;
                                    _var.ExportDisabledCounter = 1;

                                    await _window.ManageTemplate(OPEN);
                                }
                                else
                                {
                                    await _im.LMsg($"^1The export process has failed.");
                                }
                                #endregion
                            }
                        }
                        #endregion
                        break;

                    case 60:
                        if (_var.InCustomizeUI)
                        {
                            #region Multiplier
                            // Multiplier button in customize UI
                            if (btc.Packet.CFlags == ClickFlags.ISB_LMB)
                            {
                                if (_var.CustomizeMultiplier == 1)
                                    _var.CustomizeMultiplier = 3;
                                else if (_var.CustomizeMultiplier == 3)
                                    _var.CustomizeMultiplier = 5;
                                else if (_var.CustomizeMultiplier == 5)
                                    _var.CustomizeMultiplier = 10;
                                else if (_var.CustomizeMultiplier == 10)
                                    _var.CustomizeMultiplier = 20;
                                else if (_var.CustomizeMultiplier == 20)
                                    _var.CustomizeMultiplier = 1;
                            }
                            else if (btc.Packet.CFlags == ClickFlags.ISB_RMB)
                            {
                                if (_var.CustomizeMultiplier == 1)
                                    _var.CustomizeMultiplier = 20;
                                else if (_var.CustomizeMultiplier == 20)
                                    _var.CustomizeMultiplier = 10;
                                else if (_var.CustomizeMultiplier == 10)
                                    _var.CustomizeMultiplier = 5;
                                else if (_var.CustomizeMultiplier == 5)
                                    _var.CustomizeMultiplier = 3;
                                else if (_var.CustomizeMultiplier == 3)
                                    _var.CustomizeMultiplier = 1;
                            }
                            #endregion
                        }
                        
                        #region In Laptimes - CURRENT_VEHICLE
                        else if (_var.InLaptimes)
                        {
                            // Toggle SORT_BY_VEHICLE
                            _var.LaptimeSortVehicle = !_var.LaptimeSortVehicle;

                            await _window.laptimePanel(CLOSE_CONTENT);
                            await _window.laptimePanel(OPEN);
                        }
                        #endregion
                        break;

                    case 61:
                        if (_var.InSettings == true)
                        {
                            #region In Settings
                            if (_var.SettingsPageNumber == 1)
                            {
                                // User clicked kmh button
                                await _sqlContext.SaveSettingAsync("SpeedoUnit", "kmh");
                                _var.SpeedoUnit = "kmh";
                            }
                            else if (_var.SettingsPageNumber == 2)
                            {
                                await _sqlContext.SaveSettingAsync("LaptimeMessages", "true");
                                _var.RacingMessagesEnabled = true;
                            }
                            #endregion
                        }
                        #region In Laptimes - CURRENT_TRACK
                        else if (_var.InLaptimes)
                        {
                            // Toggle SORT_BY_TRACK
                            _var.LaptimeSortTrack = !_var.LaptimeSortTrack;

                            await _window.laptimePanel(CLOSE_CONTENT);
                            await _window.laptimePanel(OPEN);
                        }
                        #endregion
                        break;

                    case 62:
                        if (_var.InCustomizeUI == true)
                        {
                            #region Move Customize Window
                            if (_var.MoveCustomizerWindow == MoveCustomizerEnum.NONE)
                            {
                                _var.MoveCustomizerWindow = MoveCustomizerEnum.MOVE;
                                _var.MoveOrResize = MOVE;
                            }
                            else
                                _var.MoveCustomizerWindow = MoveCustomizerEnum.NONE;
                            #endregion
                        }
                        else if (_var.InSettings == true)
                        {
                            #region In Settings
                            if (_var.SettingsPageNumber == 1)
                            {
                                // User clicked mph button'
                                await _sqlContext.SaveSettingAsync("SpeedoUnit", "mph");
                                _var.SpeedoUnit = "mph";
                            }
                            else if (_var.SettingsPageNumber == 2)
                            {
                                await _sqlContext.SaveSettingAsync("LaptimeMessages", "false");
                                _var.RacingMessagesEnabled = false;
                            }
                            #endregion
                        }
                        #region In Laptimes - BEST_PB
                        else if (_var.InLaptimes)
                        {
                            // Toggle SORT_BY_BEST_PB
                            _var.LaptimeSortBestPB = !_var.LaptimeSortBestPB;

                            await _window.laptimePanel(CLOSE_CONTENT);
                            await _window.laptimePanel(OPEN);
                        }
                        #endregion
                        break;

                    case 63:
                        #region Help Button > Customize UI
                        if (_var.InCustomizeUI)
                        {
                            _var.InCustomizeUI = false;
                            _var.InCustomizeHelp = true;
                            await _window.RacePositions(CLOSE);
                        }
                        #endregion
                        break;

                    case 64:
                        #region Open Templates
                        if (_var.InCustomizeUI)
                        {
                            #region Hide Customize UI
                            _var.InCustomizeUI = false;
                            _var.SelectedClickIDs.Clear();
                            _var.MoveCustomizerWindow = MoveCustomizerEnum.NONE;

                            _var.Templates = await _sqlContext.GetTemplates();

                            await _im.ClearBtnRange(51, 64);
                            await _im.ClearBtn(200);
                            #endregion

                            await _sqlContext.UpdateButtonPositions();

                            _var.InTemplatesOverview = true;
                            _var.InAddTemplate = false;
                            _var.InManageTemplate = false;

                            _var.TemplateSlots = await _sqlContext.CountRows("Templates");

                            await _window.Templates(OPEN);
                        }
                        #endregion

                        #region In Settings
                        if (_var.InSettings == true)
                        {
                            // User clicked litres button
                            await _sqlContext.SaveSettingAsync("FuelUnit", "litres");
                            _var.FuelUnit = "litres";
                        }
                        #endregion
                        break;

                    case 65:
                        #region In Settings
                        if (_var.InSettings == true)
                        {
                            // User clicked percent button
                            await _sqlContext.SaveSettingAsync("FuelUnit", "percent");
                            _var.FuelUnit = "percent";
                        }
                        #endregion
                        break;

                    case 67:
                        #region In Settings
                        if (_var.InSettings)
                        {
                            // User wants to use classic gear format
                            await _sqlContext.SaveSettingAsync("GearFormat", "classic");
                            _var.GearFormat = "classic";
                        }
                        #endregion
                        break;

                    case 68:
                        #region In Settings
                        if (_var.InSettings)
                        {
                            // User wants to use modern gear format
                            await _sqlContext.SaveSettingAsync("GearFormat", "modern");
                            _var.GearFormat = "modern";
                        }
                        #endregion
                        break;

                    case 69:
                        #region In Templates
                        if (_var.InTemplatesOverview)
                        {
                            await Task.Run(async () =>
                            {
                                _var.SelectedTemplate = await _template.GetTemplateSelection(0);
                            });

                            if (_var.SelectedTemplate == null)
                                return;

                            _var.InTemplatesOverview = false;
                            _var.InManageTemplate = true;

                            await _window.Templates(CLOSE);
                            await _window.ManageTemplate(OPEN);
                        }
                        #endregion
                        break;

                    case 70:
                        #region In Settings
                        if (_var.InSettings)
                        {
                            // User wants to use classic rpm style
                            await _sqlContext.SaveSettingAsync("RpmStyle", "classic");
                            _var.RpmStyle = "classic";
                        }
                        #endregion
                        #region In Templates
                        else if (_var.InTemplatesOverview)
                        {
                            await Task.Run(async () =>
                            {
                                _var.SelectedTemplate = await _template.GetTemplateSelection(1);
                            });

                            if (_var.SelectedTemplate == null)
                                return;

                            _var.InTemplatesOverview = false;
                            _var.InManageTemplate = true;

                            await _window.Templates(CLOSE);
                            await _window.ManageTemplate(OPEN);
                        }
                        #endregion
                        break;

                    case 71:
                        #region In Settings
                        if (_var.InSettings)
                        {
                            // User wants to use modern rpm style
                            await _sqlContext.SaveSettingAsync("RpmStyle", "modern");
                            _var.RpmStyle = "modern";
                        }
                        #endregion
                        #region In Templates
                        else if (_var.InTemplatesOverview)
                        {
                            await Task.Run(async () =>
                            {
                                _var.SelectedTemplate = await _template.GetTemplateSelection(2);
                            });

                            if (_var.SelectedTemplate == null)
                                return;

                            _var.InTemplatesOverview = false;
                            _var.InManageTemplate = true;

                            await _window.Templates(CLOSE);
                            await _window.ManageTemplate(OPEN);
                        }
                        #endregion
                        break;

                    case 72:
                        #region In Templates
                        if (_var.InTemplatesOverview)
                        {
                            await Task.Run(async () =>
                            {
                                _var.SelectedTemplate = await _template.GetTemplateSelection(3);
                            });

                            if (_var.SelectedTemplate == null)
                                return;

                            _var.InTemplatesOverview = false;
                            _var.InManageTemplate = true;

                            await _window.Templates(CLOSE);
                            await _window.ManageTemplate(OPEN);
                        }
                        #endregion
                        break;

                    case 73:
                        #region In Settings
                        if (_var.InSettings)
                        {
                            _var.GreetingsEnabled = true;
                            await _sqlContext.SaveSettingAsync("GreetingEnabled", "true");
                        }
                        #endregion
                        #region In Templates
                        else if (_var.InTemplatesOverview)
                        {
                            await Task.Run(async () =>
                            {
                                _var.SelectedTemplate = await _template.GetTemplateSelection(4);
                            });

                            if (_var.SelectedTemplate == null)
                                return;

                            _var.InTemplatesOverview = false;
                            _var.InManageTemplate = true;

                            await _window.Templates(CLOSE);
                            await _window.ManageTemplate(OPEN);
                        }
                        #endregion
                        break;

                    case 74:
                        #region In Settings
                        if (_var.InSettings)
                        {
                            _var.GreetingsEnabled = false;
                            await _sqlContext.SaveSettingAsync("GreetingEnabled", "false");
                        }
                        #endregion
                        #region In Templates
                        // Apply template #1
                        else if (_var.InTemplatesOverview)
                        {
                            await _template.ApplyTemplate(0);
                        }
                        #endregion
                        break;

                    case 75:
                        #region In Templates
                        // Apply template #2
                        if (_var.InTemplatesOverview)
                        {
                            await _template.ApplyTemplate(1);
                        }
                        #endregion
                        break;

                    case 76:
                        #region In Templates
                        // Apply template #3
                        if (_var.InTemplatesOverview)
                        {
                            await _template.ApplyTemplate(2);
                        }
                        #endregion
                        break;

                    case 77:
                        #region In Templates
                        // Apply template #4
                        if (_var.InTemplatesOverview)
                        {
                            await _template.ApplyTemplate(3);
                        }
                        #endregion
                        break;

                    case 78:
                        #region In Templates
                        // Apply template #5
                        if (_var.InTemplatesOverview)
                        {
                            await _template.ApplyTemplate(4);
                        }
                        #endregion
                        break;

                    case 79:
                        #region In Settings
                        if (_var.InSettings)
                        {
                            if (_var.SettingsPageNumber == 1)
                            {
                                _var.TotalFuel = true;
                                await _sqlContext.SaveSettingAsync("TotalFuel", "true");
                            }
                            else if (_var.SettingsPageNumber == 4)
                            {
                                _var.CarContactEnabled = true;
                                await _sqlContext.SaveSettingAsync("CarContact", "true");
                            }
                        }
                        #endregion
                        #region In Templates
                        // Apply template #2
                        else if (_var.InTemplatesOverview)
                        {
                            await _template.ApplyTemplate(1);
                        }
                        #endregion
                        break;

                    case 80:
                        #region In Settings
                        if (_var.InSettings)
                        {
                            if (_var.SettingsPageNumber == 1)
                            {
                                _var.TotalFuel = false;
                                await _sqlContext.SaveSettingAsync("TotalFuel", "false");
                            }
                            else if (_var.SettingsPageNumber == 4)
                            {
                                _var.CarContactEnabled = false;
                                await _sqlContext.SaveSettingAsync("CarContact", "false");
                            }
                        }
                        #endregion
                        break;

                    case 84:
                        #region In Messages
                        if (_var.InMessages)
                        {
                            await _im.DisplayMessage(0);
                        }
                        #endregion
                        break;

                    case 85:
                        #region In Messages
                        if (_var.InMessages)
                        {
                            await _im.DisplayMessage(1);
                        }
                        #endregion
                        break;

                    case 86:
                        #region In Messages
                        if (_var.InMessages)
                        {
                            await _im.DisplayMessage(2);
                        }
                        #endregion
                        break;

                    case 87:
                        #region In Messages
                        if (_var.InMessages)
                        {
                            await _im.DisplayMessage(3);
                        }
                        #endregion
                        break;

                    case 88:
                        #region In Messages
                        if (_var.InMessages)
                        {
                            await _im.DisplayMessage(4);
                        }
                        #endregion
                        break;

                    case 89:
                        #region In Messages
                        if (_var.InMessages)
                        {
                            await _im.DisplayMessage(5);
                        }
                        #endregion
                        break;

                    case 90:
                        #region In Messages
                        if (_var.InMessages)
                        {
                            await _im.DisplayMessage(6);
                        }
                        #endregion
                        #region In Templates
                        else if (_var.InTemplatesOverview && _var.InImportTemplate == false)
                        {
                            _var.InImportTemplate = true;
                            _var.InTemplatesOverview = false;

                            await _window.ManageTemplate(CLOSE);
                            await _window.ImportTemplate(OPEN);
                        }
                        #endregion
                        break;

                    case 91:
                        #region In Messages
                        if (_var.InMessages)
                        {
                            await _im.DisplayMessage(7);
                        }
                        #endregion
                        break;

                    case 92:
                        #region In Messages
                        if (_var.InMessages)
                        {
                            await _im.DisplayMessage(8);
                        }
                        #endregion
                        break;

                    case 93:
                        #region In Messages
                        if (_var.InMessages)
                        {
                            await _im.DisplayMessage(9);
                        }
                        #endregion
                        break;
                    
                    case 97:
                        #region In Messages
                        if (_var.InMessages)
                        {
                            _var.MessagesOnlyLocalData = !_var.MessagesOnlyLocalData;

                            await _im.updateLists();

                            await _window.MessagesPanel(OPEN);
                        }
                        #endregion
                        break;
                    
                    case 98:
                        #region In Messages
                        // In Messages - Clear search box
                        if (_var.InMessages)
                        {
                            _var.MessagesSearchWord = string.Empty;
                            
                            await _im.updateLists();
                            await _window.MessagesPanel(OPEN);
                        }
                        #endregion
                        break;

                    case 195:
                        #region In Messages
                        if (_var.InMessages)
                        {
                            _var.MessagesRelativeTime = false;
                            await _sqlContext.SaveSettingAsync("DateRelativeTime", "false");
                            await _window.MessagesPanel(OPEN);
                        }
                        #endregion
                        break;

                    case 196:
                        #region In Messages
                        if (_var.InMessages)
                        {
                            _var.MessagesRelativeTime = true;
                            await _sqlContext.SaveSettingAsync("DateRelativeTime", "true");
                            await _window.MessagesPanel(OPEN);
                        }
                        #endregion
                        break;

                    case 197:
                    {
                        #region In Settings

                        if (_var.InSettings == true)
                        {
                            // Go to the previous page
                            _var.SettingsPageNumber--;

                            await _im.ClearBtnRange(54, 80);
                        }

                        #endregion

                        #region In Messages

                        else if (_var.InMessages)
                        {
                            if (lmb)
                            {
                                // Go to previousPage if currentPage is set to 2 or more
                                if (_var.MessagesPageNumber > 1)
                                {
                                    _var.MessagesPageNumber -= 1;
                                    _var.MessagesSkip -= 10;
                                }
                            }
                            else if (rmb)
                            {
                                if (_var.MessagesPageNumber > 10)
                                {
                                    _var.MessagesPageNumber -= 10;
                                    _var.MessagesSkip -= 100;
                                }
                                else
                                {
                                    _var.MessagesSkip = 0;
                                    _var.MessagesPageNumber = 1;
                                }
                            }

                            await _im.updateLists();

                            await _window.MessagesPanel(OPEN);
                        }

                        #endregion

                        #region In Laptimes

                        else if (_var.InLaptimes)
                        {
                            if (lmb)
                            {
                                if (_var.MessagesPageNumber > 1)
                                {
                                    _var.MessagesPageNumber -= 1;
                                    _var.MessagesSkip -= 10;
                                }
                                else
                                    await _im.LMsg(
                                        $"^1You can't exceed the minimum number of pages! Out of range.");
                            }
                            else if (rmb)
                            {
                                if (_var.MessagesPageNumber > 10)
                                {
                                    _var.MessagesPageNumber -= 10;
                                    _var.MessagesSkip -= 100;
                                }
                                else
                                    await _im.LMsg(
                                        $"^1You can't exceed the minimum number of pages! Out of range.");
                            }

                            await _im.ClearBtnRange(70, 146);
                            await _window.laptimePanel(OPEN);
                        }

                        #endregion
                    }
                        break;

                    case 198:
                    {
                        #region inSettings
                        
                        if (_var.InSettings)
                        {
                            // Go to the next page
                            _var.SettingsPageNumber++;

                            await _im.ClearBtnRange(54, 80);
                        }
                        #endregion
                        
                        #region In Messages
                        else if (_var.InMessages)
                        {
                            if (lmb)
                            {
                                if (_var.MessagesPageNumber <= _var.MessagesMaxPages - 1)
                                {
                                    _var.MessagesPageNumber += 1;
                                    _var.MessagesSkip += 10;
                                }
                            }
                            else if (rmb)
                            {
                                if (_var.MessagesPageNumber <= _var.MessagesMaxPages - 10)
                                {
                                    _var.MessagesPageNumber += 10;
                                    _var.MessagesSkip += 100;
                                }
                                else
                                {
                                    _var.MessagesSkip = 0;
                                    _var.MessagesPageNumber = 1;
                                }
                            }

                            await _im.updateLists();
                            
                            await _window.MessagesPanel(OPEN);
                        }
                        #endregion
                        
                        #region In Laptimes
                        else if (_var.InLaptimes)
                        {
                            if (lmb)
                            {
                                if (_var.MessagesPageNumber <= _var.MessagesMaxPages - 1)
                                {
                                    _var.MessagesPageNumber += 1;
                                    _var.MessagesSkip += 10;
                                }
                                else
                                {
                                    await _im.LMsg($"^1You can't exceed the maximum number of pages! Out of range.");
                                }
                            }
                            else if (rmb)
                            {
                                if (_var.MessagesPageNumber <= _var.MessagesMaxPages - 10)
                                {
                                    _var.MessagesPageNumber += 10;
                                    _var.MessagesSkip += 100;
                                }
                                else
                                {
                                    await _im.LMsg($"^1You can't exceed the maximum number of pages! Out of range.");
                                }
                            }
                            await _im.ClearBtnRange(70, 146);
                            await _window.laptimePanel(OPEN);
                        }
                        #endregion
                        break;
                    }

                    case 200:
                        if (_var.InCustomizeUI)
                        {
                            #region Save Button > Customize UI
                            // Save button in customize UI
                            _var.InCustomizeUI = false;
                            _var.SelectedClickIDs.Clear();
                            _var.LastSelectedId = 0;
                            _var.MoveCustomizerWindow = MoveCustomizerEnum.NONE;

                            await _im.ClearBtnRange(51, 64);
                            await _im.ClearBtn(200);

                            await _sqlContext.UpdateButtonPositions();

                            #endregion
                        }
                        else if (_var.InSettings)
                        {
                            #region Close Button > Settings
                            // Close button
                            _var.InSettings = false;
                            await _im.ClearBtnRange(51, 81);
                            await _im.ClearBtnRange(197, 200);
                            #endregion
                        }
                        else if (_var.InMessages)
                        {
                            #region Close Button > Messages
                            // Close button
                            _var.InMessages = false;
                            await _window.MessagesPanel(CLOSE);
                            _var.MessagesSkip = 0;
                            #endregion
                        }
                        #region In Customize Help
                        else if (_var.InCustomizeHelp)
                        {
                            _var.InCustomizeHelp = false;
                            _var.InCustomizeUI = true;
                        }
                        #endregion
                        #region In Templates
                        else if (_var.InTemplatesOverview)
                        {
                            _var.InTemplatesOverview = false;
                            _var.InCustomizeUI = true;

                            await _window.Templates(CLOSE);
                        }
                        #endregion
                        #region In Manage Template
                        else if (_var.InManageTemplate)
                        {
                            _var.InManageTemplate = false;
                            _var.InTemplatesOverview = true;

                            await _window.ManageTemplate(CLOSE);
                            await _window.Templates(OPEN);
                        }
                        #endregion
                        #region In Add Template
                        else if (_var.InAddTemplate)
                        {
                            _var.InAddTemplate = false;
                            await _window.AddTemplate(CLOSE);
                            await _window.Templates(OPEN);
                            _var.InTemplatesOverview = true;
                        }
                        #endregion
                        #region In Import Template
                        else if (_var.InImportTemplate)
                        {
                            _var.InImportTemplate = false;
                            await _window.ImportTemplate(CLOSE);
                            await _window.Templates(OPEN);
                            _var.InTemplatesOverview = true;
                        }
                        #endregion
                        #region In Laps Panel
                        else if (_var.InLaptimes)
                        {
                            await _window.laptimePanel(CLOSE);
                            _var.InLaptimes = false;
                        }
                        #endregion
                        break;
                }
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET, "IS_BTC", ex);
            }
        }
    }
}
