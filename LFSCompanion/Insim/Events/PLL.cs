﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using InSimDotNet.Helpers;
using LFSCompanion.Insim.Models;
using LFSCompanion.Application.SQL;
using LFSCompanion.Application.Classes;
using LFSCompanion.Insim.Classes;

namespace LFSCompanion.Insim.Events
{
    internal class PLL
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _insimMethods;
        private SQLContext _sqlContext;
        private LapManager _lapManager;

        public PLL(Variables variables,
            ExceptionProcessor exc,
            InSimMethods insimMethods,
            SQLContext sqlContext,
            LapManager lapManager)
        {
            _var = variables;
            _exc = exc;
            _insimMethods = insimMethods;
            _sqlContext = sqlContext;
            _lapManager = lapManager;
        }

        /// <summary>
        /// Manage cars leaving the track (moved to spectate)
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task PlayerLeave(object o, PacketEventArgs<IS_PLL> e)
        {
            try
            {
                var conn = _insimMethods.GetByPlid(e.Packet.PLID);

                if (conn == null)
                    return;

                #region Race Positions Window
                
                conn.OnTrack = false;
                conn.Racer.renderPosition = false;
                
                #endregion
                
                if (!conn.IsLocal)
                    return;

                _lapManager.Gui.setBestPb(int.MinValue);
                _lapManager.setSplit(1, TimeSpan.Zero);
                _lapManager.setSplit(2, TimeSpan.Zero);
                _lapManager.setSplit(3, TimeSpan.Zero);
                _lapManager.Gui.setLaptime(TimeSpan.Zero);
                _lapManager.Gui.setLastSplit(TimeSpan.Zero);
                
                conn.PLID = 0;
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET,
                    $"IS_PLL", ex);
            }
        }
    }
}
