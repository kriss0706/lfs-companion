﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Globalization;
using InSimDotNet.Helpers;
using LFSCompanion.Insim.Classes;
using LFSCompanion.Application.SQL;
using LFSCompanion.Application.Classes;
using LFSCompanion.Insim.Models.Enum;
using CarContact = LFSCompanion.Application.SQL.Models.CarContact;

namespace LFSCompanion.Insim.Events
{
    internal class BTT
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _insimMethods;
        private SQLContext _sqlContext;
        private WindowFactory _window;
        private Migration _migration;

        public BTT(Variables variables,
            ExceptionProcessor exc,
            InSimMethods insimMethods,
            SQLContext sqlContext,
            WindowFactory window,
            Migration migration)
        {
            _var = variables;
            _exc = exc;
            _insimMethods = insimMethods;
            _sqlContext = sqlContext;
            _window = window;
            _migration = migration;
        }

        /// <summary>
        /// Manage button type in the insim
        /// </summary>
        /// <param name="o"></param>
        /// <param name="btt"></param>
        /// <returns></returns>
        public async Task ButtonType(object o, PacketEventArgs<IS_BTT> btt)
        {
            try
            {
                var e = btt.Packet;

                switch (e.ClickID)
                {
                    case 54:
                        if (_var.InManageTemplate)
                        {
                            string TemplateName = e.Text;

                            #region Exclude
                            string[] exclude = {
                                "0","1","2","3","4","5","6","7","8","9",
                                ",",".","-","@","$","€","£","^"
                            };

                            string[] blacklist = {
                                "cunt",
                                "bitch",
                                "black",
                                "nigg",
                                "fuck"
                            };
                            #endregion

                            if (_var.SelectedTemplate == null || _var.SelectedTemplate.Name == null)
                                return;

                            if (blacklist.Contains(e.Text))
                            {
                                await _insimMethods.LMsg($"^1Template name cannot contain any offensive words/slangs");
                                return;
                            }

                            if (exclude.Contains(e.Text))
                            {
                                await _insimMethods.LMsg($"^1Template name cannot contain any numbers or symbols");
                                return;
                            }

                            // Rename the table
                            await _migration.RenameTable($"Template_{_var.SelectedTemplate.Name}", $"Template_{TemplateName}");

                            // Rename the template in Templates table
                            await _sqlContext.RenameTemplate(_var.SelectedTemplate.Name, TemplateName);

                            _var.InManageTemplate = false;
                            await _window.ManageTemplate(WindowStateEnum.CLOSE);
                            _var.InCustomizeUI = true;
                        }
                        break;
                    
                    case 55:
                        #region Settings > LFSW Pubstat
                        if (!_var.InSettings)
                            return;

                        if (_var.SettingsPageNumber != 4)
                            return;

                        var inp = StringHelper.StripColors(e.Text);

                        if (inp.Length < 5)
                        {
                            await _insimMethods.LMsg($"^1Setting ^7{inp} ^1is not valid. Restored to default.");
                            await _sqlContext.SaveSettingAsync("lfswPubstat", "none");
                            _var.lfswPubstat = "none";
                            return;
                        }
                        
                        await _sqlContext.SaveSettingAsync("lfswPubstat", inp);
                        _var.lfswPubstat = inp;
                        
                        #endregion
                        break;

                    case 56:
                        if (_var.InAddTemplate)
                        {
                            // User can type in a template name
                            _var.TemplateName = e.Text;

                            // Update the textbox with the template name
                            await _window.AddTemplate(WindowStateEnum.OPEN);
                        }
                        else if (_var.InImportTemplate)
                        {
                            #region Import Template
                            string[] exclude = {
                                "0","1","2","3","4","5","6","7","8","9",
                                ",",".","-","@","$","€","£","^"
                            };

                            string[] blacklist = {
                                "cunt",
                                "bitch",
                                "black",
                                "nigg",
                                "fuck"
                            };

                            if (blacklist.Contains(e.Text))
                            {
                                await _insimMethods.LMsg($"^1Template name cannot contain any numbers or symbols.");
                                return;
                            }

                            if (exclude.Contains(e.Text))
                            {
                                await _insimMethods.LMsg($"^1Template name cannot contain any offensive words/slang");
                                return;
                            }

                            _var.TemplateName = e.Text;

                            // Update the textbox with the template name
                            await _window.ImportTemplate(WindowStateEnum.OPEN);
                            #endregion
                        }
                        break;

                    case 58:
                        if (_var.InSettings)
                        {
                            /// Combinations:
                            /// with space
                            /// without space
                            /// with litres (L)
                            /// with percent (%)

                            string input = e.Text.ToUpper();
                            string[] inputArr = new string[2]; // Initialize the inputArr array

                            // Has space
                            if (input.Contains(' '))
                            {
                                // Check for litres or percent
                                inputArr = input.Split(' ');
                            }
                            else if (input.Contains('L'))
                            {
                                // Is litres
                                inputArr[0] = input.Substring(0, input.Length - 1);
                                inputArr[1] = "L";
                            }
                            else if (input.Contains('%'))
                            {
                                // Is percent
                                inputArr[0] = input.Substring(0, input.Length - 1);
                                inputArr[1] = "%";
                            }
                            else
                            {
                                inputArr[0] = input;
                                inputArr[1] = "%";
                            }

                            bool success = double.TryParse(inputArr[0],NumberStyles.Any,CultureInfo.InvariantCulture,out double val);

                            if (success)
                            {
                                if (inputArr[1] == "L")
                                {
                                    // Litres
                                    await _sqlContext.SaveSettingAsync("FuelWarningType", "litres");
                                    _var.FuelWarningType = "litres";
                                }
                                else
                                {
                                    // Percent
                                    await _sqlContext.SaveSettingAsync("FuelWarningType", "percent");
                                    _var.FuelWarningType = "percent";
                                }

                                await _sqlContext.SaveSettingAsync("FuelWarningValue", $"{val}");
                                _var.FuelWarningValue = val;
                            }
                            else
                            {
                                await _insimMethods.LMsg($"^8Parsing failed. Format: ^155.05 L ^8or ^15.05 %");
                            }
                        }
                        break;

                    case 75:
                        if (_var.InSettings)
                        {
                            string input = e.Text;
                            _var.GreetingMessage = input;
                            await _sqlContext.SaveSettingAsync("Greeting", $"{input}");
                        }
                        break;

                    case 95:
                        if (_var.InMessages)
                        {
                            _var.MessagesSearchWord = e.Text;
                            _var.MessagesPageNumber = 1;

                            await _insimMethods.updateLists();

                            await _window.MessagesPanel(WindowStateEnum.OPEN);
                            
                            
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET,
                                       $"IS_BTT", ex);
            }
        }
    }
}
