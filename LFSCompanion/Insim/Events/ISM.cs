﻿using LFSCompanion.Application.SQL;
using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LFSCompanion.Application.SQL.Seeders.SeederModels;
using LFSCompanion.Application.Classes;
using LFSCompanion.Insim.Classes;

namespace LFSCompanion.Insim.Events
{
    internal class ISM
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _insimMethods;

        public ISM(Variables variables,
            ExceptionProcessor exc,
            InSimMethods insimMethods)
        {
            _var = variables;
            _exc = exc;
            _insimMethods = insimMethods;
        }

        /// <summary>
        /// Manage multiplayer notification
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task InSimMulti(object o, PacketEventArgs<IS_ISM> e)
        {
            try
            {
                _var.GreetingsTemporarilyDisabled = true;

                await _var.insim.SendAsync(new IS_TINY
                {
                    SubT = TinyType.TINY_NCN,
                    ReqI = 255
                });

                await _var.insim.SendAsync(new IS_TINY
                {
                    SubT = TinyType.TINY_NPL,
                    ReqI = 255
                });

                await _var.insim.SendAsync(new IS_TINY
                {
                    SubT = TinyType.TINY_SST,
                    ReqI = 255
                });

                /*                await _var.insim.SendAsync(new IS_TINY
                                {
                                    SubT = TinyType.TINY_MCI,
                                    ReqI = 255
                                });

                                await _var.insim.SendAsync(new IS_TINY
                                {
                                    SubT = TinyType.TINY_ISM,
                                    ReqI = 255
                                });*/
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET,
                    $"IS_ISM", ex);
            }
        }
    }
}
