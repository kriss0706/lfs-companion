﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using LFSCompanion.Insim.Models;
using LFSCompanion.Application.Classes;
using LFSCompanion.Application.SQL;
using LFSCompanion.Insim.Classes;
using LFSCompanion.Insim.Models.Enum;

namespace LFSCompanion.Insim.Events
{
    internal class NLP
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _insimMethods;
        private SQLContext _sqlContext;
        private REST_API _api;
        private LapManager _lapManager;
        private CarInfo _carInfo;
        private WindowFactory _window;
        public NLP(Variables variables,
            ExceptionProcessor exc,
            InSimMethods insimMethods,
            SQLContext sqlContext,
            LapManager lapManager,
            CarInfo carInfo,
            WindowFactory window)
        {
            _var = variables;
            _exc = exc;
            _insimMethods = insimMethods;
            _sqlContext = sqlContext;
            _api = new(_var, _exc);
            _lapManager = lapManager;
            _carInfo = carInfo;
            _window = window;
        }

        /// <summary>
        /// Node Lap
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task NodeLaP(object o, PacketEventArgs<IS_NLP> e)
        {
            try
            {
                foreach (var n in e.Packet.Info)
                {
                    if (n == null)
                        return;
                    
                    var conn = _insimMethods.GetByPlid(n.PLID);

                    if (conn == null)
                        return;
                    
                    // Commented out during testing
                    if (_var.AppEnvironment == EnvironmentEnum.DEVELOPMENT)
                        if (conn.PlayerType.HasFlag(PlayerTypes.PLT_AI))
                            return;
                    
                    // await _insimMethods.LMsg($"{conn.PName} ({conn.UName}) Pos{conn.Racer.Position} Lap{conn.Racer.Lap}");
                }
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET,
                    $"IS_NLP", ex);
            }
        }
    }
}
