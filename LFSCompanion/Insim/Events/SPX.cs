﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Globalization;
using static LFSCompanion.Application.Classes.Variables;
using InSimDotNet.Helpers;
using LFSCompanion.Application.Classes;
using LFSCompanion.Insim.Models;
using LFSCompanion.Application.SQL;
using LFSCompanion.Insim.Classes;

namespace LFSCompanion.Insim.Events
{
    public class SPX(
        Variables variables,
        ExceptionProcessor exc,
        InSimMethods insimMethods,
        SQLContext sqlContext,
        StringFormatter stringFormatter,
        LapManager lapManager)
    {
        private Variables _var = variables;
        private ExceptionProcessor _exc = exc;
        private InSimMethods _insimMethods = insimMethods;
        private SQLContext _sqlContext = sqlContext;
        private StringFormatter _stringFormatter = stringFormatter;
        private LapManager _lapManager = lapManager;

        /// <summary>
        /// Manage split times
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task SplitTime(object o, PacketEventArgs<IS_SPX> e)
        {
            try
            {
                var spx = e.Packet;
                var conn = _insimMethods.GetByPlid(e.Packet.PLID);

                if (conn == null)
                    return;
                
                #region Racer
                
                /*if (conn.IsLocal)
                    _var.trackedLaptime += spx.STime;
                else
                    conn.Racer.trackedLapTime += spx.STime;*/

                #endregion
                
                if (conn.PlayerType.HasFlag(PlayerTypes.PLT_AI))
                    return;
                
                if (!conn.IsLocal || spx.STime.TotalHours >= 1)
                    return;
                
                _lapManager.Gui.setLastSplitType(spx.Split);
                _lapManager.setSplit(spx.Split, spx.STime);
                _lapManager.Gui.setLastSplit(spx.STime);
                
                var lastSplitObj = _lapManager.getSplit(spx.Split);
                var splitObj = spx.STime;
                var splitPb = _lapManager.getPbBySplit(_var.TrackName, conn.CarId, spx.Split).Result;
                
                var splitDiff = await _stringFormatter.getRaceTimeDifference(
                    splitPb,
                    lastSplitObj.TotalMilliseconds);
                
                var splitTime = await _stringFormatter.getRaceTime(splitObj.TotalMilliseconds);
                
                if (_var.RacingMessagesEnabled)
                    await _insimMethods.LMsg($"^7Split {spx.Split}: ^3{splitTime}{splitDiff}");
                
               
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET,
                    $"IS_SPX", ex);
            }
        }
    }
}
