﻿using InSimDotNet.Packets;
using InSimDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using LFSCompanion.Insim.Models;
using LFSCompanion.Application.Classes;
using LFSCompanion.Application.SQL;
using LFSCompanion.Insim.Classes;

namespace LFSCompanion.Insim.Events
{
    internal class PLP
    {
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _insimMethods;
        private SQLContext _sqlContext;
        private REST_API _api;
        private LapManager _lapManager;
        private CarInfo _carInfo;

        public PLP(Variables variables,
            ExceptionProcessor exc,
            InSimMethods insimMethods,
            SQLContext sqlContext,
            LapManager lapManager,
            CarInfo carInfo)
        {
            _var = variables;
            _exc = exc;
            _insimMethods = insimMethods;
            _sqlContext = sqlContext;
            _api = new(_var, _exc);
            _lapManager = lapManager;
            _carInfo = carInfo;
        }

        /// <summary>
        /// Player has entered the garage (shift+p - keeping their slot on the track)
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public async Task PLayerPit(object o, PacketEventArgs<IS_PLP> e)
        {
            try
            {
                var conn = _insimMethods.GetByPlid(e.Packet.PLID);

                if (conn == null)
                    return;
                
                #region Race Positions - Clear buttons

                // Clear the 3 buttons about the user on the Race Positions Window
                var click = conn.Racer.GetUserClickID(conn.UCID);
                conn.Racer.userClickIDs.Remove(conn.UCID);
                await _insimMethods.ClearBtnRange(click.Col1, click.Col3);

                #endregion
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.PACKET,
                    $"IS_NPL", ex);
            }
        }
    }
}
