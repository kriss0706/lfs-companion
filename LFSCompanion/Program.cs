﻿using LFSCompanion;
using LFSCompanion.Insim.Events;
using InSimDotNet;
using InSimDotNet.Packets;
using LFSCompanion.Application.Classes;
using LFSCompanion.Application.SQL;
using LFSCompanion.Application.SQL.Seeders;
using LFSCompanion.Application.Utils;
using LFSCompanion.Insim.Classes;
using LFSCompanion.Insim.Models;
using LFSCompanion.Insim.Models.Enum;
using LFSCompanion.Insim.Timers;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Net.Mime;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Timers;
using InSimDotNet.Helpers;
using static LFSCompanion.Application.SQL.Seeders.SeederModels;

internal class Program
{
    private static async Task Main(string[] args)
    {
        #region Dependency Injector

        #region Service Worker
        var services = new ServiceCollection()
            .AddSingleton<Variables>()
            .AddSingleton<ExceptionProcessor>()
            .AddSingleton<InSimMethods>()
            .AddSingleton<outGauge>()
            .AddSingleton<StringFormatter>()
            .AddSingleton<REST_API>()
            .AddSingleton<CarInfo>()
            .AddSingleton<Main>()
            .AddSingleton<Migration>()
            .AddSingleton<DateFormatter>()
            .AddSingleton<Connection>()
            .AddSingleton<ButtonManager>()
            .AddSingleton<SQLContext>()
            .AddSingleton<ButtonPositionsSeeder>()
            .AddSingleton<SettingsSeeder>()
            .AddSingleton<SeederMethods>()
            .AddSingleton<CommandProcessor>()
            .AddSingleton<Second>()
            .AddSingleton<Milli>()
            .AddSingleton<WindowFactory>()
            .AddSingleton<TemplateHelper>()
            .AddSingleton<MathFactory>()
            .AddSingleton<DirectoryFactory>()
            .AddSingleton<LapManager>()
            .AddSingleton<LFSW>()
            .BuildServiceProvider();
        #endregion

        #region Classes
        Variables vars = services.GetService<Variables>()!;
        ExceptionProcessor exception_processor = services.GetService<ExceptionProcessor>()!;
        outGauge gauge = services.GetService<outGauge>()!;
        StringFormatter string_formatter = services.GetService<StringFormatter>()!;
        REST_API rest_api = services.GetService<REST_API>()!;
        CarInfo car_info = services.GetService<CarInfo>()!;
        Migration migration = services.GetService<Migration>()!;
        DateFormatter date_formatter = services.GetService<DateFormatter>()!;
        ExceptionProcessor exc = services.GetService<ExceptionProcessor>()!;
        ButtonManager buttonManager = services.GetService<ButtonManager>()!;
        InSimMethods InSimMethods = services.GetService<InSimMethods>()!;
        SQLContext sqlcontext = services.GetService<SQLContext>()!;
        Connection connection = services.GetService<Connection>()!;
        CommandProcessor cmdProcessor = services.GetService<CommandProcessor>()!;
        Second TSecond = services.GetService<Second>()!;
        Milli TMilli = services.GetService<Milli>()!;
        Main main = services.GetService<Main>()!;
        DirectoryFactory dir = services.GetService<DirectoryFactory>()!;
        var lfsw = services.GetService<LFSW>()!;
        #endregion

        #endregion

        // Console Properties
        Console.Title = $"LFS Companion v{vars.Version.Major}.{vars.Version.Minor}.{vars.Version.Build}";
        
        await exc.Init_Logs();

        // InSim Object Declaration
        vars.insim = new InSimClient();

        // Dev Build
        #region Dev. Notice
        if(vars.AppEnvironment == EnvironmentEnum.DEVELOPMENT)
            vars.DevBuildString = " ^2[DEV-BUILD]";
        #endregion

        // Startup Operations
        
        Console.WriteLine($@"LFS Companion v{vars.Version.Major}.{vars.Version.Minor}.{vars.Version.Build} launched. Type 'help' to get started.");
        
        // Verify InSim Connection File
        await connection.Validate();

        await sqlcontext.Validate();

        await migration.Migrate();

        // Load user settings from SQL
        await InSimMethods.LoadSettings();

        // Get modded cars and store them 'CarMods' list
        #region Fill CarMods List
        try {
            vars.CarMods = await rest_api.GetVehicleBase();
        }
        catch (Exception ex) {
            await exception_processor.Log(ExceptionProcessor.LogTypeEnum.ERROR, "vars.CarMods = await rest_api.GetVehicleBase();", ex);
        }
        #endregion

        vars.LFSCars = await car_info.PopulateLfsCars();

        // Merge the two lists
        #region Merge LFSCars => CarMods List
        await Task.Run(() =>
        {
            // vars.CarMods = vars.CarMods.Concat(vars.LFSCars).ToList();
            vars.CarMods.AddRange(vars.LFSCars);
        });
        #endregion

        // Initialize the internal clickid groups
        // await click_id_manager.Startup();

        // Run InSim
        await main.Run();

        #region Encoding
        Console.OutputEncoding = Encoding.UTF8;
        Console.InputEncoding = Encoding.UTF8;
        #endregion

        #region Load Lists
        #region Player Messages
        string[] PlayerMessagesColumns = new string[] {
            "Nickname",
            "Username",
            "Message",
            "Date"
        };
        vars.PlayerMessages = await sqlcontext.LoadMessages<Messages>("PlayerMessages", PlayerMessagesColumns);
        #endregion

        #region Global Messages
        string[] GlobalMessagesColumns = new string[] {
            "Message",
            "Date"
        };
        vars.GlobalMessages = await sqlcontext.LoadMessages<Messages>("GlobalMessages", GlobalMessagesColumns);
        #endregion

        #region CarContact
        string[] CarContactColumns = new string[] {
            "SourceUName",
            "SourcePName",
            "TargetUName",
            "TargetPName",
            "Speed",
            "Date"
        };
        vars.CarContactMessages = await sqlcontext.LoadMessages<LFSCompanion.Application.SQL.Models.CarContact>("CarContact", CarContactColumns);
        #endregion

        #region Laptimes
        string[] LaptimesColumns = new string[]
        {
            "Username",
            "Playername",
            "Split1",
            "Split2",
            "Split3",
            "Laptime",
            "Vehicle",
            "Track",
            "Date"
        };

        vars.LaptimeMessages = await sqlcontext.LoadMessages<LaptimeModel>("Laptime", LaptimesColumns);
        #endregion

        #endregion

        #region Directory Checks
        await dir.Validate();
        #endregion

        #region ' Timer declarations '
        System.Timers.Timer secondTimer = new System.Timers.Timer();
        secondTimer.Elapsed += TSecond.SecondTimer_EventHandler;
        secondTimer.Interval = 1000;
        secondTimer.Enabled = true;
        secondTimer.AutoReset = true;

        System.Timers.Timer MillisecondTimer = new System.Timers.Timer();
        MillisecondTimer.Elapsed += TMilli.MilliSecondTimer_EventHandler;
        MillisecondTimer.Interval = 50;
        MillisecondTimer.Enabled = true;
        MillisecondTimer.AutoReset = true;
        #endregion

        while (true)
        {
            // Initialize Command Processor
            await cmdProcessor.Command(vars,main,exc,InSimMethods);
        }
    }
}
