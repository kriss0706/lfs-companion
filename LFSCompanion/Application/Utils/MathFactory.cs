﻿using InSimDotNet.Helpers;
using LFSCompanion.Application.Classes;
using LFSCompanion.Insim.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.Utils
{
    public class MathFactory
    {
        private Variables _vars;

        public MathFactory(Variables variables)
        {
            _vars = variables;
        }

        #region Fuel Unit Conversions

        /// <summary>
        /// Convert litres to gallons
        /// </summary>
        /// <param name="Litre"></param>
        /// <returns></returns>
        public async Task<double> LitresToGallons(double Litre)
        {
            return Litre * 0.264172;
        }

        /// <summary>
        /// Return live fuel usage
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public double getFuelConsumption(Player conn)
        {
            var lastFuelDiff = conn.AvgFuelValue - _vars.vehicleFuelLitres;

            if (conn.AvgMeters < 0.5)
                return 0;

            if (_vars.SpeedoUnit == "mph")
            {
                lastFuelDiff = conn.AvgFuelValue - _vars.vehicleFuelLitres;
                return lastFuelDiff / MathHelper.MetersToMiles(conn.AvgMeters) * 10;
            }

            return lastFuelDiff / MathHelper.MetersToKilometers(conn.AvgMeters) * 10;
        }

        #endregion
    }
}
