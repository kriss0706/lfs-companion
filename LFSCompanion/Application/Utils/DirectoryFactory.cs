﻿using LFSCompanion.Application.Extensions;
using LFSCompanion.Application.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.Utils
{
    public class DirectoryFactory
    {
        public async Task Validate()
        {
            await FolderExistence(InsimDirectoryEnum.TEMPLATE_EXPORTS);
            await FolderExistence(InsimDirectoryEnum.TEMPLATE_IMPORTS);
        }

        public async Task<string> GetDirectory(InsimDirectoryEnum Type)
        {
            string InSimPath = Directory.GetCurrentDirectory();
            return Path.Combine(InSimPath, Type.GetDescription());
        }

        public async Task FolderExistence(InsimDirectoryEnum directory)
        {
            string DirectoryPath = await GetDirectory(directory);

            if (!Directory.Exists(DirectoryPath))
                Directory.CreateDirectory(DirectoryPath);
        }
    }
}
