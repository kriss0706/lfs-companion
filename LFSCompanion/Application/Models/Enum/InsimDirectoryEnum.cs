﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.Models.Enum
{
    public enum InsimDirectoryEnum
    {
        [Description("Config")]
        CONFIG,

        [Description("Logs")]
        LOGS,

        [Description("Template Exports")]
        TEMPLATE_EXPORTS,

        [Description("Template Imports")]
        TEMPLATE_IMPORTS
    }
}
