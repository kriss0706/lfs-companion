using Newtonsoft.Json;

namespace LFSCompanion.Application.Models;

public class Pubstat
{
    public string distance { get; set; }
    public string fuel { get; set; }
    public string laps { get; set; }
    public string joined { get; set; }
    public string win { get; set; }
    public string second { get; set; }
    public string third { get; set; }
    public string races_finished { get; set; }
    public string qual { get; set; }
    public string pole { get; set; }
    public string drags { get; set; }
    public string dragwins { get; set; }
    public string country { get; set; }
    
    public string ostatus { get; set; }
    public string hostname { get; set; }
    public long last_time { get; set; }
    public string track { get; set; }
    public string car { get; set; }
}

public enum PubstatStatus
{
    NONE = 0,
    SUCCESS = 1,
    HIDDEN_STATS = 2,
    INVALID_KEY = 3,
    BAD_GATEWAY = 4,
    PARSE_ERROR = 5,
    AUTH_ERROR = 6
}