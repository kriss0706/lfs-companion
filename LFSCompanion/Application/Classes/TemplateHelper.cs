﻿using LFSCompanion.Application.SQL;
using LFSCompanion.Application.SQL.Models;
using LFSCompanion.Insim.Classes;
using LFSCompanion.Insim.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.Classes
{
    public class TemplateHelper
    {
        private InSimMethods _methods;
        private Variables _vars;
        private ExceptionProcessor _exc;
        private Migration _migration;
        private WindowFactory _window;
        private SQLContext _sqlcontext;

        public TemplateHelper(Variables vars, ExceptionProcessor exc, InSimMethods methods,
            Migration migration, WindowFactory window, SQLContext sqlcontext)
        {
            _vars = vars;
            _methods = methods;
            _exc = exc;
            _migration = migration;
            _window = window;
            _sqlcontext = sqlcontext;
        }

        public async Task<ButtonTemplate?> GetTemplateSelection(int Skip)
        {
            ButtonTemplate? template = _vars.Templates.Where(x => x.IsDeleted == 0)
                .OrderByDescending(x => x.Created)
                .Skip(Skip)
                .FirstOrDefault();

            if (template == null)
                return null;

            return template;
        }

        public async Task ApplyTemplate(int Offset)
        {
            try
            {
                await Task.Run(async () =>
                {
                    _vars.SelectedTemplate = await GetTemplateSelection(Offset);
                });

                if (_vars.SelectedTemplate == null || _vars.SelectedTemplate.Name == null)
                    return;

                if (_migration.CountRecords("ButtonPositions").Result !=
                    _migration.CountRecords($"Template_{_vars.SelectedTemplate.Name}").Result)
                {
                    await _methods.LMsg($"^1Failed to apply {_vars.SelectedTemplate.Name} template.");
                    return;
                }

                _vars.LastUsedTemplate = _vars.SelectedTemplate.Name;
                await _sqlcontext.SaveSettingAsync("LastUsedTemplate", _vars.LastUsedTemplate);

                // Delete original table that insim uses
                await _migration.DeleteTable("ButtonPositions");
                
                // Create empty table
                await _migration.CreateTable("ButtonPositions", _vars.SQLColumns_ButtonPositions);
                
                // Move records from template to original
                await _migration.MergeTables(
                $"Template_{_vars.SelectedTemplate.Name}",
                "ButtonPositions");

                _vars.InTemplatesOverview = false;
                await _window.Templates(WindowStateEnum.CLOSE);
                _vars.InCustomizeUI = true;

                await _methods.LoadSettings();
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    "ApplyTemplate() in TemplateHelper class", ex);
            }
        }
    }
}
