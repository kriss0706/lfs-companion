﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.Classes
{
    public class CarInfo
    {
        private Variables _var;
        private ExceptionProcessor _exc;

        public CarInfo(Variables var, ExceptionProcessor exceptionProcessor)
        {
            _var = var;
            _exc = exceptionProcessor;
        }

        public async Task<List<ModCarEntry>> PopulateLfsCars()
        {
            #region Populate LFS Cars
            try
            {
                // Needs to be fixed
                int S1Published = 1668501275;
                int S2Published = 1668501275;
                int S3Published = 1668501275;

                List<ModCarEntry> lfsCars = new()
                {
                    #region UF1
                    new ModCarEntry
                    {
                        Id = "UF1",
                        Name = "UF 1000",
                        Description = "The least powerful car in the game, but far from being the least fun. In fact, the closest races are to be had when behind the wheel of this car. Certainly not suited to the faster tracks however.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 41,
                            MaxPowerRpm = 5605,
                            Torque = 88,
                            MaxTorqueRpm = 3008,
                            Mass = 600,
                            Bhp = 55,
                            PowerWeightRatio = 69,
                            BhpTon = 94,
                            FuelTankSize = 35
                        }
                    },
                    #endregion

                    #region XFG
                    new ModCarEntry
                    {
                        Id = "XFG",
                        Name = "XF GTI",
                        Description = "This is the starter car of Live for Speed. Being a FWD with not too much power, it's a good car to learn your first racing skills with.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 86,
                            MaxPowerRpm = 7031,
                            Torque = 130,
                            MaxTorqueRpm = 5438,
                            Mass = 942,
                            Bhp = 115,
                            PowerWeightRatio = 91,
                            BhpTon = 124,
                            FuelTankSize = 45
                        }
                    },
                    #endregion

                    #region XRG
                    new ModCarEntry
                    {
                        Id = "XRG",
                        Name = "XR GTI",
                        Description = "Being a rear wheel driven car, but without excessive power to them, this car is great to get the feel for racing with a RWD. Learning to catch the balance in turns, it will prepare you for its bigger brother, the XR GT Turbo.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 104,
                            MaxPowerRpm = 5988,
                            Torque = 187,
                            MaxTorqueRpm = 4512,
                            Mass = 1150,
                            Bhp = 140,
                            PowerWeightRatio = 91,
                            BhpTon = 124,
                            FuelTankSize = 65
                        }
                    },
                    #endregion

                    #region LX4
                    new ModCarEntry
                    {
                        Id = "LX4",
                        Name = "LX4",
                        Description = "With only 518 kg the LX4 is a pretty light car and is quite fast around the twisted S1 tracks. Compared to its bigger brother, the LX6, the LX4 is easy to drive and ideal for beginners to learn how to control a LX-style car.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 105,
                            MaxPowerRpm = 8227,
                            Torque = 131,
                            MaxTorqueRpm = 6891,
                            Mass = 499,
                            Bhp = 140,
                            PowerWeightRatio = 210,
                            BhpTon = 286,
                            FuelTankSize = 40
                        }
                    },
                    #endregion

                    #region LX6
                    new ModCarEntry
                    {
                        Id = "LX6",
                        Name = "LX6",
                        Description = "The \"big brother\" of the LX4 has a lot more power but is also slightly heavier. Due to the road type tyres the LX6 is quite tricky to drive, so you should practice with the LX4 first.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 142,
                            MaxPowerRpm = 8402,
                            Torque = 173,
                            MaxTorqueRpm = 7066,
                            Mass = 539,
                            Bhp = 190,
                            PowerWeightRatio = 263,
                            BhpTon = 358,
                            FuelTankSize = 40
                        }
                    },
                    #endregion

                    #region RB4
                    new ModCarEntry
                    {
                        Id = "RB4",
                        Name = "RB4 GT",
                        Description = "This four wheel drive car has a lot of torque and is therefore the ideal car for the rallycross courses. The RB4 is also fun to drive on the road tracks, but is slightly slower than the GT Turbo and FXO due to its heavy weight.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 181,
                            MaxPowerRpm = 6035,
                            Torque = 340,
                            MaxTorqueRpm = 3896,
                            Mass = 1210,
                            Bhp = 243,
                            PowerWeightRatio = 150,
                            BhpTon = 204,
                            FuelTankSize = 75
                        }
                    },
                    #endregion

                    #region FXO
                    new ModCarEntry
                    {
                        Id = "FXO",
                        Name = "FXO Turbo",
                        Description = "Many racers' favourite car as it is very accessible and fun, being a fast front wheel driven car. And because it is light, it is able to maintain a high speed right through the turn.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 175,
                            MaxPowerRpm = 6357,
                            Torque = 305,
                            MaxTorqueRpm = 4336,
                            Mass = 1136,
                            Bhp = 234,
                            PowerWeightRatio = 154,
                            BhpTon = 210,
                            FuelTankSize = 75
                        }
                    },
                    #endregion

                    #region XRT
                    new ModCarEntry
                    {
                        Id = "XRT",
                        Name = "XR GT Turbo",
                        Description = "Loaded with a 2 litre turbocharged engine, transferring its power to the rear, the XR GT Turbo has become one of the favorite cars in S1.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 184,
                            MaxPowerRpm = 6035,
                            Torque = 345,
                            MaxTorqueRpm = 3896,
                            Mass = 1223,
                            Bhp = 247,
                            PowerWeightRatio = 151,
                            BhpTon = 205,
                            FuelTankSize = 75
                        }
                    },
                    #endregion

                    #region RAC
                    new ModCarEntry
                    {
                        Id = "RAC",
                        Name = "Raceabout 06",
                        Description = "Really existing prototype of a sportscar from Finland. It has the typical feeling of a mid-mounted (Saab and turbocharged) engine; tricky, but very rewarding.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 183,
                            MaxPowerRpm = 5906,
                            Torque = 360,
                            MaxTorqueRpm = 3500,
                            Mass = 800,
                            Bhp = 245,
                            PowerWeightRatio = 228,
                            BhpTon = 311,
                            FuelTankSize = 42
                        }
                    },
                    #endregion

                    #region FZ5
                    new ModCarEntry
                    {
                        Id = "FZ5",
                        Name = "FZ50",
                        Description = "The flat 6 in the back of this car delivers a good 360 bhp that you can put down well due to the car having its weight in the back. Watch out in the corners though, its heavy back may overtake the front!",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 269,
                            MaxPowerRpm = 7563,
                            Torque = 392,
                            MaxTorqueRpm = 5000,
                            Mass = 1380,
                            Bhp = 360,
                            PowerWeightRatio = 195,
                            BhpTon = 265,
                            FuelTankSize = 90
                        }
                    },
                    #endregion

                    #region UFR
                    new ModCarEntry
                    {
                        Id = "UFR",
                        Name = "UF GTR",
                        Description = "While it has less power than its competitor, the XF GTR, its lack of weight more than makes up for this. On twistier circuits it will eat the XF GTR (and a couple of sets of tyres), but loses out on the straights. Certainly requires smooth driving. Whoever said Minis were boring?",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 134,
                            MaxPowerRpm = 8227,
                            Torque = 178,
                            MaxTorqueRpm = 5871,
                            Mass = 600,
                            Bhp = 180,
                            PowerWeightRatio = 223,
                            BhpTon = 304,
                            FuelTankSize = 60
                        }
                    },
                    #endregion

                    #region XFR
                    new ModCarEntry
                    {
                        Id = "XFR",
                        Name = "XF GTR",
                        Description = "The most powerful FWD car in LFS, it brings a whole new meaning to the phrase \"power understeer\". Requires smooth driving. Is a competitor to the UF GTR.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 134,
                            MaxPowerRpm = 8227,
                            Torque = 178,
                            MaxTorqueRpm = 5871,
                            Mass = 600,
                            Bhp = 180,
                            PowerWeightRatio = 223,
                            BhpTon = 304,
                            FuelTankSize = 60
                        }
                    },
                    #endregion

                    #region FXR
                    new ModCarEntry
                    {
                        Id = "FXR",
                        Name = "FXO GTR",
                        Description = "The FXO GTR is a great car for somebody just getting used to the extra power offered by the GTR cars or for somebody who just wants to have some fun in the GTR class, but if you want wins and don't like long races you have to move to one of the rear-wheel drive GTR cars.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 365,
                            MaxPowerRpm = 6270,
                            Torque = 627,
                            MaxTorqueRpm = 4775,
                            Mass = 1100,
                            Bhp = 490,
                            PowerWeightRatio = 332,
                            BhpTon = 453,
                            FuelTankSize = 100
                        }
                    },
                    #endregion

                    #region XRR
                    new ModCarEntry
                    {
                        Id = "XRR",
                        Name = "XR GTR",
                        Description = "Is the XR GT Turbo not man enough for you? Slicks, wings, silly power and an angry face, this car has had the works done to it. Nobody knows what the LFS tuning division did to the turbocharger to get double the power out of the engine, but it works, and we are forever thankful.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 365,
                            MaxPowerRpm = 6270,
                            Torque = 627,
                            MaxTorqueRpm = 4775,
                            Mass = 1100,
                            Bhp = 490,
                            PowerWeightRatio = 332,
                            BhpTon = 453,
                            FuelTankSize = 100
                        }
                    },
                    #endregion

                    #region FZR
                    new ModCarEntry
                    {
                        Id = "FZR",
                        Name = "FZ50 GTR",
                        Description = "This is what happens when a not-so-sane person looks at the FZ50 and says \"Mmm, nice, but could do with a bit more power.\" 130bhp more to be exact. Like the XR GTR, it was given slicks, downforce, gorgeous looks and lightened, but just ended up better.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 365,
                            MaxPowerRpm = 8102,
                            Torque = 503,
                            MaxTorqueRpm = 5246,
                            Mass = 1100,
                            Bhp = 490,
                            PowerWeightRatio = 332,
                            BhpTon = 452,
                            FuelTankSize = 100
                        }
                    },
                    #endregion

                    #region MRT
                    new ModCarEntry
                    {
                        Id = "MRT",
                        Name = "MRT5",
                        Description = "Based on the real MRT5 built by the McGill Racing Team this lightweight car is ideal for autocross tracks and slow configurations. The MRT5 is fun to drive and reminds you of a kart although it has more power and a differential.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 600,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 48,
                            MaxPowerRpm = 7871,
                            Torque = 69,
                            MaxTorqueRpm = 5078,
                            Mass = 222,
                            Bhp = 64,
                            PowerWeightRatio = 216,
                            BhpTon = 295,
                            FuelTankSize = 20
                        }
                    },
                    #endregion

                    #region FBM
                    new ModCarEntry
                    {
                        Id = "FBM",
                        Name = "Formula BMW FB02",
                        Description = "A recreation of the real car. Due to its fairly low power while still having downforce, this car is perfect for the less experienced formula drivers. It also makes for good close races, for everyone, as you have more time to react to the racing circumstances.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 105,
                            MaxPowerRpm = 8948,
                            Torque = 125,
                            MaxTorqueRpm = 6756,
                            Mass = 465,
                            Bhp = 140,
                            PowerWeightRatio = 225,
                            BhpTon = 307,
                            FuelTankSize = 42
                        }
                    },
                    #endregion

                    #region FOX
                    new ModCarEntry
                    {
                        Id = "FOX",
                        Name = "Formula XR",
                        Description = "This car serves as a nice introduction to the cars with downforce. The relatively low power of the engine makes the car easy to drive once up to speed. Insane fun on twisty tracks.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 142,
                            MaxPowerRpm = 7031,
                            Torque = 221,
                            MaxTorqueRpm = 4980,
                            Mass = 490,
                            Bhp = 190,
                            PowerWeightRatio = 290,
                            BhpTon = 395,
                            FuelTankSize = 38
                        }
                    },
                    #endregion

                    #region FO8
                    new ModCarEntry
                    {
                        Id = "FO8",
                        Name = "Formula V8",
                        Description = "Formula car for the more advanced racer. It's very fast, pulling over 3Gs in the bends. Defenitely a step up compared with the Formula XR.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 335,
                            MaxPowerRpm = 9092,
                            Torque = 385,
                            MaxTorqueRpm = 7422,
                            Mass = 600,
                            Bhp = 450,
                            PowerWeightRatio = 559,
                            BhpTon = 762,
                            FuelTankSize = 125
                        }
                    },
                    #endregion

                    #region BF1
                    new ModCarEntry
                    {
                        Id = "BF1",
                        Name = "BMW Sauber F1.06",
                        Description = "A recreation of the real thing with approval from the BMW Sauber F1 team. This car is so fast, you will soon learn to respect F1-drivers more than ever.",
                        UserId = 0,
                        UserName = "LFS Developers",
                        Wip = false,
                        PublishedAt = S2Published,
                        NumDownloads = 0,
                        CurUsage = 0,
                        Rating = 0,
                        NumRatings = 0,
                        StaffPick = false,
                        TweakMod = false,
                        Version = 0,
                        LastDownloadedAt = 0,
                        Class = 0,
                        Ev = false,
                        CoverUrl = "none",
                        Collaborators = "none",
                        Vehicle = new ModCarDetails
                        {
                            IceCc = 0,
                            IceNumCylinders = 4,
                            IceLayout = 0,
                            EvRedLine = 0,
                            Drive = 0,
                            ShiftType = 0,
                            Power = 537,
                            MaxPowerRpm = 19063,
                            Torque = 284,
                            MaxTorqueRpm = 16797,
                            Mass = 530,
                            Bhp = 720,
                            PowerWeightRatio = 1012,
                            BhpTon = 1378,
                            FuelTankSize = 95
                        }
                    },
                    #endregion
                };

                return lfsCars;
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "PopulateLfsCars() in CarInfo class", ex);
                return null;
            }
            #endregion
        }

        public async Task<string> getFullVehicleName(string Vehicle)
        {
            string? fetch = null;

            foreach (var veh in _var.CarMods)
            {
                if (Vehicle == veh.Id)
                    fetch = veh.Name;
            }

            if (fetch != null)
                return fetch;

            return "vehicle not found";
        }
    }
}
