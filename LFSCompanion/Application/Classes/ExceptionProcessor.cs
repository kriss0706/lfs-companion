﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.Classes
{
    public class ExceptionProcessor
    {
        // Works for windows
        // private string logsFolder = AppDomain.CurrentDomain.BaseDirectory + "Logs";
        
        
        // Works for windows and linux
        private string logsFolder = Path.Combine(Directory.GetCurrentDirectory(), "Logs");

        /// <summary>
        /// Verify if the logs folder exist
        /// </summary>
        /// <returns></returns>
        public async Task Init_Logs()
        {
            try
            {
                if (!await VerifyLogFolder())
                    return;
                
                await VerifyLogFiles();
            }
            catch (Exception ex)
            {
                await Log(LogTypeEnum.ERROR, $"Failed to verify logs folder", ex);
            }
        }

        /// <summary>
        /// Create the logs folder
        /// </summary>
        /// <returns></returns>
        public async Task<bool> VerifyLogFolder()
        {
            try
            {
                if (!Directory.Exists(logsFolder))
                {
                    Console.WriteLine("Logs folder was not found, creating..");
                    await Task.Run(() => Directory.CreateDirectory(logsFolder));
                }
                return true;
            }
            catch (Exception ex)
            {
                await Log(LogTypeEnum.ERROR, "Failed to create log folder", ex);
                return false;
            }
        }

        /// <summary>
        /// Verify that the log file exists
        /// </summary>
        /// <returns></returns>
        public async Task<bool> VerifyLogFiles()
        {
            try
            {
                string errorFile = Path.Combine(logsFolder, "error.txt");

                if (!Directory.Exists(logsFolder))
                {
                    bool folderCreated = await VerifyLogFolder();
                    if (!folderCreated)
                        return false; // Stop execution if folder creation fails
                }

                if (!File.Exists(errorFile))
                {
                    Console.WriteLine("Log file was not found, creating...");

                    // Use async file writing
                    await File.WriteAllTextAsync(errorFile, "Error log contents");
                }

                return true; // Indicate success
            }
            catch (Exception ex)
            {
                await Log(LogTypeEnum.ERROR, "Failed to create log file.", ex);
                return false; // Indicate failure
            }
        }

        #region ' Enumeration Log Types '
        public enum LogTypeEnum
        {
            CONSOLE,
            ERROR,
            CHAT,
            ADMIN,
            CONNS,
            PACKET,
            DEBUG
        }

        Dictionary<LogTypeEnum,string> LogTypeDictionary = new()
        {
            { LogTypeEnum.ERROR, "error.txt" },
            { LogTypeEnum.CHAT, "chat.txt" },
            { LogTypeEnum.ADMIN, "admin.txt" },
            { LogTypeEnum.CONNS, "conns.txt" }
        };
        #endregion

        /// <summary>
        /// Write text to a log file
        /// </summary>
        /// <param name="Logtype"></param>
        /// <returns></returns>
        public async Task Log(LogTypeEnum Logtype, string Content, Exception? ExceptionObject)
        {
            try
            {
                switch (Logtype)
                {
                    // Log to console
                    case LogTypeEnum.CONSOLE:
                        Console.WriteLine(Content, ExceptionObject);
                        break;

                    // Log to error.txt
                    case LogTypeEnum.ERROR:
                        await SaveToFile(LogTypeDictionary[LogTypeEnum.ERROR], Content, ExceptionObject);
                        Console.WriteLine($"Caught exception - see /logs/error.txt for detailed information.");
                        Environment.Exit(0);
                        break;

                    // Often used when troubleshooting
                    case LogTypeEnum.DEBUG:
                        await SaveToFile(LogTypeDictionary[LogTypeEnum.ERROR], Content, ExceptionObject);
                        Console.WriteLine($"Caught exception - see /logs/error.txt for extra detailed information.");
                        Environment.Exit(0);
                        break;

                    // Log to conns.txt
                    case LogTypeEnum.CONNS:
                        await SaveToFile(LogTypeDictionary[LogTypeEnum.CONNS], Content, ExceptionObject);
                        break;

                    // Log to chat.txt
                    case LogTypeEnum.CHAT:
                        await SaveToFile(LogTypeDictionary[LogTypeEnum.CHAT], Content, ExceptionObject);
                        break;

                    // Log to admin.txt
                    case LogTypeEnum.ADMIN:
                        await SaveToFile(LogTypeDictionary[LogTypeEnum.ADMIN], Content, ExceptionObject);
                        break;

                    case LogTypeEnum.PACKET:
                        await SaveToFile($"error.txt", $"Packet error for {Content} : ", ExceptionObject);
                        break;
                }
            }
            catch (Exception ex)
            {
                await Log(LogTypeEnum.ERROR, "method Log() in ExceptionProcessor class", ex);
            }
        }

        public async Task SaveToFile(string FileName, string Content, Exception ExceptionObject)
        {
            try
            {
                var date = DateTime.Now;
                string filepath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,"Logs",FileName);

                await using (var writer = new StreamWriter(filepath, true))
                {
                    await writer.WriteLineAsync($"{date} - {Content} => " +
                        $"{ExceptionObject.Message} {ExceptionObject.StackTrace}" +
                        $"{ExceptionObject.InnerException} {ExceptionObject.Source}");
                }
            }
            catch (Exception ex)
            {
                await Log(LogTypeEnum.ERROR, "method SaveToFile() in ExceptionProcessor class", ex);
            }
        }
    }
}
