﻿using InSimDotNet;
using LFSCompanion.Application.SQL;
using LFSCompanion.Insim.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EXC = LFSCompanion.Application.Classes.ExceptionProcessor;

namespace LFSCompanion.Application.Classes
{
    public class CommandProcessor
    {
        private SQLContext _sqlcontext;

        public CommandProcessor(SQLContext sqlContext)
        {
            _sqlcontext = sqlContext;
        }

        /// <summary>
        /// Command Processor class is used for processing console commands.
        /// </summary>
        /// <param name="_vars"></param>
        /// <param name="Main_Class"></param>
        /// <param name="exceptionProcessor"></param>
        /// <param name="Methods"></param>
        /// <returns></returns>
        public async Task Command(
            Variables _vars,
            Main Main_Class,
            EXC exceptionProcessor,
            InSimMethods Methods)
        {
            try
            {
                string? command = Console.ReadLine();
                string?[] parts = command.Split(new char[] { ' ' },2);
                var cmd = parts[0];
                var param = parts.Length > 1 ? parts[1] : "";

                if (cmd.Length > 0)
                {
                    switch (cmd.ToLower())
                    {
                        case "help":
                        case "commands":
                        case "cmds":
                            #region ' Help Commands '
                            Console.WriteLine("LFSCompanion Terminal Commands:");
                            Console.WriteLine(" › help - Display this list of commands");
                            Console.WriteLine(" › send <text> - send a public message to LFS");
                            Console.WriteLine(" › priv <text> [debug] - send a test message ");
                            Console.WriteLine(" › exit (quit) - Terminate the insim");
                            Console.WriteLine(" › init (start) - Connect to the insim protocol");
                            Console.WriteLine(" › disconnect (stop) - Disconnect the insim protocol");
                            Console.WriteLine(" › setting <list|setting> - Personalize your experience");
                            Console.WriteLine(" › clear - Clear the chat history");
                            #endregion
                            break;

                        case "send":
                            #region ' Send a message to LFS '
                            if (param.Length > 0)
                                await Methods.SMsg(param);
                            else
                                await exceptionProcessor.Log(EXC.LogTypeEnum.CONSOLE, "You must provide a message to send!", null);
                            #endregion
                            break;

                        case "priv":
                            #region ' Send local IS_MSL message '
                            if (param.Length > 0)
                            {
                                await Methods.LMsg(param);
                            }
                            else
                            {
                                await exceptionProcessor.Log(EXC.LogTypeEnum.CONSOLE, "You must provide a message to send!", null);
                            }
                            #endregion
                            break;

                        case "quit":
                        case "exit":
                            Environment.Exit(0);
                            break;

                        case "start":
                        case "init":
                        case "connect":
                            await Main_Class.Run();
                            break;

                        case "stop":
                        case "disconnect":
                            #region ' Disconnect from insim protocol '
                            await Main_Class.Stop();
                            #endregion
                            break;


                        case "setting":
                            if (param == string.Empty || param == "list")
                            {
                                Console.WriteLine($"Available CLI settings:");
                                Console.WriteLine($"  chat - Toggle chat view in console window");
                                Console.WriteLine($"  autostart - Toggle automatic insim connection");
                            }
                            else
                            {
                                switch (param)
                                {
                                    case "chat":
                                        #region Console Chat
                                        if (_vars.ConsoleChat == true)
                                        {
                                            await _sqlcontext.SaveSettingAsync("ConsoleChat", "0");

                                            _vars.ConsoleChat = false;
                                            await exceptionProcessor.Log(EXC.LogTypeEnum.CONSOLE,
                                                $"Console chat mode has been turned off", null);
                                        }
                                        else
                                        {
                                            await _sqlcontext.SaveSettingAsync("ConsoleChat", "1");

                                            _vars.ConsoleChat = false;
                                            await exceptionProcessor.Log(EXC.LogTypeEnum.CONSOLE,
                                                $"Console chat mode has been turned on", null);
                                        }
                                        #endregion
                                        break;

                                    case "autostart":
                                        #region Auto Start
                                        if (_vars.AutoStart == true)
                                        {
                                            await _sqlcontext.SaveSettingAsync("AutoStart", "0");

                                            _vars.AutoStart = false;
                                            await exceptionProcessor.Log(EXC.LogTypeEnum.CONSOLE,
                                                $"Automatic insim connection has been turned off", null);
                                        }
                                        else
                                        {
                                            await _sqlcontext.SaveSettingAsync("AutoStart", "1");

                                            _vars.AutoStart = false;
                                            await exceptionProcessor.Log(EXC.LogTypeEnum.CONSOLE,
                                                $"Automatic insim connection has been turned on", null);
                                        }
                                        #endregion
                                        break;
                                }

                            }
                            break;

                        case "chat":
                        case "ct":
                            #region ' Toggle ConsoleChat option '
                            if (_vars.ConsoleChat)
                            {
                                await _sqlcontext.SaveSettingAsync("ConsoleChat", "0");

                                _vars.ConsoleChat = false;
                                await exceptionProcessor.Log(EXC.LogTypeEnum.CONSOLE,
                                    $"Console chat mode has been turned off", null);
                            }
                            else
                            {
                                await _sqlcontext.SaveSettingAsync("ConsoleChat", "1");

                                _vars.ConsoleChat = true;
                                await exceptionProcessor.Log(EXC.LogTypeEnum.CONSOLE,
                                    $"Console chat mode has been turned on", null);
                            }
                            #endregion
                            break;

                        case "clear":
                        case "clearhistory":
                            #region ' Clear Console Log '
                            Console.Clear();
                            #endregion
                            break;

                        default:
                            #region ' Invalid command '
                            Console.WriteLine("Invalid command. Type !help for a list of commands.");
                            #endregion
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Command length cannot be empty!");
                }
            }
            catch (Exception ex)
            {
                await exceptionProcessor.Log(EXC.LogTypeEnum.ERROR, "CommandProcessor error", ex);
            }
        }
    }
}
