﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.Classes
{
    public class DateFormatter
    {
        public DateFormatter() { }

        public async Task<string> ToRelativeTime(DateTime targetDatetime)
        {
            try
            {
                DateTime currentDateTime = DateTime.Now;
                TimeSpan timeDifference = currentDateTime - targetDatetime;

                int days = (int)timeDifference.TotalDays;
                int hours = (int)timeDifference.TotalHours % 24;
                int minutes = (int)timeDifference.TotalMinutes % 60;
                int seconds = (int)timeDifference.TotalSeconds % 60;

                string _days;
                string _hours;
                string _min;
                string _sec;

                #region Days
                if (days == 1)
                    _days = $"{days} day ";
                else if (days > 1)
                    _days = $"{days} days ";
                else
                    _days = "";
                #endregion
                #region Hours
                if (hours == 1)
                    _hours = $"{hours} hour ";
                else if (hours > 1)
                    _hours = $"{hours} hours ";
                else
                    _hours = "";
                #endregion
                #region Minutes
                if (minutes == 1)
                    _min = $"{minutes} min ";
                else if (minutes > 1)
                    _min = $"{minutes} mins ";
                else
                    _min = "";
                #endregion
                #region Seconds
                if (timeDifference.TotalSeconds < 60)
                {
                    if (seconds <= 2)
                        _sec = $"just now ";
                    else if (seconds >= 3)
                        _sec = $"{seconds} secs ";
                    else
                        _sec = "";
                }
                else
                    _sec = "";
                #endregion


                return $"{_days}{_hours}{_min}{_sec}";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
                return null;
            }
        }
    }
}
