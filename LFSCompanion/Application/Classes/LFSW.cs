﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json.Nodes;
using System.Threading.Tasks;
using System.Xml.Linq;
using LFSCompanion.Application.Extensions;
using LFSCompanion.Application.Models;

namespace LFSCompanion.Application.Classes
{
    public class LFSW
    {
        private Variables _vars;
        private ExceptionProcessor _exc;
        
        private static readonly HttpClient Client = new HttpClient();

        public LFSW(Variables vars, ExceptionProcessor exceptionProcessor)
        {
            _vars = vars;
            _exc = exceptionProcessor;
        }
        
        public async Task<Dictionary<PubstatStatus, Pubstat?>> getPubstat(string username)
        {
            var secret = _vars.lfswPubstat;
            var dict = new Dictionary<PubstatStatus, Pubstat?>();

            if (string.IsNullOrEmpty(secret) || secret == "none")
            {
                dict[PubstatStatus.INVALID_KEY] = null;
                return dict;
            }

            var pubstatUrl = $"https://www.lfsworld.net/pubstat/get_stat2.php?version=1.5&idk={secret}&s=1&racer={username}&action=pst";
            var jsonResponse = await fetchPst(pubstatUrl);

            if (string.IsNullOrWhiteSpace(jsonResponse))
            {
                dict[PubstatStatus.INVALID_KEY] = null;
                return dict;
            }

            if (jsonResponse == "pst: no valid username")
            {
                dict[PubstatStatus.HIDDEN_STATS] = null;
                return dict;
            }

            if (jsonResponse.Contains("502 (Bad Gateway)"))
            {
                dict[PubstatStatus.BAD_GATEWAY] = null;
                return dict;
            }

            if (jsonResponse == "not authed (ip)")
            {
                dict[PubstatStatus.AUTH_ERROR] = null;
                return dict;
            }

            try
            {
                var statsList = JsonConvert.DeserializeObject<List<Pubstat>>(jsonResponse);
                var stats = statsList?.FirstOrDefault();

                if (stats != null)
                {
                    dict[PubstatStatus.SUCCESS] = stats;
                }
                else
                {
                    dict[PubstatStatus.PARSE_ERROR] = null;
                }
            }
            catch (JsonException ex)
            {
                Console.WriteLine($"JSON Parsing Error: {ex.Message}");
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "JSON Parsing Error", ex);
                dict[PubstatStatus.PARSE_ERROR] = null;
            }

            return dict;
        }

        
        private static async Task<string> fetchPst(string url)
        {
            var response = await Client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }
    }
}
