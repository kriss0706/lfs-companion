﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json.Nodes;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LFSCompanion.Application.Classes
{
    public class REST_API
    {
        private Variables _vars;
        private ExceptionProcessor _exc;

        public REST_API(Variables vars, ExceptionProcessor exceptionProcessor)
        {
            _vars = vars;
            _exc = exceptionProcessor;
        }

        /// <summary>
        /// Get details from a vehicle
        /// </summary>
        /// <param name="carId"></param>
        /// <returns></returns>
        public async Task<ModCarEntry> GetVehicleDetails(string CAR_ID)
        {
            // Check if CarId is a stock car
            string[] lfsCars = { "UF1", "XFG", "XRG", "LX4", "LX6", "RB4", "FXO", "XRT", "RAC", "FZ5",
                    "UFR", "XFR", "FXR", "XRR", "FZR", "MRT", "FBM", "FOX", "FO8", "BF1" };

            if (lfsCars.Contains(CAR_ID))
            {
                ModCarEntry car = _vars.CarMods.FirstOrDefault(x => x.Id == CAR_ID);

                return car;
            }
            
            var client = new HttpClient();
            var content = new FormUrlEncodedContent(new Dictionary<string,string>
            {
                {"client_secret", _vars.Application.LFS_API_CLIENT_SECRET},
                {"client_id", _vars.Application.LFS_API_CLIENT_ID},
                {"grant_type", "client_credentials"}
            });

            var tokenResponse = await client.PostAsync("https://id.lfs.net/oauth2/access_token",content);
            var tokenText = await tokenResponse.Content.ReadAsStringAsync();
            
            if (tokenText.Contains("authentication failed"))
            {
                return new ModCarEntry();
            }
            
            var tokenObject = JsonConvert.DeserializeObject<TokenResponse>(tokenText);
            var accessToken = tokenObject.access_token;

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);
            var carEntriesResponse = await client.GetStringAsync($"https://api.lfs.net/vehiclemod/{CAR_ID}");

            return JsonConvert.DeserializeObject<ModCarEntriesDetailResponse>(carEntriesResponse).Data;
        }

        /// <summary>
        /// Get all vehicles
        /// </summary>
        /// <returns></returns>
        public async Task<List<ModCarEntry>> GetVehicleBase()
        {
            var client = new HttpClient();
            var content = new FormUrlEncodedContent(new Dictionary<string,string>
            {
                {"client_secret", _vars.Application.LFS_API_CLIENT_SECRET},
                {"client_id", _vars.Application.LFS_API_CLIENT_ID},
                {"grant_type", "client_credentials"}
            });

            var tokenResponse = await client.PostAsync("https://id.lfs.net/oauth2/access_token",content);
            var tokenText = await tokenResponse.Content.ReadAsStringAsync();

            if (tokenText.Contains("authentication failed"))
            {
                return new List<ModCarEntry>();
            }
            
            var tokenObject = JsonConvert.DeserializeObject<TokenResponse>(tokenText);
            var accessToken = tokenObject.access_token;

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);
            var carEntriesResponse = await client.GetStringAsync("https://api.lfs.net/vehiclemod/");

            return JsonConvert.DeserializeObject<ModCarEntriesResponse?>(carEntriesResponse).Data;
        }
    }

    #region Models
    /// <summary>
    /// Get base values from vehicles
    /// </summary>
    public class ModCarEntry
    {
        public string? Id { get; set; }
        public string? Name { get; set; }
        public string? DescriptionShort { get; set; }
        public string? Description { get; set; }
        public int? UserId { get; set; }
        public string? UserName { get; set; }
        public bool Wip { get; set; }
        public int? PublishedAt { get; set; }
        public int? NumDownloads { get; set; }
        public int? CurUsage { get; set; }
        public double Rating { get; set; }
        public int? NumRatings { get; set; }
        public bool StaffPick { get; set; }
        public bool TweakMod { get; set; }
        public int? Version { get; set; }
        public int? LastDownloadedAt { get; set; }
        public int? Class { get; set; }
        public bool Ev { get; set; }
        public string? CoverUrl { get; set; }
        public List<string>? ScreenshotUrls { get; set; }
        public string? Collaborators { get; set; }
        public ModCarDetails? Vehicle { get; set; }
    }

    public class ModCarDetails
    {
        public double? IceCc { get; set; }
        public int? IceNumCylinders { get; set; }
        public int? IceLayout { get; set; }
        public float EvRedLine { get; set; }
        public int? Drive { get; set; }
        public ShiftTypes ShiftType { get; set; }
        public float Power { get; set; }
        public int? MaxPowerRpm { get; set; }
        public float Torque { get; set; }
        public int? MaxTorqueRpm { get; set; }
        public float Mass { get; set; }
        public float Bhp { get; set; }
        public float PowerWeightRatio { get; set; }
        public float BhpTon { get; set; }
        public float FuelTankSize { get; set; }
    }

    public enum ShiftTypes
    {
        NONE = 0,
        H_PATTERN = 1,
        MOTORIKE = 2,
        SEQUENTIAL = 3,
        SEQUENTIAL_IGN_CUT = 4,
        PADDLE = 5,
        ELECTRIC = 6,
        CENTRIFUGAL_CLUTCH = 7
    }

    public class TokenResponse
    {
        public string? token_type { get; set; }
        public int? expires_in { get; set; }
        public string? access_token { get; set; }
    }

    class ModCarEntriesResponse
    {
        public List<ModCarEntry>? Data { get; set; }
    }

    class ModCarEntriesDetailResponse
    {
        public ModCarEntry? Data { get; set; }
    }

    #endregion
}
