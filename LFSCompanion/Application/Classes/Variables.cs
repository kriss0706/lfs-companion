﻿using InSimDotNet;
using InSimDotNet.Out;
using Microsoft.Data.Sqlite;
using System.Reflection;
using InSimDotNet.Packets;
using BS = InSimDotNet.Packets.ButtonStyles;
using Messages = LFSCompanion.Application.SQL.Seeders.SeederModels.Messages;
using LFSCompanion.Application.Models;
using LFSCompanion.Application.SQL.Models;
using LFSCompanion.Insim.Models;
using LFSCompanion.Insim.Models.Enum;
using CarContact = LFSCompanion.Application.SQL.Models.CarContact;

namespace LFSCompanion.Application.Classes
{
    #region Core Assembly
    public static class CoreAssembly
    {
        public static readonly Assembly Reference = typeof(CoreAssembly).Assembly;
        public static readonly Version Version = Reference.GetName().Version;
    }
    #endregion
    
    /// <summary>
    /// Local variables used by the application
    /// GET and SET are set on all of them
    /// </summary>
    public class Variables
    {
        public Counters Counters { get; private set; }
        public Application Application { get; private set; }
        
        public Variables()
        {
            Counters = new Counters();
            Application = new Application();
        }
        
        #region Version Related
        public EnvironmentEnum AppEnvironment { get; set; } = EnvironmentEnum.PRODUCTION;
        public Version Version { get; set; } = CoreAssembly.Version;
        public string DevBuildString { get; set; } = "";
        #endregion

        public bool ApiTestPassed { get; set; } = false;

        #region ButtonManager

        public List<ButtonGroup> ButtonList { get; } = new()
        {
            new ButtonGroup
            {
                #region Rpm

                Name = ButtonEnum.RPM,
                PrimaryClickID = 22,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List<>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.RPM_TITLE,
                        ClickID = 31
                    },
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.RPM_TEXT,
                        ClickID = 32
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "RPM",
                Body = "[RPM]"

                #endregion
            },
            new ButtonGroup
            {
                #region Turbo
                
                Name = ButtonEnum.TURBO,
                PrimaryClickID = 25,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List<>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.TURBO_TITLE,
                        ClickID = 24
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "Turbo",
                Body = $"[Turbo]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Gear
                
                Name = ButtonEnum.GEAR,
                PrimaryClickID = 16,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List<>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.GEAR_TITLE,
                        ClickID = 23
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "Gear",
                Body = "[Gear]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Speed
                
                Name = ButtonEnum.SPEED,
                PrimaryClickID = 18,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List<>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.SPEED_TITLE,
                        ClickID = 17
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "Speed",
                Body = "[Speed]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Move Customizer Window
                
                Name = ButtonEnum.CUSTOMIZER_WINDOW,
                PrimaryClickID = 62,
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 0,
                        Top = 0
                    }
                    #endregion
                },
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                IsVisible = true,
                TitleColor = "^7",
                Title = "Customizer",
                Body = "[Customizer]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Fuel
                
                Name = ButtonEnum.FUEL,
                PrimaryClickID = 11,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List<>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.FUEL_TITLE,
                        ClickID = 26
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "Fuel",
                Body = "[Fuel]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Sidemenu
                
                Name = ButtonEnum.SIDEMENU_CUSTOMIZE_UI,
                PrimaryClickID = 4,
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "Sidemenu",
                Body = "[sidemenu]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Pitlane Limiter
                
                Name = ButtonEnum.PITLANE_LIMITER,
                PrimaryClickID = 30,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List<>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.PITLANE_LIMITER_TITLE,
                        ClickID = 29
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "Pitlane Limiter",
                Body = "[Pitlane Limiter]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region DateTime
                
                Name = ButtonEnum.DATETIME,
                PrimaryClickID = 28,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List<>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.DATETIME_TITLE,
                        ClickID = 27
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "DateTime",
                Body = "[DateTime]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Blinker Left
                
                Name = ButtonEnum.BLINKER_LEFT,
                PrimaryClickID = 20,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List<>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.BLINKER_LEFT_TITLE,
                        ClickID = 45
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "Blinker Left",
                Body = "[Blinker Left]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Blinker Right
                
                Name = ButtonEnum.BLINKER_RIGHT,
                PrimaryClickID = 21,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List<>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.BLINKER_RIGHT_TITLE,
                        ClickID = 46
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "Blinker Right",
                Body = "[Blinker Right]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Lights
                
                Name = ButtonEnum.LIGHTS,
                PrimaryClickID = 12,
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "Lights",
                Body = "[Lights]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Handbrake
                
                Name = ButtonEnum.HANDBRAKE,
                PrimaryClickID = 19,
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 15,
                        Height = 15
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "Handbrake",
                Body = "[Handrake]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region TC
                
                Name = ButtonEnum.TC,
                PrimaryClickID = 14,
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "TC",
                Body = "[TC]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region ABS
                
                Name = ButtonEnum.ABS,
                PrimaryClickID = 15,
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "ABS",
                Body = "[ABS]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Fuel Consumption
                
                Name = ButtonEnum.AVG_FUEL,
                PrimaryClickID = 34,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List <>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.AVG_FUEL_CONSUMPTION_TITLE,
                        ClickID = 33
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "Fuel Consumption",
                Body = "[Fuel Consumption]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Laptime
                
                Name = ButtonEnum.LAP_TIME,
                PrimaryClickID = 36,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List <>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.LAP_TIME_TITLE,
                        ClickID = 35
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "LapTime",
                Body = "[LapTime]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Split
                
                Name = ButtonEnum.LAP_SPLIT,
                PrimaryClickID = 38,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List <>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.LAP_SPLIT_TITLE,
                        ClickID = 37
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "Split",
                Body = "[Split]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Track
                
                Name = ButtonEnum.TRACK,
                PrimaryClickID = 40,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List <>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.TRACK_TITLE,
                        ClickID = 39
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "Track",
                Body = "[Track]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region  Best PB
                
                Name = ButtonEnum.BEST_PB,
                PrimaryClickID = 42,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List <>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.BEST_PB_TITLE,
                        ClickID = 41
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 0,
                        Height = 0
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = false,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "Best PB",
                Body = "[Best PB]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Theoretical PB
                
                Name = ButtonEnum.THEORETICAL_PB,
                PrimaryClickID = 44,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List <>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.THEORETICAL_PB_TITLE,
                        ClickID = 43
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 25,
                        Height = 5
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 50,
                        Top = 50
                    }
                    #endregion
                },
                IsEnabled = false,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = ButtonStyles.ISB_DARK,
                Title = "Theoretical PB",
                Body = "[Theoretical PB]"
                
                #endregion
            },
            new ButtonGroup
            {
                #region Race Positions
                
                Name = ButtonEnum.RACE_POSITIONS,
                PrimaryClickID = 81,
                ClickIDs = new List<ButtonGroupChild>
                {
                    #region List <>
                    new ButtonGroupChild
                    {
                        Name = ButtonChild.RACE_POSITIONS_TITLE,
                        ClickID = 80
                    }
                    #endregion
                },
                Size = new List<ButtonGroupSize>
                {
                    #region List<>
                    new ButtonGroupSize
                    {
                        Width = 25,
                        Height = 10
                    }
                    #endregion
                },
                Location = new List<ButtonGroupLocation>
                {
                    #region List<>
                    new ButtonGroupLocation
                    {
                        Left = 80,
                        Top = 80
                    }
                    #endregion
                },
                IsEnabled = true,
                IsVisible = true,
                TitleColor = "^7",
                Color = "^7",
                SelectedColor = "^7",
                Style = BS.ISB_DARK,
                Title = "Race Positions",
                Body = "[Race Positions]"
                
                #endregion
            }
        };
        #endregion

        #region OS Data
        public string? OSMacAddress { get; set; }
        public string? OSUsername { get; set; }
        public string? OS { get; set; }
        #endregion

        #region LapTime
        public TimeSpan trackedLaptime { get; set; } = TimeSpan.Zero;
        public List<LaptimeAvgSpeedModel> LaptimeAvgSpeed { get; set; } = new();
        #endregion

        #region Strobe
        public bool StrobeActive { get; set; }
        public bool UseSiren { get; set; }
        public int StrobeCounter { get; set; }
        public TimeSpan StrobeOffCounter { get; set; } 
        #endregion

        public bool DisplayGui { get; set; } = true;
        public bool guiBusy { get; set; } = false;
        public bool OverwriteSidemenuVisibility { get; set; } = false;

        // Misc. Globals
        #region Misc. Globals
        public string CloseSymbolColor { get; set; } = "^1";
        public string CloseSymbol { get; set; } = "×";
        #endregion

        // Timer variables
        #region Timer Vars
        private int _startupMessageCounter;
        public int startupMessageCounter
        {
            get { return _startupMessageCounter; }
            set { _startupMessageCounter = value; }
        }
        public TimeSpan pubstatTimer { get; set; } = TimeSpan.FromSeconds(0);
        public int FuelConsumptionCounter { get; set; } = 0;
        public TimeSpan StartButtonsCounter { get; set; } = new TimeSpan(0, 0, 1);
        public int OutGaugeWarningCounter { get; set; } = 27;
        public int LoadSettingsCounter { get; set; }
        public int ListSaveCounter { get; set; } = 0;
        public TimeSpan FuelStrobe { get; set; } = TimeSpan.Zero;
        public bool FuelStrobeActive { get; set; } = false;
        public StateFlags InSimView { get; set; }
        public string TrackName { get; set; } = "";
        public bool GreetingsEnabled { get; set; }
        public bool CanGreet { get; set; } = false;
        public int GreetingMessagesCounter { get; set; } = 0;
        public bool GreetingsTemporarilyDisabled { get; set; } = false;
        public int GreetingDisabledCounter { get; set; } = 0;
        public byte AutoLightsCounter { get; set; } = 0;
        public byte AutoLightsIgnition { get; set; } = 0;
        public byte SettingsDatetimeFormatIncrement { get; set; } = 0;
        public byte fetchPbInterval { get; set; } = 5;
        public byte fetchTPBInterval { get; set; } = 5;
        #endregion

        // BTC Variables
        #region BTC

        public byte[] OnScreenButtonArray { get; } = [0,4,11,12,14,15,16,18,19,20,21,22,25,28,30,34,62];

        #endregion

        // Lists
        #region Lists - Cars
        public List<ModCarEntry> CarMods = new();
        public List<ModCarEntry> LFSCars = new();
        #endregion
        #region Lists - Messages

        #endregion

        // Session Lists
        #region Lists - Messages
        public List<Messages> GlobalMessagesCache { get; set; } = new();
        public List<Messages> PlayerMessagesCache { get; set; } = new();
        public List<CarContact> CarContactCache { get; set; } = new();
        public List<LaptimeModel> LaptimeMessagesCache { get; set; } = new();

        public List<Messages> PendingGlobalMessages { get; set; } = new();
        public List<Messages> GlobalMessages { get; set; } = new();

        public List<CarContact> PendingCarContactMessages { get; set; } = new();
        public List<CarContact> CarContactMessages { get; set; } = new();

        public List<Messages> PendingPlayerMessages { get; set; } = new();
        public List<Messages> PlayerMessages { get; set; } = new();

        public List<LaptimeModel> PendingLaptimeMessages { get; set; } = new();
        public List<LaptimeModel> LaptimeMessages { get; set; } = new();


        #endregion

        public IDictionary<byte,Player> Players = new Dictionary<byte,Player>();

        // Messages Panel
        #region Messages Panel
        public bool InMessages { get; set; }
        public int MessagesMaxPages { get; set; } = 1;
        public int MessagesRecords { get; set; } = 0;
        public int MessagesPageNumber { get; set; } = 1;
        public int MessagesSkip { get; set; } = 0;
        public int MessagesTake { get; set; } = 10;
        public BS BSMessagesPagePrevious { get; set; } = BS.ISB_CLICK | BS.ISB_LIGHT;
        public BS BSMessagesPageNext { get; set; } = BS.ISB_CLICK | BS.ISB_LIGHT;
        public string ColorMessagesPagePrevious { get; set; } = "^7";
        public string ColorMessagesPageNext { get; set; } = "^7";
        public string MessagesSearchWord { get; set; } = "";
        public bool MessagesOnlyLocalData { get; set; } = false;
        #endregion

        // Messages Button Styles
        #region Messages Button Styles
        public BS BSMessagesGlobal { get; set; } = BS.ISB_LIGHT;
        public BS BSMessagesPlayer { get; set; } = BS.ISB_LIGHT;
        public BS BSMessagesCarContact { get; set; } = BS.ISB_LIGHT;
        #endregion

        // Messages Colors
        #region Messages Colors
        public string ColorMessagesGlobal { get; set; } = "^7";
        public string ColorMessagesPlayers { get; set; } = "^7";
        public string ColorMessagesCarContact { get; set; } = "^7";
        #endregion

        public MessageTypeEnum MessageTypes { get; set; } = MessageTypeEnum.PLAYERS;

        // Help Panel
        #region Help Panel
        public bool InCustomizeHelp { get; set; }
        #endregion

        // Templates Panel
        #region Templates Panel
        public bool InTemplatesOverview { get; set; } = false;
        public bool InManageTemplate { get; set; } = false;
        public bool InAddTemplate { get; set; } = false;
        public bool InImportTemplate { get; set; } = false;
        public ButtonTemplate? SelectedTemplate { get; set; }
        public List<ButtonTemplate> Templates { get; set; } = new();
        public int TemplateSlots { get; set; } = 0;
        public string TemplateName { get; set; } = "";
        public string LastUsedTemplate { get; set; } = "";
        public BS BSExportButton { get; set; } = BS.ISB_LIGHT | BS.ISB_CLICK;
        public string ColorExportButtonColor { get; set; }
        public byte ExportDisabledCounter { get; set; } = 0;
        public BS BSImportButton { get; set; }
        public string ColorImportButton = "^7";
        public string ColorTemplateSlots = "^7";
        #endregion

        // Laptimes Panel
        #region Laptimes Panel
        public bool InLaptimes { get; set; } = false;
        public bool LaptimeSortVehicle { get; set; } = true;
        public bool LaptimeSortTrack { get; set; } = false;
        public bool LaptimeSortBestPB { get; set; } = false;

        #region ButtonStyles
        public BS BSLaptimeSortTrack { get; set; } = BS.ISB_CLICK | BS.ISB_LIGHT;
        public BS BSLaptimeSortVehicle { get; set; } = BS.ISB_CLICK | BS.ISB_LIGHT;
        public BS BSLaptimeSortBestPB { get; set; } = BS.ISB_CLICK | BS.ISB_LIGHT;
        #endregion

        #region Text Colors
        public string ColorLaptimeSortTrack { get; set; } = "^7";
        public string ColorLaptimeSortVehicle { get; set; } = "^7";
        public string ColorLaptimeSortBestPB { get; set; } = "^7";
        #endregion

        #endregion

        // Settings Panel
        #region Settings Panel
        public bool InSettings { get; set; }
        public byte SettingsPageNumber { get; set; } = 1;
        public byte SettingsFirstPage { get; set; } = 1;
        public byte SettingsLastPage { get; set; } = 4;
        public string SettingsMenuDisplay { get; set; } = "parked";
        public string FuelWarningType { get; set; } = "percent";
        public double FuelWarningValue { get; set; } = 5; // 5%
        public string SpeedoUnit { get; set; } = "kmh";
        public string FuelUnit { get; set; } = "litres";
        public string FuelUnitShort { get; set; } = "L";
        public string GearFormat { get; set; } = "classic";
        public string RpmStyle { get; set; } = "classic";
        public bool TotalFuel { get; set; }
        public string GreetingMessage { get; set; } = "Welcome, %playername%!";
        public bool MessagesRelativeTime { get; set; }
        public bool AutoLightsEnabled { get; set; } = true;
        public DateTimeFormatEnum DateTimeFormat { get; set; }
        public bool RacingMessagesEnabled { get; set; }
        public bool CarContactEnabled { get; set; } = true;
        #endregion

        // Settings Colors
        #region Settings Colors
        public string ColorSettingsPagePrevious { get; set; } = "^7";
        public string ColorSettingsPageNext { get; set; } = "^7";
        public string ColorDisplayParked { get; set; } = "^7";
        public string ColorDisplayAlways { get; set; } = "^8";
        public string ColorSpeedUnitKmh { get; set; } = "^7";
        public string ColorSpeedUnitMph { get; set; } = "^7";
        public string ColorFuelUnitLitres { get; set; } = "^7";
        public string ColorTotalFuelON { get; set; } = "^7";
        public string ColorTotalFuelOFF { get; set; } = "^7";
        public string ColorFuelUnitPercent { get; set; } = "^7";
        public string ColorGearFormatClassic { get; set; } = "^7";
        public string ColorGearFormatModern { get; set; } = "^7";
        public string ColorRpmStyleClassic { get; set; } = "^7";
        public string ColorRpmStyleModern { get; set; } = "^7";
        public string ColorConsoleChatOn { get; set; } = "^7";
        public string ColorConsoleChatOff { get; set; } = "^7";
        public string ColorGreetingMessageOn { get; set; } = "^7";
        public string ColorGreetingMessageOff { get; set; } = "^7";
        public string ColorMessagesRelativeTimeOff { get; set; } = "^7";
        public string ColorMessagesRelativeTimeOn { get; set; } = "^7";
        public string ColorAutoLightsOff { get; set; } = "^7";
        public string ColorAutoLightsOn { get; set; } = "^7";
        public string ColorDateTime { get; set; } = "^7";
        public string ColorLaptimeMessagesOn { get; set; } = "^7";
        public string ColorLaptimeMessagesOff { get; set; } = "^7";
        public string ColorCarContactOn { get; set; } = "^7";
        public string ColorCarContactOff { get; set; } = "^7";
        #endregion

        // Settings ButtonStyles
        #region Settings ButtonStyles
        public BS BSSettingsPagePrevious { get; set; }
        public BS BSSettingsPageNext { get; set; }
        public BS BSSettingsMenuDisplayParked { get; set; }
        public BS BSSettingsMenuDisplayAlways { get; set; }
        public BS BSSettingsSpeedUnitKmh { get; set; }
        public BS BSSettingsSpeedUnitMph { get; set; }
        public BS BSSettingsFuelUnitLitres { get; set; }
        public BS BSSettingsFuelUnitPercent { get; set; }
        public BS BSSettingsTotalFuelON { get; set; }
        public BS BSSettingsTotalFuelOFF { get; set; }
        public BS BSSettingsGearFormatClassic { get; set; }
        public BS BSSettingsGearFormatModern { get; set; }
        public BS BSSettingsRpmStyleClassic { get; set; }
        public BS BSSettingsRpmStyleModern { get; set; }
        public BS BSSettingsConsoleChatOn { get; set; }
        public BS BSSettingsConsoleChatOff { get; set; }
        public BS BSSettingsGreetingMessageOn { get; set; }
        public BS BSSettingsGreetingMessageOff { get; set; }
        public BS BSSettingsMessagesRelativeTimeOn { get; set; }
        public BS BSSettingsMessagesRelativeTimeOff { get; set; }
        public BS BSSettingsAutoLightsOn { get; set; }
        public BS BSSettingsAutoLightsOff { get; set; }
        public BS BSSettingsLaptimeMessagesOn { get; set; }
        public BS BSSettingsLaptimeMessagesOff { get; set; }
        public BS BSSettingsCarContactOn { get; set; }
        public BS BSSettingsCarContactOff { get; set; }
        #endregion

        // Customize UI
        #region Customize UI
        public bool InCustomizeUI { get; set; }
        public List<byte> SelectedClickIDs { get; set; } = new List<byte>();
        public byte LastSelectedId { get; set; }
        public MoveOrResizeEnum MoveOrResize { get; set; } = MoveOrResizeEnum.MOVE;
        public BS BSCustomizeMove { get; set; }
        public BS BSCustomizeResize { get; set; }
        public byte CustomizeMultiplier { get; set; } = 10;
        public BS BSArrowsCustomize { get; set; }
        public string ColorCustomizeArrows { get; set; } = "^8";
        public byte SelectedSize_H { get; set; } = 0;
        public byte SelectedSize_W { get; set; } = 0;
        public MoveCustomizerEnum MoveCustomizerWindow { get; set; } = MoveCustomizerEnum.NONE;
        public string ColorMoveCustomizerWindow { get; set; } = "^7";
        public BS BSMoveCustomizerWindow { get; set; }
        #endregion
        
        // Race Positions Panel/Window

        #region Race Positions Window

        public byte skipRacers { get; set; } = 0;
        public byte takeRacers { get; set; } = 4;
        public TimeSpan cycleRacers { get; set; } = new TimeSpan(0, 0, 30);

        #endregion

        // InSim
        #region InSim
        public InSimClient? insim { get; set; }
        public bool InsimInitialized { get; set; }
        #endregion

        // OutGauge object
        #region OutGauge
        public OutGauge? outgauge { get; set; }
        #endregion

        // OutGauge variables
        #region OutGauge Variables
        public double vehicleFuel { get; set; }
        public double vehicleRpm { get; set; }
        public double VehicleThrottle { get; set; }
        public double vehicleKmh { get; set; }
        public double vehicleMph { get; set; }
        public bool vehicleTC { get; set; }
        public bool vehicleLights { get; set; }
        public bool vehicleAbs { get; set; }
        public string vehicleGear { get; set; } = "";
        public bool vehicleHandbrake { get; set; }
        public DashLightFlags vehicleDashFlag { get; set; }
        public float vehicleTankSize { get; set; } = 0;
        public double vehicleFuelLitres { get; set; } = 0;
        public double vehicleFuelGallons { get; set; } = 0;
        public double vehicleFuelPercent { get; set; } = 0;
        public double vehicleTurbo { get; set; }
        public ShiftTypes? vehicleShiftType { get; set; }
        public bool vehiclePitSpeed { get; set; }
        #endregion

        // Session variables
        #region Session
        public bool IsLocal { get; set; }
        public int MaxRpm { get; set; }
        public byte UCID { get; set; }
        #endregion

        // Button Styles
        #region Button Styles
        public BS BSSettings { get; set; }
        public BS BSMessages { get; set; }
        public BS BSSidemenuLaptimes { get; set; }
        #endregion

        // Button Colors
        #region Button Colors
        public string? ColorSettings { get; set; }
        public string? ColorMessages { get; set; }
        #endregion

        // Button IsSelected color (in customization mode)
        #region Button is selected (color strings)
        public string? ActionMove_Sel { get; set; }
        public string? ActionResize_Sel { get; set; }
        #endregion

        // SQLite Related
        #region SQLite Related
        public SqliteConnection SQLConnection { get; set; } = new SqliteConnection("Data Source=Data.db");
        public bool ConsoleChat { get; set; }
        public bool AutoStart { get; set; } = true;
        public string lfswPubstat { get; set; } = "none";
        #endregion

        // SQLite Columns
        #region SQLite Columns
        public string[] SQLColumns_Settings { get; set; } =
        {
            "Setting VARCHAR(128) NOT NULL",
            "Value INT(2) NOT NULL",
            "Modified DATETIME"
        };
        public string[] SQLColumns_PlayerMessages { get; set; } =
        {
            "Nickname VARCHAR(128) NOT NULL",
            "Message VARCHAR(256) NOT NULL",
            "Date DATETIME NOT NULL",
            "Username VARCHAR(128) DEFAULT NULL" // new in 3.9
        };
        public string[] SQLColumns_ButtonPositions { get; set; } =
        {
            "Property VARCHAR(128) NOT NULL",
            "Value VARCHAR(128) NOT NULL",
            "Modified DATETIME NOT NULL"
        };
        public string[] SQLColumns_GlobalMessages { get; set; } =
        {
            "Date DATETIME NOT NULL",
            "Message VARCHAR(256) NOT NULL"
        };
        public string[] SQLColumns_Templates { get; set; } =
        {
            "Name VARCHAR(64) NOT NULL",
            "Created DATETIME NOT NULL",
            "IsDeleted INT(1) NOT NULL",
            "DateDeleted DATETIME NULL",
        };
        public string[] SQLColumns_CarContact { get; set; } =
        {
            "SourceUName VARCHAR(128) NOT NULL",
            "SourcePName VARCHAR(128) NOT NULL",
            "TargetUName VARCHAR(128) NOT NULL",
            "TargetPName VARCHAR(128) NOT NULL",
            "Speed INT(9) NOT NULL",
            "Date DATETIME NULL",
        };

        public string[] SQLColumns_Laptime { get; set; } =
        {
            "Username VARCHAR(128)",
            "Playername VARCHAR(128)",
            "Split1 INT(7)",
            "Split2 INT(7)",
            "Split3 INT(7)",
            "Laptime INT(7)",
            "Vehicle VARCHAR(32)",
            "Track VARCHAR(16)",
            "Date DATETIME"
        };
        #endregion

        // SQLite ButtonProperties
        #region ' SQLite Button Properties '
        #region Lights
        public byte Lights_L { get; set; }
        public byte Lights_T { get; set; }
        #endregion
        #region Abs
        public byte Abs_L { get; set; }
        public byte Abs_T { get; set; }
        #endregion
        #endregion
    }

    public class Counters
    {
        public TimeSpan CarParkedCounter { get; set; } = TimeSpan.Zero;
        public TimeSpan Siren{ get; set; }
    }

    public class Application
    {
        #region InSim Connection Variables
        public string? Host { get; set; }
        public int Port { get; set; }
        public string? AdminPassword { get; set; }
        public int OutGaugePort { get; set; }
        public string? LFS_API_CLIENT_ID { get; set; }
        public string? LFS_API_CLIENT_SECRET { get; set; }
        #endregion
    }
}
