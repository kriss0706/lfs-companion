﻿using LFSCompanion.Application.SQL;
using LFSCompanion.Insim.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.Classes
{
    public class Connection
    {
        private string FolderName = "Config";
        private string FileName = "Connection.ini";
        private Variables _var;
        private ExceptionProcessor _exc;
        private InSimMethods _insimMethods;
        private SQLContext _sqlContext;

        public Connection(Variables variables,
            ExceptionProcessor exc,
            InSimMethods insimMethods,
            SQLContext sqlContext)
        {
            _var = variables;
            _exc = exc;
            _insimMethods = insimMethods;
            _sqlContext = sqlContext;
        }

        public async Task Validate()
        {
            await FolderExist();
            await FileExist();
            await ReadConnectionFile();
        }

        #region ' Readers '

        public async Task<string?> GetProperty(string Property)
        {
            var path = Path.Combine(FolderName, FileName);
            return await GetValueForKey(path,Property);
        }

        public async Task ReadConnectionFile()
        {
            _var.Application.Host = await GetProperty("Host");
            _var.Application.Port = Convert.ToInt32(await GetProperty("Port"));
            _var.Application.AdminPassword = await GetProperty("Admin");
            _var.Application.OutGaugePort = Convert.ToInt32(await GetProperty("OutGauge"));
            _var.Application.LFS_API_CLIENT_ID = await GetProperty("LFS_API_CLIENT_ID");
            _var.Application.LFS_API_CLIENT_SECRET = await GetProperty("LFS_API_CLIENT_SECRET");
        }

        #endregion

        #region Validation
        private async Task FolderExist()
        {
            if (Directory.Exists(FolderName) == false)
                await CreateFolder(FolderName);
        }

        private async Task FileExist()
        {
            var configPath = Path.Combine(FolderName, FileName);
            
            if (!File.Exists(configPath))
                await CreateFile(FolderName, FileName);
        }
        #endregion

        #region Generation
        private async Task CreateFolder(string _FolderName)
        {
            await Task.Run(() =>
            {
                Directory.CreateDirectory(_FolderName);
            });
        }

        private async Task CreateFile(string _FolderName, string _FileName)
        {
            Console.WriteLine($@"{FileName} was not found, creating..");

            var content = new string[]
            {
                "Host = 127.0.0.1",
                "Port = 29999",
                "Admin = ",
                "OutGauge = 30000",
                "LFS_API_CLIENT_ID = [api_client_id]",
                "LFS_API_CLIENT_SECRET = [api_client_secret]"
            };

            var path = Path.Combine(_FolderName, FileName);
            await AppendFileAsync(path, content);
        }
        #endregion

        #region Methods

        private static async Task AppendFileAsync(string filePath, string[] lines)
        {
            await using StreamWriter writer = new StreamWriter(filePath, append: true);
    
            foreach (var line in lines)
            {
                await writer.WriteLineAsync(line);
            }
        }

        private static async Task<string?> GetValueForKey(string filePath, string key)
        {
            // Get the value by key in config file
            if (File.Exists(filePath))
            {
                var lines = File.ReadAllLines(filePath);

                foreach (var line in lines)
                {
                    var parts = line.Split('=');
                    if (parts.Length != 2)
                        continue;
                    
                    var currentKey = parts[0].Trim();
                    var value = parts[1].Trim();

                    if (currentKey.Equals(key, StringComparison.OrdinalIgnoreCase))
                        return value;
                }
            }

            Console.WriteLine($@"Config key {key} was not found, creating..");
            
            var missingKey = new[] { $"{key} = replace_me" };
            await AppendFileAsync(filePath, missingKey);

            return null;
        }
        #endregion
    }
}
