﻿using LFSCompanion.Insim.Classes;
using LFSCompanion.Insim.Models;
using LFSCompanion.Insim.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using InSimDotNet.Packets;

namespace LFSCompanion.Application.Classes
{
    public class ButtonManager
    {
        private Variables _vars;

        public ButtonManager(Variables variables)
        {
            _vars = variables;
        }

        #region ' Public '
        /// <summary>
        /// Get ButtonGroup
        /// </summary>
        /// <param name="buttonName"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public ButtonGroup Get(ButtonEnum buttonName)
        {
            if (_vars.ButtonList.Count == 0)
                throw new Exception("Button Storage is empty.");

            ButtonGroup? group = _vars.ButtonList
                .FirstOrDefault(x => x.Name.ToString().Contains(buttonName.ToString()));

            if (group == null)
                throw new Exception($"ClickID group {buttonName} does not exist.");
            else
                return group;
        }

        /// <summary>
        /// Get or set button size
        /// </summary>
        /// <param name="buttonName"></param>
        /// <param name="newSize"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public ButtonGroupSize Size(ButtonEnum buttonName, ButtonGroupSize? newSize = null)
        {
            if (_vars.ButtonList.Count == 0)
                throw new Exception("Button Storage is empty.");
            
            var buttonList = _vars.ButtonList.FirstOrDefault(x => x.Name == buttonName);
            
            if (buttonList?.Size == null || buttonList.Size.Count == 0)
                throw new Exception($"Button not found or size not set for {buttonName.ToString()}.");
            
            var oldSize = buttonList.Size.First();

            if (newSize == null)
                return oldSize;

            return newSize;
        }

        /// <summary>
        /// Get or set location
        /// </summary>
        /// <param name="ButtonName"></param>
        /// <param name="newLocation"></param>
        /// <param name="buttonName"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public ButtonGroupLocation Location(ButtonEnum buttonName, ButtonGroupLocation? newLocation = null)
        {
            if (_vars.ButtonList.Count == 0)
                throw new Exception("Button Storage is empty.");

            var buttonList = _vars.ButtonList.FirstOrDefault(x => x.Name == buttonName);
            
            if (buttonList?.Location == null || buttonList.Location.Count == 0)
                throw new Exception("Button not found or location not set.");

            var oldLocation = buttonList.Location.First();

            if (newLocation == null)
                return oldLocation;

            return newLocation;
        }
        
        public byte? getTitleClickId(ButtonEnum buttonName)
        {
            var clickIds = _vars.ButtonList.FirstOrDefault(x => x.Name == buttonName)?.ClickIDs;
            
            if (clickIds == null || clickIds.Count == 0)
                return null; // Return null if no valid ClickID is found
            
            return clickIds.FirstOrDefault()?.ClickID;
        }

        #endregion
    }
}
