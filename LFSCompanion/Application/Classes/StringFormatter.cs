﻿using LFSCompanion.Insim.Events;
using InSimDotNet.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LFSCompanion.Insim.Models.StringModel;

namespace LFSCompanion.Application.Classes
{
    public class StringFormatter
    {
        private ExceptionProcessor _exc;
        public StringFormatter(ExceptionProcessor exception_processor)
        {
            _exc = exception_processor;
        }

        public async Task<string> toDoubleDigits(double digit, string? symbol = null)
        {
            try
            {
                string formatted = digit.ToString("0.00",System.Globalization.CultureInfo.InvariantCulture) + symbol;
                return formatted;
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "toDoubleDigit()", ex);
                return "";
            }
        }

        public async Task<string> toSingleDigit(double digit, string? symbol = null)
        {
            try
            {
                string formatted = digit.ToString("0.0",System.Globalization.CultureInfo.InvariantCulture) + symbol;
                return formatted;
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "toDoubleDigit()", ex);
                return "";
            }
        }

        public async Task<string> toNumber(double Digit)
        {
            try
            {
                string formatted = Digit.ToString("n0",System.Globalization.CultureInfo.InvariantCulture);
                return formatted;
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "toSingleNumber() in StringFormatter class", ex);
                return "";
            }
        }

        /// <summary>
        /// Colorize state messages (connect, disconnect, pit stop etc.)
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        public async Task<string> colorizeString(string Message, string Type)
        {
            try
            {
                if (Type == "GlobalMessages")
                {
                    string[] ConnectionStrings = new string[]
                    {
                        " connected (",
                        " is connecting...",
                        " disconnected (",
                        " ^7: ",
                        " joined the spectators",
                        " left the pits (",
                        " made a pit stop",
                        "- ^9",
                        " pitted",
                        " returned to the pit lane"
                    };

                    if (Message.Contains(ConnectionStrings[0]))
                    {
                        string[] split = Message.Split(" connected ");
                        return $"^3{split[0]} ^8connected {split[1]}";
                    }
                    else if (Message.Contains(ConnectionStrings[1]))
                    {
                        string[] split = Message.Split(ConnectionStrings[1]);
                        return $"^7{split[0]} ^8is connecting...";
                    }
                    else if (Message.Contains(ConnectionStrings[2]))
                    {
                        string[] split = Message.Split(" disconnected ");
                        return $"^7{split[0]} ^8disconnected {split[1]}";
                    }
                    else if (Message.Contains(ConnectionStrings[3]))
                    {
                        if (Message.Contains(ConnectionStrings[7]))
                        {
                            string[] _split = Message.Split("- ^9");
                            return $"^7{_split[0]} ^7- ^2{_split[1]}";
                        }
                        else
                        {
                            string[] split = Message.Split(" ^7: ");
                            return $"^7{split[0]} ^7: ^2{split[1]}";
                        }
                    }
                    else if (Message.Contains(ConnectionStrings[4]))
                    {
                        string[] split = Message.Split(" joined ");
                        return $"^7{split[0]} ^8joined {split[1]}";
                    }
                    else if (Message.Contains(ConnectionStrings[5]))
                    {
                        string[] split = Message.Split(" left ");
                        return $"^7{split[0]} ^8left {split[1]}";
                    }
                    else if (Message.Contains(ConnectionStrings[6]))
                    {
                        string[] split = Message.Split(" made ");
                        return $"^7{split[0]} ^8made {split[1]}";
                    }
                    else if (Message.Contains(ConnectionStrings[8]))
                    {
                        string[] split = Message.Split(" pitted");
                        return $"^7{split[0]} ^8pitted";
                    }
                    else if (Message.Contains(ConnectionStrings[9]))
                    {
                        string[] split = Message.Split(" returned ");
                        return $"^7{split[0]} ^8returned {split[1]}";
                    }
                    else
                    {
                        return Message;
                    }
                }
                else if (Type == "PlayerMessages")
                {
                    if (Message.Contains("- ^9"))
                    {
                        string[] split = Message.Split("- ^9");
                        return $"^7{split[0]}^7- ^2{split[1]}";
                    }
                    else
                    {
                        return $"^2{Message}";
                    }
                }
                else
                {
                    return Message;
                }
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "ColorizeString() in StringFormatter class", ex);
                return "";
            }
        }

        public async Task<string> getRaceTime(double TotalMilliseconds)
        {
            try
            {
                if (TotalMilliseconds <= 0)
                    return "--";
                
                return $@"{TimeSpan.FromMilliseconds(TotalMilliseconds):%m\:ss\.ff}";
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "GetRaceTime() in StringFormatter class", ex);
                return "";
            }
        }
        
        public async Task<string> getRaceTime(TimeSpan ts)
        {
            try
            {
                if (ts.TotalHours >= 1)
                    return $@"{ts:hh\:mm\:ss\.ff}";

                if (ts.TotalMinutes >= 1)
                    return $@"{ts:mm\:ss\.ff}";

                return $@"{ts:ss\.ff}";
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "GetRaceTime() in StringFormatter class", ex);
                return string.Empty;
            }
        }
        
        public async Task<string> getRaceTimeDifference(double timeMilliseconds, double packetMilliseconds)
        {
            try
            {
                if (timeMilliseconds == 0)
                    return "";
                
                var _difference = TimeSpan.FromMilliseconds(timeMilliseconds - packetMilliseconds);
                
                #region Empty Variables
                string timediff_string;
                string diff_color;
                string diff_symbol;
                #endregion
                
                // Include minutes in string format
                timediff_string = _difference.Minutes == 0 ? $@"{_difference:ss\.ff}" : $@"{_difference:mm\:ss\.ff}";
                
                #region Difference Color
                if (packetMilliseconds < timeMilliseconds)
                {
                    // PB was beaten
                    diff_color = "^2";
                    diff_symbol = "-";
                    return $" {diff_color}[{diff_symbol}{timediff_string}]";
                }
                
                // PB was not beaten
                diff_color = "^1";
                diff_symbol = "+";
                #endregion

                return $" {diff_color}[{diff_symbol}{timediff_string}]";
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "getRaceTimeDifference() in StringFormatter class", ex);
                return "";
            }
        }

        /// <summary>
        /// Get message string - used internally
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<string> getString(StringEnum value)
        {
            try
            {
                switch (value)
                {
                    case StringEnum.ERROR_MESSAGE:
                        return "See /logs/error.txt for more information.";

                    case StringEnum.MESSAGE_PREFIX:
                        return "^6[X] ^8";
                    
                    case StringEnum.BTC_EXCEED_RANGE_MIN:
                        return "You can't exceed the minimum number of pages! Out of range.";
                    
                    case StringEnum.BTC_EXCEED_RANGE_MAX:
                        return "You can't exceed the maximum number of pages! Out of range.";
                    
                    default:
                        return "";
                }
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "Could not retrieve string!", ex);
                return "";
            }
        }
    }
}
