﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.Classes
{
    public class DateHelper
    {
        ExceptionProcessor _exc = new();

        /// <summary>
        /// Return a full date (DateTime object)
        /// </summary>
        /// <returns></returns>
        public async Task<DateTime> getFullDate()
        {
            try
            {
                DateTime date = DateTime.Now;
                return date;
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "getFullDate in DateHelper class", ex);
            }

            return DateTime.MinValue;
        }

        /// <summary>
        /// Return a full date (string)
        /// </summary>
        /// <returns></returns>
        public async Task<string> getFullDateString()
        {
            string _string = "";

            try
            {
                DateTime date = DateTime.Now;
                _string = date.ToString("dd/MM/yyyy HH:mm");
                return _string;
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "getFullDate in DateHelper class", ex);
            }

            return _string;
        }
    }
}
