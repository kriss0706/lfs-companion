﻿using LFSCompanion.Insim;
using InSimDotNet;
using InSimDotNet.Packets;
using LFSCompanion.Application.SQL;
using LFSCompanion.Application.Utils;
using LFSCompanion.Insim.Classes;
using LFSCompanion.Insim.Events;
using LFSCompanion.Insim.Models;
using System.Runtime.CompilerServices;
using EXC = LFSCompanion.Application.Classes.ExceptionProcessor;

namespace LFSCompanion.Application.Classes
{
    /// <summary>
    /// Methods used primarily for the insim protocol
    /// </summary>
    /// 

    public class Main
    {
        private InSimMethods _methods;
        private EXC _exc;
        private Variables _vars;
        private outGauge _gauge;
        private SQLContext _sqlContext;
        private ButtonManager _clickidManager;
        private WindowFactory _windowFactory;
        private Migration _migration;
        private TemplateHelper _template;
        private DirectoryFactory _dir;
        private CarInfo _carInfo;
        private StringFormatter _stringFormatter;
        private LapManager _lapManager;
        private LFSW _lfsw;

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="exc"></param>
        /// <param name="vars"></param>
        public Main(EXC exc, Variables vars, InSimMethods insimMethods, outGauge gauge, SQLContext sqlContext,
            ButtonManager clickIdManager, WindowFactory windowFactory, Migration migration, TemplateHelper template, DirectoryFactory directoryFactory,
            StringFormatter stringFormatter, CarInfo carInfo, LapManager lapManager, LFSW lfsw)
        {
            _exc = exc;
            _vars = vars;
            _methods = insimMethods;
            _gauge = gauge;
            _sqlContext = sqlContext;
            _clickidManager = clickIdManager;
            _windowFactory = windowFactory;
            _migration = migration;
            _template = template;
            _dir = directoryFactory;
            _stringFormatter = stringFormatter;
            _carInfo = carInfo;
            _lapManager = lapManager;
            _lfsw = lfsw;

        }

        /// <summary>
        /// Attempt to connect to the insim protocol
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        [STAThread]
        public async Task Run()
        {
            try
            {
                if (_vars.insim != null)
                {
                    await _vars.insim.InitializeAsync(new InSimSettings
                    {
                        Admin = _vars.Application.AdminPassword,
                        Host = _vars.Application.Host,
                        Port = _vars.Application.Port,
                        IName = "LFS Companion",
                        Flags = InSimFlags.ISF_MSO_COLS | InSimFlags.ISF_LOCAL | InSimFlags.ISF_MCI | InSimFlags.ISF_CON | InSimFlags.ISF_NLP,
                        Interval = 100 
                    });

                    _vars.startupMessageCounter = 0;
                    _vars.InsimInitialized = true;

                    await BindPackets();

                    #region IS_TINY
                    await _vars.insim.SendAsync(new IS_TINY
                    {
                        SubT = TinyType.TINY_NCN,
                        ReqI = 255
                    });

                    await _vars.insim.SendAsync(new IS_TINY
                    {
                        SubT = TinyType.TINY_NPL,
                        ReqI = 255
                    });
                    #endregion
                    
                    await _gauge.Initialize();

                    if (_vars.ConsoleChat == true)
                        await _exc.Log(
                            EXC.LogTypeEnum.CONSOLE,
                            $"Type 'chat' if you wish to disable chat in the console", null);
                    else
                        await _exc.Log(
                        EXC.LogTypeEnum.CONSOLE,
                        $"Type 'chat' if you wish to enable chat in the console", null);

                    // await _methods.LMsg($"^8InSim initialized ^2successfully ^8with version " +
                    // $"{_vars.Version.Major}.{_vars.Version.Minor}.{_vars.Version.Build}{_vars.DevBuildString}");

                    // Get the first state on application startup
                    await _vars.insim.SendAsync(new IS_TINY
                    {
                        ReqI = 1,
                        SubT = TinyType.TINY_SST
                    });
                    
                    // Change NLP interval
                    await _vars.insim.SendAsync(new IS_SMALL
                    {
                        ReqI = 0,
                        SubT = SmallType.SMALL_NLI,
                        UVal = 50
                    });
                }
            }
            catch (Exception ex)
            {
                await _exc.Log(
                    EXC.LogTypeEnum.CONSOLE,
                    $"Connection to insim protocol was unsuccessful. " +
                    $"{await _stringFormatter.getString(StringModel.StringEnum.ERROR_MESSAGE)}", ex);

                await _exc.Log(EXC.LogTypeEnum.ERROR, $"InSim Run Method", ex);
            }
        }

        /// <summary>
        /// Disconnect from the insim protocol
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        public async Task Stop()
        {
            try
            {
                if (_vars.insim != null)
                {
                    await _vars.insim.SendAsync(new IS_TINY
                    {
                        ReqI = 0,
                        SubT = TinyType.TINY_CLOSE,
                    });

                    await _exc.Log(
                        EXC.LogTypeEnum.CONSOLE,
                        $"Connection to insim protocol has been shut down.", null);

                    _vars.InsimInitialized = false;
                }
            }
            catch (Exception ex)
            {
                await _exc.Log(
                    EXC.LogTypeEnum.CONSOLE,
                    $"The attempt to stop the insim has failed. " +
                    $"{await _stringFormatter.getString(StringModel.StringEnum.ERROR_MESSAGE)}", null);

                await _exc.Log(EXC.LogTypeEnum.ERROR, $"{ex.Message} {ex.StackTrace}", ex);
            }
        }

        /// <summary>
        /// Bind InSim packets.
        /// This method allows the application to make use of them.
        /// </summary>
        /// <returns></returns>
        public async Task BindPackets()
        {
            try
            {
                if (_vars.InsimInitialized)
                {
                    MSO mso = new(_vars,_sqlContext, _methods, _exc, _windowFactory);
                    BTC btc = new(_vars,_exc,_methods, _sqlContext,
                        _clickidManager, _windowFactory, _migration, _template,
                        _dir, _stringFormatter, _carInfo, _lapManager, _lfsw);
                    NCN ncn = new(_vars,_exc,_methods, _sqlContext, _stringFormatter, _lfsw);
                    CNL cnl = new(_vars,_exc,_methods, _sqlContext);
                    NPL npl = new(_vars,_exc,_methods, _sqlContext, _lapManager, _carInfo);
                    PLP plp = new(_vars,_exc,_methods, _sqlContext, _lapManager, _carInfo);
                    NLP nlp = new(_vars,_exc,_methods, _sqlContext, _lapManager, _carInfo, _windowFactory);
                    TOC toc = new(_vars,_exc,_methods, _sqlContext, _lapManager, _carInfo, _windowFactory);
                    BTT btt = new(_vars,_exc,_methods, _sqlContext, _windowFactory, _migration);
                    STA sta = new(_vars,_exc,_methods, _sqlContext);
                    ISM ism = new(_vars,_exc,_methods);
                    BFN bfn = new(_vars, _exc, _methods, _sqlContext);
                    PLL pll = new(_vars, _exc, _methods, _sqlContext, _lapManager);
                    LAP lap = new(_vars, _exc, _methods, _sqlContext, _stringFormatter, _lapManager);
                    RST rst = new(_vars, _exc, _methods, _sqlContext, _stringFormatter, _lapManager);
                    MCI mci = new(_vars, _sqlContext, _methods);
                    CON con = new(_vars, _exc, _methods, _sqlContext);
                    CPR cpr = new(_vars, _exc, _methods, _sqlContext);
                    SPX spx = new(_vars, _exc, _methods, _sqlContext, _stringFormatter, _lapManager);

                    #region > IS_MSO <
                    _vars.insim.IS_MSO += async (o, e) =>
                    {
                        try
                        {
                            await mso.MessageOut(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR, $"BIND IS_MSO", ex);
                        }
                    };
                    #endregion
                    #region > IS_BTC <
                    _vars.insim.IS_BTC += async (o, e) =>
                    {
                        try
                        {
                            await btc.buttonClick(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR, $"BIND IS_BTC", ex);
                        }
                    };
                    #endregion
                    #region > IS_NCN <
                    _vars.insim.IS_NCN += async (o, e) =>
                    {
                        try
                        {
                            await ncn.NewConnection(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR,
                                $"BIND IS_NCN", ex);
                        }
                    };
                    #endregion
                    #region > IS_CNL <
                    _vars.insim.IS_CNL += async (o, e) =>
                    {
                        try
                        {
                            await cnl.connLeave(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR,
                                $"BIND IS_CNL", ex);
                        }
                    };
                    #endregion
                    #region > IS_NPL <
                    _vars.insim.IS_NPL += async (o, e) =>
                    {
                        try
                        {
                            await npl.NewPlayer(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR,
                                $"BIND IS_NPL", ex);
                        }
                    };
                    #endregion
                    #region > IS_PLP <
                    _vars.insim.IS_PLP += async (o, e) =>
                    {
                        try
                        {
                            await plp.PLayerPit(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR,
                                $"BIND IS_PLP", ex);
                        }
                    };
                    #endregion
                    #region > IS_NLP <
                    _vars.insim.IS_NLP += async (o, e) =>
                    {
                        try
                        {
                            await nlp.NodeLaP(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR,
                                $"BIND IS_NLP", ex);
                        }
                    };
                    #endregion
                    #region > IS_TOC <
                    _vars.insim.IS_TOC += async (o, e) =>
                    {
                        try
                        {
                            await toc.TakeOverCar(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR,
                                $"BIND IS_TOC", ex);
                        }
                    };
                    #endregion
                    #region > IS_BTT <
                    _vars.insim.IS_BTT += async (o, e) =>
                    {
                        try
                        {
                            await btt.ButtonType(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR, $"BIND IS_BTT", ex);
                        }
                    };
                    #endregion
                    #region > IS_STA <
                    _vars.insim.IS_STA += async (o, e) =>
                    {
                        try
                        {
                            await sta.StateChanged(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR, $"BIND IS_STA", ex);
                        }
                    };
                    #endregion
                    #region > IS_ISM <
                    _vars.insim.IS_ISM += async (o, e) =>
                    {
                        try
                        {
                            await ism.InSimMulti(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR, $"BIND IS_ISM", ex);
                        }
                    };
                    #endregion
                    #region > IS_BFN <
                    _vars.insim.IS_BFN += async (o, e) =>
                    {
                        try
                        {
                            await bfn.ButtonFunction(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR, $"BIND IS_BFN", ex);
                        }
                    };
                    #endregion
                    #region > IS_PLL <
                    _vars.insim.IS_PLL += async (o, e) =>
                    {
                        try
                        {
                            await pll.PlayerLeave(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR, $"BIND IS_PLL", ex);
                        }
                    };
                    #endregion
                    #region > IS_LAP <
                    _vars.insim.IS_LAP += async (o, e) =>
                    {
                        try
                        {
                            await lap.LapTime(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR, $"BIND IS_LAP", ex);
                        }
                    };
                    #endregion
                    #region > IS_RST <
                    _vars.insim.IS_RST += async (o, e) =>
                    {
                        try
                        {
                            await rst.RaceSTart(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR, $"BIND IS_RST", ex);
                        }
                    };
                    #endregion
                    #region > IS_MCI <
                    _vars.insim.IS_MCI += async (o, e) =>
                    {
                        try
                        {
                            await mci.MultiInfo(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR, $"BIND IS_MCI", ex);
                        }
                    };
                    #endregion
                    #region > IS_CON <
                    _vars.insim.IS_CON += async (o, e) =>
                    {
                        try
                        {
                            await con.CarContact(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR, $"BIND IS_CON", ex);
                        }
                    };
                    #endregion
                    #region > IS_CPR <
                    _vars.insim.IS_CPR += async (o, e) =>
                    {
                        try
                        {
                            await cpr.ConnectionRename(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR, $"BIND IS_CPR", ex);
                        }
                    };
                    #endregion
                    #region > IS_SPX <
                    _vars.insim.IS_SPX += async (o, e) =>
                    {
                        try
                        {
                            await spx.SplitTime(o, e);
                        }
                        catch (Exception ex)
                        {
                            await _exc.Log(EXC.LogTypeEnum.ERROR, $"BIND IS_SPX", ex);
                        }
                    };
                    #endregion
                }
            }
            catch (Exception ex)
            {
                await _exc.Log(
                    EXC.LogTypeEnum.CONSOLE,
                    $"Could not bind insim packets" +
                    $"{await _stringFormatter.getString(StringModel.StringEnum.ERROR_MESSAGE)}", ex);

                await _exc.Log(EXC.LogTypeEnum.ERROR, $"Could not bind insim packets", ex);
            }
        }
    }
}
