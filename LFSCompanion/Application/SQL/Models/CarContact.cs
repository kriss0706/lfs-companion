﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.SQL.Models
{
    public class CarContact
    {
        public string SourceUName { get; set; }
        public string SourcePName { get; set; }
        public string TargetUName { get; set; }
        public string TargetPName { get; set; }
        public long Speed { get; set; }
        public DateTime Date { get; set; }
    }
}
