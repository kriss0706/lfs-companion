﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.SQL.Models.Enum
{
    public enum NotifyMigrationEnum
    {
        START,
        END
    }
}
