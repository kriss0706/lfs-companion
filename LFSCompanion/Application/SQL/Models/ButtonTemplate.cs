﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.SQL.Models
{
    public class ButtonTemplate
    {
        public string? Name { get; set; }
        public DateTime? Created { get; set; }
        public int? IsDeleted { get; set; }
        public DateTime? DateDeleted { get; set; }
    }
}
