﻿using LFSCompanion.Insim.Classes;
using LFSCompanion.Application.Classes;
using LFSCompanion.Application.Models.Enum;
using LFSCompanion.Application.SQL.Models;
using LFSCompanion.Application.SQL.Seeders;
using LFSCompanion.Application.Utils;
using Microsoft.Data.Sqlite;
using Newtonsoft.Json.Linq;
using System;
using System.Buffers.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Common;
using System.Data.SqlTypes;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using static LFSCompanion.Application.SQL.Seeders.SeederModels;
using static LFSCompanion.Insim.Models.Enum.ButtonEnum;

namespace LFSCompanion.Application.SQL
{
    /// <summary>
    /// SQL Context Class
    /// </summary>
    public class SQLContext
    {
        private SqliteConnection _connection;
        private ExceptionProcessor _exceptionProcessor;
        private ButtonPositionsSeeder _ButtonPositions_Seeder;
        private SettingsSeeder _SettingsSeeder;
        private Variables _vars;
        private DirectoryFactory _dir;
        private ButtonManager _buttonManager;

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="exceptionProcessor"></param>
        /// <param name="variables"></param>
        /// <param name="buttonPositions_Seeder"></param>
        /// <param name="settings_Seeder"></param>
        public SQLContext(ExceptionProcessor exceptionProcessor, Variables variables,
            ButtonPositionsSeeder buttonPositions_Seeder, SettingsSeeder settings_Seeder, DirectoryFactory directoryFactory,
            ButtonManager buttonManager)
        {
            _vars = variables;
            _exceptionProcessor = exceptionProcessor;
            _ButtonPositions_Seeder = buttonPositions_Seeder;
            _connection = variables.SQLConnection;
            _SettingsSeeder = settings_Seeder;
            _dir = directoryFactory;
            _buttonManager = buttonManager;
        }

        /// <summary>
        /// Validate database
        /// </summary>
        /// <returns></returns>
        public async Task Validate()
        {
            try
            {
                await ValidateDatabase();
                await _ButtonPositions_Seeder.Seeder();
                await _SettingsSeeder.Seeder();
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"Validate() in SQLContext class", ex);
            }
        }

        /// <summary>
        /// Validate database file
        /// </summary>
        /// <returns></returns>
        public async Task ValidateDatabase()
        {
            try
            {
                if (!File.Exists(_connection.DataSource))
                {
                    await Task.Run(() =>
                    {
                        File.Create(_connection.DataSource).Close();
                    });
                }

                await VerifyTableExistence();
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"ValidateDatabase() in SQLContext class", ex);
            }
        }

        /// <summary>
        /// Update button positions
        /// </summary>
        /// <returns></returns>
        public async Task UpdateButtonPositions()
        {
            try
            {
                await _connection.OpenAsync();

                string query = $"UPDATE ButtonPositions SET Value = @Value1 WHERE Property = @Property1; " +
                               $"UPDATE ButtonPositions SET Value = @Value2 WHERE Property = @Property2; " +
                               $"UPDATE ButtonPositions SET Value = @Value3 WHERE Property = @Property3; " +
                               $"UPDATE ButtonPositions SET Value = @Value4 WHERE Property = @Property4; " +
                               $"UPDATE ButtonPositions SET Value = @Value5 WHERE Property = @Property5; " +
                               $"UPDATE ButtonPositions SET Value = @Value6 WHERE Property = @Property6; " +
                               $"UPDATE ButtonPositions SET Value = @Value7 WHERE Property = @Property7; " +
                               $"UPDATE ButtonPositions SET Value = @Value8 WHERE Property = @Property8; " +
                               $"UPDATE ButtonPositions SET Value = @Value9 WHERE Property = @Property9; " +
                               $"UPDATE ButtonPositions SET Value = @Value10 WHERE Property = @Property10; " +
                               $"UPDATE ButtonPositions SET Value = @Value11 WHERE Property = @Property11; " +
                               $"UPDATE ButtonPositions SET Value = @Value12 WHERE Property = @Property12; " +
                               $"UPDATE ButtonPositions SET Value = @Value13 WHERE Property = @Property13; " +
                               $"UPDATE ButtonPositions SET Value = @Value14 WHERE Property = @Property14; " +
                               $"UPDATE ButtonPositions SET Value = @Value15 WHERE Property = @Property15; " +
                               $"UPDATE ButtonPositions SET Value = @Value16 WHERE Property = @Property16; " +
                               $"UPDATE ButtonPositions SET Value = @Value17 WHERE Property = @Property17; " +
                               $"UPDATE ButtonPositions SET Value = @Value18 WHERE Property = @Property18; " +
                               $"UPDATE ButtonPositions SET Value = @Value19 WHERE Property = @Property19; " +
                               $"UPDATE ButtonPositions SET Value = @Value20 WHERE Property = @Property20; " +
                               $"UPDATE ButtonPositions SET Value = @Value21 WHERE Property = @Property21; " +
                               $"UPDATE ButtonPositions SET Value = @Value22 WHERE Property = @Property22; " +
                               $"UPDATE ButtonPositions SET Value = @Value23 WHERE Property = @Property23; " +
                               $"UPDATE ButtonPositions SET Value = @Value24 WHERE Property = @Property24; " +
                               $"UPDATE ButtonPositions SET Value = @Value25 WHERE Property = @Property25; " +
                               $"UPDATE ButtonPositions SET Value = @Value26 WHERE Property = @Property26; " +
                               $"UPDATE ButtonPositions SET Value = @Value27 WHERE Property = @Property27; " +
                               $"UPDATE ButtonPositions SET Value = @Value28 WHERE Property = @Property28; " +
                               $"UPDATE ButtonPositions SET Value = @Value29 WHERE Property = @Property29; " +
                               $"UPDATE ButtonPositions SET Value = @Value30 WHERE Property = @Property30; " +
                               $"UPDATE ButtonPositions SET Value = @Value31 WHERE Property = @Property31; " +
                               $"UPDATE ButtonPositions SET Value = @Value32 WHERE Property = @Property32; " +
                               $"UPDATE ButtonPositions SET Value = @Value33 WHERE Property = @Property33; " +
                               $"UPDATE ButtonPositions SET Value = @Value34 WHERE Property = @Property34; " +
                               $"UPDATE ButtonPositions SET Value = @Value35 WHERE Property = @Property35; " +
                               $"UPDATE ButtonPositions SET Value = @Value36 WHERE Property = @Property36; " +
                               $"UPDATE ButtonPositions SET Value = @Value37 WHERE Property = @Property37; " +
                               $"UPDATE ButtonPositions SET Value = @Value38 WHERE Property = @Property38; " +
                               $"UPDATE ButtonPositions SET Value = @Value39 WHERE Property = @Property39; " +
                               $"UPDATE ButtonPositions SET Value = @Value40 WHERE Property = @Property40; " +
                               $"UPDATE ButtonPositions SET Value = @Value41 WHERE Property = @Property41; " +
                               $"UPDATE ButtonPositions SET Value = @Value42 WHERE Property = @Property42; " +
                               $"UPDATE ButtonPositions SET Value = @Value43 WHERE Property = @Property43; " +
                               $"UPDATE ButtonPositions SET Value = @Value44 WHERE Property = @Property44; " +
                               $"UPDATE ButtonPositions SET Value = @Value45 WHERE Property = @Property45; " +
                               $"UPDATE ButtonPositions SET Value = @Value46 WHERE Property = @Property46; " +
                               $"UPDATE ButtonPositions SET Value = @Value47 WHERE Property = @Property47; " +
                               $"UPDATE ButtonPositions SET Value = @Value48 WHERE Property = @Property48; " +
                               $"UPDATE ButtonPositions SET Value = @Value49 WHERE Property = @Property49; " +
                               $"UPDATE ButtonPositions SET Value = @Value50 WHERE Property = @Property50; " +
                               $"UPDATE ButtonPositions SET Value = @Value51 WHERE Property = @Property51; " +
                               $"UPDATE ButtonPositions SET Value = @Value52 WHERE Property = @Property52; " +
                               $"UPDATE ButtonPositions SET Value = @Value53 WHERE Property = @Property53; " +
                               $"UPDATE ButtonPositions SET Value = @Value54 WHERE Property = @Property54; " +
                               $"UPDATE ButtonPositions SET Value = @Value55 WHERE Property = @Property55; " +
                               $"UPDATE ButtonPositions SET Value = @Value56 WHERE Property = @Property56; " +
                               $"UPDATE ButtonPositions SET Value = @Value57 WHERE Property = @Property57; " +
                               $"UPDATE ButtonPositions SET Value = @Value58 WHERE Property = @Property58; " +
                               $"UPDATE ButtonPositions SET Value = @Value59 WHERE Property = @Property59; " +
                               $"UPDATE ButtonPositions SET Value = @Value60 WHERE Property = @Property60; " +
                               $"UPDATE ButtonPositions SET Value = @Value61 WHERE Property = @Property61; " +
                               $"UPDATE ButtonPositions SET Value = @Value62 WHERE Property = @Property62; " +
                               $"UPDATE ButtonPositions SET Value = @Value63 WHERE Property = @Property63; " +
                               $"UPDATE ButtonPositions SET Value = @Value64 WHERE Property = @Property64; " +
                               $"UPDATE ButtonPositions SET Value = @Value65 WHERE Property = @Property65; " +
                               $"UPDATE ButtonPositions SET Value = @Value66 WHERE Property = @Property66; " +
                               $"UPDATE ButtonPositions SET Value = @Value67 WHERE Property = @Property67; " +
                               $"UPDATE ButtonPositions SET Value = @Value68 WHERE Property = @Property68; " +
                               $"UPDATE ButtonPositions SET Value = @Value69 WHERE Property = @Property69; " +
                               $"UPDATE ButtonPositions SET Value = @Value70 WHERE Property = @Property70; " +
                               $"UPDATE ButtonPositions SET Value = @Value71 WHERE Property = @Property71; " +
                               $"UPDATE ButtonPositions SET Value = @Value72 WHERE Property = @Property72; " +
                               $"UPDATE ButtonPositions SET Value = @Value73 WHERE Property = @Property73; " +
                               $"UPDATE ButtonPositions SET Value = @Value74 WHERE Property = @Property74; " +
                               $"UPDATE ButtonPositions SET Value = @Value75 WHERE Property = @Property75; " +
                               $"UPDATE ButtonPositions SET Value = @Value76 WHERE Property = @Property76; " +
                               $"UPDATE ButtonPositions SET Value = @Value77 WHERE Property = @Property77; " +
                               $"UPDATE ButtonPositions SET Value = @Value78 WHERE Property = @Property78; " +
                               $"UPDATE ButtonPositions SET Value = @Value79 WHERE Property = @Property79; " +
                               $"UPDATE ButtonPositions SET Value = @Value80 WHERE Property = @Property80; " +
                               $"UPDATE ButtonPositions SET Value = @Value81 WHERE Property = @Property81; " +
                               $"UPDATE ButtonPositions SET Value = @Value82 WHERE Property = @Property82; " +
                               $"UPDATE ButtonPositions SET Value = @Value83 WHERE Property = @Property83; " +
                               $"UPDATE ButtonPositions SET Value = @Value84 WHERE Property = @Property84; " +
                               $"UPDATE ButtonPositions SET Value = @Value85 WHERE Property = @Property85; " +
                               $"UPDATE ButtonPositions SET Value = @Value86 WHERE Property = @Property86; " +
                               $"UPDATE ButtonPositions SET Value = @Value87 WHERE Property = @Property87; " +
                               $"UPDATE ButtonPositions SET Value = @Value88 WHERE Property = @Property88; " +
                               $"UPDATE ButtonPositions SET Value = @Value89 WHERE Property = @Property89; " +
                               $"UPDATE ButtonPositions SET Value = @Value90 WHERE Property = @Property90; " +
                               $"UPDATE ButtonPositions SET Value = @Value91 WHERE Property = @Property91; " +
                               $"UPDATE ButtonPositions SET Value = @Value92 WHERE Property = @Property92; " +
                               $"UPDATE ButtonPositions SET Value = @Value93 WHERE Property = @Property93; " +
                               $"UPDATE ButtonPositions SET Value = @Value94 WHERE Property = @Property94; " +
                               $"UPDATE ButtonPositions SET Value = @Value95 WHERE Property = @Property95; " +
                               $"UPDATE ButtonPositions SET Value = @Value96 WHERE Property = @Property96; " +
                               $"UPDATE ButtonPositions SET Value = @Value97 WHERE Property = @Property97; " +
                               $"UPDATE ButtonPositions SET Value = @Value98 WHERE Property = @Property98; " +
                               $"UPDATE ButtonPositions SET Value = @Value99 WHERE Property = @Property99; " +
                               $"UPDATE ButtonPositions SET Value = @Value100 WHERE Property = @Property100; " +
                               $"UPDATE ButtonPositions SET Value = @Value101 WHERE Property = @Property101; " +
                               $"UPDATE ButtonPositions SET Value = @Value102 WHERE Property = @Property102; " +
                               $"UPDATE ButtonPositions SET Value = @Value103 WHERE Property = @Property103; ";

                using (SqliteCommand command = new SqliteCommand(query, _connection))
                {
                    #region Fuel
                    command.Parameters.AddWithValue("@Property1", "Fuel_H");
                    command.Parameters.AddWithValue("@Value1", _buttonManager.Size(FUEL).Height);

                    command.Parameters.AddWithValue("@Property2", "Fuel_W");
                    command.Parameters.AddWithValue("@Value2", _buttonManager.Size(FUEL).Width);

                    command.Parameters.AddWithValue("@Property3", "Fuel_L");
                    command.Parameters.AddWithValue("@Value3", _buttonManager.Location(FUEL).Left);

                    command.Parameters.AddWithValue("@Property4", "Fuel_T");
                    command.Parameters.AddWithValue("@Value4", _buttonManager.Location(FUEL).Top);

                    command.Parameters.AddWithValue("@Property5", "Fuel_IsVisible");
                    command.Parameters.AddWithValue("@Value5", _buttonManager.Get(FUEL).IsEnabled);
                    #endregion
                    #region Rpm
                    command.Parameters.AddWithValue("@Property6", "Rpm_H");
                    command.Parameters.AddWithValue("@Value6", _buttonManager.Size(RPM).Height);

                    command.Parameters.AddWithValue("@Property7", "Rpm_W");
                    command.Parameters.AddWithValue("@Value7", _buttonManager.Size(RPM).Width);

                    command.Parameters.AddWithValue("@Property8", "Rpm_L");
                    command.Parameters.AddWithValue("@Value8", _buttonManager.Location(RPM).Left);

                    command.Parameters.AddWithValue("@Property9", "Rpm_T");
                    command.Parameters.AddWithValue("@Value9", _buttonManager.Location(RPM).Top);

                    command.Parameters.AddWithValue("@Property10", "Rpm_IsVisible");
                    command.Parameters.AddWithValue("@Value10", _buttonManager.Get(RPM).IsEnabled);
                    #endregion
                    #region Tc
                    command.Parameters.AddWithValue("@Property11", "Tc_H");
                    command.Parameters.AddWithValue("@Value11", _buttonManager.Size(TC).Height);

                    command.Parameters.AddWithValue("@Property12", "Tc_W");
                    command.Parameters.AddWithValue("@Value12", _buttonManager.Size(TC).Width);

                    command.Parameters.AddWithValue("@Property13", "Tc_L");
                    command.Parameters.AddWithValue("@Value13", _buttonManager.Location(TC).Left);

                    command.Parameters.AddWithValue("@Property14", "Tc_T");
                    command.Parameters.AddWithValue("@Value14", _buttonManager.Location(TC).Top);

                    command.Parameters.AddWithValue("@Property15", "Tc_IsVisible");
                    command.Parameters.AddWithValue("@Value15", _buttonManager.Get(TC).IsEnabled);
                    #endregion
                    #region Lights
                    command.Parameters.AddWithValue("@Property16", "Lights_H");
                    command.Parameters.AddWithValue("@Value16", _buttonManager.Size(LIGHTS).Height);

                    command.Parameters.AddWithValue("@Property17", "Lights_W");
                    command.Parameters.AddWithValue("@Value17", _buttonManager.Size(LIGHTS).Width);

                    command.Parameters.AddWithValue("@Property18", "Lights_L");
                    command.Parameters.AddWithValue("@Value18", _vars.Lights_L);

                    command.Parameters.AddWithValue("@Property19", "Lights_T");
                    command.Parameters.AddWithValue("@Value19", _vars.Lights_T);

                    command.Parameters.AddWithValue("@Property20", "Lights_IsVisible");
                    command.Parameters.AddWithValue("@Value20", _buttonManager.Get(LIGHTS).IsEnabled);
                    #endregion
                    #region Menu
                    command.Parameters.AddWithValue("@Property21", "Menu_L");
                    command.Parameters.AddWithValue("@Value21", _buttonManager.Location(SIDEMENU_CUSTOMIZE_UI).Left);

                    command.Parameters.AddWithValue("@Property22", "Menu_T");
                    command.Parameters.AddWithValue("@Value22", _buttonManager.Location(SIDEMENU_CUSTOMIZE_UI).Top);
                    #endregion
                    #region Abs
                    command.Parameters.AddWithValue("@Property23", "Abs_H");
                    command.Parameters.AddWithValue("@Value23", _buttonManager.Size(ABS).Height);

                    command.Parameters.AddWithValue("@Property24", "Abs_W");
                    command.Parameters.AddWithValue("@Value24", _buttonManager.Size(ABS).Width);

                    command.Parameters.AddWithValue("@Property25", "Abs_L");
                    command.Parameters.AddWithValue("@Value25", _vars.Abs_L);

                    command.Parameters.AddWithValue("@Property26", "Abs_T");
                    command.Parameters.AddWithValue("@Value26", _vars.Abs_T);

                    command.Parameters.AddWithValue("@Property27", "Abs_IsVisible");
                    command.Parameters.AddWithValue("@Value27", _buttonManager.Get(ABS).IsEnabled);
                    #endregion
                    #region Gear
                    command.Parameters.AddWithValue("@Property28", "Gear_H");
                    command.Parameters.AddWithValue("@Value28", _buttonManager.Size(GEAR).Height);

                    command.Parameters.AddWithValue("@Property29", "Gear_W");
                    command.Parameters.AddWithValue("@Value29", _buttonManager.Size(GEAR).Width);

                    command.Parameters.AddWithValue("@Property30", "Gear_L");
                    command.Parameters.AddWithValue("@Value30", _buttonManager.Location(GEAR).Left);

                    command.Parameters.AddWithValue("@Property31", "Gear_T");
                    command.Parameters.AddWithValue("@Value31", _buttonManager.Location(GEAR).Top);

                    command.Parameters.AddWithValue("@Property32", "Gear_IsVisible");
                    command.Parameters.AddWithValue("@Value32", _buttonManager.Get(GEAR).IsEnabled);
                    #endregion
                    #region Speed
                    command.Parameters.AddWithValue("@Property33", "Speed_H");
                    command.Parameters.AddWithValue("@Value33", _buttonManager.Size(SPEED).Height);

                    command.Parameters.AddWithValue("@Property34", "Speed_W");
                    command.Parameters.AddWithValue("@Value34", _buttonManager.Size(SPEED).Width);

                    command.Parameters.AddWithValue("@Property35", "Speed_L");
                    command.Parameters.AddWithValue("@Value35", _buttonManager.Location(SPEED).Left);

                    command.Parameters.AddWithValue("@Property36", "Speed_T");
                    command.Parameters.AddWithValue("@Value36", _buttonManager.Location(SPEED).Top);

                    command.Parameters.AddWithValue("@Property37", "Speed_IsVisible");
                    command.Parameters.AddWithValue("@Value37", _buttonManager.Get(SPEED).IsEnabled);
                    #endregion
                    #region Handbrake
                    command.Parameters.AddWithValue("@Property38", "Handbrake_H");
                    command.Parameters.AddWithValue("@Value38", _buttonManager.Size(HANDBRAKE).Height);

                    command.Parameters.AddWithValue("@Property39", "Handbrake_W");
                    command.Parameters.AddWithValue("@Value39", _buttonManager.Size(HANDBRAKE).Width);

                    command.Parameters.AddWithValue("@Property40", "Handbrake_L");
                    command.Parameters.AddWithValue("@Value40", _buttonManager.Location(HANDBRAKE).Left);

                    command.Parameters.AddWithValue("@Property41", "Handbrake_T");
                    command.Parameters.AddWithValue("@Value41", _buttonManager.Location(HANDBRAKE).Top);

                    command.Parameters.AddWithValue("@Property42", "Handbrake_IsVisible");
                    command.Parameters.AddWithValue("@Value42", _buttonManager.Get(HANDBRAKE).IsEnabled);
                    #endregion
                    #region BlinkerL
                    command.Parameters.AddWithValue("@Property43", "BlinkerL_H");
                    command.Parameters.AddWithValue("@Value43", _buttonManager.Size(BLINKER_LEFT).Width);

                    command.Parameters.AddWithValue("@Property44", "BlinkerL_W");
                    command.Parameters.AddWithValue("@Value44", _buttonManager.Size(BLINKER_LEFT).Width);

                    command.Parameters.AddWithValue("@Property45", "BlinkerL_L");
                    command.Parameters.AddWithValue("@Value45", _buttonManager.Location(BLINKER_LEFT).Left);

                    command.Parameters.AddWithValue("@Property46", "BlinkerL_T");
                    command.Parameters.AddWithValue("@Value46", _buttonManager.Location(BLINKER_LEFT).Top);

                    command.Parameters.AddWithValue("@Property47", "BlinkerL_IsVisible");
                    command.Parameters.AddWithValue("@Value47", _buttonManager.Get(BLINKER_LEFT).IsEnabled);
                    #endregion
                    #region BlinkerR
                    command.Parameters.AddWithValue("@Property48", "BlinkerR_H");
                    command.Parameters.AddWithValue("@Value48", _buttonManager.Size(BLINKER_RIGHT).Height);

                    command.Parameters.AddWithValue("@Property49", "BlinkerR_W");
                    command.Parameters.AddWithValue("@Value49", _buttonManager.Size(BLINKER_RIGHT).Width);

                    command.Parameters.AddWithValue("@Property50", "BlinkerR_L");
                    command.Parameters.AddWithValue("@Value50", _buttonManager.Location(BLINKER_RIGHT).Left);

                    command.Parameters.AddWithValue("@Property51", "BlinkerR_T");
                    command.Parameters.AddWithValue("@Value51", _buttonManager.Location(BLINKER_RIGHT).Top);

                    command.Parameters.AddWithValue("@Property52", "BlinkerR_IsVisible");
                    command.Parameters.AddWithValue("@Value52", _buttonManager.Get(BLINKER_RIGHT).IsEnabled);
                    #endregion
                    #region DateTime
                    command.Parameters.AddWithValue("@Property53", "DateTime_H");
                    command.Parameters.AddWithValue("@Value53", _buttonManager.Size(DATETIME).Height);

                    command.Parameters.AddWithValue("@Property54", "DateTime_W");
                    command.Parameters.AddWithValue("@Value54", _buttonManager.Size(DATETIME).Width);

                    command.Parameters.AddWithValue("@Property55", "DateTime_L");
                    command.Parameters.AddWithValue("@Value55", _buttonManager.Location(DATETIME).Left);

                    command.Parameters.AddWithValue("@Property56", "DateTime_T");
                    command.Parameters.AddWithValue("@Value56", _buttonManager.Location(DATETIME).Top);

                    command.Parameters.AddWithValue("@Property57", "DateTime_IsVisible");
                    command.Parameters.AddWithValue("@Value57", _buttonManager.Get(DATETIME).IsEnabled);
                    #endregion
                    #region Customize UI
                    command.Parameters.AddWithValue("@Property58", "CustomizeUI_T");
                    command.Parameters.AddWithValue("@Value58", _buttonManager.Location(CUSTOMIZER_WINDOW).Top);
                    #endregion
                    #region Turbo
                    command.Parameters.AddWithValue("@Property59", "Turbo_H");
                    command.Parameters.AddWithValue("@Value59", _buttonManager.Size(TURBO).Height);

                    command.Parameters.AddWithValue("@Property60", "Turbo_W");
                    command.Parameters.AddWithValue("@Value60", _buttonManager.Size(TURBO).Width);

                    command.Parameters.AddWithValue("@Property61", "Turbo_L");
                    command.Parameters.AddWithValue("@Value61", _buttonManager.Location(TURBO).Left);

                    command.Parameters.AddWithValue("@Property62", "Turbo_T");
                    command.Parameters.AddWithValue("@Value62", _buttonManager.Location(TURBO).Top);

                    command.Parameters.AddWithValue("@Property63", "Turbo_IsVisible");
                    command.Parameters.AddWithValue("@Value63", _buttonManager.Get(TURBO).IsEnabled);
                    #endregion
                    #region PitSpeed
                    command.Parameters.AddWithValue("@Property64", "PitSpeed_H");
                    command.Parameters.AddWithValue("@Value64", _buttonManager.Size(PITLANE_LIMITER).Height);

                    command.Parameters.AddWithValue("@Property65", "PitSpeed_W");
                    command.Parameters.AddWithValue("@Value65", _buttonManager.Size(PITLANE_LIMITER).Width);

                    command.Parameters.AddWithValue("@Property66", "PitSpeed_L");
                    command.Parameters.AddWithValue("@Value66", _buttonManager.Location(PITLANE_LIMITER).Left);

                    command.Parameters.AddWithValue("@Property67", "PitSpeed_T");
                    command.Parameters.AddWithValue("@Value67", _buttonManager.Location(PITLANE_LIMITER).Top);

                    command.Parameters.AddWithValue("@Property68", "PitSpeed_IsVisible");
                    command.Parameters.AddWithValue("@Value68", _buttonManager.Get(PITLANE_LIMITER).IsEnabled);
                    #endregion
                    #region Avg. Fuel Consumption
                    command.Parameters.AddWithValue("@Property69", "AvgFuel_H");
                    command.Parameters.AddWithValue("@Value69", _buttonManager.Size(AVG_FUEL).Height);

                    command.Parameters.AddWithValue("@Property70", "AvgFuel_W");
                    command.Parameters.AddWithValue("@Value70", _buttonManager.Size(AVG_FUEL).Width);

                    command.Parameters.AddWithValue("@Property71", "AvgFuel_L");
                    command.Parameters.AddWithValue("@Value71", _buttonManager.Location(AVG_FUEL).Left);

                    command.Parameters.AddWithValue("@Property72", "AvgFuel_T");
                    command.Parameters.AddWithValue("@Value72", _buttonManager.Location(AVG_FUEL).Top);

                    command.Parameters.AddWithValue("@Property73", "AvgFuel_IsVisible");
                    command.Parameters.AddWithValue("@Value73", _buttonManager.Get(AVG_FUEL).IsEnabled);
                    #endregion
                    #region Laptime
                    command.Parameters.AddWithValue("@Property74", "Laptime_H");
                    command.Parameters.AddWithValue("@Value74", _buttonManager.Size(LAP_TIME).Height);

                    command.Parameters.AddWithValue("@Property75", "Laptime_W");
                    command.Parameters.AddWithValue("@Value75", _buttonManager.Size(LAP_TIME).Width);

                    command.Parameters.AddWithValue("@Property76", "Laptime_L");
                    command.Parameters.AddWithValue("@Value76", _buttonManager.Location(LAP_TIME).Left);

                    command.Parameters.AddWithValue("@Property77", "Laptime_T");
                    command.Parameters.AddWithValue("@Value77", _buttonManager.Location(LAP_TIME).Top);

                    command.Parameters.AddWithValue("@Property78", "Laptime_IsVisible");
                    command.Parameters.AddWithValue("@Value78", _buttonManager.Get(LAP_TIME).IsEnabled);
                    #endregion
                    #region SplitTime
                    command.Parameters.AddWithValue("@Property79", "SplitTime_H");
                    command.Parameters.AddWithValue("@Value79", _buttonManager.Size(LAP_SPLIT).Height);

                    command.Parameters.AddWithValue("@Property80", "SplitTime_W");
                    command.Parameters.AddWithValue("@Value80", _buttonManager.Size(LAP_SPLIT).Width);

                    command.Parameters.AddWithValue("@Property81", "SplitTime_L");
                    command.Parameters.AddWithValue("@Value81", _buttonManager.Location(LAP_SPLIT).Left);

                    command.Parameters.AddWithValue("@Property82", "SplitTime_T");
                    command.Parameters.AddWithValue("@Value82", _buttonManager.Location(LAP_SPLIT).Top);

                    command.Parameters.AddWithValue("@Property83", "SplitTime_IsVisible");
                    command.Parameters.AddWithValue("@Value83", _buttonManager.Get(LAP_SPLIT).IsEnabled);
                    #endregion
                    #region Track
                    command.Parameters.AddWithValue("@Property84", "Track_H");
                    command.Parameters.AddWithValue("@Value84", _buttonManager.Size(TRACK).Height);

                    command.Parameters.AddWithValue("@Property85", "Track_W");
                    command.Parameters.AddWithValue("@Value85", _buttonManager.Size(TRACK).Width);

                    command.Parameters.AddWithValue("@Property86", "Track_L");
                    command.Parameters.AddWithValue("@Value86", _buttonManager.Location(TRACK).Left);

                    command.Parameters.AddWithValue("@Property87", "Track_T");
                    command.Parameters.AddWithValue("@Value87", _buttonManager.Location(TRACK).Top);

                    command.Parameters.AddWithValue("@Property88", "Track_IsVisible");
                    command.Parameters.AddWithValue("@Value88", _buttonManager.Get(TRACK).IsEnabled);
                    #endregion
                    #region BestPB
                    command.Parameters.AddWithValue("@Property89", "BestPB_H");
                    command.Parameters.AddWithValue("@Value89", _buttonManager.Size(BEST_PB).Height);

                    command.Parameters.AddWithValue("@Property90", "BestPB_W");
                    command.Parameters.AddWithValue("@Value90", _buttonManager.Size(BEST_PB).Width);

                    command.Parameters.AddWithValue("@Property91", "BestPB_L");
                    command.Parameters.AddWithValue("@Value91", _buttonManager.Location(BEST_PB).Left);

                    command.Parameters.AddWithValue("@Property92", "BestPB_T");
                    command.Parameters.AddWithValue("@Value92", _buttonManager.Location(BEST_PB).Top);

                    command.Parameters.AddWithValue("@Property93", "BestPB_IsVisible");
                    command.Parameters.AddWithValue("@Value93", _buttonManager.Get(BEST_PB).IsEnabled);
                    #endregion
                    #region Theoretical PB
                    command.Parameters.AddWithValue("@Property94", "TheoreticalPB_H");
                    command.Parameters.AddWithValue("@Value94", _buttonManager.Size(THEORETICAL_PB).Height);

                    command.Parameters.AddWithValue("@Property95", "TheoreticalPB_W");
                    command.Parameters.AddWithValue("@Value95", _buttonManager.Size(THEORETICAL_PB).Width);

                    command.Parameters.AddWithValue("@Property96", "TheoreticalPB_L");
                    command.Parameters.AddWithValue("@Value96", _buttonManager.Location(THEORETICAL_PB).Left);

                    command.Parameters.AddWithValue("@Property97", "TheoreticalPB_T");
                    command.Parameters.AddWithValue("@Value97", _buttonManager.Location(THEORETICAL_PB).Top);

                    command.Parameters.AddWithValue("@Property98", "TheoreticalPB_IsVisible");
                    command.Parameters.AddWithValue("@Value98", _buttonManager.Get(THEORETICAL_PB).IsEnabled);
                    #endregion
                    #region Race Positions
                    command.Parameters.AddWithValue("@Property99", "RacePos_H");
                    command.Parameters.AddWithValue("@Value99", _buttonManager.Size(RACE_POSITIONS).Height);

                    command.Parameters.AddWithValue("@Property100", "RacePos_W");
                    command.Parameters.AddWithValue("@Value100", _buttonManager.Size(RACE_POSITIONS).Width);

                    command.Parameters.AddWithValue("@Property101", "RacePos_L");
                    command.Parameters.AddWithValue("@Value101", _buttonManager.Location(RACE_POSITIONS).Left);

                    command.Parameters.AddWithValue("@Property102", "RacePos_T");
                    command.Parameters.AddWithValue("@Value102", _buttonManager.Location(RACE_POSITIONS).Top);

                    command.Parameters.AddWithValue("@Property103", "RacePos_IsVisible");
                    command.Parameters.AddWithValue("@Value103", _buttonManager.Get(RACE_POSITIONS).IsEnabled);
                    #endregion

                    await command.ExecuteNonQueryAsync();
                }

                await _connection.CloseAsync();
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"UpdateButtonPositions() in SQLContext class",
                    ex);
            }
        }

        /// <summary>
        /// Generate all missing tables
        /// </summary>
        /// <returns></returns>
        public async Task VerifyTableExistence()
        {
            try
            {
                await CreateTable("PlayerMessages", _vars.SQLColumns_PlayerMessages);
                await CreateTable("Settings", _vars.SQLColumns_Settings);
                await CreateTable("ButtonPositions", _vars.SQLColumns_ButtonPositions);
                await CreateTable("GlobalMessages", _vars.SQLColumns_GlobalMessages);
                await CreateTable("Templates", _vars.SQLColumns_Templates);
                await CreateTable("CarContact", _vars.SQLColumns_CarContact);
                await CreateTable("Laptime", _vars.SQLColumns_Laptime);
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"TablesExistRun() in SQLContext class", ex);
            }
        }

        /// <summary>
        /// Validate table existence
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="ColumnArray"></param>
        /// <returns></returns>
        public async Task CreateTable(string TableName, string[] ColumnArray)
        {
            try
            {
                await _connection.OpenAsync();

                string columnsStr = string.Join(", ",ColumnArray);
                string query = $"CREATE TABLE IF NOT EXISTS {TableName} ({columnsStr})";

                using (SqliteCommand command = new SqliteCommand(query, _connection))
                {
                    await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"TablesExistQuery() in SQLContext class", ex);
            }
            finally
            {
                _connection.Close();
            }
        }

        /// <summary>
        /// Get specified setting from the database
        /// </summary>
        /// <param name="setting"></param>
        /// <returns></returns>
        public async Task<string> GetSettingAsync(string tableName, string columnName, string propertyValue)
        {
            try
            {
                await _connection.OpenAsync();

                using (SqliteCommand command = _connection.CreateCommand())
                {
                    command.CommandText = $"SELECT Value FROM {tableName} WHERE {columnName} = @SettingName";
                    command.Parameters.AddWithValue("@SettingName", propertyValue);

                    using (SqliteDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            if (!reader.IsDBNull(0))
                            {
                                return reader.GetString(0);
                            }
                        }
                    }
                }

                await _connection.CloseAsync();
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"GetSettingAsync() in SQLContext class", ex);
            }

            return string.Empty;
        }

        /// <summary>
        /// Load player messages to a list, a list is returned.
        /// </summary>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public async Task<List<T>> LoadMessages<T>(string tableName, string[] columns) where T : new()
        {
            List<T> data = new List<T>();

            try
            {
                using (SqliteConnection connection = new(_vars.SQLConnection.ConnectionString))
                {
                    connection.Open();

                    string query = $"SELECT * FROM {tableName} ORDER BY `Date` DESC LIMIT 10000";
                    
                    if (tableName == "Laptime")
                        query = $"SELECT * FROM {tableName}";

                    using (SqliteCommand command = new(query, connection))
                    using (SqliteDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            T dataItem = new T();

                            foreach (var property in typeof(T).GetProperties())
                            {
                                if (columns.Contains(property.Name))
                                {
                                    object value = reader[property.Name];

                                    if (value != DBNull.Value)
                                    {
                                        if (property.PropertyType == typeof(DateTime) || property.PropertyType == typeof(DateTime?))
                                        {
                                            // Handle DateTime conversion
                                            if (value is string stringValue)
                                            {
                                                DateTime dateTimeValue;
                                                if (DateTime.TryParse(stringValue, out dateTimeValue))
                                                {
                                                    property.SetValue(dataItem, dateTimeValue);
                                                }
                                                else
                                                {
                                                    // Log the issue
                                                    Console.WriteLine($"Failed to convert string to DateTime for property {property.Name}. Value: {stringValue}");
                                                }
                                            }
                                            else if (value is DateTime)
                                            {
                                                property.SetValue(dataItem, value);
                                            }
                                            else
                                            {
                                                // Log the issue
                                                Console.WriteLine($"Unexpected value type for property {property.Name}: {value.GetType().FullName}");
                                            }
                                        }
                                        else if (property.PropertyType == typeof(int?))
                                        {
                                            // Handle System.Nullable<int> conversion
                                            if (value is long longValue)
                                            {
                                                property.SetValue(dataItem, (int) longValue);
                                            }
                                            else
                                            {
                                                // Log the issue
                                                Console.WriteLine($"Unexpected value type for property {property.Name}: {value.GetType().FullName}");
                                            }
                                        }
                                        else if (property.PropertyType == typeof(double?))
                                        {
                                            // Handle System.Nullable<double> conversion
                                            if (value is long longValue)
                                            {
                                                property.SetValue(dataItem, (double) longValue);
                                            }
                                            else
                                            {
                                                // Log the issue
                                                Console.WriteLine($"Unexpected value type for property {property.Name}: {value.GetType().FullName}");
                                            }
                                        }
                                        else
                                        {
                                            // Handle other property types
                                            property.SetValue(dataItem, value);
                                        }

                                        // Log the property value
                                        /*Console.WriteLine($"{property.Name}: {property.GetValue(dataItem)}");*/
                                    }
                                }
                            }

                            data.Add(dataItem);
                        }
                    }

                    connection.Close();
                }

                return data;
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR, "Error in LoadMessages() in SQLContext class", ex);
                return data;
            }
        }



        /// <summary>
        /// Export Template to .csv file in LFS Companion folder
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public async Task<bool> ExportTableCSV(string tableName)
        {
            try
            {
                using (SqliteConnection connection = new SqliteConnection(_vars.SQLConnection.ConnectionString))
                {
                    await connection.OpenAsync();

                    using (SqliteCommand command = connection.CreateCommand())
                    {
                        command.CommandText = $"SELECT * FROM {tableName}";

                        using (SqliteDataReader reader = command.ExecuteReader())
                        {
                            DataTable dataTable = new DataTable();
                            dataTable.Load(reader);

                            // Write data to CSV file
                            await WriteDataTableToCsv(dataTable, tableName);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR, "ExportTableCSV() in SQLContext class", ex);
                return false;
            }
        }

        /// <summary>
        /// Append data to csv file
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="TemplateName"></param>
        /// <returns></returns>
        private async Task<bool> WriteDataTableToCsv(DataTable dataTable, string TemplateName)
        {
            try
            {
                string exportFolder = await _dir.GetDirectory(InsimDirectoryEnum.TEMPLATE_EXPORTS);
                string filePath = Path.Combine(exportFolder, TemplateName);

                if (_vars.SelectedTemplate == null)
                    return false;

                if (_vars.SelectedTemplate.Name == null)
                    return false;

                using (StreamWriter writer = new StreamWriter($"{filePath}.csv"))
                {
                    // Write the column headers
                    writer.WriteLine(string.Join(",", dataTable.Columns.Cast<DataColumn>().Select(col => col.ColumnName)));

                    // Write the data
                    foreach (DataRow row in dataTable.Rows)
                    {
                        writer.WriteLine(string.Join(",", row.ItemArray.Select(field => field.ToString())));
                    }
                }

                return true; // Operation success
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR, "WriteDataTableToCsv() in SQLContext class", ex);
                return false; // Operation failed
            }
        }

        /// <summary>
        /// Save specified setting to the database
        /// </summary>
        /// <param name="SettingName"></param>
        /// <param name="NewValue"></param>
        /// <returns></returns>
        public async Task SaveSettingAsync(string SettingName, string NewValue)
        {
            try
            {
                using (var connection = new SqliteConnection(_vars.SQLConnection.ConnectionString))
                {
                    await connection.OpenAsync();

                    var commandText = "UPDATE Settings SET Value = @newValue WHERE Setting = @variableName";

                    using (var command = new SqliteCommand(commandText, connection))
                    {
                        command.Parameters.AddWithValue("@newValue", NewValue);
                        command.Parameters.AddWithValue("@variableName", SettingName);

                        await command.ExecuteNonQueryAsync();
                    }

                    await connection.CloseAsync();
                }
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"SaveSetting() in SQLContext class", ex);
            }
        }

        /// <summary>
        /// Add player message to the PlayerMessages table
        /// </summary>
        /// <param name="Nickname"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public async Task InsertPlayerMessages(List<Messages> listObject)
        {
            try
            {
                using (SqliteConnection connection = new SqliteConnection(_vars.SQLConnection.ConnectionString))
                {
                    await connection.OpenAsync();

                    using (SqliteCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "INSERT INTO PlayerMessages (Nickname, Username, Message, Date) VALUES (@nickname, @username, @message, @date)";

                        foreach (Messages entry in listObject)
                        {
                            command.Parameters.AddWithValue("@nickname", entry.Nickname);
                            command.Parameters.AddWithValue("@username", entry.Username);
                            command.Parameters.AddWithValue("@message", entry.Message);
                            command.Parameters.AddWithValue("@date", entry.Date);

                            await command.ExecuteNonQueryAsync();

                            command.Parameters.Clear();
                        }
                    }

                    connection.Close();
                    _vars.PendingPlayerMessages.Clear();
                }
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR, $"InsertPlayerMessages() in SQLContext class", ex);
            }
        }

        public async Task InsertCarContact(List<CarContact>? listObject)
        {
            if (listObject == null || listObject.Count == 0)
                return; // No need to proceed if the list is empty

            try
            {
                using (SqliteConnection connection = new SqliteConnection(_vars.SQLConnection.ConnectionString))
                {
                    await connection.OpenAsync();
                    using (var transaction = await connection.BeginTransactionAsync())
                    using (SqliteCommand command = connection.CreateCommand())
                    {
                        var queryBuilder = new StringBuilder("INSERT INTO CarContact (SourceUName, SourcePName, TargetUName, TargetPName, Speed, Date) VALUES ");
                        var parameters = new List<SqliteParameter>();

                        int counter = 0;
                        foreach (var entry in listObject)
                        {
                            queryBuilder.Append($"(@SourceUName{counter}, @SourcePName{counter}, @TargetUName{counter}, @TargetPName{counter}, @Speed{counter}, @Date{counter}),");

                            parameters.Add(new SqliteParameter($"@SourceUName{counter}", entry.SourceUName));
                            parameters.Add(new SqliteParameter($"@SourcePName{counter}", entry.SourcePName));
                            parameters.Add(new SqliteParameter($"@TargetUName{counter}", entry.TargetUName));
                            parameters.Add(new SqliteParameter($"@TargetPName{counter}", entry.TargetPName));
                            parameters.Add(new SqliteParameter($"@Speed{counter}", entry.Speed));
                            parameters.Add(new SqliteParameter($"@Date{counter}", entry.Date));

                            counter++;
                        }

                        command.CommandText = queryBuilder.ToString().TrimEnd(','); // Remove trailing comma
                        command.Parameters.AddRange(parameters.ToArray());

                        await command.ExecuteNonQueryAsync();
                        await transaction.CommitAsync();
                    }
                }

                // Clear pending messages safely
                lock (_vars.PendingCarContactMessages)
                {
                    _vars.PendingCarContactMessages.Clear();
                }
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR, $"InsertCarContact() in SQLContext class", ex);
            }
        }


        public async Task InsertData<T>(string TableName, string[] ColumnNames, List<T> providedList)
        {
            try
            {
                string columnsStr = string.Join(", ", ColumnNames);
                string valuesStr = string.Join(", ", ColumnNames.Select(c => $"@{c}"));

                using (SqliteConnection connection = new SqliteConnection(_vars.SQLConnection.ConnectionString))
                {
                    await connection.OpenAsync();

                    using (SqliteCommand command = connection.CreateCommand())
                    {
                        command.CommandText = $"INSERT INTO {TableName} ({columnsStr}) VALUES ({valuesStr})";

                        foreach (var entry in providedList)
                        {
                            var properties = entry.GetType().GetProperties();

                            foreach (var property in properties)
                            {
                                var parameter = new SqliteParameter($"@{property.Name}", property.GetValue(entry));
                                command.Parameters.Add(parameter);
                            }

                            await command.ExecuteNonQueryAsync();

                            command.Parameters.Clear();
                        }
                    }

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR, $"InsertData() in SQLContext class", ex);
            }
        }


        /// <summary>
        /// Return a list with all templates
        /// </summary>
        /// <returns></returns>
        public async Task<List<ButtonTemplate>> GetTemplates()
        {
            try
            {
                List<ButtonTemplate> resultList = new List<ButtonTemplate>();

                using (SqliteConnection connection = new SqliteConnection(_vars.SQLConnection.ConnectionString))
                {
                    await connection.OpenAsync();

                    string query = "SELECT Name,Created,IsDeleted,DateDeleted FROM Templates ORDER BY Created ASC LIMIT 5";

                    using (SqliteCommand command = new SqliteCommand(query, connection))
                    {
                        using (SqliteDataReader reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                ButtonTemplate model = new ButtonTemplate
                                {
                                    Name = reader["Name"].ToString(),
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    IsDeleted = Convert.ToInt32(reader["IsDeleted"]),
                                    DateDeleted = Convert.ToDateTime(reader["DateDeleted"])
                                };

                                resultList.Add(model);
                            }
                        }
                    }
                }

                return resultList;
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR, $"GetTemplates() in SQLContext class", ex);
                return new List<ButtonTemplate>();
            }
        }

        public async Task<int> CountRows(string TableName, string Param = "")
        {
            try
            {
                int value = 0;

                using (SqliteConnection connection = new SqliteConnection(_vars.SQLConnection.ConnectionString))
                {
                    await connection.OpenAsync();

                    string query = $"SELECT COUNT(*) FROM {TableName} {Param}";

                    using (SqliteCommand command = new SqliteCommand(query, connection))
                    {
                        using (SqliteDataReader reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                value = Convert.ToInt32(reader[0]);
                            }
                        }
                    }
                }

                return value;
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR, $"CountRows() in SQLContext class", ex);
                return 0;
            }
        }

        public async Task<bool> TableExist(string tableName)
        {
            try
            {
                using (SqliteConnection connection = new SqliteConnection(_vars.SQLConnection.ConnectionString))
                {
                    await connection.OpenAsync();

                    using (SqliteCommand command = connection.CreateCommand())
                    {
                        // Check if the table exists in the database
                        command.CommandText = $"SELECT name FROM sqlite_master WHERE type='table' AND name='{tableName}';";
                        object result = command.ExecuteScalar();

                        await connection.CloseAsync();

                        return result != null && result.ToString() == tableName;
                    }
                }
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR, $"TableExist() in SQLContext class", ex);
                return false;
            }
        }

        public async Task<bool> ImportCSV(string TemplateName, string PathToCSV)
        {
            try
            {
                using (var connection = new SqliteConnection(_vars.SQLConnection.ConnectionString))
                {
                    connection.Open();

                    // Create a table to store CSV data
                    using (var command = new SqliteCommand($"CREATE TABLE IF NOT EXISTS Template_{TemplateName} " +
                        $"(Property VARCHAR(128), Value VARCHAR(128), Modified DATETIME);", connection))
                    {
                        command.ExecuteNonQuery();
                    }

                    // Read the CSV file and insert data into the SQLite table
                    using (var reader = new StreamReader(PathToCSV))
                    {
                        var headerLine = reader.ReadLine();

                        while (!reader.EndOfStream)
                        {
                            var line = reader.ReadLine();

                            // Skip first line
                            if (line == headerLine)
                                continue;

                            var values = line.Split(',');



                            using (var insertCommand = new SqliteCommand($"INSERT INTO Template_{TemplateName} (Property,Value,Modified) VALUES (@col1, @col2, @col3);", connection))
                            {
                                insertCommand.Parameters.AddWithValue("@col1", values[0]);
                                insertCommand.Parameters.AddWithValue("@col2", values[1]);
                                insertCommand.Parameters.AddWithValue("@col3", values[2]);

                                insertCommand.ExecuteNonQuery();
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR, $"ImportCSV() in SQLContext class", ex);
                return false;
            }
        }

        public async Task<bool> AddTemplate(string TemplateName)
        {

            try
            {
                bool IsCompleted = false;

                using (SqliteConnection connection = new SqliteConnection(_vars.SQLConnection.ConnectionString))
                {
                    await connection.OpenAsync();

                    using (SqliteCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "INSERT INTO Templates (Name, Created, IsDeleted, DateDeleted) VALUES (@name, @created, @isDeleted, @dateDeleted)";

                        command.Parameters.AddWithValue("@name", TemplateName);
                        command.Parameters.AddWithValue("@created", DateTime.Now);
                        command.Parameters.AddWithValue("@isDeleted", 0);
                        command.Parameters.AddWithValue("@dateDeleted", DateTime.Now);

                        await command.ExecuteNonQueryAsync();
                    }

                    await connection.CloseAsync();
                    IsCompleted = true;
                }
                return IsCompleted;
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR, $"AddTemplate() in SQLContext class", ex);
                return false;
            }
        }

        public async Task RenameTemplate(string OldName, string NewName)
        {
            try
            {
                using (SqliteConnection connection = new SqliteConnection(_vars.SQLConnection.ConnectionString))
                {
                    await connection.OpenAsync();

                    using (SqliteCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "UPDATE Templates SET Name=@NewName WHERE Name = @OldName";

                        command.Parameters.AddWithValue("@NewName", NewName);
                        command.Parameters.AddWithValue("@OldName", OldName);

                        await command.ExecuteNonQueryAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"UpdateTemplate() in SQLContext class", ex);
            }
        }

        public async Task ClearTable(string TableName)
        {
            try
            {
                using (SqliteConnection connection = new SqliteConnection(_vars.SQLConnection.ConnectionString))
                {
                    await connection.OpenAsync();

                    using (SqliteCommand command = connection.CreateCommand())
                    {
                        command.CommandText = $"DELETE FROM {TableName}";
                        await command.ExecuteNonQueryAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"ClearTable() in SQLContext class", ex);
            }
        }

        public async Task DeleteRow(string TableName, string Column, string Value)
        {
            try
            {
                using (SqliteConnection connection = new SqliteConnection(_vars.SQLConnection.ConnectionString))
                {
                    await connection.OpenAsync();

                    using (SqliteCommand command = connection.CreateCommand())
                    {
                        command.CommandText = $"DELETE FROM {TableName} WHERE {Column}='{Value}'";
                        await command.ExecuteNonQueryAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"DeleteRow() in SQLContext class", ex);
            }
        }
    }
}
