﻿using LFSCompanion.Application.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.SQL.Seeders
{
    /// <summary>
    /// Data seeder for ButtonPositions table
    /// </summary>
    public class ButtonPositionsSeeder
    {
        private Variables _variables;
        private ExceptionProcessor _exceptionProcessor;
        private SeederMethods _seederMethods;

        /// <summary>
        /// ButtonPositionsSeeder class constructor
        /// </summary>
        public ButtonPositionsSeeder(Variables variables, ExceptionProcessor exceptionProcessor, SeederMethods seederMethods)
        {
            _variables = variables;
            _exceptionProcessor = exceptionProcessor;
            _seederMethods = seederMethods;
        }

        /// <summary>
        /// ButtonPositions Seeder
        /// </summary>
        /// <returns></returns>
        public async Task Seeder()
        {
            try
            {
                var buttonPositionsSeed = new List<SeederModels.ButtonPositions>
                {
                    #region Fuel
                    new SeederModels.ButtonPositions {
                        Property = "Fuel_H",
                        Value = "7"
                    },
                    new SeederModels.ButtonPositions {
                        Property = "Fuel_W",
                        Value = "17"
                    },
                    new SeederModels.ButtonPositions {
                        Property = "Fuel_L",
                        Value = "155"
                    },
                    new SeederModels.ButtonPositions {
                        Property = "Fuel_T",
                        Value = "87"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Fuel_IsVisible",
                        Value = "1"
                    },
                    #endregion
                    #region Rpm
                    new SeederModels.ButtonPositions
                    {
                        Property = "Rpm_H",
                        Value = "12"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Rpm_W",
                        Value = "44"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Rpm_L",
                        Value = "78"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Rpm_T",
                        Value = "137"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Rpm_IsVisible",
                        Value = "1"
                    },
                    #endregion
                    #region TC
                    new SeederModels.ButtonPositions{
                        Property = "Tc_H",
                        Value = "6"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Tc_W",
                        Value = "12"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Tc_L",
                        Value = "67"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Tc_T",
                        Value = "27"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Tc_IsVisible",
                        Value = "0"
                    },
	                #endregion
                    #region Lights
                    new SeederModels.ButtonPositions
                    {
                        Property = "Lights_H",
                        Value = "5"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Lights_W",
                        Value = "17"
                    },
                    new SeederModels.ButtonPositions
                        {
                        Property = "Lights_L",
                        Value = "155"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Lights_T",
                        Value = "81"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Lights_IsVisible",
                        Value = "1"
                    },
	                #endregion
                    #region Menu
                    new SeederModels.ButtonPositions
                    {
                        Property = "Menu_L",
                        Value = "179"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Menu_T",
                        Value = "105"
                    },
	                #endregion
                    #region ABS
                    new SeederModels.ButtonPositions
                    {
                        Property = "Abs_H",
                        Value = "6"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Abs_W",
                        Value = "12"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Abs_L",
                        Value = "80"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Abs_T",
                        Value = "27"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Abs_IsVisible",
                        Value = "0"
                    },
	                #endregion
                    #region Gear
                    new SeederModels.ButtonPositions
                    {
                        Property = "Gear_H",
                        Value = "14"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Gear_W",
                        Value = "19"
                    },
                    new SeederModels.ButtonPositions
                        {
                        Property = "Gear_L",
                        Value = "56"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Gear_T",
                        Value = "136"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Gear_IsVisible",
                        Value = "1"
                    },
	                #endregion
                    #region Speed
                    new SeederModels.ButtonPositions
                    {
                        Property = "Speed_H",
                        Value = "14"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Speed_W",
                        Value = "15"
                    },
                    new SeederModels.ButtonPositions
                        {
                        Property = "Speed_L",
                        Value = "125"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Speed_T",
                        Value = "136"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Speed_IsVisible",
                        Value = "1"
                    },
	                #endregion
                    #region Handbrake
                    new SeederModels.ButtonPositions
                    {
                        Property = "Handbrake_H",
                        Value = "5"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Handbrake_W",
                        Value = "17"
                    },
                    new SeederModels.ButtonPositions
                        {
                        Property = "Handbrake_L",
                        Value = "155"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Handbrake_T",
                        Value = "109"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Handbrake_IsVisible",
                        Value = "1"
                    },
	                #endregion
                    #region Blinker Left
                    new SeederModels.ButtonPositions
                    {
                        Property = "BlinkerL_H",
                        Value = "20"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "BlinkerL_W",
                        Value = "20"
                    },
                    new SeederModels.ButtonPositions
                        {
                        Property = "BlinkerL_L",
                        Value = "49"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "BlinkerL_T",
                        Value = "67"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "BlinkerL_IsVisible",
                        Value = "1"
                    },
	                #endregion
                    #region Blinker Right
                    new SeederModels.ButtonPositions
                    {
                        Property = "BlinkerR_H",
                        Value = "20"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "BlinkerR_W",
                        Value = "20"
                    },
                    new SeederModels.ButtonPositions
                        {
                        Property = "BlinkerR_L",
                        Value = "132"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "BlinkerR_T",
                        Value = "67"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "BlinkerR_IsVisible",
                        Value = "1"
                    },
	                #endregion
                    #region Turbo 
                    new SeederModels.ButtonPositions
                    {
                        Property = "Turbo_H",
                        Value = "5"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Turbo_W",
                        Value = "25"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Turbo_L",
                        Value = "100"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Turbo_T",
                        Value = "100"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Turbo_IsVisible",
                        Value = "0"
                    },
                    #endregion
                    #region DateTime 
                    new SeederModels.ButtonPositions
                    {
                        Property = "DateTime_H",
                        Value = "6"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "DateTime_W",
                        Value = "28"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "DateTime_L",
                        Value = "135"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "DateTime_T",
                        Value = "7"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "DateTime_IsVisible",
                        Value = "1"
                    },
                    #endregion
                    #region Customize UI 
                    new SeederModels.ButtonPositions
                    {
                        Property = "CustomizeUI_T",
                        Value = "47"
                    },
                    #endregion
                    #region PitSpeed 
                    new SeederModels.ButtonPositions
                    {
                        Property = "PitSpeed_H",
                        Value = "5"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "PitSpeed_W",
                        Value = "25"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "PitSpeed_L",
                        Value = "100"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "PitSpeed_T",
                        Value = "109"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "PitSpeed_IsVisible",
                        Value = "1"
                    },
                    #endregion
                    #region AvgFuel 
                    new SeederModels.ButtonPositions
                    {
                        Property = "AvgFuel_H",
                        Value = "5"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "AvgFuel_W",
                        Value = "25"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "AvgFuel_L",
                        Value = "75"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "AvgFuel_T",
                        Value = "100"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "AvgFuel_IsVisible",
                        Value = "0"
                    },
                    #endregion
                    #region Laptime 
                    new SeederModels.ButtonPositions
                    {
                        Property = "Laptime_H",
                        Value = "11"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Laptime_W",
                        Value = "18"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Laptime_L",
                        Value = "96"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Laptime_T",
                        Value = "27"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Laptime_IsVisible",
                        Value = "0"
                    },
                    #endregion
                    #region SplitTime
                    new SeederModels.ButtonPositions
                    {
                        Property = "SplitTime_H",
                        Value = "11"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "SplitTime_W",
                        Value = "18"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "SplitTime_L",
                        Value = "77"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "SplitTime_T",
                        Value = "27"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "SplitTime_IsVisible",
                        Value = "0"
                    },
                    #endregion
                    #region Track
                    new SeederModels.ButtonPositions
                    {
                        Property = "Track_H",
                        Value = "7"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Track_W",
                        Value = "13"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Track_L",
                        Value = "135"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Track_T",
                        Value = "18"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "Track_IsVisible",
                        Value = "0"
                    },
                    #endregion
                    #region Best PB
                    new SeederModels.ButtonPositions
                    {
                        Property = "BestPB_H",
                        Value = "11"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "BestPB_W",
                        Value = "18"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "BestPB_L",
                        Value = "115"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "BestPB_T",
                        Value = "27"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "BestPB_IsVisible",
                        Value = "0"
                    },
                    #endregion
                    #region Theoretical PB
                    new SeederModels.ButtonPositions
                    {
                        Property = "TheoreticalPB_H",
                        Value = "8"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "TheoreticalPB_W",
                        Value = "20"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "TheoreticalPB_L",
                        Value = "135"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "TheoreticalPB_T",
                        Value = "30"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "TheoreticalPB_IsVisible",
                        Value = "0"
                    },
                    #endregion
                    #region Race Positions
                    new SeederModels.ButtonPositions
                    {
                        Property = "RacePos_H",
                        Value = "6"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "RacePos_W",
                        Value = "35"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "RacePos_L",
                        Value = "24"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "RacePos_T",
                        Value = "100"
                    },
                    new SeederModels.ButtonPositions
                    {
                        Property = "RacePos_IsVisible",
                        Value = "0"
                    },
                    #endregion
                };

                foreach (var buttonPosition in buttonPositionsSeed)
                {
                    if (!await _seederMethods.EntryExistsAsync(buttonPosition))
                    {
                        await _seederMethods.InsertEntryAsync(buttonPosition);
                    }
                }
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"Seeder() method in ButtonPositionsSeeder class.", ex);
            }
        }
    }
}
