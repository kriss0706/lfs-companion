﻿using LFSCompanion.Application.Classes;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.SQL.Seeders
{
    /// <summary>
    /// Methods related to data seeders
    /// </summary>
    public class SeederMethods
    {
        private Variables _variables;
        private ExceptionProcessor _exceptionProcessor;

        /// <summary>
        /// SeederMethods class constructor
        /// </summary>
        /// <param name="variables"></param>
        /// <param name="exceptionProcessor"></param>
        public SeederMethods(Variables variables, ExceptionProcessor exceptionProcessor)
        {
            _variables = variables;
            _exceptionProcessor = exceptionProcessor;
        }

        /// <summary>
        /// Checks if a given entry already exists in the specified table
        /// It takes a generic parameter T representing the seeder model and verifies if an entry with a matching Property value exists in the table
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entry"></param>
        /// <returns></returns>
        public async Task<bool> EntryExistsAsync<T>(T entry) where T : class
        {
            using (var connection = new SqliteConnection(_variables.SQLConnection.ConnectionString))
            {
                await connection.OpenAsync();

                var tableName = typeof(T).Name;
                var propertyInfo = typeof(T).GetProperties().FirstOrDefault(); // Get the first property of T
                var columnName = propertyInfo?.Name;
                var commandText = $"SELECT COUNT(*) FROM {tableName} WHERE {columnName} = @value";

                using (var command = new SqliteCommand(commandText, connection))
                {
                    command.Parameters.AddWithValue("@value", propertyInfo?.GetValue(entry));

                    var count = (long)await command.ExecuteScalarAsync();

                    return count > 0;
                }
            }
        }

        /// <summary>
        /// This method inserts a new entry into the specified table
        /// It takes a generic parameter T representing the seeder model and inserts the provided entry into the database table
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entry"></param>
        /// <returns></returns>
        public async Task InsertEntryAsync<T>(T entry) where T : class
        {
            using (var connection = new SqliteConnection(_variables.SQLConnection.ConnectionString))
            {
                await connection.OpenAsync();

                var tableName = typeof(T).Name;
                var columnNames = string.Join(",",typeof(T).GetProperties().Select(p => p.Name));
                var parameterNames = string.Join(",",typeof(T).GetProperties().Select(p => "@" + p.Name));

                var commandText = $"INSERT INTO {tableName} ({columnNames}, Modified) VALUES ({parameterNames}, @modified)";

                using (var command = new SqliteCommand(commandText, connection))
                {
                    foreach (var property in typeof(T).GetProperties())
                    {
                        command.Parameters.AddWithValue("@" + property.Name, property.GetValue(entry));
                    }

                    command.Parameters.AddWithValue("@modified", DateTime.Now);

                    await command.ExecuteNonQueryAsync();
                }
            }
        }
    }
}
