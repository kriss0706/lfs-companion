﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.SQL.Seeders
{
    public class SeederModels
    {
        /// <summary>
        /// Class model for PlayerMessages table
        /// </summary>
        public class Messages
        {
            public string? Nickname { get; set; }
            public string? Username { get; set; }
            public string? Message { get; set; }
            public DateTime Date { get; set; } = DateTime.Now;
        }

        /// <summary>
        /// Data seeder model for ButtonPositions table
        /// </summary>
        public class ButtonPositions
        {
            public string? Property { get; set; }
            public string? Value { get; set; }
            public DateTime Modified { get; set; } = DateTime.Now;
        }

        /// <summary>
        /// Data seeder model for Settings table
        /// </summary>
        public class Settings
        {
            public string Setting { get; set; }
            public string? Value { get; set; }
            public DateTime Modified { get; set; } = DateTime.Now;
        }
    }
}
