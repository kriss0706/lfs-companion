﻿using LFSCompanion.Application.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.SQL.Seeders
{
    /// <summary>
    /// Data seeder for Settings table
    /// </summary>
    public class SettingsSeeder
    {
        private Variables _variables;
        private ExceptionProcessor _exceptionProcessor;
        private SeederMethods _seederMethods;

        /// <summary>
        /// SettingsSeeder class constructor
        /// </summary>
        public SettingsSeeder(Variables variables, ExceptionProcessor exceptionProcessor, SeederMethods seederMethods)
        {
            _variables = variables;
            _exceptionProcessor = exceptionProcessor;
            _seederMethods = seederMethods;
        }

        /// <summary>
        /// Settings Seeder
        /// </summary>
        /// <returns></returns>
        public async Task Seeder()
        {
            try
            {
                var SettingsSeed = new List<SeederModels.Settings>
                {
                    new SeederModels.Settings {
                        Setting = "ConsoleChat",
                        Value = "1"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "AutoStart",
                        Value = "1"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "MenuDisplay",
                        Value = "parked"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "FuelWarningType",
                        Value = "percent"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "FuelWarningValue",
                        Value = "5"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "SpeedoUnit",
                        Value = "kmh"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "FuelUnit",
                        Value = "percent"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "GearFormat",
                        Value = "modern"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "RpmStyle",
                        Value = "classic"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "ActivityApiToken",
                        Value = ""
                    },
                    new SeederModels.Settings
                    {
                        Setting = "Greeting",
                        Value = "Hello, %playername%!"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "GreetingEnabled",
                        Value = "false"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "DateRelativeTime",
                        Value = "false"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "TotalFuel",
                        Value = "false"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "AutoLights",
                        Value = "false"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "DateTimeFormat",
                        Value = "TIME"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "AutomaticUpdates",
                        Value = "true"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "LaptimeMessages",
                        Value = "false"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "LastUsedTemplate",
                        Value = "None"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "CarContact",
                        Value = "true"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "lfswPubstat",
                        Value = "none"
                    },
                    new SeederModels.Settings
                    {
                        Setting = "UseSiren",
                        Value = "false"
                    }
                };

                foreach (var Setting in SettingsSeed)
                {
                    if (!await _seederMethods.EntryExistsAsync(Setting))
                    {
                        await _seederMethods.InsertEntryAsync(Setting);
                    }
                }
            }
            catch (Exception ex)
            {
                await _exceptionProcessor.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    $"Seeder() method in SettingsSeeder class.", ex);
            }
        }
    }
}
