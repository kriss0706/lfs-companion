﻿using LFSCompanion.Application.Classes;
using LFSCompanion.Application.SQL.Models.Enum;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFSCompanion.Application.SQL
{
    public class Migration
    {
        private Variables _var;
        private ExceptionProcessor _exc;

        public Migration(Variables variables, ExceptionProcessor exc)
        {
            _var = variables;
            _exc = exc;
        }

        public async Task Migrate()
        {
            try
            {
                string[] Columns_PlayerMessages = new string[]
                {
                    "Nickname",
                    "Username",
                    "Message",
                    "Date"
                };

                string[] Columns_Settings = new string[]
                {
                    "Setting",
                    "Value",
                    "Modified"
                };

                string[] Columns_ButtonPositions = new string[]
                {
                    "Property",
                    "Value",
                    "Modified"
                };

                string[] Columns_Templates = new string[]
                {
                    "Name",
                    "Created",
                    "IsDeleted",
                    "DateDeleted"
                };

                string[] Columns_CarContact = new string[]
                {
                    "SourceUName",
                    "SourcePName",
                    "TargetUName",
                    "TargetPName",
                    "Speed",
                    "Date"
                };

                string[] Columns_Laptime = new string[]
                {
                    "Username",
                    "Playername",
                    "Split1",
                    "Split2",
                    "Split3",
                    "Laptime",
                    "Vehicle",
                    "Track",
                    "Date"
                };

                #region Templates
                bool TemplatesExist = await ColumnsExist("Templates",Columns_Templates);

                if (TemplatesExist == false)
                {
                    string TableName = "Templates";
                    string TableName_New = "Templates_New";

                    await NotifyMigration(NotifyMigrationEnum.START, TableName);

                    // Create temporary table
                    await CreateTable(TableName_New, _var.SQLColumns_Templates);

                    // Merge Tables
                    await MergeTables(TableName, TableName_New);

                    // Drop old tablea
                    await DeleteTable(TableName);

                    // Rename temporary table
                    await RenameTable(TableName_New, TableName);

                    await NotifyMigration(NotifyMigrationEnum.END, TableName);
                }

                #endregion

                #region PlayerMessages
                bool PlayerMessagesExist = await ColumnsExist("PlayerMessages",Columns_PlayerMessages);

                // PlayerMessages table does not exist
                if (PlayerMessagesExist == false)
                {
                    string TableName = "PlayerMessages";
                    string TableName_New = "PlayerMessages_New";

                    await NotifyMigration(NotifyMigrationEnum.START, TableName);

                    // Create temporary table
                    await CreateTable(TableName_New, _var.SQLColumns_PlayerMessages);

                    // Merge Tables
                    await MergeTables(TableName, TableName_New);

                    // Drop old table
                    await DeleteTable(TableName);

                    // Rename temporary table
                    await RenameTable(TableName_New, TableName);

                    await NotifyMigration(NotifyMigrationEnum.END, TableName);
                }
                #endregion

                #region Settings
                bool SettingsExist = await ColumnsExist("Settings",Columns_Settings);

                // PlayerMessages table does not exist
                if (SettingsExist == false)
                {
                    string TableName = "Settings";
                    string TableName_New = "Settings_New";

                    await NotifyMigration(NotifyMigrationEnum.START, TableName);

                    // Create temporary table
                    await CreateTable(TableName_New, _var.SQLColumns_Settings);

                    // Merge Tables
                    await MergeTables(TableName, TableName_New);

                    // Drop old table
                    await DeleteTable(TableName);

                    // Rename temporary table
                    await RenameTable(TableName_New, TableName);

                    await NotifyMigration(NotifyMigrationEnum.END, TableName);
                }
                #endregion

                #region ButtonPositions
                bool ButtonPropertiesExist = await ColumnsExist("ButtonPositions",Columns_ButtonPositions);

                // PlayerMessages table does not exist
                if (ButtonPropertiesExist == false)
                {
                    string TableName = "ButtonPositions";
                    string TableName_New = "ButtonPositions_New";

                    await NotifyMigration(NotifyMigrationEnum.START, TableName);

                    // Create temporary table
                    await CreateTable(TableName_New, _var.SQLColumns_ButtonPositions);

                    // Merge Tables
                    await MergeTables(TableName, TableName_New);

                    // Drop old table
                    await DeleteTable(TableName);

                    // Rename temporary table
                    await RenameTable(TableName_New, TableName);

                    await NotifyMigration(NotifyMigrationEnum.END, TableName);
                }
                #endregion

                #region Car Contact
                bool CarContactExists = await ColumnsExist("CarContact",Columns_CarContact);

                if (CarContactExists == false)
                {
                    string TableName = "CarContact";
                    string TableName_New = "CarContact_New";

                    await NotifyMigration(NotifyMigrationEnum.START, TableName);

                    // Create temporary table
                    await CreateTable(TableName_New, _var.SQLColumns_CarContact);

                    // Merge Tables
                    await MergeTables(TableName, TableName_New);

                    // Drop old tablea
                    await DeleteTable(TableName);

                    // Rename temporary table
                    await RenameTable(TableName_New, TableName);

                    await NotifyMigration(NotifyMigrationEnum.END, TableName);
                }

                #endregion

                #region Laptime
                bool LaptimeExists = await ColumnsExist("Laptime",Columns_Laptime);

                if (LaptimeExists == false)
                {
                    string TableName = "Laptime";
                    string TableName_New = "Laptime_New";

                    await NotifyMigration(NotifyMigrationEnum.START, TableName);

                    // Create temporary table
                    await CreateTable(TableName_New, _var.SQLColumns_Laptime);

                    // Merge Tables
                    await MergeTables(TableName, TableName_New);

                    // Drop old table
                    await DeleteTable(TableName);

                    // Rename temporary table
                    await RenameTable(TableName_New, TableName);

                    await NotifyMigration(NotifyMigrationEnum.END, TableName);
                }

                #endregion
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR,
                    "Migrate() in Migration class", ex);
            }
        }

        public async Task CreateTable(string TableName, string[] ColumnsArray)
        {
            try
            {
                // Create temporary table
                string createTempTableQuery = $"CREATE TABLE IF NOT EXISTS {TableName} ({string.Join(", ",ColumnsArray)})";

                await _var.SQLConnection.OpenAsync();

                using (SqliteCommand createTempTableCommand = new(createTempTableQuery, _var.SQLConnection))
                {
                    createTempTableCommand.ExecuteNonQuery();
                }

                await _var.SQLConnection.CloseAsync();
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "CreateTemporaryTable() in Migration class", ex);
            }
        }

        public async Task MergeTables(string sourceTableName, string targetTableName)
        {
            try
            {
                using (SqliteConnection connection = new SqliteConnection(_var.SQLConnection.ConnectionString))
                {
                    await connection.OpenAsync(); // Use asynchronous open

                    using (SqliteCommand mergeCommand = connection.CreateCommand())
                    {
                        mergeCommand.CommandText = $"INSERT INTO {targetTableName} SELECT * FROM {sourceTableName}";
                        await mergeCommand.ExecuteNonQueryAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "MergeTables() in Migration class", ex);
                throw;
            }
        }

        private async Task<bool> ColumnsExist(string tableName, string[] columnNames)
        {
            try
            {
                string query = $"PRAGMA table_info({tableName})";

                using (var connection = new SqliteConnection(_var.SQLConnection.ConnectionString))
                {
                    await connection.OpenAsync();

                    using (var command = new SqliteCommand(query, connection))
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        int ColumnCount = reader.FieldCount;
                        var existingTableCols = new List<string>();

                        while (await reader.ReadAsync())
                        {
                            string columnName = reader.GetString(reader.GetOrdinal("name"));
                            existingTableCols.Add(columnName);
                            // Console.WriteLine($"");
                        }

                        // Match column count
                        if (existingTableCols.Count != columnNames.Count())
                            return false;

                        // Match column names
                        foreach (var providedColumns in columnNames)
                        {
                            // Column names in table does not match the provided column names
                            if (!existingTableCols.Contains(providedColumns))
                            {
                                return false;
                            }
                        }

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, "Migrate() in Migration class", ex);
                throw; // Rethrow the exception to be handled by the caller
            }
        }

        public async Task<int> CountRecords(string tableName)
        {
            if (string.IsNullOrWhiteSpace(tableName))
                throw new ArgumentException("Table name cannot be null or empty.");

            try
            {
                using (var connection = new SqliteConnection(_var.SQLConnection.ConnectionString))
                {
                    await connection.OpenAsync();

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = $"SELECT COUNT(*) FROM {tableName}";
                        var result = await command.ExecuteScalarAsync();
                        return Convert.ToInt32(result);
                    }
                }
            }
            catch (Exception ex)
            {
                await _exc.Log(ExceptionProcessor.LogTypeEnum.ERROR, nameof(CountRecords), ex);
                throw new ApplicationException("Error occurred while counting records.", ex);
            }
        }

        
        public async Task RenameTable(string OldTableName, string NewTableName)
        {
            try
            {
                // Rename table
                string createTempTableQuery = $"ALTER TABLE {OldTableName} RENAME TO {NewTableName}";

                await _var.SQLConnection.OpenAsync();

                using (SqliteCommand createTempTableCommand = new(createTempTableQuery, _var.SQLConnection))
                {
                    createTempTableCommand.ExecuteNonQuery();
                }

                await _var.SQLConnection.CloseAsync();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task DeleteTable(string TableName)
        {
            try
            {
                // Rename table
                string createTempTableQuery = $"DROP TABLE {TableName}";

                await _var.SQLConnection.OpenAsync();

                using (SqliteCommand createTempTableCommand = new(createTempTableQuery, _var.SQLConnection))
                {
                    createTempTableCommand.ExecuteNonQuery();
                }

                await _var.SQLConnection.CloseAsync();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private async Task NotifyMigration(NotifyMigrationEnum Type, string TableName)
        {
            if (Type == NotifyMigrationEnum.START)
                Console.WriteLine($"The table '{TableName}' is not up-to-date, initiating migration process..");
            else if (Type == NotifyMigrationEnum.END)
                Console.WriteLine($"The table '{TableName}' has been migrated successfully.");
        }
    }
}
