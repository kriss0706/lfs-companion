LFSCompanion has been made open source under the [MIT License](LICENSE).
There have been prior discussions about the software on the [https://www.lfs.net/forum/thread/97129](live for speed forum).