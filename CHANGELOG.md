## 4.6.51 (02/03/2025)

### Features (2 changes)
- Vehicle strobe.
- Vehicle siren.

### Improvements (4 changes)
- Modified strobe pattern.
- Strobe and siren can be set with /o commands (see /o help).
- Added siren variation, the siren switches between SIREN_SLOW and SIREN_FAST every 30 seconds.
- Fixed outdated playername (i.e player renames their driver).

## 4.6.5 (07/02/2025)

### Features (8 changes)
- Messages Panel:
  - Added checkbox to only show local data.
  - Added clear button to remove the text from the search input.
  - Paginator now shows the total amount of records.
  - Records that contains local username are now highlighted.
- Settings Panel:
  - Greeting input now fills in the old value when clicked.
  - Fuel warning input now fills in the old value when clicked.
  - Pubstat input now fills in the old value when clicked.
- A friendly GUI shows how to get the api_client and api_id values.

### Improvements (3 changes)
- Messages Panel:
  - Optimized cached lists.
  - Optimized search, you can now search for users and words in messages.
  - Added IS_TOC packet: No longer out of date nicknames.
  - The LFS Mods API id and secret is now required. Can be set in the Connection.ini configuration file.
  - Cached messagelists in the application are now limited to 10,000 records.

## 4.6.41 (04/02/2025)

### Improvements (3 changes)
- Messages Panel:
  - Paginator in messages panel now has a limit.
  - CAR_CONTACT is now searchable with:
      - Source player name.
      - Source username.
      - Target player name.
      - Target username.
  - Filter for GLOBAL no longer returns empty.

## 4.6.4 (02/02/2025)

### Improvements (5 changes)
- UI Template names now allow for whitespaces.
- InSim crash when trying to parse very long lfsw distances.
- CarContact no longer sends messages about car contact with AI.
- AutoLights setting no longer affect players local user is spectating.
- Refactored autolights logic. Its now only active if the car has been parked for 60 secs or longer.

## 4.6.3 (31/02/2025)

### Features (1 change)
- Introducing a new race positions module. It can be toggled in the ui editor.

### Improvements (2 changes)
- Side menu not showing when AI is driving.
- Added IS_PLP packet to manage when players enter the garage.

## 4.6.0 (24/01/2025)

### Improvements (3 changes)
- Migrated project to .net 8.
- Split time comparison.
- Refactored widgets, they have all been moved to a Button Manager.
- Laptimes Panel now has inclusive filtering. Meaning you can filter each one individually.

### Features (2 additions)
- Theoretical PB widget.
- When a player joins the server, information like country, total_distance, laps and wins are displayed.

### Bug Fixes (3 changes)
- Race widgets resetting themself after a lap.
- Laptimes were saved for all players for some reason.
- Configuration files were not automatically generated when running InSim for the first time.

## 4.5.7 (23/01/2024)

### Improvements(2 changes)
- Fetching button size in the customize ui panel is now done automatically
- Laptime comparison format is now showing properly (^2-1:23.45)

### Features (1 addition)
- Button to view BEST_PB with car-track combination

## 4.5.6 (20/01/2024)

### Improvements(1 change)
- Optimized messages panel. It should be way less laggy now.

## 4.5.5 (19/01/2024)

### Features (6 additions)
- Button to view splits during a race
- Button to view laptime during a race
- Laptimes panel to browse through all racing data, accessible through the side panel.
- Sorting options to the laptimes panel
- Paginator to browse through all the race results
- Laptimes panel supports full mod names

### Improvements (3 changes)
- Optimized laptime fetching and comparison
- Setting named 'Laptime messages' has been renamed to 'Racing messages'
- Database migration. The laptime table should automatically generate.

## 4.4.5 (29/11/2023)

### Features (5 additions)
- Car Contact: Implementation.
- Car Contact: Browse data in the messages panel.
- Car Contact: Setting to toggle car contact messages. This setting will only toggle the message. All car contact incidents are still logged.
- Template: Import
- Template: Export

### Improvements (3 changes)
- Messages Panel: Data were inconsistent with page number.
- Re-worked the way PlayerMessages, GlobalMessages and CarContact data is fetched to insim lists.
- Template: Can now be renamed in: Customize UI > Templates > Manage Template > Click the template name

## 4.4.4 (28/11/2023)

### Improvements (2 changes)
- It is no longer possible to create a UI tempalte with an already existing name.
- Templates are now permanently deleted upon deletion due to interference with template names.

## 4.4.3 (28/11/2023)

### Improvements (2 changes)
- Renamed Avg. Fuel to Fuel Consumption.
- Fuel Consumption is updated every second (1000ms) instead of updating every 250 meters driven.
